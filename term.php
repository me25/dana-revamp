<p style="text-align: center;"><strong>SYARAT DAN KETENTUAN UNTUK PENGGUNA DANA</strong></p>
<p style="text-align: left;">Dalam menggunakan layanan uang elektronik DANA, Pengguna setuju dan mengikatkan diri untuk mematuhi &ldquo;<strong>Syarat dan Ketentuan Pengguna DANA</strong>&rdquo; sebagai berikut:</p>
<p><strong>I. DEFINISI</strong></p>
<ol>
    <li style="list-style-type: none;">
        <ol>
            <li style="text-align: left;">DANA adalah layanan uang elektronik berbasis server yang diselenggarakan oleh PT Espay Debit Indonesia Koe (EDIK) (<strong>www.dana.id</strong>) yang merupakan pemegang lisensi resmi dengan nama DANA yang telah memperoleh izin dari Bank Indonesia melalui surat dengan No. 18/262/DKSP/Srt/B tanggal 29 Februari 2016 untuk mengoperasikan layanan Uang Elektronik berbasis server, izin dari Bank Indonesia sebagai penyelenggara transfer dana berdasarkan tanda izin No. 18/199/Sb/3 tanggal 14 November 2016 serta izin dari Bank Indonesia sebagai penyelenggara dompet elektronik melalui surat dengan No. 20/1370/DSSK/Srt/B tanggal 28 Agustus 2018.</li>
            <li style="text-align: left;">Rekening DANA adalah rekening uang elektronik yang dibuka oleh Pengguna yang disimpan pada server elektronik DANA.</li>
            <li style="text-align: left;">Rekening <em>Escrow</em> adalah rekening penampungan dana pada Bank Rekanan yang sudah terdaftar di DANA.</li>
            <li style="text-align: left;">Uang Elektronik adalah nilai uang yang tercatat dan disimpan di <em>server</em>/ rekening DANA. Dana uang elektronik akan ditampung di Rekening <em>Escrow</em> dan dapat digunakan oleh Pengguna untuk melakukan transaksi apapun dengan menggunakan aplikasi perbankan sosial DANA.</li>
            <li style="text-align: left;"><em>Merchant </em>adalah pihak-pihak baik badan hukum maupun badan usaha ataupun perorangan yang melayani transaksi menggunakan rekening DANA pada aplikasi DANA.</li>
            <li style="text-align: left;">Nomor Ponsel <em>(Handphone)</em> adalah nomor telepon seluler atau ponsel pintar <em>(smartphone)</em> yang digunakan oleh Pengguna untuk mengakses layanan DANA di aplikasi perbankan sosial DANA.</li>
            <li style="text-align: left;">Pengguna adalah pengguna DANA dan pemilik rekening DANA</li>
            <li style="text-align: left;">Perangkat Telekomunikasi adalah ponsel pintar, perangkat genggam<em>,</em> komputer pribadi berbentuk tablet atau perangkat telekomunikasi lainnya yang teknologinya mendukung layanan DANA dan menggunakan Sistem Operasi (OS) Android dan IOS termasuk sistem operasi lain yang akan ada di masa mendatang yang memungkinkan Pengguna untuk menggunakan layanan DANA.</li>
            <li style="text-align: left;">PIN (<em>Personal Identification Number</em>) adalah nomor identifikasi pribadi bagi Pengguna untuk melakukan transaksi di DANA. PIN merupakan kumpulan dari karakter-karakter yang digunakan oleh Pengguna aplikasi DANA untuk mendukung verifikasi identitas mereka dalam sistem keamanan.</li>
            <li style="text-align: left;">Transaksi adalah transaksi yang dapat dilakukan oleh Pengguna melalui layanan DANA antara lain: transaksi pembayaran, transaksi pembelian pulsa, transaksi pembayaran tagihan, transaksi pengiriman uang dan setiap transaksi lainnya yang akan dikembangkan di masa yang akan datang. Khusus layanan Bayar Teman <em>(Pay Friends)</em> dan Minta Uang <em>(Ask Money)</em> adalah merupakan layanan yang akan dikembangkan dikemudian hari.</li>
            <li style="text-align: left;">Telepon Genggam adalah telepon seluler (ponsel) atau telepon genggam <em>(handphone) </em>yang merupakan perangkat telekomunikasi elektronik yang mempunyai kemampuan dasar seperti telepon konvensional namun dapat dibawa <em>(mobile/portable)</em> dan tidak memerlukan jaringan menggunakan kabel <em>(nirkabel wireless)</em></li>
            <li style="text-align: left;">Transaksi <em>Top-Up</em> adalah transaksi penambahan saldo <em>(top-up)</em> ke dalam rekening DANA</li>
            <li style="text-align: left;">Transaksi Pembayaran adalah transaksi pembayaran atas pembelian barang dan/atau jasa di <em>Merchant</em> yang menggunakan aplikasi DANA.</li>
            <li style="text-align: left;">Penonaktifan adalah proses penutupan sementara rekening DANA dimana layanan ini masih dalam proses pengembangan.</li>
            <li style="text-align: left;">Agen adalah pihak ketiga yang bekerjasama dengan DANA dan bertindak untuk dan atas nama DANA dalam memberikan layanan DANA.</li>
        </ol>
    </li>
</ol>
<p><strong>II. KETENTUAN UMUM</strong></p>
<ol>
    <li style="list-style-type: none;">
        <ol>
            <li>DANA merupakan layanan uang elektronik berbasis <em>mobile</em> yang dapat digunakan oleh Pengguna melalui perangkat telekomunikasi.</li>
            <li>Seluruh Pengguna dengan Nomor Ponsel (<em>Handphone</em>) dari operator manapun baik pascabayar atau prabayar dapat menjadi Pengguna DANA.</li>
            <li>Seluruh Pengguna yang merupakan nasabah dari Bank Rekanan dapat menjadi Pengguna DANA.</li>
            <li>Pengguna yang belum menjadi nasabah Bank Rekanan atau disebut juga Pengguna non-Bank tetap dapat menjadi Pengguna DANA. Proses <em>Top-up</em> untuk Pengguna non-Bank dapat dilakukan melalui pembayaran secara tunai di tempat agen pembayaran DANA atau layanan pembayaran rekening virtual melalui Bank.</li>
            <li>Untuk mengakses layanan DANA, Pengguna harus mengunduh dan melakukan instalasi aplikasi DANA di Telepon Genggam Pengguna dan kemudian melakukan aktivasi melalui aplikasi DANA yang terdapat didalam Aplikasi DANA.</li>
            <li>Setiap Nomor Ponsel (<em>Handphone</em>) hanya dapat digunakan untuk 1 (satu) rekening DANA.</li>
            <li>Layanan DANA terdiri dari 2 (dua) jenis yaitu jenis terdaftar yaitu dengan batasan nilai nominal saldo sampai dengan Rp2.000.000 dan jenis terverifikasi yaitu dengan batasan nilai nominal saldo sampai dengan Rp10.000.000 sesuai dengan peraturan Bank Indonesia.</li>
            <li>Pengguna dapat melakukan Transaksi apapun apabila saldo uang elektronik yang terdapat dalam rekening DANA tersebut mencukupi.</li>
            <li>Batas maksimum Transaksi dalam 1 (satu) bulan kalender adalah Rp20.000.000 (dua puluh juta rupiah).</li>
            <li>Hal-hal berikut harus dipenuhi untuk dapat menggunakan Layanan DANA:
                <ol style="list-style-type: lower-roman;">
                    <li>Nomor Ponsel (<em>Handphone</em>) harus dalam keadaan aktif (tidak terblokir)
                    </li>
                    <li>Jaringan data dan pulsa yang mencukupi (bagi pelanggan prabayar) atau batas kredit yang mencukupi (bagi pelanggan pascabayar).
                    </li>
                </ol>
            </li>
            <li>Pengguna dan PIN:
                <ol style="list-style-type: lower-roman;">
                    <li>PIN akan diperlukan sebelum melakukan suatu Transaksi untuk memastikan keabsahan atau melakukan verifikasi bahwa Transaksi benar-benar dilakukan oleh Pengguna.</li>
                    <li>PIN dapat diganti oleh Pengguna melalui aplikasi DANA.</li>
                    <li>Untuk mengatur ulang PIN maka Pengguna akan dipandu oleh Layanan Pelanggan (Customer Service) DANA.</li>
                    <li>Pengguna wajib menjaga kerahasiaan PIN dan bertanggung jawab sepenuhnya atas kerahasiaan PIN.</li>
                    <li>Kecuali ditentukan lain, PIN dapat diubah dengan metode atau sarana otentikasi lainnya yang disediakan oleh DANA atau oleh pihak lain yang bekerjasama dengan DANA dimana metode atau sarana otentikasi tersebut telah mendapat persetujuan dari DANA.</li>
                </ol>
            </li>
        </ol>
    </li>
</ol>
<p><strong>III. AKTIVASI DANA</strong></p>
<p style="padding-left: 40px;">Pengguna DANA dapat membuka rekening DANA pertama kali dengan cara:</p>
<ol>
    <li style="list-style-type: none;">
        <ol>
            <li>
                Mengunduh aplikasi DANA secara langsung melalui Telepon Genggam yang dimiliki sebagaimana yang tersedia di Google Playstore (untuk Android) dan Apple-App Store (untuk IOS), dan kemudian;
            </li>
            <li>
                Setelah melakukan instalasi aplikasi DANA, Pengguna dapat melakukan proses aktivasi melalui aplikasi DANA dengan mengikuti petunjuk dan menyediakan informasi, antara lain:
                <ol style="list-style-type: lower-roman;">
                    <li>
                        Nama akun dari Pengguna
                    </li>
                    <li>
                        PIN (6 angka) yang dipilih sendiri oleh Pengguna
                    </li>
                    <li>
                        Setelah proses aktivasi sukses, Pengguna dapat mengakses layanan DANA.
                    </li>
                </ol>
            </li>
        </ol>
    </li>
</ol>
<p><strong>IV. TRANSAKSI DENGAN DANA .&nbsp;</strong></p>
<p style="padding-left: 40px;">Pengguna DANA dapat melakukan Transaksi sebagai berikut:</p>
<ol>
    <li style="list-style-type: none;">
        <ol>
            <li style="list-style-type: none;">
                <ol>
                    <li>Top up yaitu melakukan penambahan saldo di rekening DANA</li>
                    <li>Pembelian atau isi ulang pulsa</li>
                    <li>Pembayaran tagihan-tagihan</li>
                    <li>Berbelanja di Merchant atau pembayaran atas pembelian barang/ jasa<span style="font-weight: 400;"><br /></span></li>
                    <li>Cek saldo dan melihat riwayat transaksi keuangan sampai dengan maksimum 180 hari terakhir.</li>
                </ol>
            </li>
        </ol>
    </li>
</ol>
<p><strong>V. TRANSAKSI <em>TOP-UP</em></strong></p>
<p><em>Top-up</em>/pembayaran uang elektronik adalah proses penambahan saldo ke dalam uang elektronik DANA.</p>
<p>Terdapat beberapa metode untuk&nbsp; &nbsp;sebagai berikut:</p>
<p><span style="font-weight: 400;">1) Melalui ATM</span></p>
<p>Top-up melalui ATM adalah proses penambahan saldo ke dalam uang elekronik DANA melalui mesin ATM.</p>
<p>Top-up melalui ATM dapat dilakukan dengan metode yang sama seperti ketika Pengguna melakukan transfer antar bank. Perbedaannya adalah nomor rekening tujuan adalah Nomor Ponsel (Handphone) Pengguna sendiri yang sudah didaftarkan sebagai uang elektronik DANA di aplikasi DANA. Top-up melalui ATM dapat dilakukan dengan metode yang sama seperti melakukan transfer antar bank melalui ATM sebagai contoh:</p>
<ol style="list-style-type: lower-roman;">
    <li>Masukkan kartu ATM ke dalam mesin ATM</li>
    <li>Pilih menu &ldquo;Transfer&rdquo;</li>
    <li>Pilih menu &ldquo;Transfer ke bank lain&rdquo;</li>
    <li>Masukkan kode DANA misalnya pada bank BNI maka masukkan kode DANA pada Bank BNI yaitu 8206 dan kemudian Nomor Ponsel (Handphone) Pengguna (10-12 angka) misalnya 081212345678 maka Pengguna memasukkan nomor 8206081212345678</li>
    <li>Kemudian Pengguna memilih jumlah nilai rupiah yang hendak ditransfer misalnya 50.000.</li>
    <li>Klik &ldquo;Ya/Ok&rdquo;</li>
</ol>
<p>Biaya</p>
<p>Biaya yang dibebankan kepada Pengguna DANA adalah biaya switching yaitu sebesar Rp6.500 ditambah biaya administrasi sebesar Rp0,- dimana saat ini atau selama berlangsungnya masa promosi DANA tidak membebankan biaya administrasi kepada Pengguna namun hal ini dapat berubah sewaktu-waktu dengan memberikan pemberitahuan melalui media yang ditentukan oleh DANA. Contoh: Pengguna A hendak melakukan Top-up sebesar Rp50.000 pada bank Mandiri maka saldo DANA Pengguna A akan bertambah sebesar Rp50.000 dan saldo milik Pengguna A pada bank Mandiri akan berkurang sebesar Rp56.500 dengan rincian sebagai berikut: Top-up Rp50.000 ditambah biaya switching ATM Rp6.500 ditambah biaya administrasi Rp0,-</p>
<p>Catatan: biaya switching gratis apabila Pengguna melakukan Top-up menggunakan kartu ATM dari bank Permata pada mesin ATM milik bank Permata</p>
<p><span style="font-weight: 400;">2) Melalui </span><em><span style="font-weight: 400;">Internet Banking</span></em></p>
<p>Top-up melalui Internet Banking adalah proses penambahan saldo ke dalam uang elektronik Pengguna dengan menggunakan ID pengguna/ kata sandi (password) atau token Internet Banking Pengguna milik Bank Rekanan.</p>
<p>Persyaratan untuk top-up melalui Internet Banking adalah rekening Pengguna pada Bank Rekanan harus terdaftar sebagai Internet Banking ID Pengguna. Pada beberapa bank seperti misalnya pada bank BCA dibutuhkan pendaftaran khusus untuk dapat melakukan proses Top-up melalui klikpay BCA. Pengguna dapat menghubungi Layanan Pelanggan BCA terdekat atau melakukan pendaftaran secara online di www.klikbca.com.</p>
<p>Catatan: Sesuai dengan peraturan yang berlaku DANA tidak menyimpan data ID Pengguna dan kata sandi (password) atau token baik sementara atau permanen ke dalam sistem yang dimiliki oleh PT EDIK. Proses validasi harus dilakukan langsung oleh pihak bank dengan cara mengarahkan kembali layar Internet Banking yang berasal dari Bank Rekanan ke layar aplikasi perbankan sosial DANA.</p>
<p>Top-up melalui Internet Banking dapat dilakukan dengan mengikuti langkah-langkah sebagai berikut:</p>
<ol style="list-style-type: lower-roman;">
    <li>Masukkan &ldquo;Bank Rekanan&rdquo; dimana nomor rekening Pengguna terdaftar sebagai pengguna internet banking</li>
    <li>Pilih jalur (channel) bank (umumnya Internet Banking)</li>
    <li>Masukkan jumlah dalam Rupiah yang ingin Pengguna top-up ke uang elektronik DANA Pengguna</li>
    <li>Klik &ldquo;Proses&rdquo;</li>
    <li>Masukkan ID pengguna/kata sandi (password) atau token yang diminta oleh Bank Rekanan.</li>
</ol>
<p>Catatan:</p>
<ol style="list-style-type: lower-roman;">
    <li>Dalam tahap ini proses Transaksi sudah diambil alih oleh sistem perbankan. Aplikasi DANA akan menghasilkan status Transaksi &ldquo;Berhasil&rdquo; atau &ldquo;Gagal&rdquo; dari Bank Rekanan. Apabila transfer dari rekening Pengguna di Bank Rekanan ke Rekening Escrow milik DANA berhasil maka Bank Rekanan akan mengirimkan status &ldquo;Berhasil&rdquo; secara online dan real-time ke aplikasi DANA. Apabila proses transfer gagal dikarenakan alasan teknis atau saldo Pengguna tidak mencukupi maka Bank Rekanan akan mengirimkan status &ldquo;Gagal&rdquo;</li>
    <li>Dalam hal transfer dinyatakan &ldquo;Berhasil&rdquo; maka saldo Pengguna pada Bank Rekanan otomatis akan berkurang namun apabila saldo dalam uang elektonik DANA Pengguna belum bertambah maka Pengguna dapat menghubungi DANA di nomor Layanan Pelanggan (Customer Service) yang dapat dihubungi yang tercantum pada bagian &ldquo;Hubungi Kami&rdquo; di website <a href="http://www.dana.id">www.dana.id</a></li>
</ol>
<p><span style="font-weight: 400;">Biaya</span></p>
<p><span style="font-weight: 400;">Pengaturan khusus mengenai biaya administrasi dapat dilihat pada tabel biaya administrasi.</span></p>
<p><span style="font-weight: 400;">3)&nbsp; </span><span style="font-weight: 400;">Melalui SMS </span><em><span style="font-weight: 400;">Banking</span></em></p>
<p>Top-up melalui SMS Banking adalah proses penambahan saldo ke uang elektronik DANA oleh Pengguna dengan menggunakan fasilitas SMS Banking yang dimiliki Pengguna di Bank Rekanan. Sebagai persyaratan untuk Top-up melalui SMS Banking adalah rekening Pengguna di Bank Rekanan harus terdaftar sebagai:</p>
<ol style="list-style-type: lower-roman;">
    <li>Nasabah SMS Banking. Adapun syarat dan ketentuan atas fasilitas SMS Banking maka Pengguna dapat menghubungi Layanan Pelanggan/ Customer Service pada Bank Rekanan</li>
    <li>SMS Banking Pengguna sudah didaftarkan dengan status &ldquo;FINANCIAL&rdquo; dimana beberapa Bank Rekanan mengharuskan pendaftaran khusus pada SMS Banking agar dapat digunakan untuk bertransaksi atau cek saldo misalnya pada bank Mandiri namun Pengguna dapat menghubungi Layanan Pelanggan/ Customer Service dari tiap Bank Rekanan.</li>
    <li>Nomor SMS Banking Pengguna sudah didaftarkan di aplikasi perbankan <span style="font-weight: 400;">sosial DANA maka klik: Pengaturan &rarr; Pendaftaran SMS </span><em><span style="font-weight: 400;">Banking</span></em><span style="font-weight: 400;"> Langkah-langkah Top up melalui SMS </span><em><span style="font-weight: 400;">Banking</span></em></li>
    <li>
        Masukkan &ldquo;Bank Rekanan&rdquo;, di mana rekening Pengguna sudah didaftarkan sebagai SMS Banking (FINANCIAL)
            <br />Pilih Jalur (Channel) bank (biasanya melalui SMS Banking)
    </li>
    <li>
        Masukkan jumlah Rupiah yang ingin Pengguna top-up ke dalam uang elektronik DANA Pengguna
            <br />Bank Rekanan akan mengirimkan token melalui SMS
    </li>
    <li>
       Masukkan token yang Pengguna terima melalui SMS ke menu yang tersedia
    </li>
</ol>
<p><span style="font-weight: 400;">Biaya</span></p>
<p><span style="font-weight: 400;">Khusus mengenai biaya administrasi dapat dilihat pada tabel biaya administrasi</span></p>
<p><span style="font-weight: 400;">Catatan</span><span style="font-weight: 400;">:</span></p>
<p><span style="font-weight: 400;">Operator telekomunikasi akan mengenakan biaya pengiriman token melalui SMS dimana biaya tersebut ditentukan secara langsung oleh operator telekomunikasi.</span></p>
<p><span style="font-weight: 400;">Berikut adalah tabel biaya administrasi untuk </span><em><span style="font-weight: 400;">Top-up</span></em></p>
<table style="border-collapse: collapse; width: 100%; height: 164px;" border="1">
    <tbody>
        <tr style="height: 10px;">
            <td style="width: 5.72626%; height: 10px; text-align: center;">No</td>
            <td style="width: 27.5139%; height: 10px;">Nama Bank</td>
            <td style="width: 41.7598%; height: 10px;">Top-Up</td>
            <td style="width: 25%; height: 10px;">Biaya</td>
        </tr>
        <tr style="height: 48px;">
            <td style="width: 5.72626%; height: 48px; text-align: center;">1</td>
            <td style="width: 27.5139%; height: 48px;">Semua Bank</td>
            <td style="width: 41.7598%; height: 48px;">ATM</td>
            <td style="width: 25%; height: 48px;">Rp0*</td>
        </tr>
        <tr style="height: 48px;">
            <td style="width: 5.72626%; height: 48px; text-align: center;">2</td>
            <td style="width: 27.5139%; height: 48px;">Bank BCA</td>
            <td style="width: 41.7598%; height: 48px;">Internet Banking, SMS Banking</td>
            <td style="width: 25%; height: 48px;">Rp0*</td>
        </tr>
        <tr style="height: 48px;">
            <td style="width: 5.72626%; height: 48px; text-align: center;">3</td>
            <td style="width: 27.5139%; height: 48px;">Bank BNI</td>
            <td style="width: 41.7598%; height: 48px;">Internet Banking, SMS Banking</td>
            <td style="width: 25%; height: 48px;">Rp0*</td>
        </tr>
        <tr style="height: 48px;">
            <td style="width: 5.72626%; height: 10px; text-align: center;">4</td>
            <td style="width: 27.5139%; height: 10px;">Bank Lainnya</td>
            <td style="width: 41.7598%; height: 10px;">Internet Banking</td>
            <td style="width: 25%; height: 10px;">Rp0*</td>
        </tr>
    </tbody>
</table>
<p><em><span style="font-weight: 400;">Catatan: Biaya administrasi di atas dapat berubah sewaktu-waktu dan atas setiap perubahan akan di informasikan melalui media yang ditentukan oleh DANA</span></em></p>
<p><em>VI. SALDO DALAM REKENING DANA</em></p>
<ol>
    <li>Saldo uang elektronik yang tersimpan dalam rekening DANA tidak akan memperoleh bunga dalam bentuk apapun karena bukan merupakan simpanan sebagaimana dimaksud dalam Undang-Undang tentang Perbankan dan Undang-Undang tentang Perbankan Syariah dan tidak akan dijamin oleh Lembaga Penjamin Simpanan sebagaimana diatur dalam Undang-Undang tentang Lembaga Penjamin Simpanan.</li>
    <li>Pengguna dapat memeriksa saldo uang elektronik DANA melalui aplikasi DANA atau sarana lainnya termasuk sarana yang disediakan oleh pihak ketiga yang terhubung dengan layanan DANA.</li>
</ol>
<p><em>VII. PENARIKAN DANA DARI SALDO DANA KE REKENING BANK</em></p>
<ol>
    <li>Penarikan adalah fitur pencairan saldo DANA kepada rekening bank yang didaftarkan oleh pengguna DANA dari bank yang terdaftar dalam aplikasi DANA.</li>
    <li>Jumlah saldo penarikan ditentukan oleh Pengguna DANA dengan ketentuan: jumlah minimum adalah Rp50.000 dan maksimum Rp1.000.000 per hari.</li>
    <li>DANA tidak bertanggung jawab atas kesalahan yang terjadi karena kesalahan dalam menginput informasi bank yang didaftarkan oleh Pengguna DANA agar DANA dapat mengidentifikasi bahwa nomor rekening dan nama bank benar.</li>
    <li>Biaya yang berlaku dalam penarikan ke rekening bank akan dibebankan ke Pengguna DANA dan akan dipotong dari DANA saldo saat penarikan sedang diproses.</li>
    <li>Pengguna DANA dapat memeriksa status transaksi penarikan dalam aplikasi DANA</li>
    <li>DANA akan mengembalikan uang jika penarikan gagal</li>
    <li>DANA tidak bertanggung jawab jika ada masalah pada pihak bank dan bank tidak dapat memproses penarikan saldo DANA sejauh DANA sudah berhasil mengirimkan instruksi transfer.</li>
    <li>Pengguna DANA dapat menghubungi Layanan Pelanggan jika ada masalah dengan penarikan ke transfer bank.</li>
</ol>
<p><em>VIII. PEMBELIAN ISI ULANG PULSA DAN PEMBAYARAN TAGIHAN</em></p>
<ol>
    <li>Pembelian isi ulang pulsa selaku operator telekomunikasi atau operator lainnya dapat dilakukan melalui aplikasi DANA dengan denominasi yang tersedia atau sesuai petunjuk yang ditampilkan dalam aplikasi DANA.</li>
    <li>Pembayaran tagihan pasca bayar atau tagihan rutin bulanan seperti tagihan operator selular, tagihan telkom, tagihan PSTN dan lainnya dapat dilakukan melalui aplikasi DANA.</li>
    <li>Transaksi pembelian dan pembayaran yang dinyatakan berhasil dapat dilihat pada riwayat transaksi keuangan yang juga tersedia dalam aplikasi DANA.</li>
</ol>
<p><em>IX. PEMBELAJAAN DI MERCHANT ATAU PEMBELIAN BARANG/JASA</em></p>
<ol>
    <li>Pengguna dapat melakukan transaksi pembayaran untuk pembelanjaan di Merchant atau pembelian barang/jasa menggunakan sarana-sarana yang disediakan dalam aplikasi DANA termasuk sarana lainnya milik pihak ketiga yang terhubung dengan layanan DANA di masa yang akan datang.</li>
    <li>Pengguna dapat mengetahui informasi tentang Merchant-Merchant yang menerima pembayaran dengan DANA berikut promosi menarik lainnya melalui aplikasi DANA atau melalui sarana-sarana lainnya yang akan dikembangkan di masa yang akan datang.</li>
</ol>
<p><em>X. JANGKA WAKTU</em></p>
<p>Rekening DANA milik Pengguna akan berlaku dan memiliki jangka waktu selama rekening tersebut tidak ditutup secara permanen.</p>
<p>XI. BIAYA-BIAYA LAYANAN DANA</p>
<p><span style="font-weight: 400;">Biaya-biaya yang tercantum pada tabel berikut adalah biaya-biaya yang akan dipotong secara langsung dari saldo DANA Pengguna sehubungan dengan layanan DANA yang saat ini berlaku.</span></p>
<table style="border-collapse: collapse; width: 100%; height: 624px;" border="1">
    <tbody>
        <tr style="height: 48px;">
            <td style="width: 8.00742%; text-align: center; height: 48px;">No</td>
            <td style="width: 71.9739%; text-align: left; height: 48px;">Jenis Layanan</td>
            <td style="width: 20.0186%; text-align: center; height: 48px;">Biaya</td>
        </tr>
        <tr style="text-align: center;">
            <td style="width: 8.00742%; height: 48px;">1</td>
            <td style="width: 71.9739%; text-align: left; height: 48px;">Aktivasi DANA untuk Pengguna pertama kali</td>
            <td style="width: 20.0186%; height: 48px;">GRATIS</td>
        </tr>
        <tr style="text-align: center;">
            <td style="width: 8.00742%; height: 48px;">2</td>
            <td style="width: 71.9739%; height: 48px; text-align: left;">Peningkatan (upgrade) pendaftaran di galeri/kantor DANA*</td>
            <td style="width: 20.0186%; height: 48px;">GRATIS</td>
        </tr>
        <tr style="text-align: center;">
            <td style="width: 8.00742%; height: 48px;">3</td>
            <td style="width: 71.9739%; height: 48px; text-align: left;">Top-up/isi ulang saldo DANA melalui transfer bank*</td>
            <td style="width: 20.0186%; height: 48px;">Lihat ketentuan mengenai biaya</td>
        </tr>
        <tr style="text-align: center;">
            <td style="width: 8.00742%; height: 48px;">4</td>
            <td style="width: 71.9739%; height: 48px; text-align: left;">Top-up saldo DANA melalui galeri/kantor DANA</td>
            <td style="width: 20.0186%; height: 48px;">GRATIS</td>
        </tr>
        <tr style="text-align: center;">
            <td style="width: 8.00742%; height: 48px;">5</td>
            <td style="width: 71.9739%; height: 48px; text-align: left;">Cek saldo DANA</td>
            <td style="width: 20.0186%; height: 48px;">GRATIS</td>
        </tr>
        <tr style="text-align: center;">
            <td style="width: 8.00742%; height: 48px;">6</td>
            <td style="width: 71.9739%; height: 48px; text-align: left;">Unduh atau surel riwayat transaksi keuangan
                <br />* tersedia dimasa yang akan datang</td>
            <td style="width: 20.0186%; height: 48px;">GRATIS</td>
        </tr>
        <tr style="text-align: center;">
            <td style="width: 8.00742%; height: 48px;">7</td>
            <td style="width: 71.9739%; height: 48px; text-align: left;">Pembelian isi ulang menggunakan saldo DANA</td>
            <td style="width: 20.0186%; height: 48px;">GRATIS</td>
        </tr>
        <tr style="text-align: center;">
            <td style="width: 8.00742%; height: 48px;">8</td>
            <td style="width: 71.9739%; height: 48px; text-align: left;">Pembayaran tagihan pascabayar/tagihan menggunakan saldo DANA</td>
            <td style="width: 20.0186%; height: 48px;">GRATIS</td>
        </tr>
        <tr style="text-align: center;">
            <td style="width: 8.00742%; height: 48px;">9</td>
            <td style="width: 71.9739%; height: 48px; text-align: left;">Pembelajaan di merchant atau pembelian barang/jasa menggunakan saldo DANA</td>
            <td style="width: 20.0186%; height: 48px;">GRATIS</td>
        </tr>
        <tr style="text-align: center;">
            <td style="width: 8.00742%; height: 48px;">10</td>
            <td style="width: 71.9739%; height: 48px; text-align: left;">Mengirimkan uang ke sesama Pengguna DANA atau non-Pengguna DANA
                <br />* tersedia dimasa yang akan datang</td>
            <td style="width: 20.0186%; height: 48px;">GRATIS</td>
        </tr>
        <tr style="text-align: center;">
            <td style="width: 8.00742%; height: 48px;">11</td>
            <td style="width: 71.9739%; height: 48px; text-align: left;">Penarikan uang (cash-out) di kantor/cabang/galeri DANA
                <br />* tersedia dimasa yang akan datang</td>
            <td style="width: 20.0186%; height: 48px;">GRATIS</td>
        </tr>
        <tr style="height: 48px;">
            <td style="width: 8.00742%; text-align: center; height: 48px;">12</td>
            <td style="width: 71.9739%; height: 48px;">Top-up dan penarikan uang (cash-out) melalui Agen
                <br />*tersedia dimasa yang akan datang</td>
            <td style="width: 20.0186%; height: 48px; text-align: center;">Sesuai biaya yang berlaku di Agen</td>
        </tr>
    </tbody>
</table>
<p>XII. PEMBLOKIRAN ATAS REKENING DANA</p>
<ol>
    <li>Rekening DANA dapat diblokir dalam hal terjadinya hal-hal sebagai berikut:
        <ol style="list-style-type: lower-roman;">
            <li>Apabila Pengguna memasukkan PIN dan/atau kata sandi (password) yang salah sebanyak 4 (empat) kali berturut-turut;</li>
            <li>Apabila diminta secara resmi oleh Pengguna;</li>
            <li>Dilakukan secara sepihak oleh DANA dikarenakan terdapat satu dan lain hal yang menurut pertimbangan DANA memiliki suatu indikasi pelanggaran terhadap hukum dan ketentuan yang berlaku sehingga rekening harus segera di blokir dengan atau tanpa pemberitahuan terlebih dahulu kepada Pengguna.</li>
        </ol>
    </li>
    <li>Sehubungan dengan bagian 1 di atas DANA berhak untuk memblokir rekening DANA yang dimiliki Pengguna secara sepihak dengan atau tanpa pemberitahuan terlebih dahulu apabila terjadi satu atau lebih hal sebagaimana disebutkan dibawah ini:
        <ol style="list-style-type: lower-roman;">
            <li>Adanya permintaan dari otoritas negara atau otoritas penegak hukum yang berwenang, atau;</li>
            <li>Apabila Pengguna melakukan pelanggaran terhadap satu atau lebih Syarat dan Ketentuan Pengguna, atau;</li>
            <li>Apabila Pengguna memberikan data/ informasi dan keterangan yang tidak benar atau menyesatkan pada saat melakukan pendaftaran atau selama menggunakan layanan DANA, atau;</li>
            <li>Apabila terdapat indikasi penyalahgunaan untuk kegiatan yang melanggar hukum.</li>
            <li>Apabila DANA berdasarkan pertimbangannya sendiri yang cukup beralasan memiliki alasan kuat lainnya sehingga rekening dianggap perlu di blokir.</li>
        </ol>
    </li>
    <li>Terhadap rekening DANA yang diblokir
        <ol style="list-style-type: lower-roman;">
            <li>Terhadap hal-hal yang telah tercantum dalam butir i. sampai iii. diatas maka rekening menjadi tidak dapat digunakan untuk melakukan transaksi keuangan namun tetap dapat menerima dana (top-up) dan selanjutnya dapat diaktifkan kembali, atau;</li>
            <li>Terhadap hal-hal yang telah tercantum dalam butir iv. dan v. di atas maka rekening menjadi tidak dapat digunakan untuk melakukan transaksi keuangan apapun termasuk menerima dana (top-up) dan hanya dapat diaktifkan kembali sesuai dengan kebijakan yang berlaku di DANA.</li>
        </ol>
    </li>
    <li>Aktifasi ulang (re-aktifasi) atas rekening yang diblokir yang disebabkan alasan-alasan sebagaimana dimaksud di bagian i. dan ii. di atas dapat diajukan oleh Pengguna dengan mengikuti syarat-syarat sebagai berikut:
        <ol style="list-style-type: lower-roman;">
            <li>Rekening DANA tersebut tidak atau belum diblokir secara permanen;</li>
            <li>Pengguna merubah PIN DANA melalui aplikasi DANA dan mengikuti petunjuk yang ditampilkan pada aplikasi DANA;</li>
            <li>Apabila proses perubahan PIN DANA telah berhasil, maka layanan DANA akan menjadi aktif kembali;</li>
            <li>Apabila Pengguna mengalami kendala dalam proses perubahan PIN DANA maka Pengguna disarankan untuk menghubungi Layanan Pelanggan (Customer Service) DANA atau mengirimkan email ke help@dana.id namun persetujuan atau penolakan dilakukannya aktifasi ulang atas rekening yang diblokir akan tetap menjadi wewenang sepenuhnya dari DANA.</li>
        </ol>
    </li>
</ol>
<p>XIII. INFORMASI DAN PENGADUAN ATAS KELUHAN</p>
<ol>
    <li>Pengguna dapat mengajukan pertanyaan seputar layanan DANA atau menyampaikan keluhan atas layanan atau transaksi dengan menghubungi Layanan Pelanggan (Customer Service) DANA di nomor telepon 1500445 atau mengirimkan email ke help@dana.id.</li>
    <li>Pengguna juga dapat melihat informasi terkait dengan Pengguna layanan DANA bersama dengan promosi-promosi yang saat ini berlaku secara langsung di aplikasi DANA atau situs web DANA di www.dana.id.</li>
</ol>
<p>XIV. BATASAN TANGGUNG JAWAB</p>
<ol>
    <li>Untuk mencegah terjadinya penyalahgunaan data Pengguna maka Pengguna wajib mengingat dan menjaga kerahasiaan informasi data yang dimilikinya antara lain namun tidak terbatas pada Nomor Ponsel (Handphone) yang digunakan dan didaftarkan, jawaban dari pertanyaan rahasia yang didaftarkan atau data lainnya yang diberikan Pengguna atau diterima oleh Pengguna terkait Transaksi atau atas setiap kegiatan atau Transaksi yang terjadi/ dilakukan oleh Pengguna dengan tidak mengungkapkannya kepada pihak manapun. Pengguna bertanggung jawab sepenuhnya atas segala akibat dan risiko yang timbul sehubungan dengan kelalaian Pengguna dalam menjaga kerahasiaan informasi data rekening yang dimilikinya.</li>
    <li>Pengguna wajib menjaga Nomor Ponsel (Handphone) dan Nomor Ponsel (Handphone) alternatif yang dimiliknya dari peristiwa antara lain kehilangan, kerusakan, penyalahgunaan oleh pihak yang tidak bertanggungjawab atau pemalsuan. Pengguna dengan ini mengetahui dan menyetujui untuk melepaskan DANA dari tanggungjawab dan ganti kerugian dalam bentuk apapun dari Pengguna atau pihak manapun atas hal-hal yang terjadi diluar kesalahan dan/atau kelalaian DANA termasuk tetapi tidak terbatas kepada hal-hal sebagai berikut:
        <ol style="list-style-type: lower-roman;">
            <li>
                <p>kehilangan atau kerusakan Telepon Genggam atau Nomor Ponsel (Handphone)</p>
            </li>
            <li>
                <p>Setiap kerugian yang terjadi yang diakibatkan karena Pengguna DANA terindikasi melanggar hukum dan/atau terdapat penyalahgunaan dalam bentuk apapun oleh pihak lain yang tidak berwenang;</p>
            </li>
        </ol>
    </li>
    <li>Setiap hambatan, kerugian dan/atau kerusakan karena tidak beroperasinya dan/atau terjadinya gangguan yang disebabkan terjadinya suatu Keadaan Memaksa sebagaimana dimaksud dalam ketentuan yang mengatur mengenai Keadaan Memaksa yang tercantum dalam Syarat dan Ketentuan ini. Dalam hal terjadi kehilangan Telepon Genggam baik karena pencurian, kehilangan atau alasan apapun maka Pengguna wajib segera menghubungi Layanan Pelanggan (Customer Service) DANA untuk melakukan pemblokiran atas rekening DANA. Pengguna dengan ini membebaskan dan melepaskan DANA dari segala risiko dan akibat yang timbul dan diderita oleh Pengguna sehubungan dengan kehilangan Nomor Ponsel (Handphone) atau Nomor Ponsel (Handphone) alternatif atau kelalaian Pengguna sehubungan dengan hal tersebut dan karenanya Pengguna wajib bertanggungjawab secara penuh.\</li>
    <li>Pengguna dengan ini mengakui dan menyetujui bahwa sebagai Pengguna yang menggunakan layanan DANA Pengguna akan selalu tunduk kepada termasuk namun tidak terbatas: Syarat dan Ketentuan untuk Pengguna DANA dan seluruh peraturan perundangan yang berlaku di Indonesia dari waktu ke waktu yang mengatur khususnya tentang uang elektronik.</li>
    <li>DANA atas pertimbangannya sendiri berhak untuk menolak permohonan pendaftaran layanan DANA oleh Pengguna tanpa memberitahukan alasannya.</li>
    <li>Pengguna dengan ini diwajibkan untuk memeriksa dan memastikan bahwa seluruh informasi dan data yang didaftarkan dan diberikan pada saat melakukan Transaksi adalah akurat dan benar oleh sebab itu Pengguna membebaskan dan melepaskan DANA dari segala bentuk gugatan, tuntutan dan/atau ganti kerugian baik yang berasal dari Pengguna atau pihak manapun dan dalam bentuk apapun sehubungan dengan kelalaian Pengguna dalam memenuhi ketentuan ayat ini.</li>
    <li>Apabila Transaksi telah diselesaikan maka Pengguna dengan ini mengakui dan menyetujui bahwa transaksi TIDAK DAPAT ditarik kembali dengan alasan apapun juga dan Transaksi akan tetap diproses sesuai dengan informasi dan data yang telah didaftarkan dan dimasukkan oleh Pengguna.</li>
    <li>Pengguna akan dikenakan biaya-biaya sehubungan dengan layanan DANA sesuai dengan peraturan yang berlaku di DANA termasuk namun tidak terbatas pada biaya Transaksi, biaya layanan pesan singkat (SMS) dan biaya-biaya lainnya yang akan diinformasikan kepada Pengguna melalui media komunikasi yang digunakan untuk produk DANA. Pengguna layanan aplikasi DANA harus menggunakan fasilitas atas layanan data oleh karenanya Pengguna akan dikenakan tarif akses data atas fasilitas tersebut. Biaya dan ketentuan penggunaannya akan diatur oleh operator selular yang digunakan oleh Pengguna.</li>
    <li>Dalam hal terjadi gangguan teknis pada jaringan atau dalam hal sedang dilakukan peningkatan layanan atau jaringan, perubahan layanan atau jaringan, perbaikan dan/atau pemeliharaan layanan atau jaringan sehingga menyebabkan gangguan pada layanan DANA, maka DANA akan segera menangani dan memperbaikinya dalam jangka waktu maksimum 1 (satu) hari kerja dan atas hal tersebut Pengguna akan menerima pemberitahuan dari aplikasi DANA sehubungan dengan gangguan atas layanan tersebut.</li>
    <li>Dalam hal terjadi kesalahan sistem karena alasan apapun yang mengakibatkan terganggunya layanan DANA atau kesalahan dalam pelaksanaan layanan atau transaksi yang bukan disebabkan oleh Pengguna, maka DANA akan memperbaiki kesalahan tersebut dengan sesegera mungkin atau dalam waktu 1 (satu) hari kerja atau maksimal 3 (tiga) hari kerja sejak ditemukan kesalahan tersebut.</li>
</ol>
<p>XV. KEADAAN MEMAKSA <em>(Force Majeur)</em></p>
<p><span style="font-weight: 400;">Tidak dilaksanakannya atau tertundanya pelaksanaan sebagian atau keseluruhan kewajiban berdasarkan </span>&ldquo;Syarat dan Ketentuan untuk Pengguna DANA&rdquo;<span style="font-weight: 400;"> oleh DANA tidak akan dianggap sebagai pelanggaran terhadap </span>&ldquo;Syarat dan Ketentuan untuk Pengguna DANA&rdquo; <span style="font-weight: 400;">apabila hal tersebut disebabkan oleh suatu peristiwa</span> <span style="font-weight: 400;">yang berada di luar kendali DANA atau lazim disebut dengan istilah Keadaan Memaksa </span><em><span style="font-weight: 400;">(Force Majeur)</span></em><span style="font-weight: 400;"> termasuk namun tidak terbatas pada</span><span style="font-weight: 400;"> (a) bencana alam </span><span style="font-weight: 400;">(b) kebakaran, pemogokan buruh, perang, huru-hara, pemberontakan atau tindakan militer lainnya (c) tindakan pihak/instansi yang berwenang yang mempengaruhi kelangsungan penyediaan layanan telekomunikasi (d) tindakan pihak ketiga yang menyebabkan DANA tidak dapat memberikan layanan telekomunikasi dan (e) adanya keputusan dari instansi yang berwenang atau perubahan keputusan dari pemerintah yang berdampak pada pelaksanaan layanan DANA termasuk diantaranya perubahaan pemberlakuan tarif kepada Pengguna.</span></p>
<p><em>XVI. KEAMANAN DAN KERAHASIAAN TRANSAKSI</em></p>
<ol>
    <li>DANA sangat memperhatikan keamanan dan kenyamanan Pengguna pada saat menggunakan layanan DANA. Oleh karena itu DANA memiliki sistem keamanan ketika Pengguna melakukan transaksi untuk memberikan kepastian kepada Pengguna bahwa Transaksi dan Informasi Pengguna aman.</li>
    <li>DANA menggunakan server yang memadai dalam menjaga keamanan informasi rahasia Pengguna. Seluruh informasi yang bersifat sensitif akan dikirimkan melalui teknologi Secure Socket Layer (SSL) dalam keadaan mana data tersebut hanya dapat diakses oleh personil yang memiliki wewenang khusus yang disyaratkan untuk selalu menjaga kerahasiaan informasi tersebut.</li>
    <li>Memastikan PIN milik Pengguna tidak diketahui oleh siapapun untuk menghindari terjadinya penyalahgunaan oleh pihak lain.</li>
    <li>Dalam hal DANA memiliki kerjasama dengan pihak ketiga lainnya sehubungan dengan penyediaan akses ke layanan DANA untuk Pengguna melalui sarana pihak ketiga tersebut maka:
        <br />
        <ol style="list-style-type: upper-roman;">
            <li>DANA dapat menampilkan informasi data rekening Pengguna kepada pihak ketiga tersebut untuk tujuan pemeliharaan keamanan sistem.</li>
            <li>DANA dapat menggunakan sarana otorisasi selain DANA yang ditentukan bersama oleh DANA dan pihak ketiga yang bekerjasama dengan DANA.</li>
            <li>DANA menjamin bahwa ketentuan pelaksanaan sebagaimana yang dimaksud di bagian i) dan ii) di atas telah memenuhi standar keamanan dan ketentuan DANA serta memastikan bahwa hal tersebut tidak melanggar peraturan perundang-undangan yang berlaku.</li>
        </ol>
    </li>
</ol>
<p><em>XVII. PERNYATAAN DAN JAMINAN</em></p>
<ol>
    <li>Pengguna menyatakan dan menjamin bahwa berdasarkan hukum dan peraturan perundang-undangan yang berlaku Pengguna merupakan pihak yang dapat, cakap atau diperbolehkan menggunakan layanan yang disediakan oleh DANA;</li>
    <li>Pengguna menyatakan dan menjamin bahwa dana yang dipergunakan dalam rangka layanan transaksi bukanlah dana yang berasal dari tindak pidana yang dilarang berdasarkan peraturan perundang-undangan yang berlaku di Republik Indonesia, pembukaan rekening tidak dimaksudkan dan/atau ditujukan dalam rangka upaya melakukan tindak pidana pencucian uang sesuai dengan ketentuan peraturan perundang-undangan yang berlaku di Republik Indonesia, transaksi tidak dilakukan untuk maksud mengelabui, mengaburkan, atau menghindari pelaporan kepada Pusat Pelaporan Dan Analisa Transaksi Keuangan (PPATK) berdasarkan ketentuan peraturan perundang-undangan yang berlaku di Republik Indonesia dan Pengguna bertanggung jawab sepenuhnya serta melepaskan DANA dari segala tuntutan, klaim, atau ganti rugi dalam bentuk apapun apabila Pengguna ternyata melakukan tindak pidana pencucian uang berdasarkan ketentuan peraturan perundang-undangan yang berlaku di Republik Indonesia;</li>
    <li>Pengguna dengan ini menyatakan dan menjamin bahwa pihaknya akan bertanggung jawab sepenuhnya atas semua layanan DANA yang diakses menggunakan nomor ID pengguna miliknya;</li>
    <li>Pengguna dengan ini menyatakan dan menjamin bahwa data dan informasi yang di daftarkan atau diberikan adalah benar, akurat dan lengkap dan tidak menyesatkan dalam bentuk apapun;</li>
    <li>Pengguna tidak akan memberikan hak, wewenang dan/atau kuasa dalam bentuk apapun dan dalam kondisi apapun kepada orang atau pihak lain untuk menggunakan data, akun dan/atau PIN dan Pengguna karena alasan apapun dan dalam kondisi apapun tidak akan dan dilarang untuk mengalihkan rekening kepada orang atau pihak manapun;</li>
    <li>Pengguna menyatakan dan menjamin tidak akan dengan alasan apapun meminta kembali setiap dan seluruh data/ dokumen yang telah disampaikan kepada DANA dan karenanya DANA berhak menggunakan data dan informasi tersebut sehubungan dengan layanan Transaksi atau terkait urusan administrasi rekening Pengguna pada DANA atau promosi dan program-program marketing DANA;</li>
    <li>Pengguna menyatakan dan menjamin akan memperbaharui dan memberitahukan kepada DANA apabila ada perubahan data yang terkait dengan Pengguna;</li>
    <li>Dengan melaksanakan transaksi melalui aplikasi DANA maka Pengguna memahami bahwa seluruh komunikasi dan instruksi dari Pengguna yang diterima oleh DANA akan diperlakukan sebagai bukti otentik meskipun tidak dibuat dalam bentuk dokumen tertulis atau diterbitkan dalam bentuk dokumen yang ditandatangani;</li>
    <li>Pengguna menyatakan dan menjamin untuk membebaskan dan melepaskan DANA dari segala gugatan, tuntutan dan/atau ganti kerugian dari pihaknya maupun dari pihak manapun sehubungan dengan kelalaian Pengguna dalam memenuhi ketentuan mengenai pernyataan dan jaminan ini.</li>
</ol>
<p><em>XVIII. LAIN-LAIN</em></p>
<ol>
    <li>&ldquo;Syarat dan ketentuan untuk Pengguna DANA&rdquo; ini tunduk dan diatur serta dilaksanakan sesuai dengan hukum Republik Indonesia;</li>
    <li>Apabila terjadi perselisihan dalam penafsiran dan pelaksanaan dari &ldquo;Syarat dan Ketentuan untuk Pengguna DANA&rdquo; maka DANA dan Pengguna setuju untuk menyelesaikan perselisihan tersebut melalui jalan musyawarah. Dalam hal musyawarah tidak dapat mencapai sepakat, maka selanjutnya akan diselesaikan melalui Pengadilan Negeri Jakarta Selatan;</li>
    <li>&ldquo;Syarat dan Ketentuan untuk Pengguna DANA&rdquo; ini dibuat dalam Bahasa Indonesia dan Bahasa Inggris. Dalam hal terdapat ketidaksesuaian diantara keduanya, maka Para Pihak setuju dan sepakat untuk menggunakan Bahasa Indonesia sebagai bahasa yang mengikat;</li>
    <li>DANA termasuk nama dan logo, kode, desain, teknologi dan formula bisnisnya, dilindungi oleh suatu hak cipta, merek, paten dan hak atas kekayaan intelektual lainnya serta tunduk pada hukum dan peraturan perundang-undangan yang berlaku di wilayah Republik Indonesia. DANA (dan pihak-pihak yang terkait dengan perizinan DANA, jika ada) memiliki semua hak dan kepentingan DANA terkait hak atas kekayaan intelektual yang mengikutinya. &ldquo;Syarat dan Ketentuan Pengguna DANA&rdquo; ini tidak mengakibatkan beralihnya atau memberikan hak bagi Pengguna untuk menggunakan hak atas kekayaan intelektual DANA dalam arti yang seluas-luasnya terhadap hak-hak atas kekayaan intelektual sebagaimana disebutkan di atas;</li>
    <li>Kegagalan atau tertundanya DANA dalam mengajukan gugatan atau tuntutan terhadap dilanggarnya &ldquo;Syarat dan Ketentuan Pengguna DANA&rdquo; ini tidak menghapuskan kewajiban bagi Pengguna DANA untuk mematuhi &ldquo;Syarat dan Ketentuan Pengguna DANA&rdquo;;</li>
    <li>Pengguna dilarang untuk mengalihkan hak nya berdasarkan &ldquo;Syarat dan Ketentuan Pengguna DANA&rdquo; ini tanpa persetujuan terlebih dahulu dari DANA dimana persetujuan tersebut diberikan oleh DANA berdasarkan pertimbangan dan kebijakan DANA semata. Meski atau seandainya pun Pengguna DANA telah mengirimkan atau mengalihkan saldo kepada pihak lain, tidak berarti rekening Pengguna beralih kepada pihak lain tersebut;</li>
    <li>Jika terdapat suatu ketentuan dalam &ldquo;Syarat dan Ketentuan Pengguna DANA&rdquo; ini yang ternyata diketahui melanggar ketentuan dan perundang-undangan yang berlaku baik sebagian maupun seluruhnya, maka ketentuan yang dianggap melanggar tersebut tidak merupakan bagian dari &ldquo;Syarat dan Ketentuan Pengguna DANA&rdquo; ini dan atas syarat dan ketentuan lain tetap berlaku dan mengikat;</li>
    <li>&ldquo;Syarat dan Ketentuan Pengguna DANA&rdquo; ini baik sebagian maupun seluruhnya termasuk fitur atau layanan yang ditawarkan oleh DANA dapat dirubah, diperbaharui, dan/atau ditambah dari waktu ke waktu dengan berdasarkan kepada kebijakan yang berlaku di DANA dan terhadap perubahan, pembaharuan dan/atau penambahan tersebut Pengguna DANA dengan ini menyatakan menerima perubahan, pembaharuan dan/atau penambahan tersebut;</li>
    <li>Data pribadi Pengguna DANA diatur dalam Peraturan Data Pribadi (Privacy Policy) dimana Peraturan Data Pribadi tersebut merupakan satu kesatuan dan bagian yang tidak terpisahkan &ldquo;Syarat dan Ketentuan Pengguna DANA&rdquo; ini dan karenanya Pengguna menerima dan menyetujui Peraturan Data Pribadi (Privacy Policy) yang terkait;</li>
    <li>Transaksi Pengguna yang dilakukan melalui DANA disimpan secara otomatis pada server DANA. Pengguna mengakui dan memahami bahwa DANA dengan upaya terbaiknya telah menjaga keamanan sistemnya. Namun demikian, dalam hal terdapat perbedaan atau ketidaksesuaian antara data dan saldo dan catatan Transaksi Pengguna maka Pengguna dengan ini menyetujui bahwa data dan saldo dan catatan Transaksi yang berlaku dan diakui adalah data dan saldo dan catatan Transaksi yang dimiliki oleh DANA;</li>
    <li>Pengguna memahami bahwa dalam menggunakan rekening DANA, data pribadi dari Pengguna akan dikumpulkan, digunakan maupun diperlihatkan sehingga Pengguna dapat menikmati layanan DANA secara maksimal, karenanya Pengguna dengan ini mengizinkan DANA untuk menggunakan data pribadi tersebut kepada pihak ketiga dimana DANA memiliki kerja sama;</li>
    <li>Jika Pengguna tidak menggunakan layanan DANA selama jangka waktu tertentu, maka DANA dapat menghentikan (memblokir) rekening Pengguna;</li>
    <li>Pengguna dapat menghubungi Layanan Pelanggan (Customer Service) DANA atau masuk ke dalam aplikasi dan situs web DANA untuk mengetahui informasi sehubungan dengan layanan DANA;</li>
    <li>Pengguna akan menerima pemberitahuan mengenai layanan atau promosi terbaru dari DANA melalui media komunikasi pribadi seperti SMS atau melalui aplikasi DANA. Bila Pengguna keberatan untuk menerima pemberitahuan tersebut maka Pengguna dapat menghubungi kami:
        <p>PT Espay Debit Indonesia Koe (DANA) Gd Capital Place Office Tower Lantai 18</p>
        <p>Jalan Jend. Gatot Subroto Kav 18, Jakarta Selatan 12710 Jam kerja: Senin &ndash; Jumat, pukul 08.30 &ndash; 17.00 WIB Telepon: +622127933690 dan +622127933622</p>
        <p>Surel: <a href="mailto:help@dana.id" target="_blank" rel="noopener">help@dana.id</a></p>
    </li>
</ol>