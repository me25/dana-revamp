<html>
<?php include "includes/head.php";?>
<body>
	<?php
		$header = "clean";
		$footer = "";
	?>
	<?php include "includes/header.php";?>
	<div class="area_produk">
		<section class="card_produk">
			<div class="container">
				<div class="img">
					<img src="assets/img/dana_qr.png" alt="">
				</div>
				<div class="text">
					<h1>Dengan Dana</h1>
					<h2>Bayar menjadi <br>lebih mudah</h2>
					<div class="desc">
						Proses transaksi menjadi jauh lebih praktis dengan DANA. Scan barcode, lalu konfirmasi. Voila! Pembayaran selesai dilakukan.
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</section>
		<section class="card_produk">
			<div class="container">
				<div class="img">
					<img src="assets/img/dana_home.png" alt="">
				</div>
				<div class="text">
					<h2>Bayar tagihan jauh lebih mudah</h2>
					<div class="desc">
						Proses transaksi menjadi jauh lebih praktis dengan DANA. Scan barcode, lalu konfirmasi. Voila! Pembayaran selesai dilakukan.
					</div>
					<div class="img2">
						<img src="assets/img/produk2.png" alt="">
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</section>
		<section class="card_produk">
			<div class="container">
				<div class="img">
					<img src="assets/img/produk3.png" alt="">
				</div>
				<div class="text">
					<h2>Bayar menjadi <br>lebih mudah</h2>
					<div class="desc">
						Proses transaksi menjadi jauh lebih praktis dengan DANA. Scan barcode, lalu konfirmasi. Voila! Pembayaran selesai dilakukan.
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</section>
		<section class="card_produk">
			<div class="container">
				<div class="img">
					<img src="assets/img/produk4.png" alt="">
				</div>
				<div class="text">
					<h2>Bayar menjadi <br>lebih mudah</h2>
					<div class="desc">
						Proses transaksi menjadi jauh lebih praktis dengan DANA. Scan barcode, lalu konfirmasi. Voila! Pembayaran selesai dilakukan.
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</section>
	</div>
	
	

	
	<?php include "includes/footer.php";?>
	<?php include "includes/js.php";?>
</body>
</html>