<html>
<?php include "includes/head.php";?>
<body>
	<?php include "includes/header.php";?>
	<div class="area_promopage">
		<div id="slide_promobig">
			<a href="#" class="box_img ratio16_9">
				<div class="img_con lqd">
					<img src="assets/img/promo1.jpg" alt="">
				</div>
			</a>
			<a href="#" class="box_img ratio16_9">
				<div class="img_con lqd">
					<img src="assets/img/promo2.jpg" alt="">
				</div>
			</a>
			<a href="#" class="box_img ratio16_9">
				<div class="img_con lqd">
					<img src="assets/img/promo1.jpg" alt="">
				</div>
			</a>
			<a href="#" class="box_img ratio16_9">
				<div class="img_con lqd">
					<img src="assets/img/promo2.jpg" alt="">
				</div>
			</a>
		</div>
		<img src="assets/img/arrow_next_white2.png" alt="" class="slide_next">
		<img src="assets/img/arrow_back_white2.png" alt="" class="slide_prev">
		<div class="paging_slide" id="slide_page"></div>
	</div>
	
	
	<section class="section">
		<div class="container">
			<div class="nav2 nav2a">
				<a href="#" class="selected">Entertainment</a>
				<a href="#">Food & Beverage</a>
				<a href="#">Healthy</a>
			</div>
			<div class="area_promo">
				<div class="card_promo">
					<div class="box_img ratio16_9">
						<div class="img_con lqd">
							<img src="assets/img/promo.png" alt="">
						</div>
					</div>
					<div class="label"><span>DANA</span></div>
					<div class="text">
						<a href="promo_detail.php">
							<h3>Gebyar Pulsa Awal Tahun</h3>
						</a>
					</div>
					<div class="date">Hingga 31 Januari 2019</div>
					<a href="#" class="btn_more">Lihat Lebih Lengkap</a>
					<div class="clearfix"></div>
				</div>
				<div class="card_promo">
					<div class="box_img ratio16_9">
						<div class="img_con lqd">
							<img src="assets/img/promo.png" alt="">
						</div>
					</div>
					<div class="label"><span>DANA</span></div>
					<div class="text">
						<a href="#">
							<h3>Gebyar Pulsa Awal Tahun</h3>
						</a>
					</div>
					<div class="date">Hingga 31 Januari 2019</div>
					<a href="#" class="btn_more">Lihat Lebih Lengkap</a>
					<div class="clearfix"></div>
				</div>
				<div class="card_promo">
					<div class="box_img ratio16_9">
						<div class="img_con lqd">
							<img src="assets/img/promo.png" alt="">
						</div>
					</div>
					<div class="label"><span>DANA</span></div>
					<div class="text">
						<a href="#">
							<h3>Gebyar Pulsa Awal Tahun</h3>
						</a>
					</div>
					<div class="date">Hingga 31 Januari 2019</div>
					<a href="#" class="btn_more">Lihat Lebih Lengkap</a>
					<div class="clearfix"></div>
				</div>
				<div class="card_promo">
					<div class="box_img ratio16_9">
						<div class="img_con lqd">
							<img src="assets/img/promo.png" alt="">
						</div>
					</div>
					<div class="label"><span>DANA</span></div>
					<div class="text">
						<a href="#">
							<h3>Gebyar Pulsa Awal Tahun</h3>
						</a>
					</div>
					<div class="date">Hingga 31 Januari 2019</div>
					<a href="#" class="btn_more">Lihat Lebih Lengkap</a>
					<div class="clearfix"></div>
				</div>
				<div class="card_promo">
					<div class="box_img ratio16_9">
						<div class="img_con lqd">
							<img src="assets/img/promo.png" alt="">
						</div>
					</div>
					<div class="label"><span>DANA</span></div>
					<div class="text">
						<a href="#">
							<h3>Gebyar Pulsa Awal Tahun</h3>
						</a>
					</div>
					<div class="date">Hingga 31 Januari 2019</div>
					<a href="#" class="btn_more">Lihat Lebih Lengkap</a>
					<div class="clearfix"></div>
				</div>
				<div class="card_promo">
					<div class="box_img ratio16_9">
						<div class="img_con lqd">
							<img src="assets/img/promo.png" alt="">
						</div>
					</div>
					<div class="label"><span>DANA</span></div>
					<div class="text">
						<a href="#">
							<h3>Gebyar Pulsa Awal Tahun</h3>
						</a>
					</div>
					<div class="date">Hingga 31 Januari 2019</div>
					<a href="#" class="btn_more">Lihat Lebih Lengkap</a>
					<div class="clearfix"></div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="paging">
				<a href="#" class="selected">1</a>
				<a href="#">2</a>
				<a href="#">3</a>
			</div>
		</div>
	</section>
	

	
	<?php include "includes/footer.php";?>
	<?php include "includes/js.php";?>
</body>
</html>