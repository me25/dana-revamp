<html>
<?php include "includes/head.php";?>
<body>
	<?php
		$header = "";
		$footer = "reguler";
	?>
	<?php include "includes/header.php";?>
	<section class="hero_cover">
		<div class="box_img ratio3_1">
			<div class="img_con lqd">
				<img src="assets/img/img4.jpg" alt="">
			</div>
			<div class="text text3">
				<div class="container">
					<div class="title_cover"><h1>WORK . PLAY . GROWTH</h1></div>
					<div class="desc">
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</section>
	
	<section class="section">
		<div class="container">
			<div class="page_title"><h2>Bersama di DANA</h2></div>
			<div class="list3">
				<img src="assets/img/icon_career1.png" alt="">
				<div class="t1">Keseruan dalam bekerja</div>
				Kemudahan untuk pembelian pulsa, membayar berbagai tagihan, dan transaksi eCommerce.
			</div>
			<div class="list3">
				<img src="assets/img/icon_career2.png" alt="">
				<div class="t1">Nilai positif yang dibangun</div>
				Kemudahan untuk pembelian pulsa, membayar berbagai tagihan, dan transaksi eCommerce.
			</div>
			<div class="list3">
				<img src="assets/img/icon_career3.png" alt="">
				<div class="t1">Bersama untuk berkembang</div>
				Kemudahan untuk pembelian pulsa, membayar berbagai tagihan, dan transaksi eCommerce.
			</div>
			<div class="clearfix"></div>
		</div>
	</section>
	<section class="section">
		<div class="container">
			<div class="page_title"><h2>Employees Experience</h2></div>
			<div class="areaslide">
				<div id="slide1">
					<div class="card_slidefoto">
						<div class="ratio16_9 box_img">
							<div class="img_con">
								<img src="assets/img/img1.jpg" alt="">
							</div>
						</div>
						<div class="caption">Caption Foto 1</div>
					</div>
					<div class="card_slidefoto">
						<div class="ratio16_9 box_img">
							<div class="img_con">
								<img src="assets/img/img2.jpg" alt="">
							</div>
						</div>
						<div class="caption">Caption Foto 2</div>
					</div>
					<div class="card_slidefoto">
						<div class="ratio16_9 box_img">
							<div class="img_con">
								<img src="assets/img/img3.jpg" alt="">
							</div>
						</div>
						<div class="caption">Caption Foto 3</div>
					</div>
				</div>
				<img src="assets/img/arrow_back_black.png" alt="" class="slide1_prev">
				<img src="assets/img/arrow_next_black.png" alt="" class="slide1_next">
			</div>
			<div class="clearfix"></div>
		</div>
	</section>
	<section class="section">
		<div class="container">
			<div class="page_title"><h2>Cari minatmu</h2></div>
			<div class="nav2">
				<a href="#" class="selected">Semua</a>
				<a href="#">Jakarta</a>
				<a href="#">Bali</a>
				<a href="#">Bandung</a>
			</div>
			<div class="listing_loker">
				<div class="card_loker">
					<div class="ratio4_3 box_img">
						<div class="img_con lqd">
							<img src="assets/img/img5.jpg" alt="">
						</div>
						<div class="text">
							<h2>Human Resource</h2>
							<div class="desc">nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat</div>
							<a href="karir_detail.php" class="btn_more">Lihat lebih lanjut</a>
						</div>
					</div>
				</div>
				<div class="card_loker">
					<div class="ratio4_3 box_img">
						<div class="img_con lqd">
							<img src="assets/img/img5.jpg" alt="">
						</div>
						<div class="text">
							<h2>Human Resource</h2>
							<div class="desc">nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat</div>
							<a href="karir_detail.php" class="btn_more">Lihat lebih lanjut</a>
						</div>
					</div>
				</div>
				<div class="card_loker">
					<div class="ratio4_3 box_img">
						<div class="img_con lqd">
							<img src="assets/img/img5.jpg" alt="">
						</div>
						<div class="text">
							<h2>Human Resource</h2>
							<div class="desc">nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat</div>
							<a href="karir_detail.php" class="btn_more">Lihat lebih lanjut</a>
						</div>
					</div>
				</div>
				<div class="card_loker">
					<div class="ratio4_3 box_img">
						<div class="img_con lqd">
							<img src="assets/img/img5.jpg" alt="">
						</div>
						<div class="text">
							<h2>Human Resource</h2>
							<div class="desc">nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat</div>
							<a href="karir_detail.php" class="btn_more">Lihat lebih lanjut</a>
						</div>
					</div>
				</div>
				<div class="card_loker">
					<div class="ratio4_3 box_img">
						<div class="img_con lqd">
							<img src="assets/img/img5.jpg" alt="">
						</div>
						<div class="text">
							<h2>Human Resource</h2>
							<div class="desc">nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat</div>
							<a href="karir_detail.php" class="btn_more">Lihat lebih lanjut</a>
						</div>
					</div>
				</div>
				<div class="card_loker">
					<div class="ratio4_3 box_img">
						<div class="img_con lqd">
							<img src="assets/img/img5.jpg" alt="">
						</div>
						<div class="text">
							<h2>Human Resource</h2>
							<div class="desc">nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat</div>
							<a href="karir_detail.php" class="btn_more">Lihat lebih lanjut</a>
						</div>
					</div>
				</div>
				<div class="card_loker">
					<div class="ratio4_3 box_img">
						<div class="img_con lqd">
							<img src="assets/img/img5.jpg" alt="">
						</div>
						<div class="text">
							<h2>Human Resource</h2>
							<div class="desc">nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat</div>
							<a href="#" class="btn_more">Lihat lebih lanjut</a>
						</div>
					</div>
				</div>
				<div class="card_loker">
					<div class="ratio4_3 box_img">
						<div class="img_con lqd">
							<img src="assets/img/img5.jpg" alt="">
						</div>
						<div class="text">
							<h2>Human Resource</h2>
							<div class="desc">nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat</div>
							<a href="#" class="btn_more">Lihat lebih lanjut</a>
						</div>
					</div>
				</div>
				<div class="card_loker">
					<div class="ratio4_3 box_img">
						<div class="img_con lqd">
							<img src="assets/img/img5.jpg" alt="">
						</div>
						<div class="text">
							<h2>Human Resource</h2>
							<div class="desc">nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat</div>
							<a href="#" class="btn_more">Lihat lebih lanjut</a>
						</div>
					</div>
				</div>
				<div class="card_loker">
					<div class="ratio4_3 box_img">
						<div class="img_con lqd">
							<img src="assets/img/img5.jpg" alt="">
						</div>
						<div class="text">
							<h2>Human Resource</h2>
							<div class="desc">nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat</div>
							<a href="#" class="btn_more">Lihat lebih lanjut</a>
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</section>
	

	
	<?php include "includes/footer.php";?>
	<?php include "includes/js.php";?>
</body>
</html>