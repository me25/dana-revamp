<div id="footer" class="<?php echo $footer;?>">
	<div class="container">
		<div class="start_now">
			<img src="assets/img/dana_splashscreen.png" alt="" class="phone_bottom">
			<div class="text">
				Mulai menggunakan DANA
				<div class="download">
					<span>Unduh Aplikasi dana di sini</span>
					<a href="#"><img src="assets/img/download_play.png" alt=""></a>
					<a href="#"><img src="assets/img/download_app.png" alt=""></a>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="footer2">
			<a href="#" class="logo"><img src="assets/img/logo.png" alt=""></a>
			<div id="nav_foot">
				<a href="#">Produk & Fitur</a>
				<a href="#">Bisnis & Merchant</a>
				<a href="#">Karir</a>
				<a href="#">Promo</a>
				<a href="#">FAQ</a>
			</div>
			<div class="copy">
				&copy; 2019 DANA - PT. Espay Debit Indonesia Koe. All Rights Reserved.
			</div>

			<div class="sosmed">
				<span>Ikuti kami</span>
				<a href="#"><img src="assets/img/ico-tw.png" alt=""></a>
				<a href="#"><img src="assets/img/ico-fb.png" alt=""></a>
				<a href="#"><img src="assets/img/ico-ig.png" alt=""></a>
				<a href="#"><img src="assets/img/ico-yt.png" alt=""></a>
			</div>
		</div>

	</div>
</div>