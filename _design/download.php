<html>
<?php include "includes/head.php";?>
<body class="downloadpage">
	<?php
		$header = "clean";
		$footer = "reguler";
	?>
	<?php include "includes/header.php";?>
	<div>
		<div class="container">
			<div class="text">
				<h1>Apapun Transaksinya, Selalu Ada DANA</h1>
				<div class="download">
					<strong>Download</strong>
					<a href="#"><img src="assets/img/download_play.png" alt=""></a>
					<a href="#"><img src="assets/img/download_app.png" alt=""></a>
				</div>
			</div>
		</div>
		<div class="phonearea">
			<div class="phone">
				<img src="assets/img/dana_splashscreen.png" alt="" class="phone1">
				<img src="assets/img/dana_home2.png" alt="" class="phone2">
				<img src="assets/img/dana_qr.png" alt="" class="phone3">
				<img src="assets/img/dana_send.png" alt="" class="phone4">
				<img src="assets/img/dana_pay.png" alt="" class="phone5">
				<img src="assets/img/dana_splashscreen.png" alt="" class="phone6">
				<img src="assets/img/dana_home2.png" alt="" class="phone7">
			</div>
		</div>
	</div>
	<?php include "includes/footer.php";?>
	<?php include "includes/js.php";?>
</body>
</html>