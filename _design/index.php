<html>
<?php include "includes/head.php";?>
<body>
	<?php include "includes/header.php";?>
	<section class="hero_cover">
		<div class="box_img ratio16_9">
			<div class="img_con">
				<iframe width="560" height="315" src="https://www.youtube.com/embed/paVI2-GA3Eo?rel=0;&amp;autoplay=1&amp;mute=1&amp;loop=5" frameborder="0" allowfullscreen="" include=""></iframe>
			</div>
			<div class="text">
				<div class="container">
					<div class="img">
						<img src="assets/img/dana_home.png" alt="">
					</div>
					<div class="text2 replace">
						<h1>Apapun Transaksinya, Selalu Ada DANA</h1>
						<div class="tagline">#GantiDompet</div>
						<div class="download">
							<a href="#"><img src="assets/img/download_play.png" alt=""></a>
							<a href="#"><img src="assets/img/download_app.png" alt=""></a>
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</section>
	<section class="about">
		<div class="box_img ratio16_9 bg">
			<div class="img_con lqd"><img src="assets/img/img3.jpg" alt=""></div>
		</div>
		<div class="container">
			<img src="assets/img/img3.jpg" alt="" class="img">
			<div class="text">
				<h2>Tentang Dana</h2>
				<div class="desc">
					Kenyamanan pembelian pulsa dan paket internet hanya dengan sentuhan jari Anda. Gunakan DANA untuk pembelian dan pembayaran pulsa dimanapun dan kapanpun juga.
				</div>
				<a href="tentang.php" class="btn_more">Lihat lebih lengkap</a>
			</div>
			<div class="clearfix"></div>
		</div>
	</section>
	<section class="section">
		<div class="container">
			<div class="page_title"><h2>Transaksi dengan rasa Aman</h2></div>
			<div class="list3">
				<img src="assets/img/icon_wallet.png" alt="">
				<div class="t1">Layanan Terlengkap</div>
				Kemudahan untuk pembelian pulsa, membayar berbagai tagihan, dan transaksi eCommerce.
			</div>
			<div class="list3">
				<img src="assets/img/icon_wallet.png" alt="">
				<div class="t1">Layanan Terlengkap</div>
				Kemudahan untuk pembelian pulsa, membayar berbagai tagihan, dan transaksi eCommerce.
			</div>
			<div class="list3">
				<img src="assets/img/icon_wallet.png" alt="">
				<div class="t1">Layanan Terlengkap</div>
				Kemudahan untuk pembelian pulsa, membayar berbagai tagihan, dan transaksi eCommerce.
			</div>
			<div class="clearfix"></div>
		</div>
	</section>
	<section class="secure section">
		<div class="container">
			<div class="page_title"><h2>DANA & Secure</h2></div>
			<div class="secure_info_area">
				<div class="phone">
					<img src="assets/img/dana_qr.png" alt="">
				</div>
				<div class="circle"></div>
				<div class="info_show">
					<div class="info info1">
						<div class="t1">Easy TopUp</div>
						Kemudahan topup dana dengan beberapa metode topup
					</div>
					<div class="info info1">
						<div class="t1">Easy TopUp</div>
						Kemudahan topup dana dengan beberapa metode topup
					</div>
					<div class="info info1">
						<div class="t1">Easy TopUp</div>
						Kemudahan topup dana dengan beberapa metode topup
					</div>
					<div class="info info1">
						<div class="t1">Easy TopUp</div>
						Kemudahan topup dana dengan beberapa metode topup
					</div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>

	</section>
	<section class="testimonial section">
		<div class="container">
			<div class="page_title"><h2>Customer Success</h2></div>
			<div class="area_testimoni">
				<img src="assets/img/arrow_back_black.png" alt="" class="testi_prev">
				<img src="assets/img/arrow_next_black.png" alt="" class="testi_next">
				
				<div id="slidetesti">
					<div class="card_testimoni">
						<div class="box_img ratio1_1">
							<div class="img_con lqd">
								<img src="assets/img/testimoni.jpg" alt="">
							</div>
						</div>
						<div class="text">
							Semenjak pakai DANA, proses pembayaran jadi lebih cepat, praktis dan menyenangkan
							<div class="nama">- Santi -</div>
						</div>
					</div>
					<div class="card_testimoni">
						<div class="box_img ratio1_1">
							<div class="img_con lqd">
								<img src="assets/img/testimoni.jpg" alt="">
							</div>
						</div>
						<div class="text">
							Semenjak pakai DANA, proses pembayaran jadi lebih cepat, praktis dan menyenangkan
							<div class="nama">- Santi -</div>
						</div>
					</div>
					<div class="card_testimoni">
						<div class="box_img ratio1_1">
							<div class="img_con lqd">
								<img src="assets/img/testimoni.jpg" alt="">
							</div>
						</div>
						<div class="text">
							Semenjak pakai DANA, proses pembayaran jadi lebih cepat, praktis dan menyenangkan
							<div class="nama">- Santi -</div>
						</div>
					</div>
					<div class="card_testimoni">
						<div class="box_img ratio1_1">
							<div class="img_con lqd">
								<img src="assets/img/testimoni.jpg" alt="">
							</div>
						</div>
						<div class="text">
							Semenjak pakai DANA, proses pembayaran jadi lebih cepat, praktis dan menyenangkan
							<div class="nama">- Santi -</div>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="paging_slide" id="testi_page"></div>
			</div>
		</div>
	</section>
	<section class="section promo">
		<div class="container">
			<div class="page_title"><h2>Promo</h2></div>
			<div class="area_promo">
				<img src="assets/img/arrow_back_black.png" alt="" class="promo_prev">
				<img src="assets/img/arrow_next_black.png" alt="" class="promo_next">
				<div id="slidepromo">
					<div class="card_promo">
						<div class="box_img ratio16_9">
							<div class="img_con lqd">
								<img src="assets/img/promo.png" alt="">
							</div>
						</div>
						<div class="label"><span>DANA</span></div>
						<div class="text">
							<a href="#">
								<h3>Gebyar Pulsa Awal Tahun</h3>
							</a>
						</div>
						<div class="date">Hingga 31 Januari 2019</div>
						<a href="#" class="btn_more">Lihat Lebih Lengkap</a>
						<div class="clearfix"></div>
					</div>
					<div class="card_promo">
						<div class="box_img ratio16_9">
							<div class="img_con lqd">
								<img src="assets/img/promo.png" alt="">
							</div>
						</div>
						<div class="label"><span>DANA</span></div>
						<div class="text">
							<a href="#">
								<h3>Gebyar Pulsa Awal Tahun</h3>
							</a>
						</div>
						<div class="date">Hingga 31 Januari 2019</div>
						<a href="#" class="btn_more">Lihat Lebih Lengkap</a>
						<div class="clearfix"></div>
					</div>
					<div class="card_promo">
						<div class="box_img ratio16_9">
							<div class="img_con lqd">
								<img src="assets/img/promo.png" alt="">
							</div>
						</div>
						<div class="label"><span>DANA</span></div>
						<div class="text">
							<a href="#">
								<h3>Gebyar Pulsa Awal Tahun</h3>
							</a>
						</div>
						<div class="date">Hingga 31 Januari 2019</div>
						<a href="#" class="btn_more">Lihat Lebih Lengkap</a>
						<div class="clearfix"></div>
					</div>
					<div class="card_promo">
						<div class="box_img ratio16_9">
							<div class="img_con lqd">
								<img src="assets/img/promo.png" alt="">
							</div>
						</div>
						<div class="label"><span>DANA</span></div>
						<div class="text">
							<a href="#">
								<h3>Gebyar Pulsa Awal Tahun</h3>
							</a>
						</div>
						<div class="date">Hingga 31 Januari 2019</div>
						<a href="#" class="btn_more">Lihat Lebih Lengkap</a>
						<div class="clearfix"></div>
					</div>
					<div class="card_promo">
						<div class="box_img ratio16_9">
							<div class="img_con lqd">
								<img src="assets/img/promo.png" alt="">
							</div>
						</div>
						<div class="label"><span>DANA</span></div>
						<div class="text">
							<a href="#">
								<h3>Gebyar Pulsa Awal Tahun</h3>
							</a>
						</div>
						<div class="date">Hingga 31 Januari 2019</div>
						<a href="#" class="btn_more">Lihat Lebih Lengkap</a>
						<div class="clearfix"></div>
					</div>
					<div class="card_promo">
						<div class="box_img ratio16_9">
							<div class="img_con lqd">
								<img src="assets/img/promo.png" alt="">
							</div>
						</div>
						<div class="label"><span>DANA</span></div>
						<div class="text">
							<a href="#">
								<h3>Gebyar Pulsa Awal Tahun</h3>
							</a>
						</div>
						<div class="date">Hingga 31 Januari 2019</div>
						<a href="#" class="btn_more">Lihat Lebih Lengkap</a>
						<div class="clearfix"></div>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="paging_slide" id="promo_page"></div>
				<div class="clearfix"></div>
				<div align="center"><a href="#" class="btn_more">Lihat lebih lengkap</a></div>
			</div>
		</div>
	</section>
	<section class="merchant section">
		<div class="container">
			<div class="page_title"><h2>Merchant</h2></div>
			<div class="nav2">
				<a href="#" class="selected">All</a>
				<a href="#">Online</a>
				<a href="#">Offline</a>
				<a href="#">Bank</a>
			</div>
			<div align="center">
				<a href="#" class="card_merchant">
					<div class="box_img ratio_merchant">
						<div class="img_con">
							<img src="assets/img/maybank-logo.png" alt="">
						</div>
					</div>
				</a>
				<a href="#" class="card_merchant">
					<div class="box_img ratio_merchant">
						<div class="img_con">
							<img src="assets/img/bni-logo.png" alt="">
						</div>
					</div>
				</a>
				<a href="#" class="card_merchant">
					<div class="box_img ratio_merchant">
						<div class="img_con">
							<img src="assets/img/bca-logo.png" alt="">
						</div>
					</div>
				</a>
				<a href="#" class="card_merchant">
					<div class="box_img ratio_merchant">
						<div class="img_con">
							<img src="assets/img/bbm-hero-logo-light.png" alt="">
						</div>
					</div>
				</a>
				<a href="#" class="card_merchant">
					<div class="box_img ratio_merchant">
						<div class="img_con">
							<img src="assets/img/AlfaMidi.png" alt="">
						</div>
					</div>
				</a>
				<a href="#" class="card_merchant">
					<div class="box_img ratio_merchant">
						<div class="img_con">
							<img src="assets/img/cinemaxxi.png" alt="">
						</div>
					</div>
				</a>
				<a href="#" class="card_merchant">
					<div class="box_img ratio_merchant">
						<div class="img_con">
							<img src="assets/img/bpjskesehatan.png" alt="">
						</div>
					</div>
				</a>
				<a href="#" class="card_merchant">
					<div class="box_img ratio_merchant">
						<div class="img_con">
							<img src="assets/img/Logo_IDS.png" alt="">
						</div>
					</div>
				</a>
				<a href="#" class="card_merchant">
					<div class="box_img ratio_merchant">
						<div class="img_con">
							<img src="assets/img/bbm-hero-logo-light.png" alt="">
						</div>
					</div>
				</a>
				<a href="#" class="card_merchant">
					<div class="box_img ratio_merchant">
						<div class="img_con">
							<img src="assets/img/bca-logo.png" alt="">
						</div>
					</div>
				</a>
				<a href="#" class="card_merchant">
					<div class="box_img ratio_merchant">
						<div class="img_con">
							<img src="assets/img/bbm-hero-logo-light.png" alt="">
						</div>
					</div>
				</a>
				<a href="#" class="card_merchant">
					<div class="box_img ratio_merchant">
						<div class="img_con">
							<img src="assets/img/AlfaMidi.png" alt="">
						</div>
					</div>
				</a>
				<a href="#" class="card_merchant">
					<div class="box_img ratio_merchant">
						<div class="img_con">
							<img src="assets/img/cinemaxxi.png" alt="">
						</div>
					</div>
				</a>
				<a href="#" class="card_merchant">
					<div class="box_img ratio_merchant">
						<div class="img_con">
							<img src="assets/img/bpjskesehatan.png" alt="">
						</div>
					</div>
				</a>
				<a href="#" class="card_merchant">
					<div class="box_img ratio_merchant">
						<div class="img_con">
							<img src="assets/img/Logo_IDS.png" alt="">
						</div>
					</div>
				</a>
				<a href="#" class="card_merchant">
					<div class="box_img ratio_merchant">
						<div class="img_con">
							<img src="assets/img/bbm-hero-logo-light.png" alt="">
						</div>
					</div>
				</a>
				<a href="#" class="card_merchant">
					<div class="box_img ratio_merchant">
						<div class="img_con">
							<img src="assets/img/AlfaMidi.png" alt="">
						</div>
					</div>
				</a>
				<a href="#" class="card_merchant">
					<div class="box_img ratio_merchant">
						<div class="img_con">
							<img src="assets/img/cinemaxxi.png" alt="">
						</div>
					</div>
				</a>
				<a href="#" class="card_merchant">
					<div class="box_img ratio_merchant">
						<div class="img_con">
							<img src="assets/img/bbm-hero-logo-light.png" alt="">
						</div>
					</div>
				</a>
				<a href="#" class="card_merchant">
					<div class="box_img ratio_merchant">
						<div class="img_con">
							+100 Merchant lainnya
						</div>
					</div>
				</a>
				<div class="clearfix"></div>
			</div>
			<br>
			<div align="center"><a href="#" class="btn_more">Gabung Sekarang</a></div>
		</div>
		<br><br><br><br>
	</section>

	
	<?php include "includes/footer.php";?>
	<?php include "includes/js.php";?>
</body>
</html>