$(document).ready(function() {
    //cover height
    var window_height = $( window ).height();
    $('.cover').css("height",window_height);

    //liquid
  $(".lqd").imgLiquid();

    if ($(window).width() < 768) {
      //menu mobile
      /*$("#menu_").click(function (){
        if($(this).hasClass('menu_close')){
          $('#nav').slideDown();
          $('.menu_nav').slideDown();
          $('#sign').slideDown();
          $('.menu_dashboard').show();
          $('#menu_').addClass('menu_close_style');
          $('#menu_').removeClass('menu_close');
        }
        else{
          $('#nav').slideUp();
          $('.menu_nav').hide();
          $('#sign').slideUp('fast');
          $('#menu_').removeClass('menu_close_style');
          $('#menu_').addClass('menu_close');
        }
      });*/
      /*$("#nav a").click(function (){
        $('#nav').slideUp();
        $('.menu_nav').hide();
        $('#sign').slideUp('fast');
        $('#menu_').removeClass('menu_close_style');
        $('#menu_').addClass('menu_close');
      });*/
      /*$(".sub").click(function (){
        $('.subproduk').slideDown();
      });*/

      
      /*$(".container").click(function (){
        $('.subproduk').hide();
      });*/
      $(".sub").click(function (){
        $('.subproduk').toggle();
        $(this).toggleClass('active');

        //alert();
      });

    }
    else {
      $(".sub").click(function (){
        $('.subproduk').toggle();
        $(this).toggleClass('active');
      });
      $(".subproduk").mouseleave(function (){
        $('.subproduk').hide();
        $('.sub').removeClass('active');
      });

    }

});

$("#slidetesti").carouFredSel({
    circular: true,
    infinite: true,
    auto  : 5000,
    responsive: true,
    items: 2,
    next: '.testi_next',
    prev: '.testi_prev',
    pagination:"#testi_page"
});
$("#slidepromo").carouFredSel({
    circular: true,
    infinite: true,
    auto  : 5000,
    responsive: true,
    items: 3,
    next: '.promo_next',
    prev: '.promo_prev',
    pagination:"#promo_page"
});
$("#slide_promobig").carouFredSel({
    circular: true,
    infinite: true,
    auto  : 5000,
    responsive: true,
    items: 1,
    next: '.slide_next',
    prev: '.slide_prev',
    pagination:"#slide_page",
    scroll : {
      fx : "crossfade"
    }
});
$("#slide1").carouFredSel({
    circular: true,
    infinite: true,
    auto  : 5000,
    responsive: true,
    items: 1,
    next: '.slide1_next',
    prev: '.slide1_prev',
    scroll : {
      fx : "crossfade"
    }
});


if ($(window).width() < 768) {
  $("#produk").carouFredSel({
      circular: true,
      infinite: true,
      auto  : false,
      responsive: true,
      items: 1,
      next: '.produk_next',
      prev: '.produk_prev'
  });
}
else {
  $("#produk").carouFredSel({
      circular: true,
      infinite: true,
      auto  : false,
      responsive: true,
      items: 3,
      next: '.produk_next',
      prev: '.produk_prev'
  });
}


/*S:MODALBOX*/
if( $('div.pop_box').attr('id') == 'pop_box_now'){
    $("body").css('overflow','hidden');
}
else {
  $("body").css('overflow','scroll');
}



$(window).load(function(){
  
  $(function() {
    $(".pop_container").wrapInner( "<div id='pop_wrap'></div>" );
    $('#pop_wrap').css('height',$(window).height());
    $('#pop_wrap').css('width',$('#pop_wrap').parent('.pop_container').width());
  });
  function rescale(){
    var size = {width: $(window).width() , height: $(window).height() }
    $('#pop_wrap').css('height', size.height );
  }
  $(window).bind("resize", rescale);
  $(".box_modal2").click(function (){
    var ig_img = $(this).attr('data-img');
    var ig_name = $(this).attr('data-name');
    var ig_desc = $(this).attr('data-desc');
    $("#pop_box2").show();
    $('.pop_ig .ig_img').html('<img src="'+ ig_img +'">');
    $('.pop_ig .ig_name').html(ig_name);
    $('.pop_ig .ig_desc').html(ig_desc);
    $("body").css('overflow','hidden');
  });
});

function closepop()
{ 
  $("#pop_box_now").remove();
  $("#pop_box2").hide();
  $("body").css('overflow','scroll');
};
$(".close_box").click(function (){
  closepop();
});
$(".close_box_in").click(function (){
  parent.closepop();
});
$(".pop_next").click(function (){
  var src = $(this).attr("alt");
  parent.pop_next(src);
});
/*S:MODALBOX*/

