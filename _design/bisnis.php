<html>
<?php include "includes/head.php";?>
<body>
	<?php include "includes/header.php";?>
	<section class="hero_cover">
		<div class="box_img ratio16_9">
			<div class="img_con">
				<img src="assets/img/img2.jpg" alt="">
			</div>
			<div class="text">
				<div class="container">
					<div class="text2">
						<h1>Gabung bersama DANA untuk kemudahan transaksi</h1>
						<div class="desc">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
						</div>
						<a href="#" class="btn_more">Gabung disini</a>
						
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</section>
	<section class="merchant section bg_abu">
		<div class="container">
			<div class="page_title"><h2>Merchant</h2></div>
			<div class="nav2">
				<a href="#" class="selected">All</a>
				<a href="#">Online</a>
				<a href="#">Offline</a>
				<a href="#">Bank</a>
			</div>
			<div align="center">
				<a href="merchant_detail.php" class="card_merchant">
					<div class="box_img ratio_merchant">
						<div class="img_con">
							<img src="assets/img/maybank-logo.png" alt="">
						</div>
					</div>
				</a>
				<a href="merchant_detail.php" class="card_merchant">
					<div class="box_img ratio_merchant">
						<div class="img_con">
							<img src="assets/img/bni-logo.png" alt="">
						</div>
					</div>
				</a>
				<a href="merchant_detail.php" class="card_merchant">
					<div class="box_img ratio_merchant">
						<div class="img_con">
							<img src="assets/img/bca-logo.png" alt="">
						</div>
					</div>
				</a>
				<a href="merchant_detail.php" class="card_merchant">
					<div class="box_img ratio_merchant">
						<div class="img_con">
							<img src="assets/img/bbm-hero-logo-light.png" alt="">
						</div>
					</div>
				</a>
				<a href="merchant_detail.php" class="card_merchant">
					<div class="box_img ratio_merchant">
						<div class="img_con">
							<img src="assets/img/AlfaMidi.png" alt="">
						</div>
					</div>
				</a>
				<a href="merchant_detail.php" class="card_merchant">
					<div class="box_img ratio_merchant">
						<div class="img_con">
							<img src="assets/img/cinemaxxi.png" alt="">
						</div>
					</div>
				</a>
				<a href="merchant_detail.php" class="card_merchant">
					<div class="box_img ratio_merchant">
						<div class="img_con">
							<img src="assets/img/bpjskesehatan.png" alt="">
						</div>
					</div>
				</a>
				<a href="merchant_detail.php" class="card_merchant">
					<div class="box_img ratio_merchant">
						<div class="img_con">
							<img src="assets/img/Logo_IDS.png" alt="">
						</div>
					</div>
				</a>
				<a href="merchant_detail.php" class="card_merchant">
					<div class="box_img ratio_merchant">
						<div class="img_con">
							<img src="assets/img/bbm-hero-logo-light.png" alt="">
						</div>
					</div>
				</a>
				<a href="merchant_detail.php" class="card_merchant">
					<div class="box_img ratio_merchant">
						<div class="img_con">
							<img src="assets/img/bca-logo.png" alt="">
						</div>
					</div>
				</a>
				<a href="merchant_detail.php" class="card_merchant">
					<div class="box_img ratio_merchant">
						<div class="img_con">
							<img src="assets/img/bbm-hero-logo-light.png" alt="">
						</div>
					</div>
				</a>
				<a href="merchant_detail.php" class="card_merchant">
					<div class="box_img ratio_merchant">
						<div class="img_con">
							<img src="assets/img/AlfaMidi.png" alt="">
						</div>
					</div>
				</a>
				<a href="merchant_detail.php" class="card_merchant">
					<div class="box_img ratio_merchant">
						<div class="img_con">
							<img src="assets/img/cinemaxxi.png" alt="">
						</div>
					</div>
				</a>
				<a href="merchant_detail.php" class="card_merchant">
					<div class="box_img ratio_merchant">
						<div class="img_con">
							<img src="assets/img/bpjskesehatan.png" alt="">
						</div>
					</div>
				</a>
				<a href="merchant_detail.php" class="card_merchant">
					<div class="box_img ratio_merchant">
						<div class="img_con">
							<img src="assets/img/Logo_IDS.png" alt="">
						</div>
					</div>
				</a>
				<a href="merchant_detail.php" class="card_merchant">
					<div class="box_img ratio_merchant">
						<div class="img_con">
							<img src="assets/img/bbm-hero-logo-light.png" alt="">
						</div>
					</div>
				</a>
				<a href="merchant_detail.php" class="card_merchant">
					<div class="box_img ratio_merchant">
						<div class="img_con">
							<img src="assets/img/AlfaMidi.png" alt="">
						</div>
					</div>
				</a>
				<a href="merchant_detail.php" class="card_merchant">
					<div class="box_img ratio_merchant">
						<div class="img_con">
							<img src="assets/img/cinemaxxi.png" alt="">
						</div>
					</div>
				</a>
				<a href="merchant_detail.php" class="card_merchant">
					<div class="box_img ratio_merchant">
						<div class="img_con">
							<img src="assets/img/bbm-hero-logo-light.png" alt="">
						</div>
					</div>
				</a>
				<a href="merchant_detail.php" class="card_merchant">
					<div class="box_img ratio_merchant">
						<div class="img_con">
							+100 Merchant lainnya
						</div>
					</div>
				</a>
				<div class="clearfix"></div>
			</div>
			<br>
			<div align="center"><a href="#" class="btn_more">Gabung Sekarang</a></div>
		</div>
		<br><br><br><br>
	</section>
	
	<section class="testimonial section">
		<div class="container">
			<div class="page_title"><h2>Customer Success</h2></div>
			<div class="area_testimoni">
				<img src="assets/img/arrow_back_black.png" alt="" class="testi_prev">
				<img src="assets/img/arrow_next_black.png" alt="" class="testi_next">
				
				<div id="slidetesti">
					<div class="card_testimoni">
						<div class="box_img ratio1_1">
							<div class="img_con lqd">
								<img src="assets/img/testimoni.jpg" alt="">
							</div>
						</div>
						<div class="text">
							Semenjak pakai DANA, proses pembayaran jadi lebih cepat, praktis dan menyenangkan
							<div class="nama">- Santi -</div>
						</div>
					</div>
					<div class="card_testimoni">
						<div class="box_img ratio1_1">
							<div class="img_con lqd">
								<img src="assets/img/testimoni.jpg" alt="">
							</div>
						</div>
						<div class="text">
							Semenjak pakai DANA, proses pembayaran jadi lebih cepat, praktis dan menyenangkan
							<div class="nama">- Santi -</div>
						</div>
					</div>
					<div class="card_testimoni">
						<div class="box_img ratio1_1">
							<div class="img_con lqd">
								<img src="assets/img/testimoni.jpg" alt="">
							</div>
						</div>
						<div class="text">
							Semenjak pakai DANA, proses pembayaran jadi lebih cepat, praktis dan menyenangkan
							<div class="nama">- Santi -</div>
						</div>
					</div>
					<div class="card_testimoni">
						<div class="box_img ratio1_1">
							<div class="img_con lqd">
								<img src="assets/img/testimoni.jpg" alt="">
							</div>
						</div>
						<div class="text">
							Semenjak pakai DANA, proses pembayaran jadi lebih cepat, praktis dan menyenangkan
							<div class="nama">- Santi -</div>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="paging_slide" id="testi_page"></div>
			</div>
		</div>
	</section>
	
	

	
	<?php include "includes/footer.php";?>
	<?php include "includes/js.php";?>
</body>
</html>