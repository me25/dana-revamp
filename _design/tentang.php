<html>
<?php include "includes/head.php";?>
<body>
	<?php
		$header = "";
		$footer = "reguler";
	?>
	<?php include "includes/header.php";?>
	<section class="hero_cover">
		<div class="box_img ratio4_1">
			<div class="img_con lqd">
				<img src="assets/img/img3.jpg" alt="">
			</div>
			<div class="title_cover"><h1>Tentang Dana</h1></div>
			<div class="clearfix"></div>
		</div>
	</section>
	<div class="detail_tentang">
		<div class="text">
			<p>Satu user dan satu device bisa mendapatkan voucher tagihan 50% 1x/ minggu selama periode promo
			Pembelian salah satu produk tagihan seperti Listrik, Air, Telepon, Asuransi, BPJS, Cicilan, Kabel TV, Telepon Pasca-bayar dengan minimal pembayaran Rp 22.000 untuk mendapatkan voucher 50% dengan maksimum potongan Rp 25.000
			Pemotongan voucher sebesar 50% berlaku pada transaksi ke-2
			Voucher berlaku hingga 14 hari setelah voucher dikeluarkan
			Voucher hanya berlaku untuk satu device dan satu user
			Tidak ada minimum total pembayaran untuk menggunakan voucher
			Voucher tidak dapat digabungkan dengan promo lainnya (transaksi menggunakan voucher)
			Voucher berlaku hanya untuk produk tagihan seperti Listrik, Air, Telepon, Asuransi, BPJS, Cicilan, Kabel TV, Telepon Pasca-bayar di platform DANA
			Voucher tidak berlaku untuk pembayaran menggunakan Virtual Account
			Pembayaran dapat dilakukan menggunakan saldo DANA/Debit Card/Credit Card
			Voucher akan otomatis memotong harga total produk yang akan dibeli.
			</p>
			<img src="assets/img/img3.jpg" alt="">
			<p>
			Jika transaksi gagal, maka voucher tidak dapat dikembalikan/diuangkan
			DANA berhak, tanpa pemberitahuan sebelumnya, melakukan tindakan-tindakan yang diperlukan apabila diduga adanya tindakan kecurangan atau penyalah gunaan dari pengguna.
			</p>
			<p>
			DANA berhak untuk meminta identitas KTP dan foto jika diperlukan untuk verifikasi.
			Syarat dan ketentuan dapat berubah sewaktu – waktu tanpa pemberitahuan sebelumnya.
			</p>
		</div>
	</div>

	
	<?php include "includes/footer.php";?>
	<?php include "includes/js.php";?>
</body>
</html>