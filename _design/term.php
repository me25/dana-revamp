<html>
<?php include "includes/head.php";?>
<body>
	<?php
		$header = "clean";
		$footer = "reguler";
	?>
	<?php include "includes/header.php";?>
	<section class="hero_cover blue_cover">
		<div class="box_img ratio4_1">
			<div class="text text3">
				<div class="container">
					<div class="title_cover"><h1>Terms and condition</h1></div>
					<div class="nav2">
						<a href="faq.php">FAQ</a>
						<a href="term.php" class="selected">Terms and Condition</a>
						<a href="contact.php">Contact Us</a>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</section>
	<div class="container">
		<div class="detail_statik">
			<h2>The Services</h2>
			<p>
				Don’t misuse our Services. For example, don’t interfere with our Services or try to access them using a method other than the interface and the instructions that we provide. You may use our Services only as permitted by law, including applicable export and re-export control laws and regulations. We may suspend or stop providing our Services to you if you do not comply with our terms or policies or if we are investigating suspected misconduct.</p>
				<p>
				Using our Services does not give you ownership of any intellectual property rights in our Services or the content you access. You may not use content from our Services unless you obtain permission from its owner or are otherwise permitted by law. These terms do not grant you the right to use any branding or logos used in our Services. Don’t remove, obscure, or alter any legal notices displayed in or along with our Services.
				</p>
				<p>
				Our Services display some content that is not Google’s. This content is the sole responsibility of the entity that makes it available. We may review content to determine whether it is illegal or violates our policies, and we may remove or refuse to display content that we reasonably believe violates our policies or the law. But that does not necessarily mean that we review content, so please don’t assume that we do.
				</p>
				<p>
				In connection with your use of the Services, we may send you service announcements, administrative messages, and other information. You may opt out of some of those communications.
				</p>
				<p>
				Some of our Services are available on mobile devices. Do not use such Services in a way that distracts you and prevents you from obeying traffic or safety laws.
			</p>
			<h2>Privacy and Copyright Protection</h2>
			<p>
				DANA merupakan bentuk baru pembayaran dalam genggaman Anda. Dengan menggunakan DANA, anda bisa melakukan berbagai macam transaksi elektronik melalui berbagai layanan yang tersedia, seperti Saldo DANA, Transfer Bank, Kartu Kredit, dan juga Setor Tunai ke minimarket. Saat ini DANA hanya tersedia di BBM, namun kami akan senantiasa melakukan pengembangan kedepan demi kenyamanan Anda bertransaksi menggunakan DANA.
			</p>
		</div>
		<div class="clearfix"></div>
	</div>
	
	

	
	<?php include "includes/footer.php";?>
	<?php include "includes/js.php";?>
</body>
</html>