<html>
<?php include "includes/head.php";?>
<body>
	<?php
		$header = "clean";
		$footer = "reguler";
	?>
	<?php include "includes/header.php";?>
	<section class="hero_cover blue_cover">
		<div class="box_img ratio4_1">
			<div class="text text3">
				<div class="container">
					<div class="title_cover"><h1>Contact Us</h1></div>
					<div class="nav2">
						<a href="faq.php">FAQ</a>
						<a href="term.php">Terms and Condition</a>
						<a href="contact.php" class="selected">Contact Us</a>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</section>
	<div class="container">
		<div class="detail_statik">
			<div class="map">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d126905.32008631337!2d106.69894616487746!3d-6.291144255398767!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f3e2295710bd%3A0xd698cfefe5185fa7!2sDANA+Indonesia!5e0!3m2!1sen!2sid!4v1551577414511" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>
			<div class="contact_left">
				<form action="#">
					Silakan isi formulir di bawah, kami akan segera menjawab pesan Anda.
					<div class="group-input">
						<span>Nama</span>
						<input type="text">
					</div>
					<div class="group-input">
						<span>Email</span>
						<input type="text">
					</div>
					<div class="group-input">
						<span>Pesan</span>
						<textarea name="" id="" cols="30" rows="10"></textarea>
					</div>
					<div class="group-input">
						<input type="submit" value="Kirim" class="btn_more">
					</div>
				</form>
			</div>
			<div class="contact_right">
				<h2>DANA Indonesia</h2>
				Capital Place fl.18, <br>
				Jl. Gatot Subroto, RT.6/RW.1, <br>
				Kuningan Bar., Mampang Prpt., <br>
				Kota Jakarta Selatan, <br>
				Daerah Khusus Ibukota Jakarta 12790

				<div class="cs">
					<span>Customer Support</span>
					1-500-445
				</div>
				Hubungi nomor customer support kami, atau email ke help@dana.id apabila anda membutuhkan bantuan.
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="clearfix"></div>
	</div>
	
	

	
	<?php include "includes/footer.php";?>
	<?php include "includes/js.php";?>
</body>
</html>