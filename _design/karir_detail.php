<html>
<?php include "includes/head.php";?>
<body>
	<?php
		$header = "";
		$footer = "reguler";
	?>
	<?php include "includes/header.php";?>
	<section class="hero_cover">
		<div class="box_img ratio4_1">
			<div class="img_con lqd">
				<img src="assets/img/karir.jpg" alt="">
			</div>
			<div class="text text3">
				<div class="container">
					<div class="title_cover"><h1>JAKARTA</h1></div>
					<div class="desc">
						<b>HEADQUARTER</b><br>
						Capital Place Lt.18<br>
						 Jln. Jend. Gatot Subroto Kav 18<br>
						 Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12710
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</section>
	
	<div class="detail_karir">
		<div class="nav2 fl">
			<a href="#">Semua</a>
			<a href="#" class="selected">Jakarta</a>
			<a href="#">Bali</a>
			<a href="#">Bandung</a>
		</div>
		<div class="filter fr">
			<span>Category</span>
			<div class="select-style-box fl">
				<div class="selected">For Intereship</div>
				<div class="option">
					<a href="#">Marketing</a>
					<a href="#">Designer</a>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		<form action="" class="quick_search">
			<input type="text" placeholder="Quick Search">
			<input type="submit" value="">
			<div class="clearfix"></div>
		</form>
		<div class="clearfix"></div>
		<div class="title2">
			<h2>Digital Marketing</h2>
		</div>
		<div class="list_loker2">
			<div class="card_loker2">
				<h3>People Performance Strategist </h3>
				<div class="lokasi">Jakarta Selatan</div>
				<a href="#" class="btn_more">Selengkapnya</a>
				<div class="clearfix"></div>
			</div>
			<div class="card_loker2">
				<h3>People Performance Strategist </h3>
				<div class="lokasi">Jakarta Selatan</div>
				<a href="#" class="btn_more">Selengkapnya</a>
				<div class="clearfix"></div>
			</div>
			<div class="card_loker2">
				<h3>People Performance Strategist </h3>
				<div class="lokasi">Jakarta Selatan</div>
				<a href="#" class="btn_more">Selengkapnya</a>
				<div class="clearfix"></div>
			</div>
			<div class="card_loker2">
				<h3>People Performance Strategist </h3>
				<div class="lokasi">Jakarta Selatan</div>
				<a href="#" class="btn_more">Selengkapnya</a>
				<div class="clearfix"></div>
			</div>
		</div>
		<div class="title2">
			<h2>Designer</h2>
		</div>
		<div class="list_loker2">
			<div class="card_loker2">
				<h3>People Performance Strategist </h3>
				<div class="lokasi">Jakarta Selatan</div>
				<a href="#" class="btn_more">Selengkapnya</a>
				<div class="clearfix"></div>
			</div>
			<div class="card_loker2">
				<h3>People Performance Strategist </h3>
				<div class="lokasi">Jakarta Selatan</div>
				<a href="#" class="btn_more">Selengkapnya</a>
				<div class="clearfix"></div>
			</div>
			<div class="card_loker2">
				<h3>People Performance Strategist </h3>
				<div class="lokasi">Jakarta Selatan</div>
				<a href="#" class="btn_more">Selengkapnya</a>
				<div class="clearfix"></div>
			</div>
			<div class="card_loker2">
				<h3>People Performance Strategist </h3>
				<div class="lokasi">Jakarta Selatan</div>
				<a href="#" class="btn_more">Selengkapnya</a>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	

	
	<?php include "includes/footer.php";?>
	<?php include "includes/js.php";?>
</body>
</html>