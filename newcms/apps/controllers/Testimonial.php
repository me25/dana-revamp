<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Testimonial extends CI_Controller {

	function __construct(){
		parent::__construct();		
		$this->load->model('Modglobal');


		if (!$this->session->userdata('logged_in')) {
        	redirect('/');
        }
 
	}
	public function index()
	{
		$page = "Testimonial";
		$user_id = $this->session->userdata('id');

		$query= $this->Modglobal->find('testimonial', array('tipe' => '1'));
		$result = $query->result_array();
		$num = $query->num_rows();

		$status = array (
			'0' => 'Aktif', 
			'1' => 'Tidak Aktif'
		);

		$data = array(
			'content' => 'testimonial/index',
			'result' => $result,
			'num' => $num,
			'status' => $status,
			'page' => $page,
		);
		$this->load->view('layouts/base', $data);
	}
	public function add()
	{
		$page = "Testimonial";
		$form = "add";
		$user_id = $this->session->userdata('id');

		$status = array (
			'0' => 'Aktif', 
			'1' => 'Tidak Aktif'
		);

		$data = array(
			'content' => 'testimonial/form',
			'status' => $status,
			'page' => $page,
			'form' => $form,
		);
		$this->load->view('layouts/base', $data);
	}
	public function add_pro()
	{
		$config = array();
	    $config['upload_path'] = '../uploads/testimonial';
	    $config['allowed_types'] = '*';
	    $config['max_size']      = '0';
	    $config['overwrite']     = FALSE;

	    $this->load->library('upload');

		if ($_FILES['img']['name'])
	    {           
	        $_FILES['userfile']['name']= str_replace_img($_FILES['img']['name']);
	        $_FILES['userfile']['type']= $_FILES['img']['type'];
	        $_FILES['userfile']['tmp_name']= $_FILES['img']['tmp_name'];
	        $_FILES['userfile']['error']= $_FILES['img']['error'];
	        $_FILES['userfile']['size']= $_FILES['img']['size'];   
	         
	        $this->upload->initialize($config);
	        $this->upload->do_upload();

	        $img = str_replace_img($_FILES['img']['name']);
	        //echo $img;
	    }
	    else {
	    	$img = $this->input->post('img2');
	    } 

		$data = array(
        	'tipe' => '1',
        	'status' => $this->input->post('status'),
        	'desc' => $this->input->post('desc'),
        	'img' => $img,
        	'nama' => str_replace_text($this->input->post('nama')),
        	'date_create' => date("Y/m/d - h:i:sa"),
        	'create_by' => $this->session->userdata('id'),
        );
        $this->Modglobal->insert('testimonial', $data);
		redirect('testimonial');
	}
	public function edit()
	{
		$page = "Testimonial";
		$form = "edit";

		$id = $this->uri->segment(3);

		$query = $this->Modglobal->find('testimonial', array('id' => $id));
		$page_detail = $query->row_array();

		$status = array (
			'0' => 'Aktif', 
			'1' => 'Tidak Aktif'
		);

		$data = array(
			'content' => 'testimonial/form',
			'page' => $page,
			'page_detail' => $page_detail,
			'id' => $id,
			'status' => $status,
			'form' => $form,
		);
		$this->load->view('layouts/base', $data);
	}
	
	public function update(){

		$config = array();
	    $config['upload_path'] = '../uploads/testimonial';
	    $config['allowed_types'] = '*';
	    $config['max_size']      = '0';
	    $config['overwrite']     = FALSE;

	    $this->load->library('upload');

		if ($_FILES['img']['name'])
	    {           
	        $_FILES['userfile']['name']= str_replace_img($_FILES['img']['name']);
	        $_FILES['userfile']['type']= $_FILES['img']['type'];
	        $_FILES['userfile']['tmp_name']= $_FILES['img']['tmp_name'];
	        $_FILES['userfile']['error']= $_FILES['img']['error'];
	        $_FILES['userfile']['size']= $_FILES['img']['size'];   
	         
	        $this->upload->initialize($config);
	        $this->upload->do_upload();

	        $img = str_replace_img($_FILES['img']['name']);
	        //echo $img;
	    }
	    else {
	    	$img = $this->input->post('img2');
	    } 
		
		$data = array(
			'status' => $this->input->post('status'),
        	'desc' => $this->input->post('desc'),
        	'img' => $img,
        	'nama' => str_replace_text($this->input->post('nama')),
        	'date_edit' => date("Y/m/d - h:i:sa"),
        	'edit_by' => $this->session->userdata('id'),
        );
        $where = array(
    		'id' => $this->input->post('id'),
        );
        $this->Modglobal->update('testimonial', $data, $where);
		redirect('testimonial');
	}
	public function delete() {
		$id = $this->uri->segment(3);
		$where = array(
        	'id' => $id,
        );
		$this->Modglobal->delete('testimonial', $where);
		//echo $id;

		redirect('testimonial');
	}

	public function bisnis()
	{
		$page = "Testimonial";
		$user_id = $this->session->userdata('id');

		$query= $this->Modglobal->find('testimonial', array('tipe' => '2'));
		$result = $query->result_array();
		$num = $query->num_rows();

		$status = array (
			'0' => 'Aktif', 
			'1' => 'Tidak Aktif'
		);

		$data = array(
			'content' => 'testimonial/index_bisnis',
			'result' => $result,
			'num' => $num,
			'status' => $status,
			'page' => $page,
		);
		$this->load->view('layouts/base', $data);
	}
	public function addbisnis()
	{
		$page = "Testimonial";
		$form = "add";
		$user_id = $this->session->userdata('id');

		$status = array (
			'0' => 'Aktif', 
			'1' => 'Tidak Aktif'
		);

		$data = array(
			'content' => 'testimonial/form_bisnis',
			'status' => $status,
			'page' => $page,
			'form' => $form,
		);
		$this->load->view('layouts/base', $data);
	}
	public function addbisnis_pro()
	{
		$config = array();
	    $config['upload_path'] = '../uploads/testimonial';
	    $config['allowed_types'] = '*';
	    $config['max_size']      = '0';
	    $config['overwrite']     = FALSE;

	    $this->load->library('upload');

		if ($_FILES['img']['name'])
	    {           
	        $_FILES['userfile']['name']= str_replace_img($_FILES['img']['name']);
	        $_FILES['userfile']['type']= $_FILES['img']['type'];
	        $_FILES['userfile']['tmp_name']= $_FILES['img']['tmp_name'];
	        $_FILES['userfile']['error']= $_FILES['img']['error'];
	        $_FILES['userfile']['size']= $_FILES['img']['size'];   
	         
	        $this->upload->initialize($config);
	        $this->upload->do_upload();

	        $img = str_replace_img($_FILES['img']['name']);
	        //echo $img;
	    }
	    else {
	    	$img = $this->input->post('img2');
	    } 

		$data = array(
        	'tipe' => '2',
        	'status' => $this->input->post('status'),
        	'desc' => $this->input->post('desc'),
        	'img' => $img,
        	'nama' => str_replace_text($this->input->post('nama')),
        	'jobtitle' => str_replace_text($this->input->post('jobtitle')),
        	'date_create' => date("Y/m/d - h:i:sa"),
        	'create_by' => $this->session->userdata('id'),
        );
        $this->Modglobal->insert('testimonial', $data);
		redirect('testimonial/bisnis');
	}

	public function editbisnis()
	{
		$page = "Testimonial";
		$form = "edit";

		$id = $this->uri->segment(3);

		$query = $this->Modglobal->find('testimonial', array('id' => $id));
		$page_detail = $query->row_array();

		$status = array (
			'0' => 'Aktif', 
			'1' => 'Tidak Aktif'
		);

		$data = array(
			'content' => 'testimonial/form_bisnis',
			'page' => $page,
			'page_detail' => $page_detail,
			'id' => $id,
			'status' => $status,
			'form' => $form,
		);
		$this->load->view('layouts/base', $data);
	}
	
	public function updatebisnis(){

		$config = array();
	    $config['upload_path'] = '../uploads/testimonial';
	    $config['allowed_types'] = '*';
	    $config['max_size']      = '0';
	    $config['overwrite']     = FALSE;

	    $this->load->library('upload');

		if ($_FILES['img']['name'])
	    {           
	        $_FILES['userfile']['name']= str_replace_img($_FILES['img']['name']);
	        $_FILES['userfile']['type']= $_FILES['img']['type'];
	        $_FILES['userfile']['tmp_name']= $_FILES['img']['tmp_name'];
	        $_FILES['userfile']['error']= $_FILES['img']['error'];
	        $_FILES['userfile']['size']= $_FILES['img']['size'];   
	         
	        $this->upload->initialize($config);
	        $this->upload->do_upload();

	        $img = str_replace_img($_FILES['img']['name']);
	        //echo $img;
	    }
	    else {
	    	$img = $this->input->post('img2');
	    } 
		
		$data = array(
			'status' => $this->input->post('status'),
        	'desc' => $this->input->post('desc'),
        	'img' => $img,
        	'nama' => str_replace_text($this->input->post('nama')),
        	'jobtitle' => str_replace_text($this->input->post('jobtitle')),
        	'date_edit' => date("Y/m/d - h:i:sa"),
        	'edit_by' => $this->session->userdata('id'),
        );
        $where = array(
    		'id' => $this->input->post('id'),
        );
        $this->Modglobal->update('testimonial', $data, $where);
		redirect('testimonial/bisnis');
	}
	public function deletebisnis() {
		$id = $this->uri->segment(3);
		$where = array(
        	'id' => $id,
        );
		$this->Modglobal->delete('testimonial', $where);
		//echo $id;

		redirect('testimonial/bisnis');
	}

	
	
	
}


