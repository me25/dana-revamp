<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bisnis extends CI_Controller {

	function __construct(){
		parent::__construct();		
		$this->load->model('Modglobal');


		if (!$this->session->userdata('logged_in')) {
        	redirect('/');
        }
 
	}
	public function index()
	{
		$page = "Bisnis";

		$id = $this->uri->segment(3);

		$page_query = $this->Modglobal->find('statik', array('nama' => 'bisnis_fitur'));
		$result = $page_query->result_array();

		$page_query2 = $this->Modglobal->find('statik', array('id' => '14'));
		$page_detail = $page_query2->row_array();

		$data = array(
			'content' => 'bisnis/form',
			'page' => $page,
			'result' => $result,
			'page_detail' => $page_detail,
			'id' => $id,
		);
		$this->load->view('layouts/base', $data);
	}
	
	
	public function update(){
		
		$findArr = array(" - "," ","  ", "[", "]","&","+","!");
		$replaceArr   = array("","-","-","", "","","","");
		if($_FILES["img"]['name']) {
			$img = 'bisnis-'.time().$_FILES["img"]['name'];
			$img = str_replace($findArr, $replaceArr, $img);
			$config['upload_path']   = '../uploads/web/'; 
	        $config['allowed_types'] = '*'; 
			$config['file_name'] = $img;
	        $this->load->library('upload', $config);
	        $this->upload->initialize($config);
	        $this->upload->do_upload('img');
	    }
	    else{
	    	$img = $this->input->post('img2');
	    }
		
		$data = array(
        	'nama' => str_replace_text($this->input->post('nama')),
        	'desc' => $this->input->post('desc'),
        	'url' => $this->input->post('url'),
        	'date_edit' => date("Y/m/d - h:i:sa"),
        	'edit_by' => $this->session->userdata('id'),
        	'img' => $img,
        );
        $where = array(
    		'id' => '14',
        );
        $this->Modglobal->update('statik', $data, $where);
		redirect('bisnis');
	}
	
	
}


