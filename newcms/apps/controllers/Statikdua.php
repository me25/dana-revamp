<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Statikdua extends CI_Controller {

	function __construct(){
		parent::__construct();		
		$this->load->model('Modglobal');


		if (!$this->session->userdata('logged_in')) {
        	redirect('/');
        }
 
	}
	public function index()
	{
		$page = "statikdua";
		$user_id = $this->session->userdata('id');

		$statik_query= $this->Modglobal->find('statik', array());
		$statik = $statik_query->result_array();
		$statik_num = $statik_query->num_rows();

		$data = array(
			'content' => 'statikdua/index',
			'statik' => $statik,
			'statik_num' => $statik_num,
			//'level' => $level,
			'page' => $page,
		);
		$this->load->view('layouts/base', $data);
	}
	
	
	public function add()
	{
		$page = "statikdua";
		$form = "add";
		$user_id = $this->session->userdata('id');

		$query2= $this->Modglobal->find('statik', array());
		$detail_page = $query2->result_array();

		$data = array(
			'content' => 'statikdua/form',
			'detail_page' => $detail_page,
			'form' => $form,
		);
		$this->load->view('layouts/base', $data);
	}
	public function add_pro()
	{
		

		$data = array(
        	'nama' => $this->input->post('nama')
        );
        $this->Modglobal->insert('statik', $data);
		redirect('statikdua');
	}
	public function edit()
	{
		$page = "statikdua";
		$form = "edit";

		$id = $this->uri->segment(3);

		$query = $this->Modglobal->find('statik', array('id' => $id));
		$detail_page = $query->row_array();

		$data = array(
			'content' => 'statikdua/form',
			'page' => $page,
			'detail_page' => $detail_page,
			'id' => $id,
			'form' => $form,
		);
		$this->load->view('layouts/base', $data);
	}
	
	public function update(){

		
		$data = array(
        	'nama' => $this->input->post('nama'),
        );
        $where = array(
    		'id' => $this->input->post('id'),
        );
        $this->Modglobal->update('statik', $data, $where);
		redirect('statikdua');
	}
	
	
}


