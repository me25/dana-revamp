<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Faq extends CI_Controller {

	function __construct(){
		parent::__construct();		
		$this->load->model('Modglobal');


		if (!$this->session->userdata('logged_in')) {
        	redirect('/');
        }
 
	}
	public function index()
	{
		$page = "Faq";
		$user_id = $this->session->userdata('id');

		$query= $this->Modglobal->find('faq', array());
		$result = $query->result_array();
		$num = $query->num_rows();

		$status = array (
			'0' => 'Aktif', 
			'1' => 'Tidak Aktif'
		);

		$data = array(
			'content' => 'faq/index',
			'result' => $result,
			'num' => $num,
			'status' => $status,
			'page' => $page,
		);
		$this->load->view('layouts/base', $data);
	}
	public function add()
	{
		$page = "Faq";
		$form = "add";
		$user_id = $this->session->userdata('id');

		$query= $this->Modglobal->find('faq_cate', array());
		$cate = $query->result_array();

		$status = array (
			'0' => 'Aktif', 
			'1' => 'Tidak Aktif'
		);

		$data = array(
			'content' => 'faq/form',
			'cate' => $cate,
			'status' => $status,
			'page' => $page,
			'form' => $form,
		);
		$this->load->view('layouts/base', $data);
	}
	public function add_pro()
	{
		$data = array(
        	'tanya' => $this->input->post('tanya'),
        	'jawab' => $this->input->post('jawab'),
        	'status' => $this->input->post('status'),
        	'cate_id' => $this->input->post('cate'),
        	'date_create' => date("Y/m/d - h:i:sa"),
        	'create_by' => $this->session->userdata('id'),
        );
        $this->Modglobal->insert('faq', $data);
		redirect('faq');
	}
	public function edit()
	{
		$page = "Faq";
		$form = "edit";

		$id = $this->uri->segment(3);

		$query = $this->Modglobal->find('faq', array('id' => $id));
		$page_detail = $query->row_array();

		$query= $this->Modglobal->find('faq_cate', array());
		$cate = $query->result_array();

		$status = array (
			'0' => 'Aktif', 
			'1' => 'Tidak Aktif'
		);

		$data = array(
			'content' => 'faq/form',
			'page' => $page,
			'page_detail' => $page_detail,
			'cate' => $cate,
			'id' => $id,
			'status' => $status,
			'form' => $form,
		);
		$this->load->view('layouts/base', $data);
	}
	
	public function update(){
		
		$data = array(
			'tanya' => $this->input->post('tanya'),
        	'jawab' => $this->input->post('jawab'),
        	'status' => $this->input->post('status'),
        	'cate_id' => $this->input->post('cate'),
        	'date_edit' => date("Y/m/d - h:i:sa"),
        	'edit_by' => $this->session->userdata('id'),
        );
        $where = array(
    		'id' => $this->input->post('id'),
        );
        $this->Modglobal->update('faq', $data, $where);
		redirect('faq');
	}
	public function delete() {
		$id = $this->uri->segment(3);
		$where = array(
        	'id' => $id,
        );
		$this->Modglobal->delete('faq', $where);
		//echo $id;

		redirect('faq');
	}

	public function kategori()
	{
		$page = "Faq";
		$user_id = $this->session->userdata('id');

		$query= $this->Modglobal->find('faq_cate', array());
		$result = $query->result_array();
		$num = $query->num_rows();

		/*$query2= $this->Modglobal->find('faq_cate', array());
		$cate = $query2->row_array();*/

		$status = array (
			'0' => 'Aktif', 
			'1' => 'Tidak Aktif'
		);

		$data = array(
			'content' => 'faq/index_cate',
			'result' => $result,
			'num' => $num,
			'status' => $status,
			'page' => $page,
		);
		$this->load->view('layouts/base', $data);
	}
	public function cate_add()
	{
		$page = "Faq";
		$form = "add";
		$user_id = $this->session->userdata('id');

		$status = array (
			'0' => 'Aktif', 
			'1' => 'Tidak Aktif'
		);

		$data = array(
			'content' => 'faq/form_cate',
			'status' => $status,
			'page' => $page,
			'form' => $form,
		);
		$this->load->view('layouts/base', $data);
	}
	public function cate_pro()
	{
		
		$data = array(
        	'nama' => $this->input->post('nama'),
        	'status' => $this->input->post('status'),
        	'date_create' => date("Y/m/d - h:i:sa"),
        	'create_by' => $this->session->userdata('id'),
        );
        $this->Modglobal->insert('faq_cate', $data);
		redirect('faq/kategori');
	}

	public function cate_edit()
	{
		$page = "Faq";
		$form = "edit";

		$id = $this->uri->segment(3);

		$query = $this->Modglobal->find('faq_cate', array('id' => $id));
		$page_detail = $query->row_array();

		$status = array (
			'0' => 'Aktif', 
			'1' => 'Tidak Aktif'
		);

		$data = array(
			'content' => 'faq/form_cate',
			'page' => $page,
			'page_detail' => $page_detail,
			'id' => $id,
			'status' => $status,
			'form' => $form,
		);
		$this->load->view('layouts/base', $data);
	}
	public function cate_update()
	{
		
		$data = array(
        	'nama' => $this->input->post('nama'),
        	'status' => $this->input->post('status'),
        	'date_create' => date("Y/m/d - h:i:sa"),
        	'create_by' => $this->session->userdata('id'),
        );
        $where = array(
    		'id' => $this->input->post('id'),
        );
        $this->Modglobal->update('faq_cate', $data, $where);
		redirect('faq/kategori');
	}
	public function cate_delete() {
		$id = $this->uri->segment(3);
		$where = array(
        	'id' => $id,
        );
		$this->Modglobal->delete('faq_cate', $where);
		//echo $id;

		redirect('faq/kategori');
	}

	
	
	
}


