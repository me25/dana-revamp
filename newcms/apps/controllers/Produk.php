<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produk extends CI_Controller {

	function __construct(){
		parent::__construct();		
		$this->load->model('Modglobal');


		if (!$this->session->userdata('logged_in')) {
        	redirect('/');
        }
 
	}
	public function index()
	{
		$page = "Produk";

		$id = $this->uri->segment(3);

		$page_detail_query = $this->Modglobal->find('statik', array('id' => '4'));
		$page_detail = $page_detail_query->row_array();

		$page_query = $this->Modglobal->find('statik', array('nama' => 'produk'),'','','3','0');
		$result = $page_query->result_array();

		$tipe = array (
			'0' => 'Image Only', 
			'Video Only',
			'Video with phone frame',
			'Image with phone frame'

		);

		$data = array(
			'content' => 'produk/form',
			'page' => $page,
			'result' => $result,
			'id' => $id,
			'tipe' => $tipe,
			'page_detail' => $page_detail,
		);
		$this->load->view('layouts/base', $data);
	}
	
	
	public function update(){
		
		$findArr = array(" - "," ","  ", "[", "]","&","+","!");
		$replaceArr   = array("","-","-","", "","","","");

		$findArr = array(" - "," ","  ", "[", "]","&","+","!");
		$replaceArr   = array("","-","-","", "","","","");
		if($_FILES["img_bg"]['name']) {
			$img_bg = 'produk_bg-'.time().$_FILES["img_bg"]['name'];
			$img_bg = str_replace($findArr, $replaceArr, $img_bg);
			$config['upload_path']   = '../uploads/web/'; 
	        $config['allowed_types'] = '*'; 
			$config['file_name'] = $img_bg;
	        $this->load->library('upload', $config);
	        $this->upload->initialize($config);
	        $this->upload->do_upload('img_bg');
	    }
	    else{
	    	$img_bg = $this->input->post('img_bg2');
	    }

	    $data = array(
        	'img3' => $img_bg,
        );
        $where = array(
    		'id' => '4',
        );
        $this->Modglobal->update('statik', $data, $where);
		

	    //$this->load->library('upload');

	    $config = array();
	    $config['upload_path'] = '../uploads/web';
	    $config['allowed_types'] = '*';
	    $config['max_size']      = '0';
	    $config['overwrite']     = FALSE;

	    $this->load->library('upload');

	    $files = $_FILES;
	    

	    for($i=0; $i< count($this->input->post('id')); $i++)
	    {          
	    	if ($_FILES['img']['name'][$i])
		    {           
		        $_FILES['userfile']['name']= str_replace_img($_FILES['img']['name'][$i]);
		        $_FILES['userfile']['type']= $_FILES['img']['type'][$i];
		        $_FILES['userfile']['tmp_name']= $_FILES['img']['tmp_name'][$i];
		        $_FILES['userfile']['error']= $_FILES['img']['error'][$i];
		        $_FILES['userfile']['size']= $_FILES['img']['size'][$i];   
		         
		        $this->upload->initialize($config);
		        $this->upload->do_upload();

		        $img = str_replace_img($_FILES['img']['name'][$i]);
		        //echo $img;
		    }
		    else {
		    	$img = $this->input->post('img2')[$i];
		    } 

		    if ($_FILES['img_second']['name'][$i])
		    {           
		        $_FILES['userfile']['name']= str_replace_img($_FILES['img_second']['name'][$i]);
		        $_FILES['userfile']['type']= $_FILES['img_second']['type'][$i];
		        $_FILES['userfile']['tmp_name']= $_FILES['img_second']['tmp_name'][$i];
		        $_FILES['userfile']['error']= $_FILES['img_second']['error'][$i];
		        $_FILES['userfile']['size']= $_FILES['img_second']['size'][$i];   
		         
		        $this->upload->initialize($config);
		        $this->upload->do_upload();

		        $img_second = str_replace_img($_FILES['img_second']['name'][$i]);
		        //echo $img;
		    }
		    else {
		    	$img_second = $this->input->post('img_second2')[$i];
		    } 


	        
	        $data = array(
	        	'nama1' => $this->input->post('nama')[$i],
	        	'nama2' => $this->input->post('nama2')[$i],
	        	'tipe' => $this->input->post('tipe')[$i],
	        	'img' => $img,
	        	'img2' => $img_second,
	        	'desc' => $this->input->post('desc')[$i],
	        	'date_edit' => date("Y/m/d - h:i:sa"),
	        	'edit_by' => $this->session->userdata('id'),
	        );
	        $where = array(
	    		'id' => $this->input->post('id')[$i],
	        );
	        $this->Modglobal->update('statik', $data, $where);
	    }
		
        redirect('produk');
	}
	
	
}


