<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gabung extends CI_Controller {

	function __construct(){
		parent::__construct();		
		$this->load->model('Modglobal');


		if (!$this->session->userdata('logged_in')) {
        	redirect('/');
        }
 
	}
	public function index()
	{
		$page = "Gabung";

		$page_query = $this->Modglobal->find('bisnis_gabung', array());
		$gabung = $page_query->result_array();
		$num = $page_query->num_rows();

		$status = array (
			'0' => 'Belum direspon', 
			'1' => 'Sudah direspon'
		);

		$data = array(
			'content' => 'gabung/index',
			'page' => $page,
			'gabung' => $gabung,
			'num' => $num,
			'status' => $status,
		);
		$this->load->view('layouts/base', $data);
	}
	
	
	public function page(){
		$page = "Gabung";

		$page_query = $this->Modglobal->find('statik', array('id' => '24'));
		$page_detail = $page_query->row_array();

		$data = array(
			'content' => 'gabung/form',
			'page' => $page,
			'page_detail' => $page_detail,
		);
		$this->load->view('layouts/base', $data);
	}
	public function update(){
		$findArr = array(" - "," ","  ", "[", "]","&","+","!");
		$replaceArr   = array("","-","-","", "","","","");
		if($_FILES["img"]['name']) {
			$img = 'join-'.time().$_FILES["img"]['name'];
			$img = str_replace($findArr, $replaceArr, $img);
			$config['upload_path']   = '../uploads/web/'; 
	        $config['allowed_types'] = '*'; 
			$config['file_name'] = $img;
	        $this->load->library('upload', $config);
	        $this->upload->initialize($config);
	        $this->upload->do_upload('img');
	    }
	    else{
	    	$img = $this->input->post('img2');
	    }

		$data = array(
        	'nama' => str_replace_text($this->input->post('nama')),
        	'desc' => $this->input->post('desc'),
        	'nama1' => str_replace_text($this->input->post('nama1')),
        	'teaser' => str_replace_text($this->input->post('teaser')),
        	'nama2' => str_replace_text($this->input->post('nama2')),
        	'map' => str_replace_text($this->input->post('map')),
        	'img' => $img,
        	'date_edit' => date("Y/m/d - h:i:sa"),
        	'edit_by' => $this->session->userdata('id'),
        );
        $where = array(
    		'id' => '24',
        );
        $this->Modglobal->update('statik', $data, $where);
		redirect('gabung/page');
	}
	public function respon(){

		$id = $this->uri->segment(3);
		$data = array(
        	'respon' => 1,
        	'date_edit' => date("Y/m/d - h:i:sa"),
        	'edit_by' => $this->session->userdata('id'),
        );
        $where = array(
    		'id' => $id,
        );
        $this->Modglobal->update('bisnis_gabung', $data, $where);
		redirect('gabung');
	}
	
	
}


