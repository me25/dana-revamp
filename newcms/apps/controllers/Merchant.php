<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Merchant extends CI_Controller {

	function __construct(){
		parent::__construct();		
		$this->load->model('Modglobal');


		if (!$this->session->userdata('logged_in')) {
        	redirect('/');
        }
 
	}
	public function index()
	{
		$page = "Merchant";
		$user_id = $this->session->userdata('id');

		$query= $this->Modglobal->find('merchant', array());
		$result = $query->result_array();
		$num = $query->num_rows();

		$status = array (
			'0' => 'Aktif', 
			'1' => 'Tidak Aktif'
		);

		$data = array(
			'content' => 'merchant/index',
			'result' => $result,
			'num' => $num,
			'status' => $status,
			'page' => $page,
		);
		$this->load->view('layouts/base', $data);
	}
	public function detail()
	{
		$page = "Merchant";
		$id = $this->uri->segment(3);
		$namaalbum = $this->uri->segment(5);

		$page_query = $this->Modglobal->find('merchant', array('id' => $id));
		$page_detail = $page_query->row_array();

		$list_query= $this->Modglobal->find('merchant_list', array('merchant_id' => $id));
		$list = $list_query->result_array();
		$num = $list_query->num_rows();

		$kota_query= $this->Modglobal->find('kota', array());
		$kota = $kota_query->result_array();


		$status = array (
			'0' => 'Aktif', 
			'1' => 'Tidak Aktif'
		);

		$data = array(
			'content' => 'merchant/index_store',
			'page' => $page,
			'page_detail' => $page_detail,
			'list' => $list,
			'id' => $id,
			'num' => $num,
			'status' => $status,
			'kota' => $kota,
		);
		$this->load->view('layouts/base', $data);
	}
	public function add()
	{
		$page = "Merchant";
		$form = "add";
		$user_id = $this->session->userdata('id');

		$page_query= $this->Modglobal->find('merchant', array());
		$page_detail = $page_query->result_array();

		$cate_query= $this->Modglobal->find('merchant_cate', array('status' => '0'));
		$cate = $cate_query->result_array();

		$status = array (
			'0' => 'Aktif', 
			'1' => 'Tidak Aktif'
		);

		$imgzoom = array (
			'0' => 'Normal', 
			'1' => 'Perbesar'
		);

		$data = array(
			'content' => 'merchant/form',
			'page_detail' => $page_detail,
			'status' => $status,
			'page' => $page,
			'form' => $form,
			'cate' => $cate,
			'imgzoom' => $imgzoom,
		);
		$this->load->view('layouts/base', $data);
	}
	public function add_pro()
	{
		$findArr = array(" - "," ","  ", "[", "]","&","+","!");
		$replaceArr   = array("","-","-","", "","","","");
		if($_FILES["img"]['name']) {
			$img = 'merchant-'.time().$_FILES["img"]['name'];
			$img = str_replace($findArr, $replaceArr, $img);
			$config['upload_path']   = '../uploads/merchant/'; 
	        $config['allowed_types'] = '*'; 
			$config['file_name'] = $img;
	        $this->load->library('upload', $config);
	        $this->upload->initialize($config);
	        $this->upload->do_upload('img');
	    }
	    else{
	    	$img = $this->input->post('img2');
	    }

		$data = array(
        	'nama' => $this->input->post('nama'),
        	'status' => $this->input->post('status'),
        	'imgzoom' => $this->input->post('imgzoom'),
        	'cate' => $this->input->post('cate'),
        	'url' => $this->input->post('url'),
        	'img' => $img,
        	'date_create' => date("Y/m/d - h:i:sa"),
        	'create_by' => $this->session->userdata('id'),
        );
        $this->Modglobal->insert('merchant', $data);
		redirect('merchant');
	}
	public function edit()
	{
		$page = "Merchant";
		$form = "edit";

		$id = $this->uri->segment(3);

		$query = $this->Modglobal->find('merchant', array('id' => $id));
		$page_detail = $query->row_array();

		$cate_query= $this->Modglobal->find('merchant_cate', array('status' => '0'));
		$cate = $cate_query->result_array();

		$status = array (
			'0' => 'Aktif', 
			'1' => 'Tidak Aktif'
		);
		$imgzoom = array (
			'0' => 'Normal', 
			'1' => 'Perbesar'
		);

		$data = array(
			'content' => 'merchant/form',
			'page' => $page,
			'page_detail' => $page_detail,
			'id' => $id,
			'status' => $status,
			'form' => $form,
			'cate' => $cate,
			'imgzoom' => $imgzoom,
		);
		$this->load->view('layouts/base', $data);
	}
	
	public function update(){
		$findArr = array(" - "," ","  ", "[", "]","&","+","!");
		$replaceArr   = array("","-","-","", "","","","");
		if($_FILES["img"]['name']) {
			$img = 'merchant-'.time().$_FILES["img"]['name'];
			$img = str_replace($findArr, $replaceArr, $img);
			$config['upload_path']   = '../uploads/merchant/'; 
	        $config['allowed_types'] = '*'; 
			$config['file_name'] = $img;
	        $this->load->library('upload', $config);
	        $this->upload->initialize($config);
	        $this->upload->do_upload('img');
	    }
	    else{
	    	$img = $this->input->post('img2');
	    }
	    
		$data = array(
			'nama' => $this->input->post('nama'),
        	'status' => $this->input->post('status'),
        	'imgzoom' => $this->input->post('imgzoom'),
        	'cate' => $this->input->post('cate'),
        	'url' => $this->input->post('url'),
        	'img' => $img,
        	'date_edit' => date("Y/m/d - h:i:sa"),
        	'edit_by' => $this->session->userdata('id'),
        );
        $where = array(
    		'id' => $this->input->post('id'),
        );
        $this->Modglobal->update('merchant', $data, $where);
		redirect('merchant');
	}
	public function delete() {
		$id = $this->uri->segment(3);
		$where = array(
        	'id' => $id,
        );
		$this->Modglobal->delete('merchant', $where);
		//echo $id;

		redirect('merchant');
	}

	public function store_add()
	{
		$page = "Merchant";
		$form = "add";
		$id = $this->uri->segment(3);

		$page_query= $this->Modglobal->find('merchant', array('id' => $id));
		$page_detail = $page_query->row_array();

		$kota_query= $this->Modglobal->find('kota', array(),'nama');
		$kota = $kota_query->result_array();

		$status = array (
			'0' => 'Aktif', 
			'1' => 'Tidak Aktif'
		);

		$data = array(
			'content' => 'merchant/form_store',
			'page_detail' => $page_detail,
			'status' => $status,
			'page' => $page,
			'form' => $form,
			'id' => $id,
			'kota' => $kota,
		);
		$this->load->view('layouts/base', $data);
	}
	public function store_pro()
	{

		$kota = explode('|', $this->input->post('kota'));
		$data = array(
        	'alamat' => nl2br($this->input->post('alamat')),
        	'status' => $this->input->post('status'),
        	'kota_id' => $kota[0],
        	'kota_nama' => $kota[1],
        	'merchant_id' => $this->input->post('merchant_id'),
        	'date_create' => date("Y/m/d - h:i:sa"),
        	'create_by' => $this->session->userdata('id'),
        );
        $this->Modglobal->insert('merchant_list', $data);
		redirect('merchant/detail/'.$this->input->post('merchant_id'));
	}
	public function store_edit()
	{
		$page = "Merchant";
		$form = "edit";
		$user_id = $this->session->userdata('id');
		$id = $this->uri->segment(3);

		$page_query= $this->Modglobal->find('merchant_list', array('id' => $id));
		$page_detail = $page_query->row_array();

		$kota_query= $this->Modglobal->find('kota', array(),'nama');
		$kota = $kota_query->result_array();


		$status = array (
			'0' => 'Aktif', 
			'1' => 'Tidak Aktif'
		);

		$data = array(
			'content' => 'merchant/form_store',
			'page_detail' => $page_detail,
			'status' => $status,
			'page' => $page,
			'form' => $form,
			'kota' => $kota,
		);
		$this->load->view('layouts/base', $data);
	}
	public function store_update()
	{
		$kota = explode('|', $this->input->post('kota'));
		
		$data = array(
			'alamat' => nl2br($this->input->post('alamat')),
        	'status' => $this->input->post('status'),
        	'kota_id' => $kota[0],
        	'kota_nama' => $kota[1],
        	'date_edit' => date("Y/m/d - h:i:sa"),
        	'edit_by' => $this->session->userdata('id'),

        );
        $where = array(
    		'id' => $this->input->post('id'),
        );
        $this->Modglobal->update('merchant_list', $data, $where);
		redirect('merchant/detail/'.$this->input->post('merchant_id'));
	}
	public function store_delete() {
		$id = $this->uri->segment(3);
		$merchant_id = $this->uri->segment(4);
		$where = array(
        	'id' => $id,
        );
		$this->Modglobal->delete('merchant_list', $where);
		//echo $id;

		redirect('merchant/detail/'.$merchant_id);
	}
	public function cate()
	{
		$page = "Merchant";
		$user_id = $this->session->userdata('id');

		$query= $this->Modglobal->find('merchant_cate', array());
		$result = $query->result_array();
		$num = $query->num_rows();

		$status = array (
			'0' => 'Aktif', 
			'1' => 'Tidak Aktif'
		);

		$data = array(
			'content' => 'merchant/index_cate',
			'result' => $result,
			'num' => $num,
			'status' => $status,
			'page' => $page,
		);
		$this->load->view('layouts/base', $data);
	}
	public function cateadd()
	{
		$page = "Merchant";
		$form = "add";

		$status = array (
			'0' => 'Aktif', 
			'1' => 'Tidak Aktif'
		);

		$data = array(
			'content' => 'merchant/form_cate',
			'status' => $status,
			'page' => $page,
			'form' => $form,
		);
		$this->load->view('layouts/base', $data);
	}
	public function cateadd_pro()
	{
		$data = array(
        	'nama' => str_replace_text($this->input->post('nama')),
        	'status' => $this->input->post('status'),
        	'date_create' => date("Y/m/d - h:i:sa"),
        	'create_by' => $this->session->userdata('id'),
        );
        $this->Modglobal->insert('merchant_cate', $data);
		redirect('merchant/cate');
	}
	public function cateedit()
	{
		$page = "Merchant";
		$form = "edit";
		$user_id = $this->session->userdata('id');

		$id = $this->uri->segment(3);


		$page_query= $this->Modglobal->find('merchant_cate', array('id' => $id));
		$page_detail = $page_query->row_array();

		$status = array (
			'0' => 'Aktif', 
			'1' => 'Tidak Aktif'
		);

		$data = array(
			'content' => 'merchant/form_cate',
			'page_detail' => $page_detail,
			'status' => $status,
			'page' => $page,
			'form' => $form,
		);
		$this->load->view('layouts/base', $data);
	}
	public function cate_update()
	{
		
		$data = array(
			'nama' => str_replace_text($this->input->post('nama')),
        	'status' => $this->input->post('status'),
        	'date_edit' => date("Y/m/d - h:i:sa"),
        	'edit_by' => $this->session->userdata('id'),

        );
        $where = array(
    		'id' => $this->input->post('id'),
        );
        $this->Modglobal->update('merchant_cate', $data, $where);
		redirect('merchant/cate');
	}
	public function catedelete() {
		$id = $this->uri->segment(3);
		$merchant_id = $this->uri->segment(4);
		$where = array(
        	'id' => $id,
        );
		$this->Modglobal->delete('merchant_cate', $where);
		//echo $id;

		redirect('merchant/cate');
	}
	public function posisi()
	{
		$page = "Merchant";
		$form = "edit";

		$id = $this->uri->segment(3);

		$query = $this->Modglobal->find('merchant', array('status' => '0'),'nama');
		$merchant = $query->result_array();

		$status = array (
			'0' => 'Aktif', 
			'1' => 'Tidak Aktif'
		);

		$data = array(
			'content' => 'merchant/form_posisi',
			'page' => $page,
			'merchant' => $merchant,
			'id' => $id,
			'status' => $status,
			'form' => $form,
		);
		$this->load->view('layouts/base', $data);
	}
	public function posisi_update(){
		
		$data = array(
        	'posisi' =>'99999',
        );
        $where = array(
    		//'status' => '0',
        );
        $this->Modglobal->update('merchant', $data, $where);

        for($i=0; $i< count($this->input->post('id_posisi')); $i++)
	    {          
	        $data = array(
	        	'posisi' =>str_replace_text($this->input->post('id_posisi')[$i]),
	        );
	        $where = array(
	    		'id' => $this->input->post('merchant_id')[$i],
	        );
	        $this->Modglobal->update('merchant', $data, $where);
	    }

		redirect('merchant/posisi');
	}

	public function posisi_cate()
	{
		$page = "Merchant";
		$form = "edit";

		$id = $this->uri->segment(3);

		$query = $this->Modglobal->find('merchant_cate', array('status' => '0'),'nama');
		$merchant_cate = $query->result_array();
		$num = $query->num_rows();

		$status = array (
			'0' => 'Aktif', 
			'1' => 'Tidak Aktif'
		);

		$data = array(
			'content' => 'merchant/form_posisi_cate',
			'page' => $page,
			'merchant_cate' => $merchant_cate,
			'id' => $id,
			'status' => $status,
			'form' => $form,
			'num' => $num,
		);
		$this->load->view('layouts/base', $data);
	}
	public function posisi_cate_update(){
		
		$data = array(
        	'posisi' =>'99999',
        );
        $where = array(
    		//'status' => '0',
        );
        $this->Modglobal->update('merchant_cate', $data, $where);

        for($i=0; $i< count($this->input->post('id_posisi')); $i++)
	    {          
	        $data = array(
	        	'posisi' =>str_replace_text($this->input->post('id_posisi')[$i]),
	        );
	        $where = array(
	    		'id' => $this->input->post('merchant_id')[$i],
	        );
	        $this->Modglobal->update('merchant_cate', $data, $where);
	    }

		redirect('merchant/posisi_cate');
	}
	
	
}


