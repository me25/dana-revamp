<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hubungi extends CI_Controller {

	function __construct(){
		parent::__construct();		
		$this->load->model('Modglobal');


		if (!$this->session->userdata('logged_in')) {
        	redirect('/');
        }
 
	}
	public function index()
	{
		$page = "Hubungi";

		$page_query = $this->Modglobal->find('hubungi', array());
		$hubungi = $page_query->result_array();
		$num = $page_query->num_rows();
		
		$data = array(
			'content' => 'hubungi/index',
			'page' => $page,
			'hubungi' => $hubungi,
			'num' => $num,
		);
		$this->load->view('layouts/base', $data);
	}
	
	
}


