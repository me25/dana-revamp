<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting extends CI_Controller {

	function __construct(){
		parent::__construct();		
		$this->load->model('Modglobal');


		if (!$this->session->userdata('logged_in')) {
        	redirect('/');
        }
 
	}
	public function index()
	{
		$page = "Setting";

		$id = $this->uri->segment(3);

		$page_query = $this->Modglobal->find('setting', array('id' => '1'));
		$page_detail = $page_query->row_array();

		$data = array(
			'content' => 'setting/form',
			'page' => $page,
			'page_detail' => $page_detail,
			'id' => $id,
		);
		$this->load->view('layouts/base', $data);
	}
	
	public function update(){

		$data = array(
        	'playstore' => $this->input->post('playstore'),
        	'fb' => $this->input->post('fb'),
        	'ig' => $this->input->post('ig'),
        	'yt' => $this->input->post('yt'),
        	'tw' => $this->input->post('tw'),
        	'appstore' => $this->input->post('appstore'),
        	'email' => $this->input->post('email'),
        	'date_edit' => date("Y/m/d - h:i:sa"),
        	'edit_by' => $this->session->userdata('id'),
        );
        $where = array(
    		'id' => '1',
        );
        $this->Modglobal->update('setting', $data, $where);
		redirect('setting');
	}

	public function kota()
	{
		$page = "Setting";


		$id = $this->uri->segment(3);

		$page_query = $this->Modglobal->find('kota', array(),'nama');
		$kota = $page_query->result_array();

		

		$data = array(
			'content' => 'setting/kota',
			'page' => $page,
			'kota' => $kota,
			'id' => $id,
		);
		$this->load->view('layouts/base', $data);
	}
	public function kota_add()
	{
		$page = "Setting";
		$form = "add";


		$data = array(
			'content' => 'setting/form_kota',
			'page' => $page,
			'form' => $form,
		);
		$this->load->view('layouts/base', $data);
	}
	public function kota_pro()
	{
		$data = array(
        	'nama' => str_replace_text($this->input->post('nama')),
        );
        $this->Modglobal->insert('kota', $data);
		redirect('setting/kota');
	}
	public function kota_edit()
	{
		$page = "Setting";
		$form = "edit";

		$id = $this->uri->segment(3);

		$page_query = $this->Modglobal->find('kota', array('id' => $id));
		$page_detail = $page_query->row_array();


		$data = array(
			'content' => 'setting/form_kota',
			'page' => $page,
			'page_detail' => $page_detail,
			'form' => $form,
		);
		$this->load->view('layouts/base', $data);
	}
	public function kota_update()
	{
		$data = array(
        	'nama' => str_replace_text($this->input->post('nama')),
        );
        $where = array(
    		'id' => $this->input->post('id'),
        );
        $this->Modglobal->update('kota', $data, $where);
		redirect('setting/kota');
	}
	public function kota_delete() {
		$id = $this->uri->segment(3);
		$where = array(
        	'id' => $id,
        );
		$this->Modglobal->delete('kota', $where);
		//echo $id;

		redirect('setting/kota');
	}
	
	
}


