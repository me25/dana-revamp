<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Homeeditor extends CI_Controller {

	function __construct(){
		parent::__construct();		
		$this->load->model('Modglobal');


		if (!$this->session->userdata('logged_in')) {
        	redirect('/');
        }
 
	}
	public function index()
	{
		$page = "homeeditor";

		$page_query = $this->Modglobal->find('statik', array('id' => '15'));
		$page_detail = $page_query->row_array();

		$home_transaksi_query= $this->Modglobal->find('statik', array('nama' => 'home_transaksi'));
		$home_transaksi = $home_transaksi_query->result_array();

		$home_fitur_query= $this->Modglobal->find('statik', array('nama' => 'home_fitur'));
		$home_fitur = $home_fitur_query->result_array();

		$tipe = array (
			'0' => 'Image', 
			'1' => 'Video'
		);

		$data = array(
			'content' => 'homeeditor/form',
			'page' => $page,
			'page_detail' => $page_detail,
			'tipe' => $tipe,
			'home_fitur' => $home_fitur,
			'home_transaksi' => $home_transaksi,
		);
		$this->load->view('layouts/base', $data);
	}
	
	public function update(){
		$findArr = array(" - "," ","  ", "[", "]","&","+","!");
		$replaceArr   = array("","-","-","", "","","","");
		if($_FILES["img"]['name']) {
			$img = 'homecover-'.time().$_FILES["img"]['name'];
			$img = str_replace($findArr, $replaceArr, $img);
			$config['upload_path']   = '../uploads/web/'; 
	        $config['allowed_types'] = '*'; 
			$config['file_name'] = $img;
	        $this->load->library('upload', $config);
	        $this->upload->initialize($config);
	        $this->upload->do_upload('img');
	    }
	    else{
	    	$img = $this->input->post('img2');
	    }

	    $findArr = array(" - "," ","  ", "[", "]","&","+","!");
		$replaceArr   = array("","-","-","", "","","","");
		if($_FILES["img_phone"]['name']) {
			$img_phone = 'homephone-'.time().$_FILES["img_phone"]['name'];
			$img_phone = str_replace($findArr, $replaceArr, $img_phone);
			$config['upload_path']   = '../uploads/web/'; 
	        $config['allowed_types'] = '*'; 
			$config['file_name'] = $img_phone;
	        $this->load->library('upload', $config);
	        $this->upload->initialize($config);
	        $this->upload->do_upload('img_phone');
	    }
	    else{
	    	$img_phone = $this->input->post('img_phone2');
	    }

		$data = array(
        	'nama' => str_replace_text($this->input->post('nama')),
        	'nama1' => str_replace_text($this->input->post('nama1')),
        	'tipe' => $this->input->post('tipe'),
        	'tipe2' => $this->input->post('tipe2'),
        	'url' => $this->input->post('url'),
        	'img' => $img,
        	'img2' => $img_phone,
        	'date_edit' => date("Y/m/d - h:i:sa"),
        	'edit_by' => $this->session->userdata('id'),
        );
        $where = array(
    		'id' => '15',
        );
        $this->Modglobal->update('statik', $data, $where);


        $config = array();
	    $config['upload_path'] = '../uploads/web/';
	    $config['allowed_types'] = '*';
	    $config['max_size']      = '0';
	    $config['overwrite']     = FALSE;

	    $this->load->library('upload');

	    $files = $_FILES;

        for($i=0; $i< count($this->input->post('id_transaksi')); $i++)
	    {          
	    	if ($_FILES['img_transaksi']['name'][$i])
		    {           
		        $_FILES['userfile']['name']= str_replace_img($_FILES['img_transaksi']['name'][$i]);
		        $_FILES['userfile']['type']= $_FILES['img_transaksi']['type'][$i];
		        $_FILES['userfile']['tmp_name']= $_FILES['img_transaksi']['tmp_name'][$i];
		        $_FILES['userfile']['error']= $_FILES['img_transaksi']['error'][$i];
		        $_FILES['userfile']['size']= $_FILES['img_transaksi']['size'][$i];   
		         
		        $this->upload->initialize($config);
		        $this->upload->do_upload();

		        $img_transaksi = str_replace_img($_FILES['img_transaksi']['name'][$i]);
		        //echo $img;
		    }
		    else {
		    	$img_transaksi = $this->input->post('img_transaksi2')[$i];
		    } 

	        
	        $data = array(
	        	'nama1' =>str_replace_text($this->input->post('nama_transaksi')[$i]),
	        	'desc' => str_replace_text($this->input->post('desc_transaksi')[$i]),
	        	'img' => $img_transaksi,
	        	'date_edit' => date("Y/m/d - h:i:sa"),
	        	'edit_by' => $this->session->userdata('id'),
	        );
	        $where = array(
	    		'id' => $this->input->post('id_transaksi')[$i],
	        );
	        $this->Modglobal->update('statik', $data, $where);
	    }


	   /* $config = array();
	    $config['upload_path'] = '../uploads/web';
	    $config['allowed_types'] = '*';
	    $config['max_size']      = '0';
	    $config['overwrite']     = FALSE;

	    $this->load->library('upload');

	    $files = $_FILES;*/

	    for($i=0; $i< count($this->input->post('id_fitur')); $i++)
	    {          
	    	if ($_FILES['img_fitur']['name'][$i])
		    {           
		        $_FILES['userfile']['name']= str_replace_img($_FILES['img_fitur']['name'][$i]);
		        $_FILES['userfile']['type']= $_FILES['img_fitur']['type'][$i];
		        $_FILES['userfile']['tmp_name']= $_FILES['img_fitur']['tmp_name'][$i];
		        $_FILES['userfile']['error']= $_FILES['img_fitur']['error'][$i];
		        $_FILES['userfile']['size']= $_FILES['img_fitur']['size'][$i];   
		         
		        $this->upload->initialize($config);
		        $this->upload->do_upload();

		        $img_fitur = str_replace_img($_FILES['img_fitur']['name'][$i]);
		    }
		    else {
		    	$img_fitur = $this->input->post('img_fitur2')[$i];
		    } 

	        
	        $data = array(
	        	'nama1' =>str_replace_text($this->input->post('nama_fitur')[$i]),
	        	'desc' => str_replace_text($this->input->post('desc_fitur')[$i]),
	        	'img' => $img_fitur,
	        	'tipe' => $this->input->post('tipe3')[$i],
	        	'date_edit' => date("Y/m/d - h:i:sa"),
	        	'edit_by' => $this->session->userdata('id'),
	        );
	        $where = array(
	    		'id' => $this->input->post('id_fitur')[$i],
	        );
	        $this->Modglobal->update('statik', $data, $where);
	    }


		redirect('homeeditor');
	}
	
	
}


