<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Karir extends CI_Controller {

	function __construct(){
		parent::__construct();		
		$this->load->model('Modglobal');


		if (!$this->session->userdata('logged_in')) {
        	redirect('/');
        }
 
	}
	public function index()
	{
		$page = "Karir";
		$user_id = $this->session->userdata('id');

		$query= $this->Modglobal->find('karir', array());
		$result = $query->result_array();
		$num = $query->num_rows();

		$status = array (
			'0' => 'Aktif', 
			'1' => 'Tidak Aktif'
		);

		$data = array(
			'content' => 'karir/index',
			'result' => $result,
			'num' => $num,
			'status' => $status,
			'page' => $page,
		);
		$this->load->view('layouts/base', $data);
	}
	public function add()
	{
		$page = "Karir";
		$form = "add";
		$user_id = $this->session->userdata('id');

		$query= $this->Modglobal->find('city', array());
		$city = $query->result_array();

		$query2= $this->Modglobal->find('karir_cate', array());
		$cate = $query2->result_array();

		$status = array (
			'0' => 'Aktif', 
			'1' => 'Tidak Aktif'
		);

		$data = array(
			'content' => 'karir/form',
			'cate' => $cate,
			'city' => $city,
			'status' => $status,
			'page' => $page,
			'form' => $form,
		);
		$this->load->view('layouts/base', $data);
	}
	public function add_pro()
	{
		$data = array(
        	'cate' => $this->input->post('cate'),
        	'kota' => $this->input->post('kota'),
        	'status' => $this->input->post('status'),
        	'area' => $this->input->post('area'),
        	'url' => $this->input->post('url'),
        	'nama' => $this->input->post('nama'),
        	'date_create' => date("Y/m/d - h:i:sa"),
        	'create_by' => $this->session->userdata('id'),
        );
        $this->Modglobal->insert('karir', $data);
		redirect('karir');
	}
	public function edit()
	{
		$page = "Karir";
		$form = "edit";

		$id = $this->uri->segment(3);

		$query = $this->Modglobal->find('karir', array('id' => $id));
		$page_detail = $query->row_array();

		$query2= $this->Modglobal->find('city', array());
		$city = $query2->result_array();

		$query3= $this->Modglobal->find('karir_cate', array());
		$cate = $query3->result_array();

		$status = array (
			'0' => 'Aktif', 
			'1' => 'Tidak Aktif'
		);

		$data = array(
			'content' => 'karir/form',
			'page' => $page,
			'page_detail' => $page_detail,
			'cate' => $cate,
			'city' => $city,
			'id' => $id,
			'status' => $status,
			'form' => $form,
		);
		$this->load->view('layouts/base', $data);
	}
	
	public function update(){
		
		$data = array(
			'cate' => $this->input->post('cate'),
        	'kota' => $this->input->post('kota'),
        	'status' => $this->input->post('status'),
        	'area' => $this->input->post('area'),
        	'url' => $this->input->post('url'),
        	'nama' => $this->input->post('nama'),
        	'date_edit' => date("Y/m/d - h:i:sa"),
        	'edit_by' => $this->session->userdata('id'),
        );
        $where = array(
    		'id' => $this->input->post('id'),
        );
        $this->Modglobal->update('karir', $data, $where);
		redirect('karir');
	}
	public function delete() {
		$id = $this->uri->segment(3);
		$where = array(
        	'id' => $id,
        );
		$this->Modglobal->delete('karir', $where);
		//echo $id;

		redirect('karir');
	}

	public function job()
	{
		$page = "Karir";
		$user_id = $this->session->userdata('id');

		$query= $this->Modglobal->find('karir_cate', array());
		$result = $query->result_array();
		$num = $query->num_rows();

		$status = array (
			'0' => 'Aktif', 
			'1' => 'Tidak Aktif'
		);

		$data = array(
			'content' => 'karir/index_job',
			'result' => $result,
			'num' => $num,
			'status' => $status,
			'page' => $page,
		);
		$this->load->view('layouts/base', $data);
	}
	public function job_add()
	{
		$page = "Karir";
		$form = "add";
		$user_id = $this->session->userdata('id');



		$status = array (
			'0' => 'Aktif', 
			'1' => 'Tidak Aktif'
		);

		$data = array(
			'content' => 'karir/form_job',
			'status' => $status,
			'page' => $page,
			'form' => $form,
		);
		$this->load->view('layouts/base', $data);
	}
	public function job_pro()
	{
		$findArr = array(" - "," ","  ", "[", "]","&","+","!");
		$replaceArr   = array("","-","-","", "","","","");
		if($_FILES["img"]['name']) {
			$img = 'karir-'.time().$_FILES["img"]['name'];
			$img = str_replace($findArr, $replaceArr, $img);
			$config['upload_path']   = '../uploads/karir/'; 
	        $config['allowed_types'] = '*'; 
			$config['file_name'] = $img;
	        $this->load->library('upload', $config);
	        $this->upload->initialize($config);
	        $this->upload->do_upload('img');
	    }
	    else{
	    	$img = $this->input->post('img2');
	    }

	    if($_FILES["img_thumb"]['name']) {
			$img_thumb = 'karirthumb-'.time().$_FILES["img_thumb"]['name'];
			$img_thumb = str_replace($findArr, $replaceArr, $img_thumb);
			$config['upload_path']   = '../uploads/karir/'; 
	        $config['allowed_types'] = '*'; 
			$config['file_name'] = $img_thumb;
	        $this->load->library('upload', $config);
	        $this->upload->initialize($config);
	        $this->upload->do_upload('img_thumb');
	    }
	    else{
	    	$img_thumb = $this->input->post('img_thumb2');
	    }
		
		$data = array(
        	'nama' => str_replace_text($this->input->post('nama')),
        	'desc' => $this->input->post('desc'),
        	'status' => $this->input->post('status'),
        	'img' => $img,
        	'img2' => $img_thumb,
        	'date_create' => date("Y/m/d - h:i:sa"),
        	'create_by' => $this->session->userdata('id'),
        );
        $this->Modglobal->insert('karir_cate', $data);
		redirect('karir/job');
	}

	public function job_edit()
	{
		$page = "Karir";
		$form = "edit";

		$id = $this->uri->segment(3);

		$query = $this->Modglobal->find('karir_cate', array('id' => $id));
		$page_detail = $query->row_array();

		$status = array (
			'0' => 'Aktif', 
			'1' => 'Tidak Aktif'
		);

		$data = array(
			'content' => 'karir/form_job',
			'page' => $page,
			'page_detail' => $page_detail,
			'id' => $id,
			'status' => $status,
			'form' => $form,
		);
		$this->load->view('layouts/base', $data);
	}
	public function job_update()
	{
		$findArr = array(" - "," ","  ", "[", "]","&","+","!");
		$replaceArr   = array("","-","-","", "","","","");
		if($_FILES["img"]['name']) {
			$img = 'karir-'.time().$_FILES["img"]['name'];
			$img = str_replace($findArr, $replaceArr, $img);
			$config['upload_path']   = '../uploads/karir/'; 
	        $config['allowed_types'] = '*'; 
			$config['file_name'] = $img;
	        $this->load->library('upload', $config);
	        $this->upload->initialize($config);
	        $this->upload->do_upload('img');
	    }
	    else{
	    	$img = $this->input->post('img2');
	    }
	    if($_FILES["img_thumb"]['name']) {
			$img_thumb = 'karirthumb-'.time().$_FILES["img_thumb"]['name'];
			$img_thumb = str_replace($findArr, $replaceArr, $img_thumb);
			$config['upload_path']   = '../uploads/karir/'; 
	        $config['allowed_types'] = '*'; 
			$config['file_name'] = $img_thumb;
	        $this->load->library('upload', $config);
	        $this->upload->initialize($config);
	        $this->upload->do_upload('img_thumb');
	    }
	    else{
	    	$img_thumb = $this->input->post('img_thumb2');
	    }
		
		$data = array(
        	'nama' => str_replace_text($this->input->post('nama')),
        	'desc' => $this->input->post('desc'),
        	'status' => $this->input->post('status'),
        	'date_edit' => date("Y/m/d - h:i:sa"),
        	'edit_by' => $this->session->userdata('id'),
        	'img' => $img,
        	'img2' => $img_thumb,
        );
        $where = array(
    		'id' => $this->input->post('id'),
        );
        $this->Modglobal->update('karir_cate', $data, $where);
		redirect('karir/job');
	}
	public function job_delete() {
		$id = $this->uri->segment(3);
		$where = array(
        	'id' => $id,
        );
		$this->Modglobal->delete('karir_cate', $where);
		//echo $id;

		redirect('karir/job');
	}

	public function kota()
	{
		$page = "Karir";
		$user_id = $this->session->userdata('id');

		$query= $this->Modglobal->find('city', array());
		$result = $query->result_array();
		$num = $query->num_rows();

		$status = array (
			'0' => 'Aktif', 
			'1' => 'Tidak Aktif'
		);

		$data = array(
			'content' => 'karir/index_kota',
			'result' => $result,
			'num' => $num,
			'status' => $status,
			'page' => $page,
		);
		$this->load->view('layouts/base', $data);
	}
	public function kota_add()
	{
		$page = "Karir";
		$form = "add";
		$user_id = $this->session->userdata('id');

		$status = array (
			'0' => 'Aktif', 
			'1' => 'Tidak Aktif'
		);

		$data = array(
			'content' => 'karir/form_kota',
			'status' => $status,
			'page' => $page,
			'form' => $form,
		);
		$this->load->view('layouts/base', $data);
	}
	public function kota_pro()
	{
		$findArr = array(" - "," ","  ", "[", "]","&","+","!");
		$replaceArr   = array("","-","-","", "","","","");
		if($_FILES["img"]['name']) {
			$img = 'kota-'.time().$_FILES["img"]['name'];
			$img = str_replace($findArr, $replaceArr, $img);
			$config['upload_path']   = '../uploads/karir/'; 
	        $config['allowed_types'] = '*'; 
			$config['file_name'] = $img;
	        $this->load->library('upload', $config);
	        $this->upload->initialize($config);
	        $this->upload->do_upload('img');
	    }
	    else{
	    	$img = $this->input->post('img2');
	    }
		
		$data = array(
        	'nama' => str_replace_text($this->input->post('nama')),
        	'desc' => $this->input->post('desc'),
        	'img' => $img,
        	'status' => $this->input->post('status'),
        	'date_create' => date("Y/m/d - h:i:sa"),
        	'create_by' => $this->session->userdata('id'),
        );
        $this->Modglobal->insert('city', $data);
		redirect('karir/kota');
	}
	public function kota_edit()
	{
		$page = "Karir";
		$form = "edit";

		$id = $this->uri->segment(3);

		$query = $this->Modglobal->find('city', array('id' => $id));
		$page_detail = $query->row_array();

		$status = array (
			'0' => 'Aktif', 
			'1' => 'Tidak Aktif'
		);

		$data = array(
			'content' => 'karir/form_kota',
			'page' => $page,
			'page_detail' => $page_detail,
			'id' => $id,
			'status' => $status,
			'form' => $form,
		);
		$this->load->view('layouts/base', $data);
	}
	public function kota_update()
	{
		$findArr = array(" - "," ","  ", "[", "]","&","+","!");
		$replaceArr   = array("","-","-","", "","","","");
		if($_FILES["img"]['name']) {
			$img = 'kota-'.time().$_FILES["img"]['name'];
			$img = str_replace($findArr, $replaceArr, $img);
			$config['upload_path']   = '../uploads/karir/'; 
	        $config['allowed_types'] = '*'; 
			$config['file_name'] = $img;
	        $this->load->library('upload', $config);
	        $this->upload->initialize($config);
	        $this->upload->do_upload('img');
	    }
	    else{
	    	$img = $this->input->post('img2');
	    }
		
		$data = array(
        	'nama' => str_replace_text($this->input->post('nama')),
        	'desc' => $this->input->post('desc'),
        	'img' => $img,
        	'status' => $this->input->post('status'),
        	'date_edit' => date("Y/m/d - h:i:sa"),
        	'edit_by' => $this->session->userdata('id'),
        );
        $where = array(
    		'id' => $this->input->post('id'),
        );
        $this->Modglobal->update('city', $data, $where);
		redirect('karir/kota');
	}
	public function kota_delete() {
		$id = $this->uri->segment(3);
		$where = array(
        	'id' => $id,
        );
		$this->Modglobal->delete('city', $where);
		//echo $id;

		redirect('karir/kota');
	}
	public function foto()
	{
		$page = "Karir";
		$user_id = $this->session->userdata('id');

		$query= $this->Modglobal->find('karir_foto', array());
		$result = $query->result_array();
		$num = $query->num_rows();

		$status = array (
			'0' => 'Aktif', 
			'1' => 'Tidak Aktif'
		);

		$data = array(
			'content' => 'karir/index_foto',
			'result' => $result,
			'num' => $num,
			'status' => $status,
			'page' => $page,
		);
		$this->load->view('layouts/base', $data);
	}
	public function fotoadd()
	{
		$page = "Karir";
		$form = "add";
		$user_id = $this->session->userdata('id');



		$status = array (
			'0' => 'Aktif', 
			'1' => 'Tidak Aktif'
		);

		$data = array(
			'content' => 'karir/form_foto',
			'status' => $status,
			'page' => $page,
			'form' => $form,
		);
		$this->load->view('layouts/base', $data);
	}
	public function fotopro()
	{
		$findArr = array(" - "," ","  ", "[", "]","&","+","!");
		$replaceArr   = array("","-","-","", "","","","");
		if($_FILES["img"]['name']) {
			$img = 'karirfoto-'.time().$_FILES["img"]['name'];
			$img = str_replace($findArr, $replaceArr, $img);
			$config['upload_path']   = '../uploads/karir/'; 
	        $config['allowed_types'] = '*'; 
			$config['file_name'] = $img;
	        $this->load->library('upload', $config);
	        $this->upload->initialize($config);
	        $this->upload->do_upload('img');
	    }
	    else{
	    	$img = $this->input->post('img2');
	    }
		
		$data = array(
        	'nama' => $this->input->post('nama'),
        	'status' => $this->input->post('status'),
        	'img' => $img,
        	'date_create' => date("Y/m/d - h:i:sa"),
        	'create_by' => $this->session->userdata('id'),
        );
        $this->Modglobal->insert('karir_foto', $data);
		redirect('karir/foto');
	}

	public function fotoedit()
	{
		$page = "Karir";
		$form = "edit";

		$id = $this->uri->segment(3);

		$query = $this->Modglobal->find('karir_foto', array('id' => $id));
		$page_detail = $query->row_array();

		$status = array (
			'0' => 'Aktif', 
			'1' => 'Tidak Aktif'
		);

		$data = array(
			'content' => 'karir/form_foto',
			'page' => $page,
			'page_detail' => $page_detail,
			'id' => $id,
			'status' => $status,
			'form' => $form,
		);
		$this->load->view('layouts/base', $data);
	}
	public function fotoupdate()
	{
		$findArr = array(" - "," ","  ", "[", "]","&","+","!");
		$replaceArr   = array("","-","-","", "","","","");
		if($_FILES["img"]['name']) {
			$img = 'karirfoto-'.time().$_FILES["img"]['name'];
			$img = str_replace($findArr, $replaceArr, $img);
			$config['upload_path']   = '../uploads/karir/'; 
	        $config['allowed_types'] = '*'; 
			$config['file_name'] = $img;
	        $this->load->library('upload', $config);
	        $this->upload->initialize($config);
	        $this->upload->do_upload('img');
	    }
	    else{
	    	$img = $this->input->post('img2');
	    }
		
		$data = array(
        	'nama' => $this->input->post('nama'),
        	'status' => $this->input->post('status'),
        	'date_edit' => date("Y/m/d - h:i:sa"),
        	'edit_by' => $this->session->userdata('id'),
        	'img' => $img,
        );
        $where = array(
    		'id' => $this->input->post('id'),
        );
        $this->Modglobal->update('karir_foto', $data, $where);
		redirect('karir/foto');
	}
	public function fotodelete() {
		$id = $this->uri->segment(3);
		$where = array(
        	'id' => $id,
        );
		$this->Modglobal->delete('karir_foto', $where);
		//echo $id;

		redirect('karir/foto');
	}

	public function editor()
	{
		$page = "Karir";
		$user_id = $this->session->userdata('id');

		$query= $this->Modglobal->find('statik', array('id' => '16'));
		$page_detail = $query->row_array();

		$karirquery= $this->Modglobal->find('statik', array('nama' => 'karir_fitur'));
		$karir = $karirquery->result_array();

		$status = array (
			'0' => 'Aktif', 
			'1' => 'Tidak Aktif'
		);

		$data = array(
			'content' => 'karir/form_page',
			'page_detail' => $page_detail,
			'status' => $status,
			'page' => $page,
			'karir' => $karir,
		);
		$this->load->view('layouts/base', $data);
	}
	public function editorupdate()
	{
		$findArr = array(" - "," ","  ", "[", "]","&","+","!");
		$replaceArr   = array("","-","-","", "","","","");
		if($_FILES["img"]['name']) {
			$img = 'karir-'.time().$_FILES["img"]['name'];
			$img = str_replace($findArr, $replaceArr, $img);
			$config['upload_path']   = '../uploads/karir/'; 
	        $config['allowed_types'] = '*'; 
			$config['file_name'] = $img;
	        $this->load->library('upload', $config);
	        $this->upload->initialize($config);
	        $this->upload->do_upload('img');
	    }
	    else{
	    	$img = $this->input->post('img2');
	    }
		
		$data = array(
        	'nama' => $this->input->post('nama'),
        	'desc' => $this->input->post('desc'),
        	'date_edit' => date("Y/m/d - h:i:sa"),
        	'edit_by' => $this->session->userdata('id'),
        	'img' => $img,
        );
        $where = array(
    		'id' => '16',
        );
        $this->Modglobal->update('statik', $data, $where);

        $config = array();
	    $config['upload_path'] = '../uploads/karir';
	    $config['allowed_types'] = '*';
	    $config['max_size']      = '0';
	    $config['overwrite']     = FALSE;

	    $this->load->library('upload');

	    $files = $_FILES;
	    

	    for($i=0; $i< count($this->input->post('id_fitur')); $i++)
	    {          
	    	if ($_FILES['img_fitur']['name'][$i])
		    {           
		        $_FILES['userfile']['name']= str_replace_img($_FILES['img_fitur']['name'][$i]);
		        $_FILES['userfile']['type']= $_FILES['img_fitur']['type'][$i];
		        $_FILES['userfile']['tmp_name']= $_FILES['img_fitur']['tmp_name'][$i];
		        $_FILES['userfile']['error']= $_FILES['img_fitur']['error'][$i];
		        $_FILES['userfile']['size']= $_FILES['img_fitur']['size'][$i];   
		         
		        $this->upload->initialize($config);
		        $this->upload->do_upload();

		        $img = str_replace_img($_FILES['img_fitur']['name'][$i]);
		        //echo $img;
		    }
		    else {
		    	$img = $this->input->post('img_fitur2')[$i];
		    } 

	        
	        $data = array(
	        	'nama1' => $this->input->post('nama1')[$i],
	        	'desc' => $this->input->post('desc1')[$i],
	        	'img' => $img,
	        	'date_edit' => date("Y/m/d - h:i:sa"),
	        	'edit_by' => $this->session->userdata('id'),
	        );
	        $where = array(
	    		'id' => $this->input->post('id_fitur')[$i],
	        );
	        $this->Modglobal->update('statik', $data, $where);
	    }
		redirect('karir/editor');
	}
	public function posisi()
	{
		$page = "Karir";
		$form = "edit";

		$id = $this->uri->segment(3);

		$query = $this->Modglobal->find('karir_cate', array('status' => '0'),'nama');
		$karir = $query->result_array();
		$num = $query->num_rows();

		$status = array (
			'0' => 'Aktif', 
			'1' => 'Tidak Aktif'
		);

		$data = array(
			'content' => 'karir/form_posisi',
			'page' => $page,
			'karir' => $karir,
			'id' => $id,
			'status' => $status,
			'form' => $form,
			'num' => $num,
		);
		$this->load->view('layouts/base', $data);
	}
	public function posisi_update(){
		
		$data = array(
        	'posisi' =>'99999',
        );
        $where = array(
    		//'status' => '0',
        );
        $this->Modglobal->update('karir_cate', $data, $where);

        for($i=0; $i< count($this->input->post('id_posisi')); $i++)
	    {          
	        $data = array(
	        	'posisi' =>str_replace_text($this->input->post('id_posisi')[$i]),
	        );
	        $where = array(
	    		'id' => $this->input->post('merchant_id')[$i],
	        );
	        $this->Modglobal->update('karir_cate', $data, $where);
	    }

		redirect('karir/posisi');
	}
	public function posisikota()
	{
		$page = "Karir";
		$form = "edit";

		$id = $this->uri->segment(3);

		$query = $this->Modglobal->find('city', array('status' => '0'),'nama');
		$city = $query->result_array();
		$num = $query->num_rows();

		$status = array (
			'0' => 'Aktif', 
			'1' => 'Tidak Aktif'
		);

		$data = array(
			'content' => 'karir/form_city_posisi',
			'page' => $page,
			'city' => $city,
			'id' => $id,
			'status' => $status,
			'form' => $form,
			'num' => $num,
		);
		$this->load->view('layouts/base', $data);
	}
	public function posisikota_update(){
		
		$data = array(
        	'posisi' =>'99999',
        );
        $where = array(
    		//'status' => '0',
        );
        $this->Modglobal->update('city', $data, $where);

        for($i=0; $i< count($this->input->post('id_posisi')); $i++)
	    {          
	        $data = array(
	        	'posisi' =>str_replace_text($this->input->post('id_posisi')[$i]),
	        );
	        $where = array(
	    		'id' => $this->input->post('karir_id')[$i],
	        );
	        $this->Modglobal->update('city', $data, $where);
	    }

		redirect('karir/posisikota');
	}
	

	
	
	
}


