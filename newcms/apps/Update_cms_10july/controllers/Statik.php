<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Statik extends CI_Controller {

	function __construct(){
		parent::__construct();		
		$this->load->model('Modglobal');


		if (!$this->session->userdata('logged_in')) {
        	redirect('/');
        }
 
	}
	public function index()
	{
		$page = "Statik";

		$id = $this->uri->segment(3);

		$page_query = $this->Modglobal->find('statikpage', array('id' => '1'));
		$page_detail = $page_query->row_array();

		$data = array(
			'content' => 'hubungikami/form',
			'page' => $page,
			'page_detail' => $page_detail,
			'id' => $id,
		);
		$this->load->view('layouts/base', $data);
	}
	
	public function tentang()
	{
		$page = "Statik";

		$id = $this->uri->segment(3);

		$page_query = $this->Modglobal->find('statik', array('id' => '1'));
		$page_detail = $page_query->row_array();

		$data = array(
			'content' => 'statik/tentang',
			'page' => $page,
			'page_detail' => $page_detail,
			'id' => $id,
		);
		$this->load->view('layouts/base', $data);
	}
	
	public function tentangupdate(){
		$findArr = array(" - "," ","  ", "[", "]","&","+","!");
		$replaceArr   = array("","-","-","", "","","","");
		if($_FILES["img"]['name']) {
			$img = 'tentang-'.time().$_FILES["img"]['name'];
			$img = str_replace($findArr, $replaceArr, $img);
			$config['upload_path']   = '../uploads/web/'; 
	        $config['allowed_types'] = '*'; 
			$config['file_name'] = $img;
	        $this->load->library('upload', $config);
	        $this->upload->initialize($config);
	        $this->upload->do_upload('img');
	    }
	    else{
	    	$img = $this->input->post('img2');
	    }

	    if($_FILES["img_bg"]['name']) {
			$img_bg = 'tentang2-'.time().$_FILES["img_bg"]['name'];
			$img_bg = str_replace($findArr, $replaceArr, $img_bg);
			$config['upload_path']   = '../uploads/web/'; 
	        $config['allowed_types'] = '*'; 
			$config['file_name'] = $img_bg;
	        $this->load->library('upload', $config);
	        $this->upload->initialize($config);
	        $this->upload->do_upload('img_bg');
	    }
	    else{
	    	$img_bg = $this->input->post('img_bg2');
	    }

		$data = array(
        	'nama' => $this->input->post('nama'),
        	'desc' => $this->input->post('desc'),
        	'teaser' => $this->input->post('teaser'),
        	'img' => $img,
        	'img2' => $img_bg,
        	'date_edit' => date("Y/m/d - h:i:sa"),
        	'edit_by' => $this->session->userdata('id'),
        );
        $where = array(
    		'id' => '1',
        );
        $this->Modglobal->update('statik', $data, $where);
		redirect('statik/tentang');
	}
	public function terms()
	{
		$page = "Statik";

		$id = $this->uri->segment(3);

		$page_query = $this->Modglobal->find('statik', array('id' => '2'));
		$page_detail = $page_query->row_array();

		$data = array(
			'content' => 'statik/terms',
			'page' => $page,
			'page_detail' => $page_detail,
			'id' => $id,
		);
		$this->load->view('layouts/base', $data);
	}
	
	public function termsupdate(){
		
		$data = array(
        	'nama' => $this->input->post('nama'),
        	'desc' => $this->input->post('desc'),
        	'date_edit' => date("Y/m/d - h:i:sa"),
        	'edit_by' => $this->session->userdata('id'),
        );
        $where = array(
    		'id' => '2',
        );
        $this->Modglobal->update('statik', $data, $where);
		redirect('statik/terms');
	}
	public function contact()
	{
		$page = "Statik";

		$id = $this->uri->segment(3);

		$page_query = $this->Modglobal->find('statik', array('id' => '3'));
		$page_detail = $page_query->row_array();

		$data = array(
			'content' => 'statik/contact',
			'page' => $page,
			'page_detail' => $page_detail,
			'id' => $id,
		);
		$this->load->view('layouts/base', $data);
	}
	
	public function contactupdate(){
		
		$data = array(
        	'nama' => $this->input->post('nama'),
        	'desc' => $this->input->post('desc'),
        	'email' => str_replace_text($this->input->post('email')),
        	'nomor' => str_replace_text($this->input->post('nomor')),
        	'map' => str_replace_text($this->input->post('map')),
        	'nama1' => str_replace_text($this->input->post('nama1')),
        	'teaser' => $this->input->post('teaser'),
        	'date_edit' => date("Y/m/d - h:i:sa"),
        	'edit_by' => $this->session->userdata('id'),
        );
        $where = array(
    		'id' => '3',
        );
        $this->Modglobal->update('statik', $data, $where);
		redirect('statik/contact');
	}
	
	
}


