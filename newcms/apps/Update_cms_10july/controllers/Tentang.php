<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tentang extends CI_Controller {

	function __construct(){
		parent::__construct();		
		$this->load->model('Modglobal');


		if (!$this->session->userdata('logged_in')) {
        	redirect('/');
        }
 
	}
	public function index()
	{
		$page = "tentang";

		$page_query = $this->Modglobal->find('tentang', array('id' => '1'));
		$page_detail = $page_query->row_array();

		$data = array(
			'content' => 'tentang/form',
			'page' => $page,
			'page_detail' => $page_detail,
		);
		$this->load->view('layouts/base', $data);
	}
	
	public function update(){
		$findArr = array(" - "," ","  ", "[", "]","&","+","!");
		$replaceArr   = array("","-","-","", "","","","");
		if($_FILES["img"]['name']) {
			$img = 'tentang-'.time().$_FILES["img"]['name'];
			$img = str_replace($findArr, $replaceArr, $img);
			$config['upload_path']   = '../uploads/web/'; 
	        $config['allowed_types'] = '*'; 
			$config['file_name'] = $img;
	        $this->load->library('upload', $config);
	        $this->upload->initialize($config);
	        $this->upload->do_upload('img');
	    }
	    else{
	    	$img = $this->input->post('img2');
	    }

		$data = array(
        	'desc_intro' => $this->input->post('desc_intro'),
        	'desc1' => $this->input->post('desc1'),
        	'desc2' => $this->input->post('desc2'),
        	'desc3' => $this->input->post('desc3'),
        	'desc4' => $this->input->post('desc4'),
        	'desc5' => $this->input->post('desc5'),
        );
        $where = array(
    		'id' => '1',
        );
        $this->Modglobal->update('tentang', $data, $where);
		redirect('tentang/');
	}
	
	
}


