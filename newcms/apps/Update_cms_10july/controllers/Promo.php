<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Promo extends CI_Controller {

	function __construct(){
		parent::__construct();		
		$this->load->model('Modglobal');


		if (!$this->session->userdata('logged_in')) {
        	redirect('/');
        }
 
	}
	public function index()
	{
		$page = "Promo";
		$user_id = $this->session->userdata('id');

		$query= $this->Modglobal->find('promo', array());
		$result = $query->result_array();
		$num = $query->num_rows();

		$status = array (
			'0' => 'Publish', 
			'1' => 'Tidak Publish'
		);

		$data = array(
			'content' => 'promo/index',
			'result' => $result,
			'num' => $num,
			'status' => $status,
			'page' => $page,
		);
		$this->load->view('layouts/base', $data);
	}
	public function add()
	{
		$page = "Promo";
		$form = "add";
		$user_id = $this->session->userdata('id');

		$query2= $this->Modglobal->find('promo_cate', array());
		$cate = $query2->result_array();

		$status = array (
			'0' => 'Publish', 
			'1' => 'Tidak Publish'
		);

		$data = array(
			'content' => 'promo/form',
			'cate' => $cate,
			'status' => $status,
			'page' => $page,
			'form' => $form,
		);
		$this->load->view('layouts/base', $data);
	}
	public function add_pro()
	{
		$findArr = array(" - "," ","  ", "[", "]","&","+","!");
		$replaceArr   = array("","-","-","", "","","","");
		if($_FILES["img"]['name']) {
			$img = 'promo-'.time().$_FILES["img"]['name'];
			$img = str_replace($findArr, $replaceArr, $img);
			$config['upload_path']   = '../uploads/promo/'; 
	        $config['allowed_types'] = '*'; 
			$config['file_name'] = $img;
	        $this->load->library('upload', $config);
	        $this->upload->initialize($config);
	        $this->upload->do_upload('img');
	    }
	    else{
	    	$img = $this->input->post('img2');
	    }

		if($_FILES["img_thumb"]['name']) {
			$img_thumb = 'promothumb-'.time().$_FILES["img_thumb"]['name'];
			$img_thumb = str_replace($findArr, $replaceArr, $img_thumb);
			$config['upload_path']   = '../uploads/promo/'; 
	        $config['allowed_types'] = '*'; 
			$config['file_name'] = $img_thumb;
	        $this->load->library('upload', $config);
	        $this->upload->initialize($config);
	        $this->upload->do_upload('img_thumb');
	    }
	    else{
	    	$img_thumb = $this->input->post('img_thumb2');
	    }

/*
	    $today = strtotime(date('Y-m-d'));
		$date_publish = strtotime($this->input->post('date_publish'));

	    if($date_publish < $today) {
	    	$status = "0";
	    }
	    else {
	    	$status ="1";
	    }*/

		$data = array(
        	'cate' => $this->input->post('cate'),
        	'status' => $this->input->post('status'),
        	'nama' => $this->input->post('nama'),
        	'desc' => $this->input->post('desc'),
        	'teaser' => $this->input->post('teaser'),
        	'date_end' => $this->input->post('date_end'),
        	'date_publish' => $this->input->post('date_publish'),
        	'img' => $img,
        	'img2' => $img_thumb,
        	'date_create' => date("Y/m/d - h:i:sa"),
        	'create_by' => $this->session->userdata('id'),
        );
        $this->Modglobal->insert('promo', $data);
		redirect('promo');
	}
	public function edit()
	{
		$page = "Promo";
		$form = "edit";

		$id = $this->uri->segment(3);

		$query = $this->Modglobal->find('promo', array('id' => $id));
		$page_detail = $query->row_array();

		$query2= $this->Modglobal->find('promo_cate', array());
		$cate = $query2->result_array();

		$status = array (
			'0' => 'Publish', 
			'1' => 'Tidak Publish'
		);

		$data = array(
			'content' => 'promo/form',
			'page' => $page,
			'page_detail' => $page_detail,
			'cate' => $cate,
			'id' => $id,
			'status' => $status,
			'form' => $form,
		);
		$this->load->view('layouts/base', $data);
	}
	
	public function update(){

		$findArr = array(" - "," ","  ", "[", "]","&","+","!");
		$replaceArr   = array("","-","-","", "","","","");
		if($_FILES["img"]['name']) {
			$img = 'promo-'.time().$_FILES["img"]['name'];
			$img = str_replace($findArr, $replaceArr, $img);
			$config['upload_path']   = '../uploads/promo/'; 
	        $config['allowed_types'] = '*'; 
			$config['file_name'] = $img;
	        $this->load->library('upload', $config);
	        $this->upload->initialize($config);
	        $this->upload->do_upload('img');
	    }
	    else{
	    	$img = $this->input->post('img2');
	    }

	    if($_FILES["img_thumb"]['name']) {
			$img_thumb = 'promothumb-'.time().$_FILES["img_thumb"]['name'];
			$img_thumb = str_replace($findArr, $replaceArr, $img_thumb);
			$config['upload_path']   = '../uploads/promo/'; 
	        $config['allowed_types'] = '*'; 
			$config['file_name'] = $img_thumb;
	        $this->load->library('upload', $config);
	        $this->upload->initialize($config);
	        $this->upload->do_upload('img_thumb');
	    }
	    else{
	    	$img_thumb = $this->input->post('img_thumb2');
	    }

		$data = array(
        	'cate' => $this->input->post('cate'),
        	'status' => $this->input->post('status'),
        	'nama' => $this->input->post('nama'),
        	'desc' => $this->input->post('desc'),
        	'teaser' => $this->input->post('teaser'),
        	'date_end' => $this->input->post('date_end'),
        	'date_publish' => $this->input->post('date_publish'),
        	'img' => $img,
        	'img2' => $img_thumb,
        	'date_edit' => date("Y/m/d - h:i:sa"),
        	'edit_by' => $this->session->userdata('id'),
        );
        $where = array(
    		'id' => $this->input->post('id'),
        );
        $this->Modglobal->update('promo', $data, $where);
		redirect('promo');
	}
	public function delete() {
		$id = $this->uri->segment(3);
		$where = array(
        	'id' => $id,
        );
		$this->Modglobal->delete('promo', $where);
		//echo $id;

		redirect('promo');
	}

	public function cate()
	{
		$page = "Promo";
		$user_id = $this->session->userdata('id');

		$query= $this->Modglobal->find('promo_cate', array());
		$result = $query->result_array();
		$num = $query->num_rows();

		$status = array (
			'0' => 'Aktif', 
			'1' => 'Tidak Aktif'
		);

		$data = array(
			'content' => 'promo/index_cate',
			'result' => $result,
			'num' => $num,
			'status' => $status,
			'page' => $page,
		);
		$this->load->view('layouts/base', $data);
	}
	public function cate_add()
	{
		$page = "Promo";
		$form = "add";
		$user_id = $this->session->userdata('id');



		$status = array (
			'0' => 'Aktif', 
			'1' => 'Tidak Aktif'
		);

		$data = array(
			'content' => 'promo/form_cate',
			'status' => $status,
			'page' => $page,
			'form' => $form,
		);
		$this->load->view('layouts/base', $data);
	}
	public function cate_pro()
	{
		$data = array(
        	'nama' => $this->input->post('nama'),
        	'status' => $this->input->post('status'),
        	'date_create' => date("Y/m/d - h:i:sa"),
        	'create_by' => $this->session->userdata('id'),
        );
        $this->Modglobal->insert('promo_cate', $data);
		redirect('promo/cate');
	}

	public function cate_edit()
	{
		$page = "Promo";
		$form = "edit";

		$id = $this->uri->segment(3);

		$query = $this->Modglobal->find('promo_cate', array('id' => $id));
		$page_detail = $query->row_array();

		$status = array (
			'0' => 'Aktif', 
			'1' => 'Tidak Aktif'
		);

		$data = array(
			'content' => 'promo/form_cate',
			'page' => $page,
			'page_detail' => $page_detail,
			'id' => $id,
			'status' => $status,
			'form' => $form,
		);
		$this->load->view('layouts/base', $data);
	}
	public function cate_update()
	{
		
		$data = array(
        	'nama' => $this->input->post('nama'),
        	'status' => $this->input->post('status'),
        	'date_edit' => date("Y/m/d - h:i:sa"),
        	'edit_by' => $this->session->userdata('id'),
        );
        $where = array(
    		'id' => $this->input->post('id'),
        );
        $this->Modglobal->update('promo_cate', $data, $where);
		redirect('promo/cate');
	}
	public function cate_delete() {
		$id = $this->uri->segment(3);
		$where = array(
        	'id' => $id,
        );
		$this->Modglobal->delete('promo_cate', $where);
		//echo $id;

		redirect('promo/cate');
	}
	public function posisi()
	{
		$page = "Promo";
		$form = "edit";

		$id = $this->uri->segment(3);

		$query = $this->Modglobal->find('promo_cate', array('status' => '0'),'nama');
		$promo = $query->result_array();
		$num = $query->num_rows();

		$status = array (
			'0' => 'Aktif', 
			'1' => 'Tidak Aktif'
		);

		$data = array(
			'content' => 'promo/form_posisi',
			'page' => $page,
			'promo' => $promo,
			'id' => $id,
			'status' => $status,
			'form' => $form,
			'num' => $num,
		);
		$this->load->view('layouts/base', $data);
	}
	public function posisi_update(){
		
		$data = array(
        	'posisi' =>'99999',
        );
        $where = array(
    		//'status' => '0',
        );
        $this->Modglobal->update('promo_cate', $data, $where);

        for($i=0; $i< count($this->input->post('id_posisi')); $i++)
	    {          
	        $data = array(
	        	'posisi' =>str_replace_text($this->input->post('id_posisi')[$i]),
	        );
	        $where = array(
	    		'id' => $this->input->post('promo_id')[$i],
	        );
	        $this->Modglobal->update('promo_cate', $data, $where);
	    }

		redirect('promo/posisi');
	}
	public function setupdate()
	{
		$promo_set_query= $this->Modglobal->find('promo', array('date_end!=' => ''));
		$promo_set = $promo_set_query->result_array();
		$promo_num = $promo_set_query->num_rows();

		echo $promo_num;

		foreach ($promo_set as $row_promo) {

			$today = strtotime(date('Y-m-d'));
			$date_end = strtotime($row_promo['date_end']);
			$num2 = $today - $date_end;

			if($today > $date_end) {
				echo '<br>';
			}
			else {
				$data = array(
		        	'date_publish' => date('Y-m-d'),
		        );
		        $where = array(
		    		'id' => $row_promo['id'],
		        );
		        $this->Modglobal->update('promo', $data, $where);

		        echo 'berhasil<br>';
			}

		}

		//echo 'berhasil';
	}
	
}


