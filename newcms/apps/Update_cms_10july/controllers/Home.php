<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct(){
		parent::__construct();		
		$this->load->model('Modglobal');


		if (!$this->session->userdata('logged_in')) {
        	redirect('/');
        }
 
	}
	public function index()
	{
		$page = "dashboard";
		$data = array(
			'content' => 'dashboard/index',
			'page' => $page,
		);
		$this->load->view('layouts/base', $data);
	}
	public function password()
	{
		$data = array(
			'content' => 'dashboard/password',
		);
		$this->load->view('layouts/base', $data);
	}
	public function password_update(){
		$user_id = $this->session->userdata('id');

		$old_password = md5($this->input->post('password_current'));
		$new_password = md5($this->input->post('password_new'));
		$re_password = md5($this->input->post('password_verify'));

		$query = $this->Modglobal->find('user', array('id' => $user_id, 'password' => $old_password));
		$cek_user = $query->num_rows();

		if($cek_user > 0){
			if($this->input->post('password_new') == $this->input->post('password_verify')) {
				$where = array(
		        	'id' => $user_id,
		        );
		        $data = array(
		        	'password' => $new_password,
		        );
				$this->Modglobal->update('user', $data, $where);
				$this->session->set_flashdata('success', 'Project berhasil di pin');
			}
			else{
				$this->session->set_flashdata('verify_fail', 'Project berhasil di pin');
			}
		}
		else {
			$this->session->set_flashdata('fail', 'Project berhasil di pin');
		}
		
		redirect('home/password');
	}
	public function editor()
	{
		$page = "editor";

		$page_query = $this->Modglobal->find('homeditor', array('id' => '1'));
		$page_detail = $page_query->row_array();

		$data = array(
			'content' => 'dashboard/editor',
			'page' => $page,
			'page_detail' => $page_detail,
		);
		$this->load->view('layouts/base', $data);
	}
	public function update(){
		$findArr = array(" - "," ","  ", "[", "]","&","+","!");
		$replaceArr   = array("","-","-","", "","","","");
		if($_FILES["cover"]['name']) {
			$cover = 'cover-'.time().$_FILES["cover"]['name'];
			$cover = str_replace($findArr, $replaceArr, $cover);
			$config['upload_path']   = '../uploads/web/'; 
	        $config['allowed_types'] = '*'; 
			$config['file_name'] = $cover;
	        $this->load->library('upload', $config);
	        $this->upload->initialize($config);
	        $this->upload->do_upload('cover');
	    }
	    else{
	    	$cover = $this->input->post('cover2');
	    }
	    if($_FILES["img_why"]['name']) {
			$img_why = 'img_why-'.time().$_FILES["img_why"]['name'];
			$img_why = str_replace($findArr, $replaceArr, $img_why);
			$config['upload_path']   = '../uploads/web/'; 
	        $config['allowed_types'] = '*'; 
			$config['file_name'] = $img_why;
	        $this->load->library('upload', $config);
	        $this->upload->initialize($config);
	        $this->upload->do_upload('img_why');
	    }
	    else{
	    	$img_why = $this->input->post('img_why2');
	    }
	    
	    if($_FILES["how1_img"]['name']) {
			$how1_img = 'how1_img-'.time().$_FILES["how1_img"]['name'];
			$how1_img = str_replace($findArr, $replaceArr, $how1_img);
			$config['upload_path']   = '../uploads/web/'; 
	        $config['allowed_types'] = '*'; 
			$config['file_name'] = $how1_img;
	        $this->load->library('upload', $config);
	        $this->upload->initialize($config);
	        $this->upload->do_upload('how1_img');
	    }
	    else{
	    	$how1_img = $this->input->post('how1_img2');
	    }
	    if($_FILES["how2_img"]['name']) {
			$how2_img = 'how2_img-'.time().$_FILES["how2_img"]['name'];
			$how2_img = str_replace($findArr, $replaceArr, $how2_img);
			$config['upload_path']   = '../uploads/web/'; 
	        $config['allowed_types'] = '*'; 
			$config['file_name'] = $how2_img;
	        $this->load->library('upload', $config);
	        $this->upload->initialize($config);
	        $this->upload->do_upload('how2_img');
	    }
	    else{
	    	$how2_img = $this->input->post('how2_img2');
	    }
	    if($_FILES["how3_img"]['name']) {
			$how3_img = 'how3_img-'.time().$_FILES["how3_img"]['name'];
			$how3_img = str_replace($findArr, $replaceArr, $how3_img);
			$config['upload_path']   = '../uploads/web/'; 
	        $config['allowed_types'] = '*'; 
			$config['file_name'] = $how3_img;
	        $this->load->library('upload', $config);
	        $this->upload->initialize($config);
	        $this->upload->do_upload('how3_img');
	    }
	    else{
	    	$how3_img = $this->input->post('how3_img2');
	    }

		$data = array(
        	'title' => $this->input->post('title'),
        	'subtitle' => $this->input->post('subtitle'),
        	'cover' => $cover,
        	'why' => $this->input->post('why'),
        	'img_why' => $img_why,
        	'how' => $this->input->post('how'),
        	'how1' => $this->input->post('how1'),
        	'how1_desc' => $this->input->post('how1_desc'),
        	'how1_img' => $how1_img,
        	'how2' => $this->input->post('how2'),
        	'how2_desc' => $this->input->post('how2_desc'),
        	'how2_img' => $how2_img,
        	'how3' => $this->input->post('how3'),
        	'how3_desc' => $this->input->post('how3_desc'),
        	'how3_img' => $how3_img,
        	'client' => $this->input->post('client'),
        );
        $where = array(
    		'id' => '1',
        );
        $this->Modglobal->update('homeditor', $data, $where);
		redirect('home/editor');
	}
	
	
}


