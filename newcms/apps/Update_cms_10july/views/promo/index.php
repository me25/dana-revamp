
<div class="content_ful">
	<div class="table_show">
		<div class="table_head">
			<div class="info"><?php echo $num;?> Promo</div>
			<a href="<?php echo base_url('promo/add');?>" class="btn_add">+ Tambah</a>
			<div class="clearfix"></div>
		</div>
		<table id="table_sort" class="table_style" cellspacing="0" width="100%"data-page-length="10" >
		    <thead>
		        <tr>
		        	<th>No</th>
		            <th>Kategori</th>
		            <th width="30%">Nama</th>
		            <th>Image</th>
		            <th>Date Publish</th>
		            <th>Date End</th>
		            <th>Status</th>
		            <th width="50px" class="arrow_non">Action</th>
		        </tr>
		    </thead>
		    <tbody>
		    	<?php 
		    	$no_urut = 0;
		    	foreach ($result as $row) { 
		    		$no_urut++;
		    		?>
		    		<tr class="box_modal_full2" alt="member_detail.php">
		    			<td><?php echo $no_urut;?></td>
		    			<td>
		    				<?php
		    					$query2= $this->Modglobal->find('promo_cate', array('id' => $row['cate']));
								$cate = $query2->row_array();

								echo $cate['nama'];
		    				?>
		    			</td>
			            <td><?php echo $row['nama'];?></td>
			            <td>
			            	<?php
				      			if($row['img']){
				      				echo '<img src="../uploads/promo/'.$row['img'].'" alt="" height="80">';
				      			}
				      		?>
			            </td>
			            <td><?php 
			            	echo $row['date_publish'];
			            	?>
			            </td>
			            <td><?php echo $row['date_end'];?></td>
			            <td><?php echo $status[$row['status']];?></td>
			            <td class="action">
			            	<a href="<?php echo base_url('promo/edit/').$row['id'];?>"><img src="<?php echo assets_url('images');?>/ico_edit.png" alt=""></a>
			            	<a href="<?php echo base_url('promo/delete/').$row['id'];?>" class="delete"><img src="<?php echo assets_url('images');?>/ico_delete.png" alt=""></a>
			            </td>
			        </tr>

		    		<?php

		    	}?>
		        
		        
		        
		    </tbody>
		</table>
	</div>
</div>