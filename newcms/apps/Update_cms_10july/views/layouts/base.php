<?php if ($this->session->userdata('logged_in')) { ?>
<html>
<head>
	<?php $this->load->view('includes/head')?>	
</head>
<?php 
	if ($this->uri->segment(1) == 'login') {
	}
	else{
		$this->load->view('includes/header');
		$this->load->view('includes/menu');
	}
?>
<?php
if (isset($content) && !empty($content)) {
  $this->load->view($content);
}
?>
<div class="clearfix"></div>
<?php $this->load->view('includes/js')?>
</body>
</html>
<?php

}
else{
	redirect(base_url("login"));
}
?>