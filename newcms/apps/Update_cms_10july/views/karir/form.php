
<div class="content_ful">
	<div class="table_show">
		<?php
			if($form == "add"){
				$title = "Tambah";
				$action = 'karir/add_pro';
			}
			else{
				$title = "Edit";
				$action = 'karir/update';
			}
		?>
		<div class="table_head">
			<div class="info"><h2><?php echo $title;?></h2></div>
			<div class="clearfix"></div>
		</div>


		<hr color="#eee">
		<form action="<?php echo base_url($action);?>" class="form_1" method="post"   enctype="multipart/form-data">
			<input type="hidden" name="id" value="<?php echo $page_detail['id'];?>">
			<div class="form-group form-group-col-2">
		    	<strong>Job Title</strong>
		      	<div class="select-style">
					<span></span>
					<select name="cate" id="" required="">
						<?php
						foreach ($cate as $row_cate) {
							if($page_detail['cate'] == $row_cate['id']){
								echo '<option value="'.$row_cate['id'].'" selected>'.$row_cate['nama'].'</option>';
							}
							else{
								echo '<option value="'.$row_cate['id'].'">'.$row_cate['nama'].'</option>';
							}
						}
					?>
					</select>

				</div>
		    </div>
		    <div class="clearfix"></div>
		    <div class="form-group form-group-col-2">
		      	<strong>Nama Posisi</strong>
		      	
		      	<input type="text" name="nama" value="<?php echo $page_detail['nama'];?>" required="required">
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
		    <div class="form-group form-group-col-2">
		    	<strong>Kota</strong>
		      	<div class="select-style">
					<span></span>
					<select name="kota" id="" required="">
						<?php
						foreach ($city as $row_city) {
							if($page_detail['kota'] == $row_city['id']){
								echo '<option value="'.$row_city['id'].'" selected>'.$row_city['nama'].'</option>';
							}
							else{
								echo '<option value="'.$row_city['id'].'">'.$row_city['nama'].'</option>';
							}
						}
					?>
					</select>

				</div>
		    </div>
		    <div class="clearfix"></div>
		    <div class="form-group form-group-col-2">
		      	<strong>Region/Daerah</strong>
		      	
		      	<input type="text" name="area" value="<?php echo $page_detail['area'];?>" required="required">
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
		   
		    <div class="form-group form-group-col-2">
		      	<strong>URL</strong>
		      	<input type="text" name="url" value="<?php echo $page_detail['url'];?>">
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
		    
		    <div class="form-group form-group-col-2">
		    	<strong>Status</strong>
		      	<div class="select-style">
					<span></span>
					<select name="status" id="" required="">
						<?php
						foreach ($status as $i => $row_status) {
							if($member['status'] == $i){
								echo '<option value="'.$i.'" selected>'.$row_status.'</option>';
							}
							else{
								echo '<option value="'.$i.'">'.$row_status.'</option>';
							}
						}
					?>
					</select>

				</div>
		    </div>
		    <div class="clearfix"></div>
		    <br>
		    <div>
		    	<a href="<?php echo base_url('member');?>" class="btn_cancel close_box">CANCEL</a>
		    	<input type="submit" value="SAVE" class="btn_save close_box">
		    </div>
		</form>
	</div>
</div>

<div id="pop_box2" class="pop_box" style="display:none;">
	<div class="popbox_bg_close"></div>
	
</div>