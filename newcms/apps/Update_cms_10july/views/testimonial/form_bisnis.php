
<div class="content_ful">
	<div class="table_show">
		<?php
			if($form == "add"){
				$title = "Tambah";
				$action = 'testimonial/addbisnis_pro';
			}
			else{
				$title = "Edit";
				$action = 'testimonial/updatebisnis';
			}
		?>
		<div class="table_head">
			<div class="info"><h2><?php echo $title;?></h2></div>
			<div class="clearfix"></div>
		</div>


		<hr color="#eee">
		<form action="<?php echo base_url($action);?>" class="form_1" method="post"   enctype="multipart/form-data">
			<input type="hidden" name="id" value="<?php echo $page_detail['id'];?>">
			
		    <div class="form-group form-group-col-2">
		      	<strong>Nama</strong>
		      	
		      	<input type="text" name="nama" value="<?php echo $page_detail['nama'];?>" required="required">
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
		    <div class="form-group form-group-col-2">
		      	<strong>Title</strong>
		      	
		      	<input type="text" name="jobtitle" value="<?php echo $page_detail['jobtitle'];?>" required="required">
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
		    <div class="form-group form-group-col-2">
		      	<strong>Image</strong>
		      	<input type="file" name="img" class="uploadFile2">
		      	<input type="hidden" name="img2" value="<?php echo $page_detail['img'];?>" class="img_show">
		      	<div class="imagePreview2">
		      		<?php
		      			if($page_detail['img']){
		      				echo '<img src="../../../uploads/testimonial/'.$page_detail['img'].'" alt="" height="200">';
		      			}
		      		?>
				</div>
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
		    <div class="form-group">
		      	<strong>Testimonial</strong>
		      	<textarea name="desc" id="" cols="30" rows="5" class="tinymc"><?php echo $page_detail['desc'];?></textarea>
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
		    
		    <div class="form-group form-group-col-2">
		    	<strong>Status</strong>
		      	<div class="select-style">
					<span></span>
					<select name="status" id="" required="">
						<?php
						foreach ($status as $i => $row_status) {
							if($member['status'] == $i){
								echo '<option value="'.$i.'" selected>'.$row_status.'</option>';
							}
							else{
								echo '<option value="'.$i.'">'.$row_status.'</option>';
							}
						}
					?>
					</select>

				</div>
		    </div>
		    <div class="clearfix"></div>
		    <br>
		    <div>
		    	<a href="<?php echo base_url('testimonial/bisnis');?>" class="btn_cancel close_box">CANCEL</a>
		    	<input type="submit" value="SAVE" class="btn_save close_box">
		    </div>
		</form>
	</div>
</div>
<script>
	$(function() {
	    $(".uploadFile").on("change", function()
	    {

	        var files = !!this.files ? this.files : [];
	        if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support


	        var ini = $(this).parent().find('.imagePreview');
	 
	        if (/^image/.test( files[0].type)){ // only image file
	            var reader = new FileReader(); // instance of the FileReader
	            reader.readAsDataURL(files[0]); // read the local file
	 
	            reader.onloadend = function(){ // set image data as background of div

	                $(ini).html('<img src="'+this.result+'" height="200"/>');
	            }
	        }
	    });

	    $(".uploadFile2").on("change", function()
	    {

	        var files = !!this.files ? this.files : [];
	        if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support


	        var ini = $(this).parent().find('.imagePreview2');
	 
	        if (/^image/.test( files[0].type)){ // only image file
	            var reader = new FileReader(); // instance of the FileReader
	            reader.readAsDataURL(files[0]); // read the local file
	 
	            reader.onloadend = function(){ // set image data as background of div

	                $(ini).html('<img src="'+this.result+'" height="200"/><img src="assets/images/ico_delete.png" alt="" class="hapus">');
	                $(".hapus").click(function() {
				    	$(this).parent().parent().find('.img_show').val("");
				    	$(this).parent().parent().find('.uploadFile2').val("");
				    	$(this).parent().parent().find('.imagePreview2').html("");
				    	
				    	//alert(isi);
				    });
	            }
	        }
	    });

	    $(".hapus").click(function() {
	    	$(this).parent().parent().find('.img_show').val("");
	    	$(this).parent().parent().find('.imagePreview').html("");
	    	
	    	//alert(isi);
	    });
	    

	});
</script>