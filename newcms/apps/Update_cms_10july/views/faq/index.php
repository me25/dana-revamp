
<div class="content_ful">
	<div class="table_show">
		<div class="table_head">
			<div class="info"><?php echo $num;?> FAQ</div>
			<a href="<?php echo base_url('faq/add');?>" class="btn_add">+ Tambah</a>
			<div class="clearfix"></div>
		</div>
		<table id="table_sort" class="table_style" cellspacing="0" width="100%"data-page-length="10" >
		    <thead>
		        <tr>
		            <th>Category</th>
		            <th width="20%">Pertanyaan</th>
		            <th>Jawaban</th>
		            <th>Status</th>
		            <th width="50px" class="arrow_non">Action</th>
		        </tr>
		    </thead>
		    <tbody>
		    	<?php foreach ($result as $row) { 
		    		?>
		    		<tr class="box_modal_full2" alt="member_detail.php">
		    			<td>
		    				<?php
		    					$query2= $this->Modglobal->find('faq_cate', array('id' => $row['cate_id']));
								$cate = $query2->row_array();

								echo $cate['nama'];
		    				?>
		    			</td>
			            <td><?php echo $row['tanya'];?></td>
			            <td><?php echo $row['jawab'];?></td>
			            <td><?php echo $status[$row['status']];?></td>
			            <td class="action">
			            	<a href="<?php echo base_url('faq/edit/').$row['id'];?>"><img src="<?php echo assets_url('images');?>/ico_edit.png" alt=""></a>
			            	<a href="<?php echo base_url('faq/delete/').$row['id'];?>" class="delete"><img src="<?php echo assets_url('images');?>/ico_delete.png" alt=""></a>
			            </td>
			        </tr>

		    		<?php

		    	}?>
		        
		        
		        
		    </tbody>
		</table>
	</div>
</div>