
<div class="content_ful">
	<div class="table_show">
		<?php
			if($form == "add"){
				$title = 'Tambah '.$page.' Store';
				$action = 'merchant/store_pro';
			}
			else{
				$title = 'Ubah '.$page.' Store';
				$action = 'merchant/store_update';
			}
		?>
		<div class="table_head">
			<div class="info"><h2><?php echo $title;?></h2></div>
			<div class="clearfix"></div>
		</div>


		<hr color="#eee">
		<form action="<?php echo base_url($action);?>" class="form_1" method="post"  enctype="multipart/form-data">
			<div class="form-group">
		      	<strong>Alamat</strong>
		      	<input type="hidden" name="id" value="<?php echo $page_detail['id'];?>">
		      	<input type="hidden" name="merchant_id" value="<?php echo $id.$page_detail['merchant_id'];?>">
		      	<textarea name="alamat" id="" cols="30" rows="10" class="tinymc"><?php echo str_replace_rehtml($page_detail['alamat']);?></textarea>
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
		    <div class="form-group form-group-col-2">
		    	<strong>Kota</strong>
		      	<div class="select-style">
					<span></span>
					<select name="kota" id="" required="required">
						<option value="">Pilih</option>
						<?php
						foreach ($kota as $row_kota) {

							if($page_detail['kota_id'] == $row_kota['id']){
								echo '<option value="'.$row_kota['id'].'|'.$row_kota['nama'].'" selected>'.$row_kota['nama'].'</option>';
							}
							else{
								echo '<option value="'.$row_kota['id'].'|'.$row_kota['nama'].'">'.$row_kota['nama'].'</option>';
							}
						}
					?>
					</select>

				</div>
		    </div>
		    <div class="clearfix"></div>
		    <div class="form-group form-group-col-2">
		    	<strong>Status</strong>
		      	<div class="select-style">
					<span></span>
					<select name="status" id="" required="">
						<?php
						foreach ($status as $i => $row_status) {
							if($page_detail['status'] == $i){
								echo '<option value="'.$i.'" selected>'.$row_status.'</option>';
							}
							else{
								echo '<option value="'.$i.'">'.$row_status.'</option>';
							}
						}
					?>
					</select>

				</div>
		    </div>
		    <div class="clearfix"></div>
		    <br>
		    <div>
		    	<a href="<?php echo base_url('merchant/detail/').$id.$page_detail['merchant_id'];?>" class="btn_cancel close_box">CANCEL</a>
		    	<input type="submit" value="SAVE" class="btn_save close_box">
		    </div>
		</form>
	</div>
</div>

<script>
	$(function() {
	    $("#uploadFile").on("change", function()
	    {
	        var files = !!this.files ? this.files : [];
	        if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support
	 
	        if (/^image/.test( files[0].type)){ // only image file
	            var reader = new FileReader(); // instance of the FileReader
	            reader.readAsDataURL(files[0]); // read the local file
	 
	            reader.onloadend = function(){ // set image data as background of div
	                //$("#imagePreview").css("background-image", "url("+this.result+")");
	                $("#imagePreview").html('<img src="'+this.result+'" height="200"/>');
	            }
	        }
	    });
	});
</script>