


<div class="content_ful">
	<div class="table_head">
			<h1>Produk</h1>
		</div>
	<div class="table_show">
		<form action="<?php echo base_url('produk/update');?>" class="form_1" method="post" enctype="multipart/form-data">
			<div class="form-group">
		      	<strong>Background Image</strong>
		      	<input type="file" name="img_bg" class="uploadFile">
		      	<h6>Image Size:1440x810px (rasio 16:9), max file size 200kb</h6>
		      	<input type="hidden" name="img_bg2" value="<?php echo $page_detail['img3'];?>">
		      	<div class="imagePreview">
		      		<br>
		      		<?php
		      			if($page_detail['img3']){
		      				echo '<img src="../uploads/web/'.$page_detail['img3'].'" alt="" height="200">';
		      			}
		      		?>
				</div>
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>

			<?php foreach ($result as $row) { 
				?>
					<div class="form-group">
				      	<strong>Sub Judul</strong>
				      	<input type="text" name="nama2[]" value="<?php echo $row['nama2'];?>">
				      	<div class="clearfix"></div>
				    </div>
				    <div class="clearfix"></div>
					<div class="form-group">
				      	<strong>Judul</strong>
				      	<input type="hidden" name="id[]" value="<?php echo $row['id'];?>" required="required">
				      	<input type="text" name="nama[]" value="<?php echo $row['nama1'];?>" required="required">
				      	<div class="clearfix"></div>
				    </div>
				    <div class="clearfix"></div>
				    <div class="form-group">
				      	<strong>Deskripsi</strong>
				      	<textarea name="desc[]" id="" cols="30" rows="5" class="tinymc"><?php echo $row['desc'];?> </textarea>
				      	<div class="clearfix"></div>
				    </div>
				    <div class="clearfix"></div>
				    <div class="form-group">
			      	<strong>Primary Phone</strong>
			      	<div style="width:25%; float: left;">
			      		<div class="select-style">
							<span></span>
							<select name="tipe[]" id="" required="">
								<?php
								foreach ($tipe as $i => $row2) {
									if($row['tipe'] == $i){
										echo '<option value="'.$i.'" selected>'.$row2.'</option>';
									}
									else{
										echo '<option value="'.$i.'">'.$row2.'</option>';
									}
								}
							?>
							</select>

						</div>
			      	</div>
			      	<div style="width: 75%; float: right;">
			      		<input type="file" name="img[]" class="uploadFile">
			      		<h6>Image Size:400x600px, max file size 1mb , untuk video ukuran rasio 9:16, format file: mp4</h6>
				      	<input type="hidden" name="img2[]" value="<?php echo $row['img'];?>">
				      	<div class="imagePreview">
				      		<br>
				      		<?php
				      			if($row['img']){
					      			if($row['tipe'] == "0"){
						      			echo '<img src="../uploads/web/'.$row['img'].'" alt="" height="200">';
						      		}
						      		else {
						      			echo'<video width="160" height="320" controls>
										  <source src="../uploads/web/'.$row['img'].'" type="video/mp4">
										</video>';
						      		}
						      	}
				      		?>
						</div>
			      	</div>
			      	
			      	<div class="clearfix"></div>
			    </div>
				    <div class="clearfix"></div>
				    <div class="form-group">
				      	<strong>Secondary Image</strong>
				      	<input type="file" name="img_second[]" class="uploadFile2">
				      	<h6>Image Size:550x210px, max file size 1mb</h6>
				      	<input type="hidden" name="img_second2[]" value="<?php echo $row['img2'];?>" class="img_show">
				      	<div class="imagePreview2">
				      		<?php
				      			if($row['img2']){
				      				echo '<img src="../uploads/web/'.$row['img2'].'" alt="" height="200">';
				      				echo '<img src="'.assets_url('images').'/ico_delete.png" alt="" class="hapus">';
				      			}
				      		?>
				      		
						</div>
				      	<div class="clearfix"></div>
				    </div>
				    <div class="clearfix"></div>
				    <br>
				    <hr>
				    <br>
				<?php
			}
			?>
		    <br><br>
		    
		    <div>
		    	<!-- <a href="<?php echo base_url('about');?>" class="btn_cancel close_box">CANCEL</a> -->
		    	<input type="submit" value="SAVE" class="btn_save close_box">
		    </div>
		</form>
	</div>
</div>

<script>
	$(function() {
	    $(".uploadFile").on("change", function()
	    {

	        var files = !!this.files ? this.files : [];
	        if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support


	        var ini = $(this).parent().find('.imagePreview');
	 
	        if (/^image/.test( files[0].type)){ // only image file
	            var reader = new FileReader(); // instance of the FileReader
	            reader.readAsDataURL(files[0]); // read the local file
	 
	            reader.onloadend = function(){ // set image data as background of div

	                $(ini).html('<img src="'+this.result+'" height="200"/>');
	            }
	        }
	    });

	    $(".uploadFile2").on("change", function()
	    {

	        var files = !!this.files ? this.files : [];
	        if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support


	        var ini = $(this).parent().find('.imagePreview2');
	 
	        if (/^image/.test( files[0].type)){ // only image file
	            var reader = new FileReader(); // instance of the FileReader
	            reader.readAsDataURL(files[0]); // read the local file
	 
	            reader.onloadend = function(){ // set image data as background of div

	                $(ini).html('<img src="'+this.result+'" height="200"/><img src="assets/images/ico_delete.png" alt="" class="hapus">');
	                $(".hapus").click(function() {
				    	$(this).parent().parent().find('.img_show').val("");
				    	$(this).parent().parent().find('.uploadFile2').val("");
				    	$(this).parent().parent().find('.imagePreview2').html("");
				    	
				    	//alert(isi);
				    });
	            }
	        }
	    });

	    $(".hapus").click(function() {
	    	//alert();
	    	$(this).parent().parent().find('.img_show').val("");
	    	$(this).parent().parent().find('.imagePreview2').html("");
	    	
	    	//alert(isi);
	    });
	    

	});
</script>