


<div class="content_ful">
	<div class="table_head">
			<h1>Editorial Halaman Gabung</h1>
		</div>
	<div class="table_show">
		<form action="<?php echo base_url('gabung/update');?>" class="form_1" method="post" enctype="multipart/form-data">
			<div class="form-group">
		      	<strong>Judul Page Gabung</strong>
		      	<input type="text" name="nama" value="<?php echo $page_detail['nama'];?>" required="required">
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
		    <div class="form-group">
		      	<strong>Deskripsi Page Gabung</strong>
		      	<textarea name="desc" id="" cols="30" rows="10" class="tinymc"><?php echo $page_detail['desc'];?></textarea>
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
		    <div class="form-group">
		      	<strong>Cover Image</strong>
		      	<input type="file" name="img" class="uploadFile">
		      	<h6>Image Size:1440x810px (rasio 16:9), max file size 1mb</h6>
		      	<input type="hidden" name="img2" value="<?php echo $page_detail['img'];?>">
		      	<div class="imagePreview" title="tes">
		      		<br>
		      		<?php
		      			if($page_detail['img']){
		      				echo '<img src="../../uploads/web/'.$page_detail['img'].'" alt="" height="200">';
		      			}
		      		?>
				</div>
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
		    <div class="form-group">
		      	<strong>Judul Notifikasi Sukses</strong>
		      	<input type="text" name="nama1" value="<?php echo $page_detail['nama1'];?>" required="required">
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
		    <div class="form-group">
		      	<strong>Deskripsi Notifikasi Sukses</strong>
		      	<textarea name="teaser" id="" cols="30" rows="10" class="tinymc"><?php echo $page_detail['teaser'];?></textarea>
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>

		    <div class="form-group">
		      	<strong>Judul Email Terkirim</strong>
		      	<input type="text" name="nama2" value="<?php echo $page_detail['nama2'];?>" required="required">
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
		    <div class="form-group">
		      	<strong>Deskripsi Email Terkirim</strong>
		      	<textarea name="map" id="" cols="30" rows="10" class="tinymc"><?php echo $page_detail['map'];?></textarea>
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
		    
		    <br>
		    <div>
		    	<input type="submit" value="SAVE" class="btn_save close_box">
		    </div>
		</form>
	</div>
</div>

<script>
	$(function() {
	    $(".uploadFile").on("change", function()
	    {

	        var files = !!this.files ? this.files : [];
	        if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support


	        var ini = $(this).parent().find('.imagePreview');
	 
	        if (/^image/.test( files[0].type)){ // only image file
	            var reader = new FileReader(); // instance of the FileReader
	            reader.readAsDataURL(files[0]); // read the local file
	 
	            reader.onloadend = function(){ // set image data as background of div

	                $(ini).html('<img src="'+this.result+'" height="200"/>');
	            }
	        }
	    });

	    $(".uploadFile2").on("change", function()
	    {

	        var files = !!this.files ? this.files : [];
	        if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support


	        var ini = $(this).parent().find('.imagePreview2');
	 
	        if (/^image/.test( files[0].type)){ // only image file
	            var reader = new FileReader(); // instance of the FileReader
	            reader.readAsDataURL(files[0]); // read the local file
	 
	            reader.onloadend = function(){ // set image data as background of div

	                $(ini).html('<img src="'+this.result+'" height="200"/><img src="assets/images/ico_delete.png" alt="" class="hapus">');
	                $(".hapus").click(function() {
				    	$(this).parent().parent().find('.img_show').val("");
				    	$(this).parent().parent().find('.uploadFile2').val("");
				    	$(this).parent().parent().find('.imagePreview2').html("");
				    	
				    	//alert(isi);
				    });
	            }
	        }
	    });

	    $(".hapus").click(function() {
	    	$(this).parent().parent().find('.img_show').val("");
	    	$(this).parent().parent().find('.imagePreview').html("");
	    	
	    	//alert(isi);
	    });
	    

	});
</script>