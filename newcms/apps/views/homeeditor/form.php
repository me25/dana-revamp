


<div class="content_ful">
	<div class="table_head">
			<h1>Home Editor</h1>
		</div>
	<div class="table_show">
		<form action="<?php echo base_url('homeeditor/update');?>" class="form_1" method="post" enctype="multipart/form-data">
		    <div class="form-group">
		      	<strong>Judul 1</strong>
		      	<input type="text" name="nama" value="<?php echo $page_detail['nama'];?>" required="required">
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
		    <div class="form-group">
		      	<strong>Judul 2</strong>
		      	<input type="text" name="nama1" value="<?php echo $page_detail['nama1'];?>" required="required">
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
		    <div class="form-group">
		      	<strong>Set Aktif Image/Video Cover</strong>
		      	<div class="form-group" style="width:25%; float:left;">
		      		<div class="select-style">
						<span></span>
						<select name="tipe" id="" required="">
							<?php
							foreach ($tipe as $i => $row) {
								if($page_detail['tipe'] == $i){
									echo '<option value="'.$i.'" selected>'.$row.'</option>';
								}
								else{
									echo '<option value="'.$i.'">'.$row.'</option>';
								}
							}
						?>
						</select>

					</div>
		      	</div>
		      	<div class="clearfix"></div>
		    </div>
		    <div class="form-group">
		      	<strong>Image Cover</strong>
		      	<input type="file" name="img" class="uploadFile">
		      	<h6>Image Size:1440x810px (rasio 16:9), max file size 1mb</h6>
		      	<input type="hidden" name="img2" value="<?php echo $page_detail['img'];?>">
		      	<div class="imagePreview">
		      		<br>
		      		<?php
		      			if($page_detail['img']){
		      				echo '<img src="../uploads/web/'.$page_detail['img'].'" alt="" height="200">';
		      			}
		      		?>
				</div>
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
		    <div class="form-group">
		      	<strong>Youtube Video Cover ID</strong>
		      	<input type="text" name="url" value="<?php echo $page_detail['url'];?>" required="required">
		      	<h6>Sample url:https://www.youtube.com/watch?v=paVI2-GA3Eo, yg di copy paste "paVI2-GA3Eo"</h6>
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>

		    <div class="form-group">
		      	<strong>Image/Video on Phone</strong>
		      	<div style="width: 20%; float: left;">
		      		<div class="select-style">
						<span></span>
						<select name="tipe2" id="" required="">
							<?php
							foreach ($tipe as $i => $row) {
								if($page_detail['tipe2'] == $i){
									echo '<option value="'.$i.'" selected>'.$row.'</option>';
								}
								else{
									echo '<option value="'.$i.'">'.$row.'</option>';
								}
							}
						?>
						</select>

					</div>
		      	</div>
		      	<div style="width: 75%; float: right;">
		      		<input type="file" name="img_phone" class="uploadFile">
		      		<h6>Image Size:360x740px (rasio 9:19), untuk video format file: mp4</h6>
			      	<input type="hidden" name="img_phone2" value="<?php echo $page_detail['img2'];?>">
			      	<div class="imagePreview">
			      		<br>
			      		<?php
			      			if($page_detail['img2']){
				      			if($page_detail['tipe2'] == "0"){
					      			echo '<img src="../uploads/web/'.$page_detail['img2'].'" alt="" height="200">';
					      		}
					      		else {
					      			echo'<video width="160" height="320" controls>
									  <source src="../uploads/web/'.$page_detail['img2'].'" type="video/mp4">
									</video>';
					      		}
					      	}
			      		?>
					</div>
		      	</div>
		      	
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
		    <br><br><hr><br>
		    <h2>Transaksi dengan rasa Aman</h2>
		    <?php foreach ($home_transaksi as $row) { 
			?>
				<br>
				<div class="form-group">
			      	<strong>Fitur</strong>
			      	<input type="hidden" name="id_transaksi[]" value="<?php echo $row['id'];?>" required="required">
			      	<input type="text" name="nama_transaksi[]" value="<?php echo $row['nama1'];?>">
			      	<div class="clearfix"></div>
			    </div>
			    <div class="clearfix"></div>
			    <div class="form-group">
			      	<strong>Teaser Text</strong>
			      	<input type="text" name="desc_transaksi[]" value="<?php echo $row['desc'];?>">
			      	<div class="clearfix"></div>
			    </div>
			    <div class="clearfix"></div>
				<div class="form-group">
			      	<strong>Image</strong>
			      	<input type="file" name="img_transaksi[]" class="uploadFile file150">
			      	<input type="hidden" name="img_transaksi2[]" value="<?php echo $row['img'];?>">
			      	<h6>Max file size: 150kb</h6>
			      	<div class="imagePreview" title="tes">
			      		<?php
			      			if($row['img']){
			      				echo '<img src="../uploads/web/'.$row['img'].'" alt="" height="200">';
			      			}
			      		?>
					</div>
			      	<div class="clearfix"></div>
			    </div>
			    <div class="clearfix"></div>
			    
			 <?php } ?>
			 <br><br><hr><br>
			 <h2>DANA & Secure</h2>
		    <?php foreach ($home_fitur as $row_fitur) { 
			?>
				<br>
				<div class="form-group">
			      	<strong>Fitur</strong>
			      	<input type="hidden" name="id_fitur[]" value="<?php echo $row_fitur['id'];?>" required="required">
			      	<input type="text" name="nama_fitur[]" value="<?php echo $row_fitur['nama1'];?>">
			      	<div class="clearfix"></div>
			    </div>
			    <div class="clearfix"></div>
			    <div class="form-group">
			      	<strong>Teaser Text</strong>
			      	<input type="text" name="desc_fitur[]" value="<?php echo $row_fitur['desc'];?>">
			      	<div class="clearfix"></div>
			    </div>
			    <div class="clearfix"></div>
				<div class="form-group">
		      	<strong>Image/Video on Phone</strong>
		      	<div style="width: 20%; float: left;">
		      		<div class="select-style">
						<span></span>
						<select name="tipe3[]" id="" required="">
							<?php
							foreach ($tipe as $i => $row) {
								if($row_fitur['tipe'] == $i){
									echo '<option value="'.$i.'" selected>'.$row.'</option>';
								}
								else{
									echo '<option value="'.$i.'">'.$row.'</option>';
								}
							}
						?>
						</select>

					</div>
		      	</div>
		      	<div style="width: 75%; float: right;">
		      		<input type="file" name="img_fitur[]" class="uploadFile">
		      		<h6>Image Size:360x740px (rasio 9:19), untuk video format file: mp4, max file size:5mb</h6>
			      	<input type="hidden" name="img_fitur2[]" value="<?php echo $row_fitur['img'];?>">
			      	<div class="imagePreview">
			      		<br>
			      		<?php
			      			if($row_fitur['img']){
				      			if($row_fitur['tipe'] == "0"){
					      			echo '<img src="../uploads/web/'.$row_fitur['img'].'" alt="" height="200">';
					      		}
					      		else {
					      			echo'<video width="160" height="320" controls>
									  <source src="../uploads/web/'.$row_fitur['img'].'" type="video/mp4">
									</video>';
					      		}
					      	}
			      		?>
					</div>
		      	</div>
		      	
		      	<div class="clearfix"></div>
		    </div>
			    <div class="clearfix"></div>
			    
			 <?php } ?>
		   
		   
		    <br>
		    <div>
		    	<input type="submit" value="SAVE" class="btn_save close_box">
		    </div>
		</form>
	</div>
</div>

<script>
	$(function() {
	    $(".uploadFile").on("change", function()
	    {

	        var files = !!this.files ? this.files : [];
	        if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support


	        var ini = $(this).parent().find('.imagePreview');
	 
	        if (/^image/.test( files[0].type)){ // only image file
	            var reader = new FileReader(); // instance of the FileReader
	            reader.readAsDataURL(files[0]); // read the local file
	 
	            reader.onloadend = function(){ // set image data as background of div

	                $(ini).html('<img src="'+this.result+'" height="200"/>');
	            }
	        }
	    });

	    $(".uploadFile2").on("change", function()
	    {

	        var files = !!this.files ? this.files : [];
	        if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support


	        var ini = $(this).parent().find('.imagePreview2');
	 
	        if (/^image/.test( files[0].type)){ // only image file
	            var reader = new FileReader(); // instance of the FileReader
	            reader.readAsDataURL(files[0]); // read the local file
	 
	            reader.onloadend = function(){ // set image data as background of div

	                $(ini).html('<img src="'+this.result+'" height="200"/><img src="assets/images/ico_delete.png" alt="" class="hapus">');
	                $(".hapus").click(function() {
				    	$(this).parent().parent().find('.img_show').val("");
				    	$(this).parent().parent().find('.uploadFile2').val("");
				    	$(this).parent().parent().find('.imagePreview2').html("");
				    	
				    	//alert(isi);
				    });
	            }
	        }
	    });

	    $(".hapus").click(function() {
	    	$(this).parent().parent().find('.img_show').val("");
	    	$(this).parent().parent().find('.imagePreview').html("");
	    	
	    	//alert(isi);
	    });

	     $(".file150").on("change", function()
	    {
	        if(this.files[0].size > 150000){
		       alert("File is too big!");
		       this.value = "";
		    };
	    });
	    $(".file250").on("change", function()
	    {
	        if(this.files[0].size > 250000){
		       alert("File is too big!");
		       this.value = "";
		    };
	    });
	    $(".file500").on("change", function()
	    {
	        if(this.files[0].size > 500000){
		       alert("File is too big!");
		       this.value = "";
		    };
	    });
	    

	});
</script>