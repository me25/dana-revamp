<!doctype html>	
<?php if ($this->session->userdata('logged_in')) { ?>
<html>
<head>
	<?php $this->load->view('includes/head')?>	
</head>
<body class="basic">
<?php
if (isset($content) && !empty($content)) {
  $this->load->view($content);
}
?>
<div class="clearfix"></div>
<?php $this->load->view('includes/js')?>
</body>
</html>
<?php

}
else{
	redirect(base_url("login"));
}
?>