<!doctype html>	
<?php if ($this->session->userdata('logged_in')) { ?>
<html>
<head>
	<?php $this->load->view('includes/head')?>	
</head>
<body class="pop1">
	<div class="frame_pop">
		<a href="#"><img src="" alt=""></a>
		<a href="#" class="close_pop_btn close_box_in tip-left" data-tip="Tutup"><img src="<?php echo assets_url('img')?>/ico_close.png" alt=""></a>
<?php 
	if ($this->uri->segment(1) == 'login') {
	}
	else{}
?>
<?php
if (isset($content) && !empty($content)) {
  $this->load->view($content);
}
?>
</div>
<div class="clearfix"></div>
<?php $this->load->view('includes/js')?>
<script>
function myFunction() {
    // Declare variables
    var input, filter, ul, li, a, i;
    input = document.getElementById('nama');
    filter = input.value.toUpperCase();
    ul = document.getElementById("myUL");
    li = ul.getElementsByTagName('li');

    // Loop through all list items, and hide those who don't match the search query
    for (i = 0; i < li.length; i++) {
        a = li[i].getElementsByTagName("strong")[0];
        if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
        } else {
            li[i].style.display = "none";
        }
    }
}
var config = {
  '.chosen-select'           : {},
  '.chosen-select-deselect'  : { allow_single_deselect: true },
  '.chosen-select-no-single' : { disable_search_threshold: 10 },
  '.chosen-select-no-results': { no_results_text: 'Oops, nothing found!' },
  '.chosen-select-rtl'       : { rtl: true },
  '.chosen-select-width'     : { width: '95%' }
}
for (var selector in config) {
  $(selector).chosen(config[selector]);
}
</script>
</body>
</html>
<?php

}
else{
	redirect(base_url("login"));
}
?>