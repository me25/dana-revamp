


<div class="content_ful">
	<div class="table_head">
			<h1>Setting</h1>
		</div>
	<div class="table_show">
		<form action="<?php echo base_url('setting/update');?>" class="form_1" method="post" enctype="multipart/form-data">
		    <div class="form-group">
		      	<strong>Email</strong>
		      	<input type="email" name="email" value="<?php echo $page_detail['email'];?>">
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>

		    <div class="form-group">
		      	<strong>Facebook URL</strong>
		      	<input type="text" name="fb" value="<?php echo $page_detail['fb'];?>">
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
		   
		    <div class="form-group">
		      	<strong>Twitter URL</strong>
		      	<input type="text" name="tw" value="<?php echo $page_detail['tw'];?>">
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>

		    <div class="form-group">
		      	<strong>Youtube URL</strong>
		      	<input type="text" name="yt" value="<?php echo $page_detail['yt'];?>">
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>

		    <div class="form-group">
		      	<strong>Instagram URL</strong>
		      	<input type="text" name="ig" value="<?php echo $page_detail['ig'];?>">
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>

		    <div class="form-group">
		      	<strong>Appstore URL</strong>
		      	<input type="text" name="appstore" value="<?php echo $page_detail['appstore'];?>">
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>

		    <div class="form-group">
		      	<strong>Playstore URL</strong>
		      	<input type="text" name="playstore" value="<?php echo $page_detail['playstore'];?>">
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>

		   
		    <br>
		    <div>
		    	<!-- <a href="<?php echo base_url('about');?>" class="btn_cancel close_box">CANCEL</a> -->
		    	<input type="submit" value="SAVE" class="btn_save close_box">
		    </div>
		</form>
	</div>
</div>

<script>
	$(function() {
	    $("#uploadFile").on("change", function()
	    {
	        var files = !!this.files ? this.files : [];
	        if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support
	 
	        if (/^image/.test( files[0].type)){ // only image file
	            var reader = new FileReader(); // instance of the FileReader
	            reader.readAsDataURL(files[0]); // read the local file
	 
	            reader.onloadend = function(){ // set image data as background of div
	                //$("#imagePreview").css("background-image", "url("+this.result+")");
	                $("#imagePreview").html('<img src="'+this.result+'" height="200"/>');
	            }
	        }
	    });

	});
</script>