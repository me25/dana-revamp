
<div class="content_ful">
	<div class="table_show">
		<div class="table_head">
			<div class="info"><?php echo $num;?> Merchant <?php echo $page_detail['nama'];?></div>
			<a href="<?php echo base_url('merchant/store_add/').$id;?>" class="btn_add">+ Tambah</a>
			<div class="clearfix"></div>
		</div>
		<table id="table_sort" class="table_style" cellspacing="0" width="100%"data-page-length="10" >
		    <thead>
		        <tr>
		            <th width="50%">Alamat</th>
		            <th width="20%">Kota</th>
		            <th>Status</th>
		            <th width="50px" class="arrow_non">Action</th>
		        </tr>
		    </thead>
		    <tbody>
		    	<?php foreach ($list as $row) { 
		    		?>
		    		<tr>
			            <td width="50%"><?php echo $row['alamat'];?></td>
			            <td>
			            	<?php
			            		$kota_query= $this->Modglobal->find('kota', array('id' => $row['kota_id']));
								$kota = $kota_query->row_array();

								echo $kota['nama'];
			            	?>
			            </td>
			            <td><?php echo $status[$row['status']];?></td>
			            <td class="action">
			            	<a href="<?php echo base_url('merchant/store_edit/').$row['id'];?>"><img src="<?php echo assets_url('images');?>/ico_edit.png" alt=""></a>
			            	<a href="<?php echo base_url('merchant/store_delete/').$row['id'].'/'.$row['merchant_id'];?>" class="delete"><img src="<?php echo assets_url('images');?>/ico_delete.png" alt=""></a>
			            </td>
			        </tr>

		    		<?php

		    	}?>
		        
		        
		        
		    </tbody>
		</table>
	</div>
</div>