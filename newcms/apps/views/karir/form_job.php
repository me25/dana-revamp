
<div class="content_ful">
	<div class="table_show">
		<?php
			if($form == "add"){
				$title = "Tambah";
				$action = 'karir/job_pro';
			}
			else{
				$title = "Edit";
				$action = 'karir/job_update';
			}
		?>
		<div class="table_head">
			<div class="info"><h2><?php echo $title;?></h2></div>
			<div class="clearfix"></div>
		</div>


		<hr color="#eee">
		<form action="<?php echo base_url($action);?>" class="form_1" method="post"   enctype="multipart/form-data">
		    <div class="form-group form-group-col-2">
		      	<strong>Nama</strong>
		      	<input type="hidden" name="id" value="<?php echo $page_detail['id'];?>">
		      	<input type="text" name="nama" value="<?php echo $page_detail['nama'];?>" required="required">
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
		    <div class="form-group form-group-col-2">
		      	<strong>Image Cover</strong>
		      	<input type="file" name="img_thumb" class="uploadFile file250">
		      	<input type="hidden" name="img_thumb2" value="<?php echo $page_detail['img2'];?>">
		      	<h6>Image Size:1440x360px, max file size 250kb</h6>
		      	<div class="imagePreview">
		      		<br>
		      		<?php
		      			if($page_detail['img2']){
		      				echo '<img src="../../../uploads/karir/'.$page_detail['img2'].'" alt="" height="200">';
		      			}
		      		?>
				</div>
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
		    <div class="form-group form-group-col-2">
		      	<strong>Image Thumbnail </strong>
		      	<input type="file" name="img" class="uploadFile file150">
		      	<input type="hidden" name="img2" value="<?php echo $page_detail['img'];?>">
		      	<h6>Image Size:480x270px (rasio 16:9), max file size 150kb</h6>
		      	<div class="imagePreview">
		      		<br>
		      		<?php
		      			if($page_detail['img']){
		      				echo '<img src="../../../uploads/karir/'.$page_detail['img'].'" alt="" height="200">';
		      			}
		      		?>
				</div>
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
		    <div class="form-group">
		      	<strong>Deskripsi</strong>
		      	<textarea name="desc" id="" cols="30" rows="5" class=""><?php echo $page_detail['desc'];?></textarea>
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
		    <div class="form-group form-group-col-2">
		    	<strong>Status</strong>
		      	<div class="select-style">
					<span></span>
					<select name="status" id="" required="">
						<?php
						foreach ($status as $i => $row_status) {
							if($page_detail['status'] == $i){
								echo '<option value="'.$i.'" selected>'.$row_status.'</option>';
							}
							else{
								echo '<option value="'.$i.'">'.$row_status.'</option>';
							}
						}
					?>
					</select>

				</div>
		    </div>
		    <div class="clearfix"></div>
		    <br>
		    <div>
		    	<a href="<?php echo base_url('karir/job');?>" class="btn_cancel close_box">CANCEL</a>
		    	<input type="submit" value="SAVE" class="btn_save close_box">
		    </div>
		</form>
	</div>
</div>

<div id="pop_box2" class="pop_box" style="display:none;">
	<div class="popbox_bg_close"></div>
	
</div>

<script>
	$(function() {
	    $(".uploadFile").on("change", function()
	    {

	        var files = !!this.files ? this.files : [];
	        if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support


	        var ini = $(this).parent().find('.imagePreview');
	 
	        if (/^image/.test( files[0].type)){ // only image file
	            var reader = new FileReader(); // instance of the FileReader
	            reader.readAsDataURL(files[0]); // read the local file
	 
	            reader.onloadend = function(){ // set image data as background of div

	                $(ini).html('<img src="'+this.result+'" height="200"/>');
	            }
	        }
	    });

	    $(".uploadFile2").on("change", function()
	    {

	        var files = !!this.files ? this.files : [];
	        if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support


	        var ini = $(this).parent().find('.imagePreview2');
	 
	        if (/^image/.test( files[0].type)){ // only image file
	            var reader = new FileReader(); // instance of the FileReader
	            reader.readAsDataURL(files[0]); // read the local file
	 
	            reader.onloadend = function(){ // set image data as background of div

	                $(ini).html('<img src="'+this.result+'" height="200"/><img src="assets/images/ico_delete.png" alt="" class="hapus">');
	                $(".hapus").click(function() {
				    	$(this).parent().parent().find('.img_show').val("");
				    	$(this).parent().parent().find('.uploadFile2').val("");
				    	$(this).parent().parent().find('.imagePreview2').html("");
				    	
				    	//alert(isi);
				    });
	            }
	        }
	    });

	    $(".hapus").click(function() {
	    	$(this).parent().parent().find('.img_show').val("");
	    	$(this).parent().parent().find('.imagePreview').html("");
	    	
	    	//alert(isi);
	    });
	     $(".file150").on("change", function()
	    {
	        if(this.files[0].size > 150000){
		       alert("File is too big!");
		       this.value = "";
		    };
	    });
	    $(".file250").on("change", function()
	    {
	        if(this.files[0].size > 250000){
		       alert("File is too big!");
		       this.value = "";
		    };
	    });
	    $(".file500").on("change", function()
	    {
	        if(this.files[0].size > 500000){
		       alert("File is too big!");
		       this.value = "";
		    };
	    });

	});
</script>