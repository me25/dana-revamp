
<div class="content_ful">
	<div class="table_show">
		<?php
			$title = "Page Editor";
			$action = 'karir/editorupdate';
		?>
		<div class="table_head">
			<div class="info"><h2><?php echo $title;?></h2></div>
			<div class="clearfix"></div>
		</div>


		<hr color="#eee">
		<form action="<?php echo base_url($action);?>" class="form_1" method="post"   enctype="multipart/form-data">
		    <div class="form-group">
		      	<strong>Judul</strong>
		      	<input type="text" name="nama" value="<?php echo $page_detail['nama'];?>" required="required">
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
		    <div class="form-group">
		      	<strong>Deskripsi</strong>
		      	<textarea name="desc" id="" cols="30" rows="5" class="tinymc"><?php echo $page_detail['desc'];?></textarea>
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
		    <div class="form-group">
		      	<strong>Image</strong>
		      	<input type="file" name="img" id="uploadFile">
		      	<h6>Image Size:1440x576px (rasio 3:1), max file size 1mb</h6>
		      	<input type="hidden" name="img2" value="<?php echo $page_detail['img'];?>">
		      	<div id="imagePreview">
		      		<?php
		      			if($page_detail['img']){
		      				echo '<img src="../../uploads/karir/'.$page_detail['img'].'" alt="" height="200">';
		      			}
		      		?>
				</div>
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
		    <?php foreach ($karir as $row) { 
			?>
				<br>
				<hr>
				<div class="form-group">
			      	<strong>Fitur</strong>
			      	<input type="hidden" name="id_fitur[]" value="<?php echo $row['id'];?>" required="required">
			      	<input type="text" name="nama1[]" value="<?php echo $row['nama1'];?>">
			      	<div class="clearfix"></div>
			    </div>
			    <div class="clearfix"></div>
			    <div class="form-group">
			      	<strong>Teaser Text</strong>
			      	<input type="text" name="desc1[]" value="<?php echo $row['desc'];?>">
			      	<div class="clearfix"></div>
			    </div>
			    <div class="clearfix"></div>
				<div class="form-group">
			      	<strong>Image</strong>
			      	<input type="file" name="img_fitur[]" class="uploadFile">
			      	<input type="hidden" name="img_fitur2[]" value="<?php echo $row['img'];?>">
			      	<div class="imagePreview" title="tes">
			      		<?php
			      			if($row['img']){
			      				echo '<img src="../../uploads/karir/'.$row['img'].'" alt="" height="200">';
			      			}
			      		?>
					</div>
			      	<div class="clearfix"></div>
			    </div>
			    <div class="clearfix"></div>
			    
			<?php } ?>
		    <br>
		    <div>
		    	<a href="<?php echo base_url('karir/editor');?>" class="btn_cancel close_box">CANCEL</a>
		    	<input type="submit" value="SAVE" class="btn_save close_box">
		    </div>
		</form>
	</div>
</div>

<div id="pop_box2" class="pop_box" style="display:none;">
	<div class="popbox_bg_close"></div>
	
</div>