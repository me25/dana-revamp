
<div class="content_ful">
	<div class="table_show">
		<?php
				$title = 'Setting Posisi Kota ';
				$action = 'karir/posisikota_update';
		?>
		<div class="table_head">
			<div class="info"><h2><?php echo $title;?></h2></div>
			<div class="clearfix"></div>
		</div>


		<hr color="#eee">
		<form action="<?php echo base_url($action);?>" class="form_1" method="post"  enctype="multipart/form-data">
			<?php
				for($i = 1; $i<=$num; $i++) {
			    	?>
			    	<div class="form-group">
					<div style="width: 5%; float: left;">
						<?php echo $i;?>
						<input type="hidden" name="id_posisi[]" value="<?php echo $i;?>">
					</div>
					<div style="width: 80%; float: left;">
				      	<div class="select-style">
							<span></span>
							<select name="karir_id[]" id="" required="">
								<?php
								echo '<option value="0" selected>Kosong</option>';
								foreach ($city as $row) {
									if($row['posisi'] == $i){
										echo '<option value="'.$row['id'].'" selected>'.$row['nama'].'</option>';
									}
									else{
										echo '<option value="'.$row['id'].'">'.$row['nama'].'</option>';
									}
								}
							?>
							</select>

						</div>
					</div>
			    </div>
			    <div class="clearfix"></div>
			    <br>
			    	<?php
				}
			?>
			
		    <br>
		    <div>
		    	<a href="<?php echo base_url('merchant');?>" class="btn_cancel close_box">CANCEL</a>
		    	<input type="submit" value="SAVE" class="btn_save close_box">
		    </div>
		</form>
	</div>
</div>

<script>
	$(function() {
	    $("#uploadFile").on("change", function()
	    {
	        var files = !!this.files ? this.files : [];
	        if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support
	 
	        if (/^image/.test( files[0].type)){ // only image file
	            var reader = new FileReader(); // instance of the FileReader
	            reader.readAsDataURL(files[0]); // read the local file
	 
	            reader.onloadend = function(){ // set image data as background of div
	                //$("#imagePreview").css("background-image", "url("+this.result+")");
	                $("#imagePreview").html('<img src="'+this.result+'" height="200"/>');
	            }
	        }
	    });
	});
</script>