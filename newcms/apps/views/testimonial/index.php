
<div class="content_ful">
	<div class="table_show">
		<div class="table_head">
			<div class="info"><?php echo $num;?> Testimonial Customer</div>
			<a href="<?php echo base_url('testimonial/add');?>" class="btn_add">+ Tambah</a>
			<div class="clearfix"></div>
		</div>
		<table id="table_sort" class="table_style" cellspacing="0" width="100%"data-page-length="10" >
		    <thead>
		        <tr>
		            <th>Nama</th>
		            <th>Image</th>
		            <th>Testimonial</th>
		            <th>Status</th>
		            <th width="50px" class="arrow_non">Action</th>
		        </tr>
		    </thead>
		    <tbody>
		    	<?php foreach ($result as $row) { 
		    		?>
		    		<tr>
			            <td><?php echo $row['nama'];?></td>
			            <td>
			            	<?php
				      			if($row['img']){
				      				echo '<img src="../uploads/testimonial/'.$row['img'].'" alt="" height="80">';
				      			}
				      		?>
			            </td>
			            <td><?php echo $row['desc'];?></td>
			            <td><?php echo $status[$row['status']];?></td>
			            <td class="action">
			            	<a href="<?php echo base_url('testimonial/edit/').$row['id'];?>"><img src="<?php echo assets_url('images');?>/ico_edit.png" alt=""></a>
			            	<a href="<?php echo base_url('testimonial/delete/').$row['id'];?>" class="delete"><img src="<?php echo assets_url('images');?>/ico_delete.png" alt=""></a>
			            </td>
			        </tr>

		    		<?php

		    	}?>
		        
		        
		        
		    </tbody>
		</table>
	</div>
</div>