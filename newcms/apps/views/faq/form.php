
<div class="content_ful">
	<div class="table_show">
		<?php
			if($form == "add"){
				$title = "Add FAQ";
				$action = 'faq/add_pro';
			}
			else{
				$title = "Edit FAQ";
				$action = 'faq/update';
			}
		?>
		<div class="table_head">
			<div class="info"><h2><?php echo $title;?></h2></div>
			<div class="clearfix"></div>
		</div>


		<hr color="#eee">
		<form action="<?php echo base_url($action);?>" class="form_1" method="post">
			<div class="form-group form-group-col-2">
		    	<strong>Category</strong>
		      	<div class="select-style">
					<span></span>
					<select name="cate" id="" required="">
						<?php
						foreach ($cate as $row_cate) {
							if($page_detail['cate_id'] == $row_cate['id']){
								echo '<option value="'.$row_cate['id'].'" selected>'.$row_cate['nama'].'</option>';
							}
							else{
								echo '<option value="'.$row_cate['id'].'">'.$row_cate['nama'].'</option>';
							}
						}
					?>
					</select>

				</div>
		    </div>
		    <div class="clearfix"></div>
		    <div class="form-group form-group-col-2">
		      	<strong>Tanya</strong>
		      	<input type="hidden" name="id" value="<?php echo $page_detail['id'];?>">
		      	<input type="text" name="tanya" value="<?php echo $page_detail['tanya'];?>" required="required">
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
		   
		    <div class="form-group  form-group-col-2">
		      	<strong>Jawab</strong>
		      	<textarea name="jawab" id="" cols="30" rows="10" class="tinymc"><?php echo $page_detail['jawab'];?></textarea>
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
		    
		    <div class="form-group form-group-col-2">
		    	<strong>Status</strong>
		      	<div class="select-style">
					<span></span>
					<select name="status" id="" required="">
						<?php
						foreach ($status as $i => $row_status) {
							if($member['status'] == $i){
								echo '<option value="'.$i.'" selected>'.$row_status.'</option>';
							}
							else{
								echo '<option value="'.$i.'">'.$row_status.'</option>';
							}
						}
					?>
					</select>

				</div>
		    </div>
		    <div class="clearfix"></div>
		    <br>
		    <div>
		    	<a href="<?php echo base_url('member');?>" class="btn_cancel close_box">CANCEL</a>
		    	<input type="submit" value="SAVE" class="btn_save close_box">
		    </div>
		</form>
	</div>
</div>

<div id="pop_box2" class="pop_box" style="display:none;">
	<div class="popbox_bg_close"></div>
	
</div>