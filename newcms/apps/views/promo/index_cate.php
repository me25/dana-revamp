
<div class="content_ful">
	<div class="table_show">
		<div class="table_head">
			<div class="info"><?php echo $num;?> Kategori</div>
			<a href="<?php echo base_url('promo/cate_add');?>" class="btn_add">+ Tambah</a>
			<div class="clearfix"></div>
		</div>
		<table id="table_sort" class="table_style" cellspacing="0" width="100%"data-page-length="10" >
		    <thead>
		        <tr>
		            <th width="70%">Nama</th>
		            <th>Status</th>
		            <th width="50px" class="arrow_non">Action</th>
		        </tr>
		    </thead>
		    <tbody>
		    	<?php foreach ($result as $row) { 
		    		?>
		    		<tr class="box_modal_full2" alt="member_detail.php">
			            <td><?php echo $row['nama'];?></td>
			            <td><?php echo $status[$row['status']];?></td>
			            <td class="action">
			            	<a href="<?php echo base_url('promo/cate_edit/').$row['id'];?>"><img src="<?php echo assets_url('images');?>/ico_edit.png" alt=""></a>
			            	<a href="<?php echo base_url('promo/cate_delete/').$row['id'];?>" class="delete"><img src="<?php echo assets_url('images');?>/ico_delete.png" alt=""></a>
			            </td>
			        </tr>

		    		<?php

		    	}?>
		        
		        
		        
		    </tbody>
		</table>
	</div>
</div>