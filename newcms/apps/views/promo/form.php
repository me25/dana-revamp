
<div class="content_ful">
	<div class="table_show">
		<?php
			if($form == "add"){
				$title = "Tambah";
				$action = 'promo/add_pro';
			}
			else{
				$title = "Edit";
				$action = 'promo/update';
			}
		?>
		<div class="table_head">
			<div class="info"><h2><?php echo $title;?></h2></div>
			<div class="clearfix"></div>
		</div>


		<hr color="#eee">
		<form action="<?php echo base_url($action);?>" class="form_1" method="post"   enctype="multipart/form-data">
			<input type="hidden" name="id" value="<?php echo $page_detail['id'];?>">
			<div class="form-group form-group-col-2">
		    	<strong>Kategori</strong>
		      	<div class="select-style">
					<span></span>
					<select name="cate" id="" required="">
						<?php
						foreach ($cate as $row_cate) {
							if($page_detail['cate'] == $row_cate['id']){
								echo '<option value="'.$row_cate['id'].'" selected>'.$row_cate['nama'].'</option>';
							}
							else{
								echo '<option value="'.$row_cate['id'].'">'.$row_cate['nama'].'</option>';
							}
						}
					?>
					</select>

				</div>
		    </div>
		    <div class="clearfix"></div>
		    <div class="form-group form-group-col-2">
		      	<strong>Judul</strong>
		      	
		      	<input type="text" name="nama" value="<?php echo $page_detail['nama'];?>" required="required">
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
		    <div class="form-group form-group-col-2">
		      	<strong>Image Cover</strong>
		      	<input type="file" name="img_thumb" class="uploadFile file250">
		      	<input type="hidden" name="img_thumb2" value="<?php echo $page_detail['img2'];?>">
		      	<h6>Image Size:1440x575px, max file size 250kb</h6>
		      	<div class="imagePreview">
		      		<br>
		      		<?php
		      			if($page_detail['img2']){
		      				echo '<img src="../../../uploads/promo/'.$page_detail['img2'].'" alt="" height="200">';
		      			}
		      		?>
				</div>
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
		    <div class="form-group form-group-col-2">
		      	<strong>Image Thumbnail </strong>
		      	<input type="file" name="img" class="uploadFile file150">
		      	<input type="hidden" name="img2" value="<?php echo $page_detail['img'];?>">
		      	<h6>Image Size:480x270px (rasio 16:9), max file size 150kb</h6>
		      	<div class="imagePreview">
		      		<br>
		      		<?php
		      			if($page_detail['img']){
		      				echo '<img src="../../../uploads/promo/'.$page_detail['img'].'" alt="" height="200">';
		      			}
		      		?>
				</div>
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>

		    <div class="form-group">
		      	<strong>Deskripsi Singkat</strong>
		      	<textarea name="teaser" id="" cols="30" rows="10" class="tinymc"><?php echo $page_detail['teaser'];?></textarea>
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
		    
		    <div class="form-group">
		      	<strong>Deskripsi</strong>
		      	<textarea name="desc" id="" cols="30" rows="10" class="tinymc"><?php echo $page_detail['desc'];?></textarea>
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
		    <div class="form-group" style="width: 20%;">
		      	<strong>Tanggal Publish</strong>
		      	
		      	<input type="date" name="date_publish" value="<?php echo $page_detail['date_publish'];?>" required="required">
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
		    <div class="form-group" style="width: 20%;">
		      	<strong>Tanggal Berakhir</strong>
		      	
		      	<input type="date" name="date_end" value="<?php echo $page_detail['date_end'];?>">
		      	<div class="clearfix"></div>
		    </div>
		    <div class="clearfix"></div>
		    <div class="form-group form-group-col-2">
		    	<strong>Status</strong>
		      	<div class="select-style">
					<span></span>
					<select name="status" id="" required="">
						<?php
						foreach ($status as $i => $row_status) {
							if($page_detail['status'] == $i){
								echo '<option value="'.$i.'" selected>'.$row_status.'</option>';
							}
							else{
								echo '<option value="'.$i.'">'.$row_status.'</option>';
							}
						}
					?>
					</select>

				</div>
		    </div>
		    <div class="clearfix"></div>
		    <br>
		    <div>
		    	<a href="<?php echo base_url('promo');?>" class="btn_cancel close_box">CANCEL</a>
		    	<input type="submit" value="SAVE" class="btn_save close_box">
		    </div>
		</form>
	</div>
</div>

<script>
	$(function() {
	    $(".uploadFile").on("change", function()
	    {

	        var files = !!this.files ? this.files : [];
	        if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support


	        var ini = $(this).parent().find('.imagePreview');
	 
	        if (/^image/.test( files[0].type)){ // only image file
	            var reader = new FileReader(); // instance of the FileReader
	            reader.readAsDataURL(files[0]); // read the local file
	 
	            reader.onloadend = function(){ // set image data as background of div

	                $(ini).html('<img src="'+this.result+'" height="200"/>');
	            }
	        }
	    });

	    $(".uploadFile2").on("change", function()
	    {

	        var files = !!this.files ? this.files : [];
	        if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support


	        var ini = $(this).parent().find('.imagePreview2');
	 
	        if (/^image/.test( files[0].type)){ // only image file
	            var reader = new FileReader(); // instance of the FileReader
	            reader.readAsDataURL(files[0]); // read the local file
	 
	            reader.onloadend = function(){ // set image data as background of div

	                $(ini).html('<img src="'+this.result+'" height="200"/><img src="assets/images/ico_delete.png" alt="" class="hapus">');
	                $(".hapus").click(function() {
				    	$(this).parent().parent().find('.img_show').val("");
				    	$(this).parent().parent().find('.uploadFile2').val("");
				    	$(this).parent().parent().find('.imagePreview2').html("");
				    	
				    	//alert(isi);
				    });
	            }
	        }
	    });

	    $(".hapus").click(function() {
	    	$(this).parent().parent().find('.img_show').val("");
	    	$(this).parent().parent().find('.imagePreview').html("");
	    	
	    	//alert(isi);
	    });

	     $(".file150").on("change", function()
	    {
	        if(this.files[0].size > 150000){
		       alert("File is too big!");
		       this.value = "";
		    };
	    });
	    $(".file250").on("change", function()
	    {
	        if(this.files[0].size > 250000){
		       alert("File is too big!");
		       this.value = "";
		    };
	    });
	    $(".file500").on("change", function()
	    {
	        if(this.files[0].size > 500000){
		       alert("File is too big!");
		       this.value = "";
		    };
	    });
	    

	});
</script>