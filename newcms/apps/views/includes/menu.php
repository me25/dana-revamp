<header>
	<a class="logo">
		<img src="<?php echo assets_url('images');?>/logo_dana.png" alt="">
	</a>
	<nav>
		<a href="<?php echo base_url('home');?>" class="<?php if($page=="dashboard") {echo 'selected';}?>">
			<span class="sign"></span>
			<span class="ico">
				<img src="<?php echo assets_url('images');?>/ico_dashboard.png" alt="">
			</span>
			<span class="menu">Dashboard</span>
		</a>
		<a href="<?php echo base_url('homeeditor');?>" class="<?php if($page=="dashboard") {echo 'selected';}?>">
			<span class="sign"></span>
			<span class="ico">
				<img src="<?php echo assets_url('images');?>/ico_reporting.png" alt="">
			</span>
			<span class="menu">Home Editor</span>
		</a>
		<a href="<?php echo base_url('produk');?>" class="<?php if($page=="Produk") {echo 'selected';}?>">
			<span class="sign"></span>
			<span class="ico">
				<img src="<?php echo assets_url('images');?>/ico_reporting.png" alt="">
			</span>
			<span class="menu">Produk</span>
		</a>
		<a href="<?php echo base_url('bisnis');?>" class="<?php if($page=="Bisnis") {echo 'selected';}?>">
			<span class="sign"></span>
			<span class="ico">
				<img src="<?php echo assets_url('images');?>/ico_reporting.png" alt="">
			</span>
			<span class="menu">Bisnis & Merchant</span>
		</a>
		<div href="" class="menu_show <?php if($page=="Merchant"){ echo 'selected';}?>">
			<span class="sign"></span>
			<span class="ico">
				<img src="<?php echo assets_url('images');?>/ico_reporting.png" alt="">
			</span>
			<span class="menu">Merchant</span>
			<div class="submenu">
				<a href="<?php echo base_url('merchant');?>">Listing</a>
				<a href="<?php echo base_url('merchant/posisi');?>">Posisi</a>
				<a href="<?php echo base_url('merchant/cate');?>">Kategori</a>
				<a href="<?php echo base_url('merchant/posisi_cate');?>">Posisi Kategori</a>
			</div>
		</div>
		<div href="#" class="<?php if($page=="Promo"){ echo 'selected';}?> menu_show">
			<span class="sign"></span>
			<span class="ico">
				<img src="<?php echo assets_url('images');?>/ico_reporting.png" alt="">
			</span>
			<span class="menu">Promo</span>
			<div class="submenu">
				<a href="<?php echo base_url('promo');?>">Listing</a>
				<a href="<?php echo base_url('promo/cate');?>">Kategori</a>
				<a href="<?php echo base_url('promo/posisi');?>">Posisi Kategori</a>
			</div>
		</div>
		<div href="#" class="<?php if($page=="Testimonial"){ echo 'selected';}?> menu_show">
			<span class="sign"></span>
			<span class="ico">
				<img src="<?php echo assets_url('images');?>/ico_reporting.png" alt="">
			</span>
			<span class="menu">Testimonial</span>
			<div class="submenu">
				<a href="<?php echo base_url('testimonial');?>">Customer</a>
				<a href="<?php echo base_url('testimonial/bisnis');?>">Bisnis</a>
			</div>
		</div>
		<div href="#" class="<?php if($page=="Karir"){ echo 'selected';}?> menu_show">
			<span class="sign"></span>
			<span class="ico">
				<img src="<?php echo assets_url('images');?>/ico_reporting.png" alt="">
			</span>
			<span class="menu">Karir</span>
			<div class="submenu">
				<a href="<?php echo base_url('karir/editor');?>">Page Editor</a>
				<a href="<?php echo base_url('karir');?>">Listing</a>
				<a href="<?php echo base_url('karir/kota');?>">Kota</a>
				<a href="<?php echo base_url('karir/posisikota');?>">Posisi Kota</a>
				<a href="<?php echo base_url('karir/job');?>">Job Title</a>
				<a href="<?php echo base_url('karir/posisi');?>">Posisi Job Title</a>
				<a href="<?php echo base_url('karir/foto');?>">Slide Foto</a>
			</div>
		</div>
		
		<div href="#" class="<?php if($page=="Faq"){ echo 'selected';}?> menu_show">
			<span class="sign"></span>
			<span class="ico">
				<img src="<?php echo assets_url('images');?>/ico_reporting.png" alt="">
			</span>
			<span class="menu">FAQ</span>
			<div class="submenu">
				<a href="<?php echo base_url('faq');?>">List</a>
				<a href="<?php echo base_url('faq/kategori');?>">Kategori</a>
			</div>
		</div>
		<div href="#" class="<?php if($page=="Gabung"){ echo 'selected';}?> menu_show">
			<span class="sign"></span>
			<span class="ico">
				<img src="<?php echo assets_url('images');?>/ico_reporting.png" alt="">
			</span>
			<span class="menu">Gabung DANA</span>
			<div class="submenu">
				<a href="<?php echo base_url('gabung');?>">Listing</a>
				<a href="<?php echo base_url('gabung/page');?>">Editor</a>
			</div>
		</div>
		<a href="<?php echo base_url('hubungi');?>" class="<?php if($page=="Hubungi") {echo 'selected';}?>">
			<span class="sign"></span>
			<span class="ico">
				<img src="<?php echo assets_url('images');?>/ico_reporting.png" alt="">
			</span>
			<span class="menu">Hubungi DANA</span>
		</a>
		<div href="#" class="<?php if($page=="Statik"){ echo 'selected';}?> menu_show">
			<span class="sign"></span>
			<span class="ico">
				<img src="<?php echo assets_url('images');?>/ico_reporting.png" alt="">
			</span>
			<span class="menu">Statik Page</span>
			<div class="submenu">
				<a href="<?php echo base_url('statik/tentang');?>">Tentang Dana</a>
				<a href="<?php echo base_url('statik/terms');?>">Terms Condition</a>
				<a href="<?php echo base_url('statik/contact');?>">Contact Us</a>
			</div>
		</div>

		<div href="<?php echo base_url('setting');?>" class="<?php if($page=="Setting") {echo 'selected';}?> menu_show">
			<span class="sign"></span>
			<span class="ico">
				<img src="<?php echo assets_url('images');?>/ico_reporting.png" alt="">
			</span>
			<span class="menu">Setting</span>
			<div class="submenu">
				<a href="<?php echo base_url('setting');?>">Account</a>
				<a href="<?php echo base_url('setting/kota');?>">Kota</a>
			</div>
		</div>
		<a href="<?php echo base_url('member');?>" class="<?php if($page=="member") {echo 'selected';}?>">
			<span class="sign"></span>
			<span class="ico">
				<img src="<?php echo assets_url('images');?>/ico_reporting.png" alt="">
			</span>
			<span class="menu">Admin</span>
		</a>
	</nav>
</header>
