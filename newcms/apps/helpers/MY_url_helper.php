<?php
function assets_url($path, $witch_assets='assets')
{
	$asset = ($witch_assets == 'bower')?'bower_components':$witch_assets;
	return base_url($asset.'/'.$path);
} 

function date_to_path($datetime)
{
	$date = explode(' ', $datetime);
	$date_split = explode('-', $date[0]);
	return implode('/', $date_split);
}

function image_url($path)
{
	return base_url('uploads'.'/'.$path);
}

function makelink($s) {
	$string = $s;
    //$url = '@(https?://([-\w\.]+)+(:\d+)?(/([\w/_\.-]*(\?\S+)?)?)?)@';  
    //$string = preg_replace($url, '<a href="$0" target="_blank" title="$0">$0</a>', $string);
    return $string;
}

function thumb($fullname, $width, $height)
{
    // Path to image thumbnail in your root
    $dir = './uploads/files/';
    $dir_new = './uploads/files_new/';
    $url = base_url() . 'uploads/files_new/';
    // Get the CodeIgniter super object
    $CI = &get_instance();
    // get src file's extension and file name
    $extension = pathinfo($fullname, PATHINFO_EXTENSION);
    $filename = pathinfo($fullname, PATHINFO_FILENAME);
    $image_org = $dir . $filename . "." . $extension;
    $image_thumb = $dir_new . $filename . "-" . $height . '_' . $width . "." . $extension;
    $image_returned = $url . $filename . "-" . $height . '_' . $width . "." . $extension;

    if (!file_exists($image_thumb)) {
        // LOAD LIBRARY
        $CI->load->library('image_lib');
        // CONFIGURE IMAGE LIBRARY
        $config['source_image'] = $image_org;
        $config['new_image'] = $image_thumb;
        $config['width'] = $width;
        $config['height'] = $height;
        $CI->image_lib->initialize($config);
        $CI->image_lib->resize();
        $CI->image_lib->clear();
    }
    return $image_returned;
}
function thumb_user($fullname, $width, $height)
{
    // Path to image thumbnail in your root
    $dir = './uploads/users/';
    $url = base_url() . '/uploads/users/';
    // Get the CodeIgniter super object
    $CI = &get_instance();
    // get src file's extension and file name
    $extension = pathinfo($fullname, PATHINFO_EXTENSION);
    $filename = pathinfo($fullname, PATHINFO_FILENAME);
    $image_org = $dir . $filename . "." . $extension;
    $image_thumb = $dir . $filename . "-" . $height . '_' . $width . "." . $extension;
    $image_returned = $url . $filename . "-" . $height . '_' . $width . "." . $extension;

    if (!file_exists($image_thumb)) {
        // LOAD LIBRARY
        $CI->load->library('image_lib');
        // CONFIGURE IMAGE LIBRARY
        $config['source_image'] = $image_org;
        $config['new_image'] = $image_thumb;
        $config['width'] = $width;
        $config['height'] = $height;
        $CI->image_lib->initialize($config);
        $CI->image_lib->resize();
        $CI->image_lib->clear();
    }
    return $image_returned;
}

function date_full($date) {
    $date1 = explode('/', $date);
    $month = array("","Jan", "Feb", "Mar","Apr","Mei","Jun","Jul","Agt","Sep","Okt","Nov","Des");
    if($date1[1] == 10 or $date1[1] == 11 or $date1[1] == 12){
        $month1 = $date1[1];
    }
    else{
        $month1 = str_replace("0","",$date1[1]);
    }
    
    $month2 = $month[$month1];

    echo $date1[0].' '.$month2.' '.$date1[2];
}
function user_img($id,$username,$pic,$user) {
    echo'<a href="'.base_url('warga/detail/').$id.'/'.$username.'" class="box_img ratio1_1 tip '.$user.'" data-tip="'.$username.'" target="_parent">
        <div class="img_con lqd">';
            if($pic == ""){
                $nama = substr($username, 0,1);
                echo $nama;
            }
            else{
                echo'<img src="'.thumb_user('uploads/users/'.$pic, 200, 0).'" alt="">';
            }
        echo'</div>
    </a>';
}
function file_up($pic) {
    $ext = pathinfo($pic, PATHINFO_EXTENSION);

    if($ext=="png" or $ext=="PNG" or $ext=="jpg" or $ext=="jpeg" or $ext=="gif" or $ext=="JPG"){
        echo'<a href="'.image_url('files/').$pic.'" target="_blank" data-fancybox="gallery" data-srcset="'.image_url('files/').$pic.'" data-caption="&lt;a href=&quot;'.image_url('files/').$pic.'&quot; target=&quot;_blank &quot;&gt;Unduh File&lt;/a&gt;">
            <div class="ratio1_1 box_img">
                <div class="img_con lqd">
                    <img src="'.image_url('files/').$pic.'" alt="">
                </div>
            </div>
        </a>';
        //<img src="'.thumb($pic, 200, 0).'" alt="">
    }
    else {
        echo' <a href="'.image_url('files/').$pic.'" target="_blank">
            <div class="ratio1_1 box_img lqd">
                <div class="img_con lqd"><img src="'.assets_url('img/f_').$ext.'.png'.'"></div>
            </div>
        </a>';
    }
}
function str_replace_text($text) {
    $findArr = array(
                "-",
                "!",
                '"', 
                "'",
                "&",
                "<",
                ">",
                "=",
                "/>"
            );
    $replaceArr   = array(
                "&#45;",
                "&#33;",
                "&#34;",
                "&#039;",
                "&amp;",
                "&#60;",
                "&#62;",
                "&#60;"
                

            );

    $text2 = str_replace($findArr, $replaceArr, $text);
    //echo $text2;
    return $text2;
}
function str_replace_img($text) {
    $findArr = array(" - "," ","  ", "[", "]","&","+","!");
    $replaceArr   = array("_","_","_","_", "_","_","_","_");

    $text2 = str_replace($findArr, $replaceArr, $text);
    //echo $text2;
    return $text2;
}

function str_replace_encode($text) {
    $findArr   = array(
                "&#45;",
                "&#33;",
                "&#34;",
                "&#039;"
            );
    $replaceArr = array(
                "-",
                "!",
                '"', 
                "'"
            );
    

    $text2 = str_replace($findArr, $replaceArr, $text);
    //echo $text2;
    return $text2;
}
function str_replace_rehtml($text) {
    $findArr = array("&nbsp;","&lt;","<br/>","<br />");
    $replaceArr   = array(" ","<","","");

    $text2 = str_replace($findArr, $replaceArr, $text);
    //echo $text2;
    return $text2;
}
function str_replace_url($text) {
    $findArr = array(
                '',
                '!',
                '?',
                "'",
                ",",
                ' ',
                '&',
                '+',
                "%",
                '}',
                '{',
                '(',
                ')',
                '<',
                '>',
                '*',
                ':',
                ';',
                '|',
                '/',
                ']',
                '['
            );
    $replaceArr   = array(
                '_',
                '',
                '',
                '_',
                '_',
                '_',
                '_',
                '_',
                '_',
                '_',
                '_',
                '_',
                '_',
                '_',
                '_',
                '_',
                '_',
                '_',
                '_',
                '_',
                '',
                ''
            );

    $text2 = str_replace($findArr, $replaceArr, $text);
    //echo $text2;
    return $text2;
}