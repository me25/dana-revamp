
	<section class="hero_cover karir_cover">
		<div class="box_img ratio4_1">
			<?php
			if($id =="0"){
				if($city_url == ""){
					echo'<div class="img_con lqd">
						<img src="'.base_url('uploads/karir/').$page_detail_karir['img'].'" alt="">
					</div>
					<div class="text text3">
						<div class="container">
							<div class="title_cover"><h1>Karir</h1></div>
						</div>
					</div>';
				}
				else {
					echo'<div class="img_con lqd">
						<img src="'.base_url('uploads/karir/').$city_filter['img'].'" alt="">
					</div>
					<div class="text text3">
						<div class="container">
							<div class="title_cover"><h1>'.$city_filter['nama'].'</h1></div>
							<div class="desc">
								'.$city_filter['desc'].'
							</div>
						</div>
					</div>';
				}
			}
			else {
				echo'<div class="img_con lqd">
					<img src="'.base_url('uploads/karir/').$page_detail['img2'].'" alt="">
				</div>
				<div class="text text3">
					<div class="container">
						<div class="title_cover"><h1>'.$page_detail['nama'].'</h1></div>
						<div class="desc">
							'.$page_detail['desc'].'
						</div>
					</div>
				</div>';
			}
			?>
			<div class="clearfix"></div>
		</div>
	</section>
	
	<div class="detail_karir">
		<div class="nav2 fl">
			
			<?php

				if($city_url == ""){
					echo '<a href="'.base_url('karir/detail/').$this->uri->segment(3).'/'.$this->uri->segment(4).'" class="selected">Semua</a>';
				}
				else {
					echo '<a href="'.base_url('karir/detail/').$this->uri->segment(3).'/'.$this->uri->segment(4).'">Semua</a>';
				}

				foreach ($city as $row) {
					if($row['id'] == $city_filter['id']){
						echo'<a href="'.base_url('karir/detail/').$this->uri->segment(3).'/'.$this->uri->segment(4).'/'.$row['nama'].'" class="selected">'.$row['nama'].'</a>';
					}
					else {
						echo'<a href="'.base_url('karir/detail/').$this->uri->segment(3).'/'.$this->uri->segment(4).'/'.$row['nama'].'">'.$row['nama'].'</a>';
					}
				}
			?>
		</div>
		<div class="filter fr">
			<span>Category</span>
			<div class="select-style-box fl">
				<div class="selected">
					<?php 
					if($id == "0"){
						echo'Semua';
					}
					else {
						echo $page_detail['nama'];
					}
					?>
					
				</div>
				<div class="option">
					<?php 
						echo'<a href="'.base_url('karir/detail/0/all/').$this->uri->segment(5).'">Semua</a>';
					?>
					<?php
					foreach ($karir_cate as $row) {
						echo'<a href="'.base_url('karir/detail/').$row['id'].'/'.str_replace($row['nama']).'/'.$this->uri->segment(5).'">'.$row['nama'].'</a>';
					}
					?>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		<br><br>
		<div class="list_loker2">
			<?php
			if($id =="0"){
				foreach ($karir_cate as $row) {
					if($city_url){
						$job_query2= $this->Modglobal->find('karir', array('cate' => $row['id'],'kota' => $city_filter['id']),'nama');
					}
					else{
						$job_query2= $this->Modglobal->find('karir', array('cate' => $row['id']),'nama');
					}
					$job_list2 = $job_query2->result_array();

					$num = $job_query2->num_rows();

					if($num > 0){
						echo'<div class="title2">
							<h2>'.$row['nama'].'</h2>
						</div>';
					}
					else {
						
					}

					foreach ($job_list2 as $row) {
						echo'<div class="card_loker2">
							<h3>'.$row['nama'].'</h3>
							<div class="lokasi">'.$row['area'].'</div>
							<a href="'.$row['url'].'" class="btn_more" target="_blank">Selengkapnya</a>
							<div class="clearfix"></div>
						</div>';
					}
				}
			}
			else{
				foreach ($job_list as $row) {
					echo'<div class="card_loker2">
						<h3>'.$row['nama'].'</h3>
						<div class="lokasi">'.$row['area'].'</div>
						<a href="'.$row['url'].'" class="btn_more" target="_blank">Selengkapnya</a>
						<div class="clearfix"></div>
					</div>';
				}
			}
			?>
		</div>
	</div>