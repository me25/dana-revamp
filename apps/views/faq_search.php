<section class="hero_cover blue_cover hero_faq">
	<div class="box_img ratio4_1">
		<div class="text text3">
			<div class="container">
				<div class="title_cover"><h3>Halo! Apa yang bisa kami bantu?</h3></div>
				<form action="<?php echo base_url('faq/search');?>" class="search_header" method="post">
					<input type="text" name="keyword" placeholder="Tulis pertanyaan atau kata kunci yang ingin kamu cari" value="<?php echo $keyword;?>">
					<input type="submit" value="">
					<div class="clearfix"></div>
				</form>
				<div class="nav2">
					<a href="<?php echo base_url('faq');?>" class="selected">FAQ</a>
					<a href="<?php echo base_url('terms');?>">Terms and Conditions</a>
					<a href="<?php echo base_url('contactus');?>">Contact Us</a>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</section>
<div class="container">
	<div class="detail_right detail__ faq_search">
		<div class="result">Pencarian "<?php echo $keyword;?>" ditemukan <?php echo $num;?> artikel</div>
		<h1><?php echo $faq_cate_selected['nama'];?></h1>
		<?php foreach ($faq as $row) { 
			/*$tanya = str_replace('/\b'.$keyword.'/\b','<span class="keyword_highlight">'.$keyword.'</span>', $row['tanya']);
			$jawab = str_replace('/\b'.$keyword.'/\b','<span class="keyword_highlight">'.$keyword.'</span>', $row['jawab']);*/

			$tanya = $row['tanya'];
			$jawab = $row['jawab'];
			echo '<h2>'.$tanya.'</h2>';
			echo $jawab;		
		}
		?>
	</div>
	<div class="clearfix"></div>
</div>
