<section class="hero_cover blue_cover hero_faq">
	<div class="box_img ratio4_1">
		<div class="text text3">
			<div class="container">
				<div class="title_cover"><h3>Halo! Apa yang bisa kami bantu?</h3></div>
				<form action="<?php echo base_url('faq/search');?>" class="search_header" method="post">
					<input type="text" name="keyword" placeholder="Tulis pertanyaan atau kata kunci yang ingin kamu cari">
					<input type="submit" value="">
					<div class="clearfix"></div>
				</form>
				<div class="nav2">
					<a href="<?php echo base_url('faq');?>" class="selected">FAQ</a>
					<a href="<?php echo base_url('terms');?>">Terms and Conditions</a>
					<a href="<?php echo base_url('contactus');?>">Contact Us</a>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</section>
<div class="container">
	<div class="nav_left">
		<?php foreach ($faq_cate as $row) { 
			if($row['id'] == $id) {
				echo'<a href="'.base_url('faq/index/').str_replace_url($row['nama']).'" class="selected">'.$row['nama'].'</a>';
			}
			else {
				echo'<a href="'.base_url('faq/index/').str_replace_url($row['nama']).'" class="">'.$row['nama'].'</a>';
			}
		}
		?>
	</div>
	<div class="detail_right detail__">
		<h1><?php echo $faq_cate_selected['nama'];?></h1>
		<?php foreach ($faq as $row) { 
			echo '<h2>'.$row['tanya'].'</h2>';
			echo str_replace_img_url($row['jawab']);		
		}
		?>
	</div>
	<div class="clearfix"></div>
</div>
