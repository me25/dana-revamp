
	<section class="hero_cover">
		<div class="box_img ratio3_1">
			<div class="img_con lqd">
				<img src="<?php echo base_url('uploads/karir/').$page_detail['img'];?>" alt="">
			</div>
			<div class="text text3">
				<div class="container">
					<br><br><br><br><br>
					<div class="title_cover"><h1><?php echo $page_detail['nama'];?></h1></div>
					<div class="desc detail__">
						<?php echo $page_detail['desc'];?>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</section>
	
	<section class="section">
		<div class="container">
			<div class="page_title"><h2>Bekerja Bersama DANA</h2></div>
			<?php foreach ($karir as $row) { 
				echo'<div class="list3">
						<img src="'.base_url('uploads/karir/').$row['img'].'" alt="">
						<div class="t1">'.$row['nama1'].'</div>
						<div class="desc">
						'.$row['desc'].'
						</div>
					</div>';
				}
			?>
			<div class="clearfix"></div>
		</div>
	</section>
	<section class="section">
		<div class="container">
			<div class="page_title"><h2>Cerita DANAM8s</h2></div>
			<div class="areaslide">
				<div id="slide1">
					<?php foreach ($foto as $row) { 
						echo'<div class="card_slidefoto">
							<div class="ratio16_9 box_img">
								<div class="img_con">
									<img src="'.base_url('uploads/karir/').$row['img'].'" alt="">
								</div>
							</div>
							<div class="caption">'.$row['nama'].'</div>
						</div>';
					}
					?>
				</div>
				<img src="assets/img/arrow_back_black.png" alt="" class="slide1_prev">
				<img src="assets/img/arrow_next_black.png" alt="" class="slide1_next">
			</div>
			<div class="clearfix"></div>
		</div>
	</section>
	<section class="section">
		<div class="container">
			<div class="page_title"><h2>Temukan Minatmu</h2></div>
			<div class="nav2">
				<a href="#" class="selected">Semua</a>
				<?php foreach ($city as $row) { 
					echo '<a href="'.base_url('karir/detail/0/all/').$row['nama'].'">'.$row['nama'].'</a>';
				}
				?>
			</div>
			<div class="listing_loker">
				<?php foreach ($job as $row) { 
					echo'<div class="card_loker">
							<div class="ratio4_3 box_img">
								<div class="img_con lqd">
									<img src="'.base_url('uploads/karir/').$row['img'].'" alt="">
								</div>
								<div class="text">
									<h2>'.$row['nama'].'</h2>
									<div class="desc">'.$row['desc'].'</div>
									<a href="'.base_url('karir/detail/').$row['id'].'/'.str_replace_url($row['nama']).'" class="btn_more">Lihat Info Selengkapnya</a>
								</div>
							</div>
						</div>';
					}
				?>
			</div>
			<div class="clearfix"></div>
		</div>
	</section>
	
