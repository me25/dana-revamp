<?php echo $script_captcha; // javascript recaptcha ?>

<section class="hero_cover blue_cover">
	<div class="box_img ratio4_1">
		<div class="img_con lqd">
			<img src="<?php echo base_url('uploads/web/').$page_detail['img'];?>" alt="">
		</div>
		<div class="text text3">
			<div class="container">
				<div class="title_cover"><h1><?php echo $page_detail['nama'];?></h1></div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</section>
<section>
	<div class="container">
		<div class="detail_gabung">
			<form action="<?php echo base_url('bisnis/gabung_pro');?>" method="post" class=" form">
				<?php echo $page_detail['desc'];?>
				<div class="group-input">
					<span>Nama</span>
					<input type="text" required="required" name="nama">
				</div>
				<div class="group-input">
					<span>Nama Bisnis</span>
					<input type="text" required="required" name="nama_bisnis">
				</div>
				<div class="group-input">
					<span>Email</span>
					<input type="email" required="required" name="email">
				</div>
				<div class="group-input">
					<span>No. Telepon</span>
					<input type="text" required="required" name="tlp">
				</div>
				<div class="form-group">
	                <?php echo $captcha // tampilkan recaptcha ?>
	            </div>
				
				<div class="group-input">
					<input type="submit" value="Kirim" class="btn_more">
				</div>
			</form>
		</div>
	</div>
</section>
