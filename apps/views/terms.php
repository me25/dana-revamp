<section class="hero_cover blue_cover hero_faq">
	<div class="box_img ratio4_1">
		<div class="text text3">
			<div class="container">
				<div class="title_cover"><h1><?php echo $page_detail['nama'];?></h1></div>
				<div class="nav2">
					<a href="<?php echo base_url('faq');?>">FAQ</a>
					<a href="<?php echo base_url('terms');?>" class="selected">Terms and Conditions</a>
					<a href="<?php echo base_url('contactus');?>">Contact Us</a>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</section>
<div class="container">
	<div class="detail_statik detail__">
		<?php echo str_replace_img_url($page_detail['desc']);?>
	</div>
	<div class="clearfix"></div>
</div>