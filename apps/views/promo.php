
<div class="area_promopage">
	<div id="slide_promobig">
		<?php foreach ($promohl as $row) { ?>
			<a href="<?php echo base_url('promo/detail/').$row['id'].'/'.str_replace_url($row['nama']);?>" class="box_img ratio3_1">
				<div class="img_con lqd">
					<img src="<?php echo base_url('uploads/promo/').$row['img2'];?>" alt="">
				</div>
			</a>
		<?php }?>
	</div>
	<img src="assets/img/arrow_next_white2.png" alt="" class="slide_next">
	<img src="assets/img/arrow_back_white2.png" alt="" class="slide_prev">
	<div class="paging_slide" id="slide_page"></div>
</div>


<section class="section">
	<div class="container">
		<div class="nav2 nav2a">
			<?php 
				if($id==""){
					echo'<a href="'.base_url('promo/').'" class="selected">All</a>';
				}
				else {
					echo'<a href="'.base_url('promo/').'">All</a>';
				}
			?>
			
			<?php foreach ($promocate as $row) { 
				if($row['id'] == $promocate_id['id']){
					echo'<a href="'.base_url('promo/cate/').str_replace_url($row['nama']).'" class="selected">'.$row['nama'].'</a>';
				}
				else {
					echo'<a href="'.base_url('promo/cate/').str_replace_url($row['nama']).'">'.$row['nama'].'</a>';
				}
				//echo $promocate_id['id'];
			}
			?>
		</div>
		<div class="area_promo">
			<?php foreach ($promo as $row) { ?>
				<div class="card_promo">
					<div class="box_img ratio16_9">
						<div class="img_con lqd">
							<img src="<?php echo base_url('uploads/promo/').$row['img'];?>" alt="">
						</div>
					</div>
					<!-- <div class="label"><span>DANA</span></div> -->
					<div class="text">
						<a href="<?php echo base_url('promo/detail/').$row['id'].'/'.str_replace_url($row['nama']);?>">
							<h3><?php echo $row['nama'];?></h3>
						</a>
					</div>
					<?php
						if($row['date_end']) {
							echo'<div class="date">Hingga '.date_full($row['date_end']).'</div>';
						}
						else {
							echo'<div class="date">&nbsp;</div>';
						}
					?>
					<a href="<?php echo base_url('promo/detail/').$row['id'].'/'.str_replace_url($row['nama']);?>" class="btn_more">Lihat Info Selengkapnya</a>
					<div class="clearfix"></div>
				</div>
			<?php }?>
			<div class="clearfix"></div>
		</div>
		<div class="paging">
            <?php
                for ($i = 1; $i <= $total_page; $i++) {
                    if($page_number == $i){
                        echo '<a href="'.base_url('promo/index/').$i.'" class="selected">'.$i.'</a>';
                    }
                    else {
                        echo '<a href="'.base_url('promo/index/').$i.'">'.$i.'</a>';
                    }
            }
            ?>
        </div>
	</div>
</section>