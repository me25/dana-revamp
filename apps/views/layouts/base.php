<!doctype html>	
<html>
<head>
	<?php $this->load->view('includes/head')?>	
</head>
<body class="<?php echo $body;?>">
	<?php $this->load->view('includes/header')?>
<?php
if (isset($content) && !empty($content)) {
  $this->load->view($content);
}
?>
<div class="clearfix"></div>
<?php $this->load->view('includes/footer')?>
<?php $this->load->view('includes/js')?>
</body>
</html>