<?php error_reporting(0);?>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<title>DANA - Bentuk Baru Pembayaran dalam Genggaman</title>

<meta name=keyword content="digital, wallet, payment, indonesia">
<meta name=description content="Dana adalah bentuk baru uang tunai yang lebih baik. Transaksi apapun, berapapun dan dimanapun jadi mudah bersama DANA. Ambil bagian dalam transformasi keuangan digital di Indonesia sekarang!">
<link rel=icon type=image/png sizes=32x32 href=<?php echo base_url('/assets/img/');?>favicon-32x32.png>
<link rel=icon type=image/png sizes=16x16 href=<?php echo base_url('/assets/img/');?>favicon-16x16.png>
<!--[if IE]><link rel="shortcut icon" href="/assets/img/icons/favicon.ico"><![endif]-->
<link rel=manifest href=/assets/manifest.json>
<meta name=theme-color content=#108ee9>
<meta name=apple-mobile-web-app-capable content=yes>
<meta name=apple-mobile-web-app-status-bar-style content=black>
<meta name=apple-mobile-web-app-title content=dana_web>
<link rel=apple-touch-icon href="<?php echo base_url('/assets/img/');?>apple-touch-icon-152x152.png">
<meta name=msapplication-TileImage content=<?php echo base_url('/assets/img/');?>msapplication-icon-144x144.png>
<meta name=msapplication-TileColor content=#000000>
<meta itemprop=name content="DANA - Bentuk Baru Pembayaran Dalam Genggaman">
<meta itemprop=description content="Dana adalah bentuk baru uang tunai yang lebih baik. Transaksi apapun, berapapun dan dimanapun jadi mudah bersama DANA. Ambil bagian dalam transformasi keuangan digital di Indonesia sekarang!">
<meta itemprop=image content=<?php echo base_url('assets/img/');?>twitter-share.jpg>
<meta name=twitter:card content=summary>
<meta name=twitter:title content="DANA - Bentuk Baru Pembayaran Dalam Genggaman">
<meta name=twitter:description content="Dana adalah bentuk baru uang tunai yang lebih baik. Transaksi apapun, berapapun dan dimanapun jadi mudah bersama DANA. Ambil bagian dalam transformasi keuangan digital di Indonesia sekarang!">
<meta name=twitter:image content="<?php echo base_url('/assets/img/');?>twitter-share.jpg">
<meta property=og:url content="<?php echo base_url();?>">
<meta property=og:type content=article>
<meta property=og:title content="DANA - Bentuk Baru Pembayaran Dalam Genggaman">
<meta property=og:description content="Dana adalah bentuk baru uang tunai yang lebih baik. Transaksi apapun, berapapun dan dimanapun jadi mudah bersama DANA. Ambil bagian dalam transformasi keuangan digital di Indonesia sekarang!">
<meta property=og:image content=https://www.dana.id/share_img/fb-share.jpg>
<link href="<?php echo assets_url('css');?>/dana.style.css?1234" rel="stylesheet" type="text/css">
<script type="text/javascript" language="javascript" src="<?php echo assets_url('js');?>/jquery.min.js"></script>
