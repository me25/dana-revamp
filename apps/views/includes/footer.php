<div id="footer" class="<?php echo $footer;?>">
	<div class="container">
		<div class="start_now">
			<img src="<?php echo assets_url('img'); ?>/dana_splashscreen.png" alt="" class="phone_bottom">
			<div class="text">
				<b>#GANTIDOMPET Sekarang</b>
				<div class="download">
					<span>Download DANA Now</span>
					<a href="<?php echo $setting['playstore']; ?>" target="_blank"><img src="<?php echo assets_url('img'); ?>/download_play.png" alt=""></a>
					<a href="<?php echo $setting['appstore']; ?>" target="_blank"><img src="<?php echo assets_url('img'); ?>/download_app.png" alt=""></a>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="clearfix"></div>
		<div class="footer2">
			<a href="#" class="logo"><img src="<?php echo assets_url('img'); ?>/logo.png" alt=""></a>
			<div id="nav_foot">
				<a href="<?php echo base_url('produk'); ?>">Produk & Fitur</a>
				<a href="<?php echo base_url('bisnis'); ?>">Bisnis & Partner</a>
				<a href="<?php echo base_url('karir'); ?>">Karir</a>
				<a href="<?php echo base_url('promo'); ?>">Promo</a>
				<a href="<?php echo base_url('faq'); ?>">FAQ</a>
			</div>
			<div class="copy">
				&copy; <?php echo date("Y");?> DANA - PT. Espay Debit Indonesia Koe. All Rights Reserved.
			</div>

			<div class="sosmed">
				<span>Ikuti kami</span>
				<a href="<?php echo $setting['tw']; ?>" target="_blank"><img src="<?php echo assets_url('img'); ?>/ico-tw.png" alt=""></a>
				<a href="<?php echo $setting['fb']; ?>" target="_blank"><img src="<?php echo assets_url('img'); ?>/ico-fb.png" alt=""></a>
				<a href="<?php echo $setting['ig']; ?>" target="_blank"><img src="<?php echo assets_url('img'); ?>/ico-ig.png" alt=""></a>
				<a href="<?php echo $setting['yt']; ?>" target="_blank"><img src="<?php echo assets_url('img'); ?>/ico-yt.png" alt=""></a>
			</div>
		</div>

	</div>
</div>