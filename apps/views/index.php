
	<section class="hero_cover">
		<div class="box_img ratio16_9">
			<div class="img_con">
				<?php
				if($home['tipe']=="1"){
					echo '<iframe width="560" height="315" src="https://www.youtube.com/embed/'.$home['url'].'?autoplay=1&controls=0&mute=1&loop=1&playlist='.$home['url'].'&showinfo=0" frameborder="0" allowfullscreen="" include="" class="video_yt" onload="playVideo()"></iframe>';
				}
				else {
					echo '<img src="'.base_url('uploads/web/').$home['img'].'">';
				}
				?>
			</div>
			<div class="text">
				<div class="container">
					<div class="img">
						<img src="<?php echo base_url('assets/img/').'phone_polos.png';?>" alt="">
						<div class="box_img ratio9_16 content_phone">
							<div class="img_con">
								<?php
								if($home['tipe2']=="1"){
									echo '<video width="320" height="640" autoplay muted loop>
										  <source src="'.base_url('uploads/web/').$home['img2'].'" type="video/mp4">
										</video>';
								}
								else {
									echo '<img src="'.base_url('uploads/web/').$home['img2'].'">';
								}
								?>
							</div>
						</div>
					</div>
					<div class="text2 replace">
						<h1><?php echo str_replace_logo_white($home['nama']);?></h1>
						<div class="tagline"><?php echo $home['nama1'];?></div>
						<div class="download">
							<a href="<?php echo $setting['playstore']; ?>" target="_blank"><img src="<?php echo assets_url('img'); ?>/download_play.png" alt=""></a>
							<a href="<?php echo $setting['appstore']; ?>" target="_blank"><img src="<?php echo assets_url('img'); ?>/download_app.png" alt=""></a>
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</section>
	<section class="about">
		<!-- <div class="box_img ratio16_9 bg">
			<div class="img_con lqd"><img src="<?php echo base_url('uploads/web/').$tentang['img2'];?>" alt=""></div>
		</div> -->
		<div class="container">
			<img src="<?php echo base_url('uploads/web/').$tentang['img'];?>" alt="" class="img">
			<div class="text">
				<h2><b><?php echo str_replace_dana($tentang['nama']);?></b></h2>
				<div class="desc">
					<?php echo $tentang['teaser'];?>
				</div>
				<a href="<?php echo base_url('tentang');?>" class="btn_more">Lebih Lanjut</a>
			</div>
			<div class="clearfix"></div>
		</div>
	</section>
	<section class="section">
		<div class="container">
			<div class="page_title"><h2><?php echo str_replace_dana('Transaksi Dengan Aman & Nyaman');?></h2></div>
			<?php foreach ($transaksi as $row) { 
				echo'<div class="list3">
					<img src="'.base_url('uploads/web/').$row['img'].'" alt="">
					<div class="t1">'.$row['nama1'].'</div>
					'.$row['desc'].'
				</div>';
			}
			?>
			<div class="clearfix"></div>
		</div>
	</section>
	
	<section class="secure section">
		<div class="container">
			<div class="page_title"><h2><?php echo str_replace_dana('Keamanan DANA Dompet Digital');?></h2></div>
			<div class="secure_info_area">
				<div class="phone">
					<div class="frame_phone">
						<img src="<?php echo base_url('assets/img/').'phone_polos.png';?>" alt="">
					</div>
					<?php foreach ($fitur as $row) { 
						if($row['tipe']=="0"){
							echo'<div class="img_secure" id="secure'.$row['id'].'">
								<img src="'.base_url('uploads/web/').$row['img'].'" alt="">
							</div>';
						}
						else {
							echo '<div class="img_secure" id="secure'.$row['id'].'"><video width="320" height="640" autoplay muted loop>
							  <source src="'.base_url('uploads/web/').$row['img'].'" type="video/mp4">
							</video></div>';
						}
					}
					?>
					<!-- <img src="assets/img/dana_qr.png" alt=""> -->
				</div>
				<div class="circle"></div>
				<div class="info_show" id="tab1">
					<?php foreach ($fitur as $row) { 
						echo'<a class="info info1" href="#secure'.$row['id'].'">
							<div class="t1">'.$row['nama1'].'</div>
							'.$row['desc'].'
						</a>';
					}
					?>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>

	</section>
	<section class="testimonial section">
		<div class="container">
			<div class="page_title"><h2><?php echo str_replace_dana('Cerita TemanDANA');?></h2></div>
			<div class="area_testimoni">
				<img src="assets/img/arrow_back_black.png" alt="" class="testi_prev">
				<img src="assets/img/arrow_next_black.png" alt="" class="testi_next">
				
				<div id="slidetesti">
					<?php foreach ($testimonial as $row) { ?>

						<div class="card_testimoni_full">
							<div class="box_img ratio16_9">
								<div class="img_con lqd">
									<img src="<?php echo base_url('uploads/testimonial/').$row['img'];?>" alt="">
								</div>
							</div>
							<!-- <div class="text">
								<div class="isi">
									"<?php echo $row['desc'];?>"
									<div class="nama"><b><?php echo $row['nama'];?></b></div>
								</div>
							</div> -->
							<div class="clearfix"></div>
						</div>

					<?php }?>
				</div>
				<div class="clearfix"></div>
				<div class="paging_slide" id="testi_page"></div>
			</div>
		</div>
	</section>
	<section class="section promo">
		<div class="container">
			<div class="page_title"><h2>Promo</h2></div>
			<div class="area_promo">
				<img src="assets/img/arrow_back_black.png" alt="" class="promo_prev">
				<img src="assets/img/arrow_next_black.png" alt="" class="promo_next">
				<div id="slidepromo" class="desktop">
					<?php foreach ($promo as $row) { ?>
						<div class="card_promo">
							<div class="box_img ratio16_9">
								<div class="img_con lqd">
									<img src="<?php echo base_url('uploads/promo/').$row['img'];?>" alt="">
								</div>
							</div>
							<!-- <div class="label"><span>DANA</span></div> -->
							<div class="text">
								<a href="<?php echo base_url('promo/detail/').$row['id'].'/'.str_replace_url($row['nama']);?>">
									<h3><?php echo $row['nama'];?></h3>
								</a>
							</div>
							<?php
								if($row['date_end']) {
									echo'<div class="date">Hingga '.date_full($row['date_end']).'</div>';
								}
								else {
									echo'<div class="date">&nbsp;</div>';
								}
							?>
							
							<a href="<?php echo base_url('promo/detail/').$row['id'].'/'.str_replace_url($row['nama']);?>" class="btn_more">Lihat Info Selengkapnya</a>
							<div class="clearfix"></div>
						</div>
					<?php }?>
				</div>
				<div id="slidepromo_mobile" class="mobile">
					<?php foreach ($promo as $row) { ?>
						<a class="card_promo" href="<?php echo base_url('promo/detail/').$row['id'].'/'.str_replace_url($row['nama']);?>">
							<div class="box_img ratio16_9">
								<div class="img_con lqd">
									<img src="<?php echo base_url('uploads/promo/').$row['img'];?>" alt="">
								</div>
							</div>
							<!-- <div class="label"><span>DANA</span></div> -->
							<div class="text">
									<h3><?php echo $row['nama'];?></h3>
							</div>
							<?php
								if($row['date_end']) {
									echo'<div class="date">Hingga '.$row['date_end'].'</div>';
								}
								else {
									echo'<div class="date">&nbsp;</div>';
								}
							?>
							
							<div class="btn_more">Lihat Info Selengkapnya</div>
							<div class="clearfix"></div>
						</a>
					<?php }?>
				</div>
				<div class="clearfix"></div>
				<div class="paging_slide desktop" id="promo_page"></div>
				<div class="paging_slide mobile" id="promo_page_mobile"></div>
				<div class="clearfix"></div>
				<div align="center"><a href="<?php echo base_url('promo');?>" class="btn_more">Lihat Info Selengkapnya</a></div>
			</div>
		</div>
	</section>
	<section class="merchant section">
		<div class="container">
			<div class="page_title"><h2>Partner</h2></div>
			<div id="filterOptions" class="nav2">
			    <a href="#" class="selected all">All</a>
			    <?php
					foreach ($merchant_cate as $row) { 
						echo'<a href="#" class="merchant_'.$row['id'].'">'.$row['nama'].'</a>';
					}
				?>
			  </div>
			<div align="center"  id="ourHolder">
				<?php foreach ($merchant as $row) { 
					if($row['imgzoom'] == "1") {
						$class = "img_kecil";
					}
					else{
						$class ="";
					}

					echo '
						<a href="'.base_url('bisnis/detail/').$row['id'].'/'.str_replace_url($row['nama']).'" class="card_merchant item merchant_'.$row['cate'].'">
								<div class="box_img ratio_merchant">
									<div class="img_con">
										<img src="'.base_url('uploads/merchant/').$row['img'].'" alt="" class="'.$class.'">
									</div>
								</div>
							</a>
						';
				}
				?>
				<a href="<?php echo base_url('bisnis');?>" class="card_merchant card_merchant100 item">
					<div class="box_img ratio_merchant">
						<div class="img_con">
							1000+ Merchant lainnya
						</div>
					</div>
				</a>
				<div class="clearfix"></div>
			</div>
			<br>
			<div align="center"><a href="<?php echo base_url('bisnis/gabung');?>"" class="btn_more">Gabung Sekarang</a></div>
		</div>
		<br><br><br><br>
	</section>

<!-- <script type="text/javascript">
function playVideo(){
   $('.video_yt').click();
}
</script>
 -->