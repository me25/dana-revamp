<section class="hero_cover blue_cover">
	<div class="box_img ratio4_1">
		<div class="text text3">
			<div class="container">
				<div class="title_cover"><h1><?php echo $page_detail['nama'];?></h1></div>
			</div>
		</div>
		<div class="img_con lqd">
			<img src="<?php echo base_url('uploads/web/').$page_detail['img2'];?>" alt="">
		</div>
		<div class="clearfix"></div>
	</div>
</section>
<div class="detail_tentang">
	<div class="text detail__">
		<?php echo str_replace_img_url($page_detail['desc']);?>
	</div>
	<div class="clearfix"></div>
</div>