<section class="hero_cover">
	<div class="box_img ratio16_9">
		<div class="img_con">
			<img src="<?php echo base_url('uploads/web/').$page_detail['img'];?>" alt="">
		</div>
		<div class="text">
			<div class="container">
				<div class="text2 text2a">
					<h1><?php echo str_replace_logo_white($page_detail['nama']);?></h1>
					<div class="desc detail__">
						<?php echo $page_detail['desc'];?>
					</div>
					<a href="<?php echo base_url('bisnis/gabung');?>" class="btn_more">Gabung disini</a>
					
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</section>
<section class="merchant section bg_abu">
	<div class="container">
		<div class="page_title"><h2>Partner</h2></div>
		<div id="filterOptions" class="nav2">
		    <a href="#" class="selected all">All</a>
		    <?php
				foreach ($merchant_cate as $row) { 
					echo'<a href="#" class="merchant_'.$row['id'].'">'.$row['nama'].'</a>';
				}
			?>
		  </div>
		<div align="center"  id="ourHolder">
			<?php foreach ($merchant as $row) { 
					if($row['imgzoom'] == "1") {
						$class = "img_kecil";
					}
					else{
						$class ="";
					}

					echo '
						<a href="'.base_url('bisnis/detail/').$row['id'].'/'.str_replace_url($row['nama']).'" class="card_merchant item merchant_'.$row['cate'].'">
								<div class="box_img ratio_merchant">
									<div class="img_con">
										<img src="'.base_url('uploads/merchant/').$row['img'].'" alt="" class="'.$class.'">
									</div>
								</div>
							</a>
						';
				}
				?>
			<a href="<?php echo base_url('bisnis');?>" class="card_merchant item card_merchant100">
				<div class="box_img ratio_merchant">
					<div class="img_con">
						1000+ Merchant lainnya
					</div>
				</div>
			</a>
			<div class="clearfix"></div>
		</div>
		<br>
		<div align="center"><a href="<?php echo base_url('bisnis/gabung');?>" class="btn_more">Gabung Sekarang</a></div>
	</div>
	<br><br><br><br>
</section>

<!-- <section class="testimonial section">
	<div class="container">
		<div class="page_title"><h2>Cerita Partner</h2></div>
		<div class="area_testimoni">
			<img src="<?php echo base_url('assets/img/');?>arrow_back_black.png" alt="" class="testi_prev">
			<img src="<?php echo base_url('assets/img/');?>arrow_next_black.png" alt="" class="testi_next">
			
			<div id="slidetesti">
				<?php foreach ($testimonial as $row) { ?>
					<div class="card_testimoni_full">
						<div class="box_img ratio16_9">
							<div class="img_con lqd">
								<img src="<?php echo base_url('uploads/testimonial/').$row['img'];?>" alt="">
							</div>
						</div>
						
						<div class="clearfix"></div>
					</div>

				<?php }?>
			</div>
			<div class="clearfix"></div>
			<div class="paging_slide" id="testi_page"></div>
		</div>
	</div>
</section> -->