<?php echo $script_captcha; // javascript recaptcha ?>

<?php if($this->session->flashdata('email_sent')) {
    ?>
    <script type="text/javascript">
        alert("<?php echo $this->session->flashdata('email_sent');?>");
    </script>
    <?php
} else{}
?>
<?php if($this->session->flashdata('email_gagal')) {
    ?>
    <script type="text/javascript">
        alert("<?php echo $this->session->flashdata('email_gagal');?>");
    </script>
    <?php
} else{}
?>

    <section class="hero_cover blue_cover hero_faq">
	<div class="box_img ratio4_1">
		<div class="text text3">
			<div class="container">
				<div class="title_cover"><h1><?php echo $page_detail['nama'];?></h1></div>
				<div class="nav2">
					<a href="<?php echo base_url('faq');?>">FAQ</a>
					<a href="<?php echo base_url('terms');?>">Terms and Conditions</a>
					<a href="<?php echo base_url('contactus');?>"  class="selected">Contact Us</a>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</section>
<div class="container">
	<div class="detail_statik">
		<?php
		if($page_detail['map']==""){}
		else{
			echo'<div class="map">
				<iframe src="'.$page_detail['map'].'" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>';
		}
		?>
		<div class="contact_right detail__">
			<?php echo $page_detail['desc'];?>

			<div class="cs">
				<span>Customer Support</span>
				<?php echo $page_detail['nomor'];?>
			</div>
			Hubungi nomor customer support kami, atau email ke <?php echo $page_detail['email'];?> apabila anda membutuhkan bantuan.
		</div>
		<div class="contact_left detail__">
			<form action="<?php echo base_url('contactus/sendemail');?>" method="post" class=" form">
				Silakan isi formulir di bawah, kami akan segera menjawab pesan Anda.
				<div class="group-input">
					<span>Nama</span>
					<input type="text" required="required" name="nama">
				</div>
				<div class="group-input">
					<span>Email</span>
					<input type="email" required="required" name="email">
				</div>
				<div class="group-input">
					<span>Pesan</span>
					<textarea name="pesan" id="" cols="30" rows="10" required="required"></textarea>
				</div>
				<div class="form-group">
	                <?php echo $captcha // tampilkan recaptcha ?>
	            </div>
				
				<div class="group-input">
					<input type="submit" value="Kirim" class="btn_more">
				</div>
			</form>
    </script>
		</div>
		
		<div class="clearfix"></div>
	</div>
	<div class="clearfix"></div>
</div>