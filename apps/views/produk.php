<div class="area_produk">
	<?php foreach ($produk as $row) { 
	?>
		<?php
			if($row['img3']!=="") {
				?>
				<section class="card_produk" style="background-image: url('<?php echo base_url('uploads/web/').$row['img3'];?>')">
				<?php
			}
			else {
				echo '<section class="card_produk" >';
			}
		?>
			<div class="container">
				<div class="text">
					<h1><?php echo str_replace_logo_white($row['nama2']);?></h1>
					<h2><?php echo $row['nama1'];?></h2>
					<div class="desc">
						<?php echo $row['desc'];?>
						<?php
						if($row['img2']){
							echo'<img src="'.base_url('uploads/web/').$row['img2'].'" alt="" class="img2">';
						}
						?>
					</div>
				</div>
				<div class="img">
					<?php
						if($row['tipe']=='1'){
							echo'
								<video width="320" height="640" autoplay muted loop>
								  <source src="'.base_url('uploads/web/').$row['img'].'" type="video/mp4">
								</video>
								';

						}
						elseif($row['tipe']=='2'){
							echo'
								<div class="frame_phone">
									<img src="'.base_url('assets/img/').'phone_polos.png'.'" alt="">
									<video width="320" height="640" autoplay muted loop>
									  <source src="'.base_url('uploads/web/').$row['img'].'" type="video/mp4">
									</video>
								</div>
								';

						}
						elseif($row['tipe']=='3'){
							echo'
								<div class="frame_phone">
									<img src="'.base_url('assets/img/').'phone_polos.png'.'" alt="">
									<img src="'.base_url('uploads/web/').$row['img'].'" alt="" class="img_">
								</div>
								';

						}
						else{
							echo'<img src="'.base_url('uploads/web/').$row['img'].'" alt="">';
						}
					?>
					
				</div>
				<div class="clearfix"></div>
			</div>
		</section>
	<?php
	}
	?>
</div>
<br><br><br><br><br>