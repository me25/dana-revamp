<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contactus extends CI_Controller {

	function __construct(){
		parent::__construct();		
		$this->load->model('Modglobal');
		$this->load->helper(array('url', 'form'));
        $this->load->library(array('form_validation', 'Recaptcha'));
 
	}
	public function index()
	{
		$page = "faq";
		$header = "clean";
		$footer = "reguler";

		
		$query= $this->Modglobal->find('statik', array('id' => '3'));
		$page_detail = $query->row_array();



		$data = array(
			'content' => 'contact',
			'page' => $page,
			'footer' => $footer,
			'header' => $header,
			'page_detail' => $page_detail,
			'captcha' => $this->recaptcha->getWidget(), // menampilkan recaptcha
            'script_captcha' => $this->recaptcha->getScriptTag(), // javascript recaptcha ditaruh di 
		);
		$this->load->view('layouts/base', $data);
	}
	public function berhasil()
	{
		$page = "faq";
		$header = "clean";
		$footer = "reguler";

		
		$query= $this->Modglobal->find('statik', array('id' => '3'));
		$page_detail = $query->row_array();

		$data = array(
			'content' => 'contact_berhasil',
			'page' => $page,
			'footer' => $footer,
			'header' => $header,
			'page_detail' => $page_detail,
		);
		$this->load->view('layouts/base', $data);
	}
	public function sendemail2()
	{
		$email_query= $this->Modglobal->find('setting', array('id' => '1'));
		$email = $email_query->row_array();



		//Load email library
		$this->load->library('email');

		
		//SMTP & mail configuration
		$config = array(
		    'protocol'  => 'smtp',
		    'smtp_host' => 'ssl://mail.titikterangproject.com',
		    'smtp_port' => 465,
		    'smtp_user' => 'send@titikterangproject.com',
		    'smtp_pass' => 't1t1kt3email',
		    'mailtype'  => 'html',
		    'charset'   => 'utf-8'
		);

		/*$config = array(
		    'protocol'  => 'smtp',
		    'smtp_host' => 'ssl://email-smtp.us-east-1.amazonaws.com',
		    'smtp_port' => 465,
		    'smtp_user' => 'AKIAJHBBOG27PPNRO5LA',
		    'smtp_pass' => 'BI4TlW2gImeenAU+Xht84QJpUkLKBUnGaiqtpKKDx9BC',
		    'mailtype'  => 'html',
		    'charset'   => 'utf-8'
		);*/

		$from_email = $this->input->post('email');
		//$from_email = 'send@titikterangproject.com';
		$to_email = $email['email'];
		//$to_email = 'me@danirus.com';
		$nama = $this->input->post('nama');
		$pesan = $this->input->post('pesan');

		$this->email->initialize($config);
		$this->email->set_mailtype("html");
		$this->email->set_newline("\r\n");

		//Email content
		$htmlContent = '<h1>Email dari '.$nama.'</h1>';
		$htmlContent .= '<p>Nama: '.$nama.'<br>';
		$htmlContent .= 'Email: '.$from_email.'<br>';
		$htmlContent .= 'Pesan:<br>'.$pesan.'</p>';

		$this->email->to($to_email);
		$this->email->from($from_email,$nama);
		$this->email->subject('[Hubungi Kami] Email Baru dari '.$nama);
		$this->email->message($htmlContent);

		//Send email
		//$this->email->send();

        if($this->email->send()) 
            $this->session->set_flashdata("email_sent","Pesan Anda berhasil dikirim");
            
        else 
            $this->session->set_flashdata("email_sent","Pesan Anda gagal terkirim");
        //$this->load->view('contact_email_form');

        redirect('contactus');
    }
    public function sendemail()
	{
		$email_query= $this->Modglobal->find('setting', array('id' => '1'));
		$email = $email_query->row_array();

		$statik_query= $this->Modglobal->find('statik', array('id' => '3'));
		$statik = $statik_query->row_array();

		$recaptcha = $this->input->post('g-recaptcha-response');
        $response = $this->recaptcha->verifyResponse($recaptcha);

        if (!isset($response['success']) || $response['success'] <> true) {
            $this->index();
            //echo 'gagal';
            $this->session->set_flashdata("email_gagal","reCaptcha Belum Anda Pilih");
            redirect('contactus');
        } else {
            // lakukan proses validasi login disini
            // pada contoh ini saya anggap login berhasil dan saya hanya menampilkan pesan berhasil
            // tapi ini jangan di contoh ya menulis echo di controller hahahaha
            $data = array(
	        	'nama' => str_replace_text($this->input->post('nama')),
	        	'email' => $this->input->post('email'),
	        	'pesan' => str_replace_text($this->input->post('pesan')),
	        	'date_create' => date("Y/m/d - h:i:sa"),
	        );
	        $this->Modglobal->insert('hubungi', $data);

	        ///SEND EMAIL

			$from_email = $this->input->post('email');
			$to_email = $email['email']; 
			$subject = 'Kontak Baru dari '.str_replace_text($this->input->post('nama'));
			$content = '<b>'.str_replace_text($this->input->post('nama')).'('.$this->input->post('email').') Menghubungi DANA</b><br><br>nama: '.str_replace_text($this->input->post('nama')).'<br>Email: '.$this->input->post('email').'<br> Pesan: <br>'.str_replace_text($this->input->post('pesan'));
			$link = 'https://dana.id/';

			$this->load->library('email'); 
			$this->load->helper('file');


			$this->email->set_newline("\r\n");
			$this->email->from($from_email); 
			$this->email->to($to_email);
			$this->email->set_mailtype("html");
			$this->email->subject($subject); 
			$email_template = read_file('mail.html');

			$email_body = str_replace(
				array(
					'{subject}',
					'{content}',
					'{link}'
				), 
				array(
					$subject,
					$content,
					$link
				), 
				$email_template
			);
			
			$this->email->message($email_body);

	        $this->email->send();

            $this->session->set_flashdata("email_sent","Pesan Anda berhasil dikirim");
            redirect('contactus/berhasil');
        }

		
    }
	
	
	
}


