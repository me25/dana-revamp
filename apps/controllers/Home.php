<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct(){
		parent::__construct();		
		$this->load->model('Modglobal');


	}
	public function index()
	{

		$page = "home";
		$header = "";
		$footer = "";

		$today = strtotime(date('Y-m-d'));

		$promo_publish_query= $this->Modglobal->find('promo', array('date_publish!=' => ''));
		$promo_publish = $promo_publish_query->result_array();

		//echo $promo_num;

		foreach ($promo_publish as $row_publish) {
			
			$date_publish = strtotime($row_publish['date_publish']);
			$num2 = $today - $date_publish;

			if($today > $date_publish || $num2 == '0') {
				$status = "0";
		    }
		    else {
		    	$status ="1";
		    }
		    $data = array(
	        	'status' => $status,
	        	'date_edit' => date('d-m-Y h:i:s'),
	        );
	        $where = array(
	    		'id' => $row_publish['id'],
	        );
	        $this->Modglobal->update('promo', $data, $where);
		}



		$promo_set_query= $this->Modglobal->find('promo', array('status' => '0','date_end!=' => ''));
		$promo_set = $promo_set_query->result_array();

		foreach ($promo_set as $row_promo) {

			$date_promo = strtotime($row_promo['date_end']);

			if($today > $date_promo) {
				$data = array(
		        	'status' => '1',
		        	'date_edit' => date('d-m-Y h:i:s'),
		        );
		        $where = array(
		    		'id' => $row_promo['id'],
		        );
		        $this->Modglobal->update('promo', $data, $where);
		    }
		}

		

		$setting_query= $this->Modglobal->find('setting', array('id' => '1'));
		$setting = $setting_query->row_array();

		$tentang_query= $this->Modglobal->find('statik', array('id' => '1'));
		$tentang = $tentang_query->row_array();

		$home_query= $this->Modglobal->find('statik', array('id' => '15'));
		$home = $home_query->row_array();

		$transaksi_query= $this->Modglobal->find('statik', array('nama' => 'home_transaksi'));
		$transaksi = $transaksi_query->result_array();

		$fitur_query= $this->Modglobal->find('statik', array('nama' => 'home_fitur'));
		$fitur = $fitur_query->result_array();

		$testimonial_query= $this->Modglobal->find('testimonial', array('tipe' => '1','status' => '0'), 'rand()', '','6','0' );
		$testimonial = $testimonial_query->result_array();

		$promo_query= $this->Modglobal->find('promo', array('status' => '0'), 'id desc', '','6','0' );
		$promo = $promo_query->result_array();

		$merchant_query= $this->Modglobal->find('merchant', array('status' => '0'),'posisi','','21','0');
		$merchant = $merchant_query->result_array();

		$merchant_cate_query= $this->Modglobal->find('merchant_cate', array('status' => '0'),'posisi');
		$merchant_cate = $merchant_cate_query->result_array();

		




		

		$data = array(
			'content' => 'index',
			'page' => $page,
			'footer' => $footer,
			'header' => $header,
			'merchant' => $merchant,
			'merchant_cate' => $merchant_cate,
			'promo' => $promo,
			'testimonial' => $testimonial,
			'setting' => $setting,
			'tentang' => $tentang,
			'home' => $home,
			'transaksi' => $transaksi,
			'fitur' => $fitur,
		);
		$this->load->view('layouts/base', $data);
	}
	
	
	
	
}


