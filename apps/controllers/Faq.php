<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Faq extends CI_Controller {

	function __construct(){
		parent::__construct();		
		$this->load->model('Modglobal');


 
	}
	public function index()
	{
		$page = "faq";
		$header = "clean";
		$footer = "reguler";

		$findArr = array("%20","&");
		$replaceArr   = array(" ","-");

		$id = str_replace_url_encode($this->uri->segment(3));

		if($id == ""){
			$idx = "Akun dan Profil";
		}
		else {
			$idx = "$id";
		}

		$faq_cate_selected_query= $this->Modglobal->find('faq_cate', array('nama' => $idx));
		$faq_cate_selected = $faq_cate_selected_query->row_array();
		$num = $faq_cate_selected_query->num_rows();

		if($id == ""){
			$id = "3";
		}
		else {
			$id = $faq_cate_selected['id'];
		}

		$setting_query= $this->Modglobal->find('setting', array('id' => '1'));
		$setting = $setting_query->row_array();

		$faq_query= $this->Modglobal->find('faq', array('status' => '0', 'cate_id' => $id));
		$faq = $faq_query->result_array();

		$faq_cate_query= $this->Modglobal->find('faq_cate', array('status' => '0'));
		$faq_cate = $faq_cate_query->result_array();


		$data = array(
			'content' => 'faq',
			'page' => $page,
			'footer' => $footer,
			'header' => $header,
			'faq' => $faq,
			'faq_cate' => $faq_cate,
			'setting' => $setting,
			'id' => $id,
			'num' => $num,
			'faq_cate_selected' => $faq_cate_selected,
		);
		$this->load->view('layouts/base', $data);
	}

	public function search()
	{
		$page = "faq";
		$header = "clean";
		$footer = "reguler";

		$findArr = array("%20","&");
		$replaceArr   = array(" ","-");

		$id = str_replace_url_encode($this->uri->segment(3));
		$keyword = $this->input->post('keyword');


		if($id == ""){
			$id = "3";
		}
		else {
			$id = $faq_cate_selected['id'];
		}

		$setting_query= $this->Modglobal->find('setting', array('id' => '1'));
		$setting = $setting_query->row_array();

		$faq_query= $this->Modglobal->find('faq', array('status' => '0'),'',array('jawab' => $keyword));
		$faq = $faq_query->result_array();
		$num = $faq_query->num_rows();



		$data = array(
			'content' => 'faq_search',
			'page' => $page,
			'footer' => $footer,
			'header' => $header,
			'faq' => $faq,
			'setting' => $setting,
			'id' => $id,
			'num' => $num,
			'keyword' => $keyword,
		);
		$this->load->view('layouts/base', $data);
	}
	
	
	
}


