<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bisnis extends CI_Controller {

	function __construct(){
		parent::__construct();		
		$this->load->model('Modglobal');
		$this->load->helper(array('url', 'form'));
        $this->load->library(array('form_validation', 'Recaptcha'));
 
	}
	public function index()
	{
		$page = "bisnis";
		$header = "clean";
		$footer = "";

		$query= $this->Modglobal->find('statik', array('id' => '14'));
		$page_detail = $query->row_array();

		$setting_query= $this->Modglobal->find('setting', array('id' => '1'));
		$setting = $setting_query->row_array();

		$testimonial_query= $this->Modglobal->find('testimonial', array('tipe' => '2','status' => '0'), 'rand()', '','6','0' );
		$testimonial = $testimonial_query->result_array();

		$merchant_query= $this->Modglobal->find('merchant', array('status' => '0'),'posisi');
		$merchant = $merchant_query->result_array();

		$merchant_cate_query= $this->Modglobal->find('merchant_cate', array('status' => '0'),'posisi');
		$merchant_cate = $merchant_cate_query->result_array();

		$data = array(
			'content' => 'bisnis',
			'page' => $page,
			'footer' => $footer,
			'header' => $header,
			'page_detail' => $page_detail,
			'testimonial' => $testimonial,
			'setting' => $setting,
			'merchant' => $merchant,
			'merchant_cate' => $merchant_cate,
		);
		$this->load->view('layouts/base', $data);
	}
	public function detail()
	{
		$page = "bisnis";
		$header = "reguler";
		$footer = "reguler";

		$setting_query= $this->Modglobal->find('setting', array('id' => '1'));
		$setting = $setting_query->row_array();

		$id = $this->uri->segment(3);
		$kota_id = $this->uri->segment(5);
		
		//echo $kota_id;
		$query= $this->Modglobal->find('merchant', array('id' => $id));
		$page_detail = $query->row_array();

		if($kota_id ==""){
			$merchant_query= $this->Modglobal->find('merchant_list', array('merchant_id' => $id));
			//echo'kosong';
		}
		else {
			$kota = explode("|",$_GET['kota']);
			$merchant_query= $this->Modglobal->find('merchant_list', array('merchant_id' => $id, 'kota_id' => $kota[0]));
			//echo 'ada';
			
		}
		$merchant_list = $merchant_query->result_array();

		$data = array(
			'content' => 'merchant',
			'page' => $page,
			'footer' => $footer,
			'header' => $header,
			'page_detail' => $page_detail,
			'merchant_list' => $merchant_list,
			'id' => $id,
			'setting' => $setting,
		);
		$this->load->view('layouts/base', $data);
	}
	public function gabung()
	{
		$page = "bisnis";
		$header = "clean";
		$footer = "reguler";

		$setting_query= $this->Modglobal->find('setting', array('id' => '1'));
		$setting = $setting_query->row_array();

		$page_query= $this->Modglobal->find('statik', array('id' => '24'));
		$page_detail = $page_query->row_array();

		$data = array(
			'content' => 'bisnis_form',
			'page' => $page,
			'footer' => $footer,
			'header' => $header,
			'setting' => $setting,
			'page_detail' => $page_detail,
			'captcha' => $this->recaptcha->getWidget(), // menampilkan recaptcha
            'script_captcha' => $this->recaptcha->getScriptTag(), // javascript recaptcha ditaruh di 
		);
		$this->load->view('layouts/base', $data);
	}
	public function gabung_pro()
	{
		$setting_query= $this->Modglobal->find('setting', array('id' => '1'));
		$setting = $setting_query->row_array();

		$statik_query= $this->Modglobal->find('statik', array('id' => '24'));
		$statik = $statik_query->row_array();
		$recaptcha = $this->input->post('g-recaptcha-response');
        $response = $this->recaptcha->verifyResponse($recaptcha);
        if (!isset($response['success']) || $response['success'] <> true) {
            $this->index();
            //echo 'gagal';
            $this->session->set_flashdata("email_gagal","reCaptcha Belum Anda Pilih");
            redirect('bisnis');
        } else {

			$data = array(
	        	'nama' => str_replace_text($this->input->post('nama')),
	        	'nama_bisnis' => str_replace_text($this->input->post('nama_bisnis')),
	        	'email' => $this->input->post('email'),
	        	'respon' => '0',
	        	'tlp' => str_replace_text($this->input->post('tlp')),
	        	'date_create' => date("Y/m/d - h:i:sa"),
	        );

	        $this->Modglobal->insert('bisnis_gabung', $data);

			///SEND EMAIL
			$from_email = $this->input->post('email'); 
			$to_email = $this->input->post('email').','.$setting['email']; 
			$subject = $statik['nama2'];
			$content = '<b>Hai '.str_replace_text($this->input->post('nama')).'</b><br><br>'.$statik['map'];
			$link = 'http://dana.id/';

			$this->load->library('email'); 
			$this->load->helper('file');

			
			$this->email->set_newline("\r\n");
			$this->email->from($from_email, 'DANA'); 
			$this->email->to($to_email);
			$this->email->set_mailtype("html");
			$this->email->subject($subject); 
			$email_template = read_file('mail.html');

			$email_body = str_replace(
				array(
					'{subject}',
					'{content}',
					'{link}'
				), 
				array(
					$subject,
					$content,
					$link
				), 
				$email_template
			);
			
			$this->email->message($email_body);
	        $this->email->send();

			$this->session->set_flashdata("email_sent","Pesan Anda berhasil dikirim");
        	redirect('bisnis/berhasil');
	    }
	}
	public function berhasil()
	{
		$page = "bisnis";
		$header = "clean";
		$footer = "reguler";

		$setting_query= $this->Modglobal->find('setting', array('id' => '1'));
		$setting = $setting_query->row_array();

		$page_query= $this->Modglobal->find('statik', array('id' => '24'));
		$page_detail = $page_query->row_array();

		$data = array(
			'content' => 'bisnis_berhasil',
			'page' => $page,
			'footer' => $footer,
			'header' => $header,
			'setting' => $setting,
			'page_detail' => $page_detail,
		);
		$this->load->view('layouts/base', $data);
	}
	
	
	
}


