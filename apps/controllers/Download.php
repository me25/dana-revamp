<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Download extends CI_Controller {

	function __construct(){
		parent::__construct();		
		$this->load->model('Modglobal');


 
	}
	public function index()
	{
		$page = "download";
		$header = "clean";
		$footer = "reguler";
		$body = "downloadpage";

		$setting_query= $this->Modglobal->find('setting', array('id' => '1'));
		$setting = $setting_query->row_array();

		$data = array(
			'content' => 'download',
			'page' => $page,
			'footer' => $footer,
			'header' => $header,
			'body' => $body,
			'setting' => $setting,
		);
		$this->load->view('layouts/base', $data);
	}
	
	
	
}


