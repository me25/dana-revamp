<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produk extends CI_Controller {

	function __construct(){
		parent::__construct();		
		$this->load->model('Modglobal');


 
	}
	public function index()
	{
		$page = "produk";
		$header = "clean";
		$footer = "";

		$setting_query= $this->Modglobal->find('setting', array('id' => '1'));
		$setting = $setting_query->row_array();

		$query= $this->Modglobal->find('statik', array('nama' => 'produk'),'','','3','0');
		$produk = $query->result_array();

		$detail_query= $this->Modglobal->find('statik', array('id' => '4'));
		$page_detail = $detail_query->row_array();

		$data = array(
			'content' => 'produk',
			'page' => $page,
			'footer' => $footer,
			'header' => $header,
			'produk' => $produk,
			'setting' => $setting,
			'page_detail' => $page_detail,
		);
		$this->load->view('layouts/base', $data);
	}
	
	
	
}


