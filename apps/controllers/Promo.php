<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Promo extends CI_Controller {

	function __construct(){
		parent::__construct();		
		$this->load->model('Modglobal');


 
	}
	public function index()
	{
		$page = "promo";
		$header = "";
		$footer = "";
		
		$today = strtotime(date('Y-m-d'));

		$promo_publish_query= $this->Modglobal->find('promo', array('date_publish!=' => ''));
		$promo_publish = $promo_publish_query->result_array();

		//echo $promo_num;

		foreach ($promo_publish as $row_publish) {
			
			$date_publish = strtotime($row_publish['date_publish']);
			$num2 = $today - $date_publish;

			if($today > $date_publish || $num2 == '0') {
				$status = "0";
		    }
		    else {
		    	$status ="1";
		    }
		    $data = array(
	        	'status' => $status,
	        	'date_edit' => date('d-m-Y h:i:s'),
	        );
	        $where = array(
	    		'id' => $row_publish['id'],
	        );
	        $this->Modglobal->update('promo', $data, $where);
		}



		$promo_set_query= $this->Modglobal->find('promo', array('status' => '0','date_end!=' => ''));
		$promo_set = $promo_set_query->result_array();

		foreach ($promo_set as $row_promo) {

			$date_promo = strtotime($row_promo['date_end']);

			if($today > $date_promo) {
				$data = array(
		        	'status' => '1',
		        	'date_edit' => date('d-m-Y h:i:s'),
		        );
		        $where = array(
		    		'id' => $row_promo['id'],
		        );
		        $this->Modglobal->update('promo', $data, $where);
		    }
		}

		$setting_query= $this->Modglobal->find('setting', array('id' => '1'));
		$setting = $setting_query->row_array();

		$page_number = $this->uri->segment(3);

		if($page_number == "") {
			redirect('promo/index/1');
		}

		$artikel_query= $this->Modglobal->find('promo', array('status' => '0'),'id desc');
		$jumlah_data = $artikel_query->num_rows();
		$data_show = '9';
		$total_page = ceil($jumlah_data/$data_show);
		$start_page = ($page_number*$data_show) - $data_show;

		$promohl_query= $this->Modglobal->find('promo', array('status' => '0'), 'id desc', '','4','0' );
		$promohl = $promohl_query->result_array();

		$promo_query= $this->Modglobal->find('promo', array('status' => '0'),'id desc','',$data_show, $start_page);
		$promo = $promo_query->result_array();

		//$promo_query= $this->Modglobal->find('promo', array('status' => '0'), 'id desc', '','6','2' );
		//$promo = $promo_query->result_array();

		$promocate_query= $this->Modglobal->find('promo_cate', array('status' => '0'), 'posisi');
		$promocate = $promocate_query->result_array();

		$data = array(
			'content' => 'promo',
			'page' => $page,
			'footer' => $footer,
			'header' => $header,
			'promo' => $promo,
			'promohl' => $promohl,
			'promocate' => $promocate,
			'total_page' => $total_page,
			'page_number' => $page_number,
			'setting' => $setting,
		);
		$this->load->view('layouts/base', $data);
	}

	public function cate()
	{

		$setting_query= $this->Modglobal->find('setting', array('id' => '1'));
		$setting = $setting_query->row_array();

		$page = "promo";
		$header = "";
		$footer = "";

		$id = $this->uri->segment(3);

		$promoid_query= $this->Modglobal->find('promo_cate', array('nama' => $id));
		$promoid = $promoid_query->row_array();

		$page_number = $this->uri->segment(4);

		if($page_number == "") {
			redirect('promo/cate/'.$id.'/1');
		}

		$artikel_query= $this->Modglobal->find('promo', array('status' => '0','cate' => $promoid['id']),'id desc');
		$jumlah_data = $artikel_query->num_rows();
		$data_show = '9';
		$total_page = ceil($jumlah_data/$data_show);
		$start_page = ($page_number*$data_show) - $data_show;

		$promohl_query= $this->Modglobal->find('promo', array('status' => '0','cate' => $promoid['id']), 'id desc', '','2','0' );
		$promohl = $promohl_query->result_array();

		$promo_query= $this->Modglobal->find('promo', array('status' => '0','cate' => $promoid['id']),'id desc','',$data_show, $start_page);
		$promo = $promo_query->result_array();


		$promocate_query= $this->Modglobal->find('promo_cate', array('status' => '0'), 'posisi');
		$promocate = $promocate_query->result_array();

		$promocate_id_query= $this->Modglobal->find('promo_cate', array('nama' => $id));
		$promocate_id = $promocate_id_query->row_array();

		//echo $promoid['id'];

		$data = array(
			'content' => 'promo',
			'page' => $page,
			'footer' => $footer,
			'header' => $header,
			'promo' => $promo,
			'promohl' => $promohl,
			'promocate' => $promocate,
			'id' => $id,
			'total_page' => $total_page,
			'page_number' => $page_number,
			'setting' => $setting,
			'promocate_id' => $promocate_id,
		);
		$this->load->view('layouts/base', $data);
	}
	public function detail()
	{
		$page = "promo";
		$header = "reguler";
		$footer = "reguler";

		$setting_query= $this->Modglobal->find('setting', array('id' => '1'));
		$setting = $setting_query->row_array();


		$id = $this->uri->segment(3);

		$query= $this->Modglobal->find('promo', array('id' => $id));
		$page_detail = $query->row_array();

		$promo_query= $this->Modglobal->find('promo', array('status' => '0'),'rand()','','3', '0');
		$promo = $promo_query->result_array();

		$data = array(
			'content' => 'promo_detail',
			'page' => $page,
			'footer' => $footer,
			'header' => $header,
			'page_detail' => $page_detail,
			'promo' => $promo,
			'id' => $id,
			'setting' => $setting,
		);
		$this->load->view('layouts/base', $data);
	}
	
	
	
}


