<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Karir extends CI_Controller {

	function __construct(){
		parent::__construct();		
		$this->load->model('Modglobal');
 
	}
	public function index()
	{
		$page = "karir";
		$header = "clean";
		$footer = "";

		$query= $this->Modglobal->find('statik', array('id' => '16'));
		$page_detail = $query->row_array();

		$setting_query= $this->Modglobal->find('setting', array('id' => '1'));
		$setting = $setting_query->row_array();

		$query2= $this->Modglobal->find('statik', array('nama' => 'karir_fitur'));
		$karir = $query2->result_array();

		$query3= $this->Modglobal->find('karir_foto', array());
		$foto = $query3->result_array();

		$query4= $this->Modglobal->find('karir_cate', array('status' => '0'),'posisi');
		$job = $query4->result_array();

		$query5= $this->Modglobal->find('city', array('status' => '0'),'posisi');
		$city = $query5->result_array();

		$data = array(
			'content' => 'karir',
			'page' => $page,
			'footer' => $footer,
			'header' => $header,
			'page_detail' => $page_detail,
			'setting' => $setting,
			'karir' => $karir,
			'foto' => $foto,
			'job' => $job,
			'city' => $city,
		);
		$this->load->view('layouts/base', $data);
	}
	public function detail()
	{
		$page = "karir";
		$header = "";
		$footer = "reguler";

		$setting_query= $this->Modglobal->find('setting', array('id' => '1'));
		$setting = $setting_query->row_array();

		$id = $this->uri->segment(3);

		if($id = "0") {
			$id ="";
		}
		else {
			$id = $this->uri->segment(3);
		}

		$city_url = $this->uri->segment(5);

		if($city_url) {
			$filter_city = $city_url;
			$query_city= $this->Modglobal->find('city', array('nama' => $filter_city),'posisi');
			$city_filter = $query_city->row_array();

			$job_query= $this->Modglobal->find('karir', array('cate' => $id, 'kota' => $city_filter['id']),'nama');
		}
		else{
			$city_filter = "";
			if($id == "0") {
				$job_query= $this->Modglobal->find('karir', array());
			}
			else {
				$job_query= $this->Modglobal->find('karir', array('cate' => $id),'nama');
			}
			
		}

		$job_list = $job_query->result_array();

		$query= $this->Modglobal->find('karir_cate', array('id' => $id));
		$page_detail = $query->row_array();

		$query5= $this->Modglobal->find('city', array('status' => '0'),'posisi');
		$city = $query5->result_array();

		$query6= $this->Modglobal->find('karir_cate', array('status' => '0'));
		$karir_cate = $query6->result_array();

		$karir_query= $this->Modglobal->find('statik', array('id' => '16'));
		$page_detail_karir = $karir_query->row_array();

		$data = array(
			'content' => 'karir_detail',
			'page' => $page,
			'footer' => $footer,
			'header' => $header,
			'page_detail' => $page_detail,
			'page_detail_karir' => $page_detail_karir,
			'job_list' => $job_list,
			'id' => $id,
			'setting' => $setting,
			'karir_cate' => $karir_cate,
			'city' => $city,
			'city_filter' => $city_filter,
			'city_url' => $city_url,
		);
		$this->load->view('layouts/base', $data);
	}


	
}


