<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tentang extends CI_Controller {

	function __construct(){
		parent::__construct();		
		$this->load->model('Modglobal');
 
	}
	public function index()
	{
		$page = "tentang";
		$header = "clean";
		$footer = "reguler";

		
		$query= $this->Modglobal->find('statik', array('id' => '1'));
		$page_detail = $query->row_array();

		$data = array(
			'content' => 'tentang',
			'page' => $page,
			'footer' => $footer,
			'header' => $header,
			'page_detail' => $page_detail,
		);
		$this->load->view('layouts/base', $data);
	}
	
	
	
}


