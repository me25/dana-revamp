<?php
class Modglobal extends CI_Model {
    function __construct()
    {
        parent::__construct();
    }
   
    public function find($table, $where=array(), $order='', $like=array(), $limit=null, $page=null)
    {
        
        if ($order) {
            $this->db->order_by($order);
        }
        if ($like) {
            $this->db->like($like);
        }
        if ($where) {
            $this->db->where($where);
        }
        if (!is_null($limit) && !is_null($page)) {
            $this->db->limit($limit, $page);
        }
        return $this->db->select()
            ->from($table)
            ->get();
    }

    public function progress()
    {
        $query = $this->db->get('project_task');
        return $query->result();
    }


    //CRUD
    public function insert($table, $data)
    {
        return $this->db->insert($table, $data);
    }

    public function insert_batch($table, $data)
    {
        return $this->db->insert_batch($table, $data);
    }

    public function insert_id()
    {
        return $this->db->insert_id();
    }

    public function update($table, $data, $where)
    {
        $this->db->where($where);
        return $this->db->update($table, $data);
    }

    public function delete($table, $where)
    {
        $this->db->where($where);
        return $this->db->delete($table);
    }
}
?>