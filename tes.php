
<html>
<head>
    <head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="description" content="This is an example of a meta description. This will often show up in search results.">
<meta name="msapplication-TileColor" content="#0d2b7d">

<title>DANA CMS</title>
<link href="https://dana.id/newcms/assets/css/style.css?1" rel="stylesheet" type="text/css">
<script type="text/javascript" language="javascript" src="https://dana.id/newcms/assets/js/jquery.min.js"></script>
</head>
    
</head>
<div class="navbartop">
    <div class="user">
        <div class="name">admin</div>
        <div class="user_drop">
            <div>
                <a href="https://dana.id/newcms/home/password/" >Change Password</a>
                <a href="https://dana.id/newcms/auth/logout/">Logout</a>
            </div>
        </div>
    </div>
</div><header>
    <a class="logo">
        <img src="https://dana.id/newcms/assets/images/logo_dana.png" alt="">
    </a>
    <nav>
        <a href="https://dana.id/newcms/home" class="">
            <span class="sign"></span>
            <span class="ico">
                <img src="https://dana.id/newcms/assets/images/ico_dashboard.png" alt="">
            </span>
            <span class="menu">Dashboard</span>
        </a>
        <a href="https://dana.id/newcms/homeeditor" class="">
            <span class="sign"></span>
            <span class="ico">
                <img src="https://dana.id/newcms/assets/images/ico_reporting.png" alt="">
            </span>
            <span class="menu">Home Editor</span>
        </a>
        <a href="https://dana.id/newcms/produk" class="">
            <span class="sign"></span>
            <span class="ico">
                <img src="https://dana.id/newcms/assets/images/ico_reporting.png" alt="">
            </span>
            <span class="menu">Produk</span>
        </a>
        <a href="https://dana.id/newcms/bisnis" class="">
            <span class="sign"></span>
            <span class="ico">
                <img src="https://dana.id/newcms/assets/images/ico_reporting.png" alt="">
            </span>
            <span class="menu">Bisnis & Merchant</span>
        </a>
        <div href="" class="menu_show ">
            <span class="sign"></span>
            <span class="ico">
                <img src="https://dana.id/newcms/assets/images/ico_reporting.png" alt="">
            </span>
            <span class="menu">Merchant</span>
            <div class="submenu">
                <a href="https://dana.id/newcms/merchant">Listing</a>
                <a href="https://dana.id/newcms/merchant/posisi">Posisi</a>
                <a href="https://dana.id/newcms/merchant/cate">Kategori</a>
                <a href="https://dana.id/newcms/merchant/posisi_cate">Posisi Kategori</a>
            </div>
        </div>
        <div href="#" class=" menu_show">
            <span class="sign"></span>
            <span class="ico">
                <img src="https://dana.id/newcms/assets/images/ico_reporting.png" alt="">
            </span>
            <span class="menu">Promo</span>
            <div class="submenu">
                <a href="https://dana.id/newcms/promo">Listing</a>
                <a href="https://dana.id/newcms/promo/cate">Kategori</a>
                <a href="https://dana.id/newcms/promo/posisi">Posisi Kategori</a>
            </div>
        </div>
        <div href="#" class=" menu_show">
            <span class="sign"></span>
            <span class="ico">
                <img src="https://dana.id/newcms/assets/images/ico_reporting.png" alt="">
            </span>
            <span class="menu">Testimonial</span>
            <div class="submenu">
                <a href="https://dana.id/newcms/testimonial">Customer</a>
                <a href="https://dana.id/newcms/testimonial/bisnis">Bisnis</a>
            </div>
        </div>
        <div href="#" class=" menu_show">
            <span class="sign"></span>
            <span class="ico">
                <img src="https://dana.id/newcms/assets/images/ico_reporting.png" alt="">
            </span>
            <span class="menu">Karir</span>
            <div class="submenu">
                <a href="https://dana.id/newcms/karir/editor">Page Editor</a>
                <a href="https://dana.id/newcms/karir">Listing</a>
                <a href="https://dana.id/newcms/karir/kota">Kota</a>
                <a href="https://dana.id/newcms/karir/posisikota">Posisi Kota</a>
                <a href="https://dana.id/newcms/karir/job">Job Title</a>
                <a href="https://dana.id/newcms/karir/posisi">Posisi Job Title</a>
                <a href="https://dana.id/newcms/karir/foto">Slide Foto</a>
            </div>
        </div>
        
        <div href="#" class=" menu_show">
            <span class="sign"></span>
            <span class="ico">
                <img src="https://dana.id/newcms/assets/images/ico_reporting.png" alt="">
            </span>
            <span class="menu">FAQ</span>
            <div class="submenu">
                <a href="https://dana.id/newcms/faq">List</a>
                <a href="https://dana.id/newcms/faq/kategori">Kategori</a>
            </div>
        </div>
        <div href="#" class="selected menu_show">
            <span class="sign"></span>
            <span class="ico">
                <img src="https://dana.id/newcms/assets/images/ico_reporting.png" alt="">
            </span>
            <span class="menu">Gabung DANA</span>
            <div class="submenu">
                <a href="https://dana.id/newcms/gabung">Listing</a>
                <a href="https://dana.id/newcms/gabung/page">Editor</a>
            </div>
        </div>
        <a href="https://dana.id/newcms/hubungi" class="">
            <span class="sign"></span>
            <span class="ico">
                <img src="https://dana.id/newcms/assets/images/ico_reporting.png" alt="">
            </span>
            <span class="menu">Hubungi DANA</span>
        </a>
        <div href="#" class=" menu_show">
            <span class="sign"></span>
            <span class="ico">
                <img src="https://dana.id/newcms/assets/images/ico_reporting.png" alt="">
            </span>
            <span class="menu">Statik Page</span>
            <div class="submenu">
                <a href="https://dana.id/newcms/statik/tentang">Tentang Dana</a>
                <a href="https://dana.id/newcms/statik/terms">Terms Condition</a>
                <a href="https://dana.id/newcms/statik/contact">Contact Us</a>
            </div>
        </div>

        <div href="https://dana.id/newcms/setting" class=" menu_show">
            <span class="sign"></span>
            <span class="ico">
                <img src="https://dana.id/newcms/assets/images/ico_reporting.png" alt="">
            </span>
            <span class="menu">Setting</span>
            <div class="submenu">
                <a href="https://dana.id/newcms/setting">Account</a>
                <a href="https://dana.id/newcms/setting/kota">Kota</a>
            </div>
        </div>
        <a href="https://dana.id/newcms/member" class="">
            <span class="sign"></span>
            <span class="ico">
                <img src="https://dana.id/newcms/assets/images/ico_reporting.png" alt="">
            </span>
            <span class="menu">Admin</span>
        </a>
    </nav>
</header>

<div class="content_ful">
    <div class="table_show">
        <div class="table_head">
            <div class="info">2363 Hubungi DANA</div>
            <div class="clearfix"></div>
        </div>
        <table id="table_sort" class="table_style" cellspacing="0" width="100%"data-page-length="10" >
            <thead>
                <tr>
                    <th width="20%">Tanggal</th>
                    <th>Nama</th>
                    <th>Email</th>
                    <th>Pesan</th>
                </tr>
            </thead>
            <tbody>
                                    

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/05/31 - 11:07:3</td>
                        <td>Sunardi</td>
                        <td>ahaulioe@gmail.com</td>
                        <td>Anak saya blum punya ktp, bagaimana cara update dana anak saya ke premium</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/01 - 12:53:1</td>
                        <td>Hendra</td>
                        <td>hesaalvian06@gmail.com</td>
                        <td>Pulsa saya kapan masuknya saldo saya sudah terpotong gimana si knapa lambat sekali </td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/01 - 01:09:2</td>
                        <td>ikang fauzi</td>
                        <td>icank8686@gmail.com</td>
                        <td>transaksi kediua sya pusla smatrfren 2019   0601    1112    1280    0100    1664    9860    7181    255 yang 50k belum di proses.tlong untuk sgra memproses nya.trimaksh</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/01 - 01:12:3</td>
                        <td>Nazil Ilham</td>
                        <td>nazil.ilham11@gmail.com</td>
                        <td>Saya Ingin Melakukan pembayaran Bukalapak dengan DANA tetapi nomor telepon yang digunakan telah hilang</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/01 - 01:20:3</td>
                        <td>Pandi Baskara</td>
                        <td>pandiaraksab@gmail.com</td>
                        <td>Transaksi pembelian pulsa saya di nomor transaksi berikut 2019  0601111212800100166096107246695 belum saya terima. Tolong di proses segera di proses.</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/01 - 01:35:2</td>
                        <td>Rica</td>
                        <td>zaisy968@gmail.com</td>
                        <td>Saldo saya sudah terpotong di aplikasi dana saya tapi pulsa blm jugamasuk</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/01 - 01:36:0</td>
                        <td>Rica</td>
                        <td>zaisy968@gmail.com</td>
                        <td>Saldo saya sudah terpotong di aplikasi dana saya tapi pulsa blm jugamasuk</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/01 - 01:40:5</td>
                        <td>Zaenal Arifin</td>
                        <td>za054404@gmail.com</td>
                        <td>Pembayaran saya belum masuk, lama sekali?? 2019 0601    1112    1280    0100    1665    4220    7231    573</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/01 - 02:18:0</td>
                        <td>Irmawati</td>
                        <td>ismailwawe@yahoo.coom.id</td>
                        <td>Butuh dana 1000000</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/01 - 02:32:4</td>
                        <td>yuda</td>
                        <td>yyudaaging71@gmail.com</td>
                        <td>Sya akan menggunakan pitir ini</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/01 - 03:30:1</td>
                        <td>Yuli sandi</td>
                        <td>yuliarlieyuli776@gmail.com</td>
                        <td>Cara Gisi saldo atau deposit y g mm,,,</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/01 - 03:52:2</td>
                        <td>Ahmad choidir</td>
                        <td>ahmadchoidir5664@gmail.com</td>
                        <td>Tolong pin saya d reset dan d ganti yg baru saya lupa</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/01 - 03:54:5</td>
                        <td>Ali kodir</td>
                        <td>alikodir55@gmail.com</td>
                        <td>Pergantian nomor telepon Di akun alikodir55@gmail.com ,,mohon sisa dana di bukalapak di nomor yang telah hangus 082313121273 dialihkan dinomor telepon yang baru 081229522815 agar bisa buat belanja lagi di buka lapak.karena buka lapak mengarahkan di help@dana.id yang bisa membantu</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/01 - 04:46:4</td>
                        <td>Bangkit</td>
                        <td>bangkit130426@gmail.com</td>
                        <td>08112110570 no saya

Kenapa sampe skr jadi ga bisa transfer lagi?
Gimana nih solusinya? Uang saya ga bisa di pake
Di dm ke ig ga ada solusi</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/01 - 04:56:4</td>
                        <td>Muhammad Nasyir</td>
                        <td>januariperkasa@gmail.com</td>
                        <td>Kak, request pulsa xl sebesar 50k ke 0819 1552 7010 no. Invoice SKG&amp;#45;108830227 kok belum masuk ya? Tolong dicek ulang kak. Terimakasih</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/01 - 05:41:5</td>
                        <td>Naura</td>
                        <td>nourachantika@gmail.com</td>
                        <td>No dana 08311253115
Id transaksi 02019  0531    1112    1280    0100    1667    0450    7285    567
Pembelian pulsa senilai Rp 100.000 belum di trima tetapi saldo sudah terpotong
Pemelian dari tanggal 31 mei 2019</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/01 - 07:32:5</td>
                        <td>Rozie</td>
                        <td>roziefight05@gmail.com</td>
                        <td>Tombol bagikan tidak berfungsi
Di dana kaget
</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/01 - 07:33:5</td>
                        <td>Rozie</td>
                        <td>roziefight05@gmail.com</td>
                        <td>Tombol bagikan tidak berfungsi
Di dana kaget
</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/01 - 08:14:2</td>
                        <td>Ikhwanto</td>
                        <td>Ikhwanto0889@gmail.com</td>
                        <td>
Pak tadi saya transfer ke bank di aplikasi dana ada pemberitahuan SUKSES tapi di Atm saya belum masuk pak uangnya. Yg saya transfer pake apps dana</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/01 - 09:02:4</td>
                        <td>Chandra Pratama Wijaya</td>
                        <td>cpratamawijaya@gmail.com</td>
                        <td>Saya beli (Top Up) Diamond untuk Game Speed Drifter sebesar Rp300.000 tapi  tidak masuk kr ID game saya ini 11822383815005934
Silahkan dicek...ini nomer transaksi saya
2019    0531    1112    1280    0110    1666    7870    7839    171
Via Codapay bayar Lewat Dana..screen lshoot dan bukti lainya lengkap..segera di tidak lanjut .</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/01 - 09:15:1</td>
                        <td>Ikhwanto</td>
                        <td>Ikhwanto0889@gmail.com</td>
                        <td>Masalah tranfer sukses tapi saya cek di atm saya kok belum masuk</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/01 - 09:16:2</td>
                        <td>Siti rohani</td>
                        <td>srohani983@gmail.com</td>
                        <td>Kemaren Saia byr listrik atas nama BPK Romli TPI knp blum ada konfirmasinya.na
D tertera d riwayat orderan msh dlm proses..</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/01 - 09:16:4</td>
                        <td>Lina anugerah</td>
                        <td>lina.usagi.chan@gmail.com</td>
                        <td>Lupa pin dana di bukalapak
Bagaimana cara reset nya?
</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/01 - 09:16:5</td>
                        <td>Siti rohani</td>
                        <td>srohani983@gmail.com</td>
                        <td>Kemaren Saia byr listrik atas nama BPK Romli TPI knp blum ada konfirmasinya.na
D tertera d riwayat orderan msh dlm proses..</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/01 - 10:47:0</td>
                        <td>Rumbi</td>
                        <td>agrorumbi1@gmail.com</td>
                        <td>case :

saya pernah daftar aplikasi Dana namun nomor HP saya sdh hilang, Nomor lama 082343380560 apakah itu bisa digantikan/direset karena nomor baru saya sekarang di 082343786137 nama dan nomor KTP juga sama

Terima Kasih</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/01 - 11:01:3</td>
                        <td>Novita ronika</td>
                        <td>novitaronika1905@gmail.com</td>
                        <td>Kenapa pemesanan tiket tix id dibatalkan terus?</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/01 - 11:02:0</td>
                        <td>Novita ronika</td>
                        <td>novitaronika1905@gmail.com</td>
                        <td>Kenapa pemesanan tiket tix id dibatalkan terus?</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/01 - 12:03:2</td>
                        <td>Cherley</td>
                        <td>cherley58@gmail.com</td>
                        <td>Cara membuka dana yang twlah terblokir karena salah pin</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/01 - 12:15:3</td>
                        <td>Fernando ngai</td>
                        <td>ngaifernando@gmail.com</td>
                        <td>Kenapa ya mau top up saldo lewat atm BRI  gak bisa ya?</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/01 - 12:16:1</td>
                        <td>Fernando ngai</td>
                        <td>ngaifernando@gmail.com</td>
                        <td>Kenapa ya mau top up saldo lewat atm BRI  gak bisa ya?</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/01 - 12:41:1</td>
                        <td>Kurnia</td>
                        <td>reizan.kurnia@gmail.com</td>
                        <td>Reset password</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/01 - 12:58:1</td>
                        <td>Handy Kurniawan Halim</td>
                        <td>handyhalim@yahoo.com</td>
                        <td>Akun dana saya terkunci dan saya sudah mengganti dengan PIN baru. Tetapi masih tidak bisa. Tolong dibantu</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/01 - 01:02:2</td>
                        <td>Novianti</td>
                        <td>cerryberry0@gmail.com</td>
                        <td>Saya ingin memblokir/membekukan akun Dana dgn nomer hp 083878772262, karena sim card dgn hilang,,bagaimana cara &amp; prosedurnya? </td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/01 - 01:02:4</td>
                        <td>Novianti</td>
                        <td>cerryberry0@gmail.com</td>
                        <td>Saya ingin memblokir/membekukan akun Dana dgn nomer hp 083878772262, karena sim card dgn hilang,,bagaimana cara &amp; prosedurnya? </td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/01 - 01:16:4</td>
                        <td>Welly Tunggala</td>
                        <td>wellytunggala@gmail.com</td>
                        <td>Halo,

Apakah DANA ini tidak menyediakan Rest APIs khusus untuk web developer? agar para programmer web penjualan online dapat menggunakan DANA sebagai salah satu media pembayaran.

Thanks</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/01 - 01:29:0</td>
                        <td>Toto Indarto</td>
                        <td>t2.indarto@gmail.com</td>
                        <td>Saya mempunyai account Dana di nomor 081807770666 dan 081212336699. Saya baru tahu untuk 1 ktp hanya bisa 1 account dana premium.

Apakah account saya di nomor 081807770666 bisa diubah ke nomor 081212336699, sehingga saya hanya punya 1 account saja?

Terima kasih.</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/01 - 01:53:4</td>
                        <td>Darmawati</td>
                        <td>islahudindarmawati@gmail.com</td>
                        <td>My tlg cek trx pulsa internet 50 ke nomor 085333661126 pulsa internetnya apakah dobel masuk nya</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/01 - 01:54:3</td>
                        <td>Ahmad choidir</td>
                        <td>ahmadchoidir5664@gmail.com</td>
                        <td>Bagai mana cara agar uang saya tidak di bekukan karna saya mau membayar belanja saya </td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/01 - 01:55:2</td>
                        <td>Nazil Ilham</td>
                        <td>nazil.ilham11@gmail.com</td>
                        <td>Bagaimana caran nya saya login ke akun DANA yang nomor telepon nya telah hilang? 
Dan akun DANA tersebut telah terhubung dengan akun Bukalapak, Apakah saya bisa mengganti nomor telepon tersebut atau mendapatkan refund ke akun Bukalapak?</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/01 - 01:58:4</td>
                        <td>Asep budiawan</td>
                        <td>abudiawan66@gmail.com</td>
                        <td>Mau ganti no hp</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/01 - 01:59:1</td>
                        <td>Asep budiawan</td>
                        <td>abudiawan66@gmail.com</td>
                        <td>Mau ganti no hp</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/01 - 02:01:0</td>
                        <td>Asep budiawan</td>
                        <td>abudiawan66@gmail.com</td>
                        <td>Mau ganti no hp</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/01 - 02:03:3</td>
                        <td>Gilang</td>
                        <td>orangtajirpuol@gmail.com</td>
                        <td>Kak ini kok isi pulsa masih pending???
Id transaksi: 2019  0601    1112    1280    0100    1663    0310    7221    772
</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/01 - 02:03:5</td>
                        <td>Gilang</td>
                        <td>orangtajirpuol@gmail.com</td>
                        <td>Kak ini kok isi pulsa masih pending???
Id transaksi: 2019  0601    1112    1280    0100    1663    0310    7221    772
</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/01 - 02:56:1</td>
                        <td>sjachrizal dahlan</td>
                        <td>sjachrizal05@gmail.com</td>
                        <td>Saya mau bayar listrik, saya tekan logo listrik bisa, tapi halamannya kosong. Mohon bantuannya.</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/01 - 02:56:4</td>
                        <td>sjachrizal dahlan</td>
                        <td>sjachrizal05@gmail.com</td>
                        <td>Saya mau bayar listrik, saya tekan logo listrik bisa, tapi halamannya kosong. Mohon bantuannya.</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/01 - 03:10:5</td>
                        <td>sjachrizal dahlan</td>
                        <td>sjachrizal05@gmail.com</td>
                        <td>Saya mau bayar listrik, saya tekan logo listrik bisa, tapi halamannya kosong. Mohon bantuannya.</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/01 - 03:35:1</td>
                        <td>Herry</td>
                        <td>cs.care22@gmail.com</td>
                        <td>I hereby inform you that I want to close my fund account because I already have another fund account. Please help me to immediately close my account.</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/01 - 05:00:5</td>
                        <td>ERWIN NUGROHO</td>
                        <td>campingmakmu@gmail.com</td>
                        <td>Halo dana boleh kah saya minta kaos dana yg seperti diacara tv pesbuker karena saya sangat bangga dengan aplikasi dana karena memudahkan dan mempercepat transaksi saya. Kalau saya sudah dapet kaos nya saya akan mempromosikan aplikasi dana dengan ketemen2 saya.

Alamat: Mojoroto, Kota Kediri, Jawa Timur
Kode pos: 64117
Alamat lengkap: jalan semeru gg. 1 no. 22 RT 03/RW 08</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/01 - 05:27:3</td>
                        <td>Ahmad choidir</td>
                        <td>ahmadchoidir5664@gmail.com</td>
                        <td>Saya lupa pin saya dan saldo saya terblokir terlalu lama tolong bantu
</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/01 - 05:30:5</td>
                        <td>Ahmad choidir</td>
                        <td>ahmadchoidir5664@gmail.com</td>
                        <td>Saya lupa pin saya dan saldo saya terblokir terlalu lama tolong bantu
</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/01 - 05:46:2</td>
                        <td>Ida</td>
                        <td>ida.limlicu21@gmail.com</td>
                        <td>Saya tadi pagi ad membeli pulsa 25.000 dengan :
no.invoice : SKG&amp;#45;108845009
 id transaksi :
2019    0601    1112    1280    0100    1662    4370    7345    077. 
Tp voucer nya sampai sekarang tidak kunjung muncul. Tolong di  beri respon atas komplain saya ini. Terima kasih</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/01 - 05:54:4</td>
                        <td>RIKA FEBRILIANTI</td>
                        <td>rfebrilianti20@gmail.com</td>
                        <td>SAYA LUPA PASSWORD KETIKA HARUS MEMASUKAN PIN DANA SEHINGGA TIDAK BISA DI AKSES, PADAHAL SAYA SANGAT MEMBUTUHKAN UNTUK MENGGUNAKAN TIX ID , SETIAP SAYA TELEPON CS, BELUM SELESAI BERBICARA PULSA SUDAH HABIS DULUAN.</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/01 - 06:44:3</td>
                        <td>Sopiah</td>
                        <td>trulysovie@yahoo.co.id</td>
                        <td>&amp;#34;Saya beli paket extra xl 12rbu untuk 30gb. Tapi ternyata saya hanya mendapatkan link untuk membeli pulsa/data. Saya merasa dibodohi.&amp;#34;
Jadi saya menyia2kan uang 12rbu saya hanya untuk mendapatkan link.
Harap jangan membodohi customer dengan penawaran menggiurkan tanpa ada keterangan apapun. Terimakasih</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/01 - 07:17:3</td>
                        <td>Khairul Fatah</td>
                        <td>iruulfhat@gmail.com</td>
                        <td>Saya memiliki voucher tagihan DANA, kenapa tidak bisa digunakan? Sudah Saya coba berkali kali tetap tidak bisa</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/01 - 07:30:2</td>
                        <td>Asti Hartini</td>
                        <td>astihartinii@gmail.com</td>
                        <td>Cashback Ramayana / Robinson masih berlaku gak ? </td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/01 - 07:31:3</td>
                        <td>Asti Hartini</td>
                        <td>astihartinii@gmail.com</td>
                        <td>Cashback Ramayana / Robinson masih berlaku gak ? </td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/01 - 07:50:0</td>
                        <td>M. Ariansyah</td>
                        <td>Heffi@email.co</td>
                        <td>No hp 082371283973  ku rusak/ilang sigyal. Sdh brp kli coba daftar no lain, layanan blsan, no kk anda sdah ada. Mhn dibantu gmn solusi nya, bisa daftar no bru tx</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/01 - 07:51:0</td>
                        <td>M. Ariansyah</td>
                        <td>Heffi@email.co</td>
                        <td>No hp 082371283973  ku rusak/ilang sigyal. Sdh brp kli coba daftar no lain, layanan blsan, no kk anda sdah ada. Mhn dibantu gmn solusi nya, bisa daftar no bru tx</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/01 - 08:30:0</td>
                        <td>sharon</td>
                        <td>sh9ron@yahoo.com</td>
                        <td>
saya tidak mendapatkan cashback di 2 merchant dan saya akan berpikir berkali&amp;#45;kali lagi untuk menggunakn DANA jika hal ini tidak selesai dan kemungkinan kecil akn merekomendasikn DANA kepada teman dan saudara: 
dominos  transaksi id 201905291112    12800100166169707155816 pada 29 mei 2019 10:51:05
saya belum menggunakan voucher cashback sama sekali pada minggu ini dan blm sama sekali digunakan

kamu tea transaksi id
2019053110121481030100166163404863911
pada 31 mei 2019 16:35:30
karena ini udh 1 minggu berlalu sy menggunakan promo 50%

tolong diperhatikan permasalahan ini
saya akan memikirkan kembali untuk menggunakan aplikasi DANA krn saya kecewa dgn ini jika tidak di infus cashbacknya, terimakasih</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/01 - 08:30:5</td>
                        <td>sharon</td>
                        <td>sh9ron@yahoo.com</td>
                        <td>
saya tidak mendapatkan cashback di 2 merchant dan saya akan berpikir berkali&amp;#45;kali lagi untuk menggunakn DANA jika hal ini tidak selesai dan kemungkinan kecil akn merekomendasikn DANA kepada teman dan saudara: 
dominos  transaksi id 201905291112    12800100166169707155816 pada 29 mei 2019 10:51:05
saya belum menggunakan voucher cashback sama sekali pada minggu ini dan blm sama sekali digunakan

kamu tea transaksi id
2019053110121481030100166163404863911
pada 31 mei 2019 16:35:30
karena ini udh 1 minggu berlalu sy menggunakan promo 50%

tolong diperhatikan permasalahan ini
saya akan memikirkan kembali untuk menggunakan aplikasi DANA krn saya kecewa dgn ini jika tidak di infus cashbacknya, terimakasih</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/01 - 08:43:1</td>
                        <td>Arfan alfarisi</td>
                        <td>alfarisyarfan@gmail.com</td>
                        <td>Saya ingin mengganti nomer lama saya yg sudah tidak aktif dengan nomer baru agar dapet kode otp di bukalapak.</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/01 - 09:01:4</td>
                        <td>Wida widiawati</td>
                        <td>Widiafazrila86@gmail.com</td>
                        <td>Pembelian paket data ko blom sukses juga.masih proses trus.
Id t ransaksi 2019  0601    1112    1280    0100    1662    0340    7282    900</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/01 - 09:02:1</td>
                        <td>Arfan alfarisi</td>
                        <td>alfarisyarfan@gmail.com</td>
                        <td>Saya ingin mengganti nomer hp yg lama yg sudah tidak aktif ke nomer yg baru.
Tolong di bantu.</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/01 - 09:48:0</td>
                        <td>Moch kudhori anwar</td>
                        <td>p.ndhory@gmail.com</td>
                        <td>Saya mau ganti no telp kok ribet banget ya</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/01 - 10:08:1</td>
                        <td>Nadya Alvianti</td>
                        <td>nadyalvnt@gmail.com</td>
                        <td>Malam. Saya nadya, saya lupa pin dan akhirnya ke blokir. Bagaimana agar saya bisa ganti pin baru? Thanks</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/01 - 10:16:5</td>
                        <td>Samsuddin</td>
                        <td>wanlovecia43@gmail.com</td>
                        <td>Kode vaucer google play tidak bisa di gunakan</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/01 - 10:35:0</td>
                        <td>Moch kudhori anwar</td>
                        <td>p.ndhory@gmail.com</td>
                        <td>Saya mau ganti no telp, karena no telp yang lama tidak aktif, dan say blm sempat login akun dana, gmn solusinya</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/01 - 10:49:2</td>
                        <td>sandra</td>
                        <td>sandrasusanto809@gmail.com</td>
                        <td>cashback saya blm masuk wkt saya beli tiket prj dan beli kopi torabika biasanya lsg masuk ke wallet saya dan sampai skrg blm masuk jg utk menghubungi lsg cs dana sangat sulit sekli knp ya?mohon bantuan secepatnya makasih.</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/01 - 10:52:1</td>
                        <td>Rizal</td>
                        <td>rizalzal591@gmail.com</td>
                        <td>Saya ingin menghapus/menutup akun saya</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/01 - 11:01:2</td>
                        <td>Fresya</td>
                        <td>Fresyaghaniy@gmail.com</td>
                        <td>Saldo saya tiba2 di bekukan sudah lebh dri 2 bulan.. Tolong segera di bukakn agar sy bisa transksi lagi</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/01 - 11:01:5</td>
                        <td>Fresya</td>
                        <td>Fresyaghaniy@gmail.com</td>
                        <td>Saldo saya tiba2 di bekukan sudah lebh dri 2 bulan.. Tolong segera di bukakn agar sy bisa transksi lagi</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/02 - 12:13:3</td>
                        <td>Wahyu Nugraha</td>
                        <td>nugragawahyu561@gmail.com</td>
                        <td>Selamat malam, akun tix id saya tidak bisa di gunakan transaksi membeli tiket setelah ganti nomer telpon saya. Mohon bantuannya terimakasih.</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/02 - 12:32:0</td>
                        <td>Agnes Ibrahim Lubis</td>
                        <td>neschatziworld@gmail.com</td>
                        <td>Saya ingin menghapus akun dana saya, karna beberapa kali saya tidak mendapat cashback dan voucher, sudah melapor 2 kali katanya akan ditindaklanjuti, sampai sekarang tidak ada yang sesuai dengan jawaban keluhan saya, terima kasih. Nomor akun dana saya 082211887087.</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/02 - 01:49:0</td>
                        <td>muhammad sidik</td>
                        <td>msidik051117@gmail.com</td>
                        <td>gimana carra merubah no telepon yang telah hangus masa berlakunya dan gimana cara mendapatkan uang saya di akun dana sedangkan no hangus masa berlakunya </td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/02 - 02:05:5</td>
                        <td>Fabian</td>
                        <td>fabiqnrasyidr98@gmail.com</td>
                        <td>
1 089643586224
2 fabian
3 saldo dana 0
4 blum perna transaksi
5 blum perna transaksi
6 blum perna transaksi
7 no yang lama hilang dan saya mau bikin akun dana yang baru</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/02 - 02:42:1</td>
                        <td>Aria</td>
                        <td>areekuzuma@gmail.com</td>
                        <td>Pagi.. untuk saat ini say ingin akun saya di hapus. Dikarenakan sim card saya hilang</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/02 - 04:33:0</td>
                        <td>Iwan</td>
                        <td>kiwik1097@gmail.com</td>
                        <td>Tolong bantu saya tidak bisa verifikasi identitas dikarenakan sudah terdaftar di hp saya yang satunya. Tapi hp itu ilang akun dana yang didaftarkan memakai nomor (081547831770) dan nama yang terdaftar di akun saya (IWAN). .TOLONG bantuannya</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/02 - 04:33:1</td>
                        <td>Iwan</td>
                        <td>kiwik1097@gmail.com</td>
                        <td>Tolong bantu saya tidak bisa verifikasi identitas dikarenakan sudah terdaftar di hp saya yang satunya. Tapi hp itu ilang akun dana yang didaftarkan memakai nomor (081547831770) dan nama yang terdaftar di akun saya (IWAN). .TOLONG bantuannya</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/02 - 05:31:3</td>
                        <td>Ahmad Thoriqussalam</td>
                        <td>AhmadThoriqussalam@yahoo.co.id</td>
                        <td>Saya ingin melapor tentang akun yang menyalagunakan app dana.
Pada tanggal 03 dan 04 saya telah ditipu untuk melakukan DANATOPUP DNID 0813524915 (Rekening CIMB Niaga 8059081352491545) melalui ATM BNI.

Pada kejadian tersebut saya telah menghubungi pihak kepolisian dan bank BNI.

Maka dari itu saya menghubungi pihak DANA app untuk melakukan tindakan lebih lanjut.

Terima kasih</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/02 - 06:07:3</td>
                        <td>Alz</td>
                        <td>gbcoksatu@gmail.com</td>
                        <td>Pada tanggal 1 juni saya melakukan topup unipin senilai 50rb, akan tetapi saya tidak mendapatkan promo cashback 50% ke Dana.

Apakah promo quotanya sudah habis?
Untuk mengetahui quota promo dimana ya?</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/02 - 06:17:2</td>
                        <td>Riko pratama</td>
                        <td>rikopratama943@gmail.com</td>
                        <td>Voucer saya gak bisa di pakai... Apa penyebab nya gak bisa di pakai?&amp;#33;</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/02 - 07:31:5</td>
                        <td>Gaung Samudra Aji</td>
                        <td>gaungsamudra14@gmail.com</td>
                        <td>Selamat Pagi

Terakhir top up 100.000 dan sudah transaksi 1x sebesar 66.000 dengan sisa saldo 34.000.

Saya merasa yakin bahwa sudah mengisi PIN dengan benar dan mencoba mengubah kembali tapi masih gagal masuk padahal saya tau persis PIN dana saya, yaitu 163778. Bagaimana cara mengatasinya? 

</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/02 - 07:53:5</td>
                        <td>Rizqi</td>
                        <td>rizqiaria1409@gmail.com</td>
                        <td>Saya tidak bisa kirim uang</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/02 - 09:23:4</td>
                        <td>Ratna dianti</td>
                        <td>ratnadianti789@yahoo.com</td>
                        <td>Saya kemaren tertipu oleh seorang yg menggunakan dana dompet ini.. Saya disuruh top up sampe 1jt lebih.. Apa anda bisa membantu untuk mengembalikan uang saya? Atau bantu sekedar mengetahui penipu tersebut?</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/02 - 09:38:2</td>
                        <td>Vesty Prihatin</td>
                        <td>gilbert.sumarsikin@gmail.com</td>
                        <td>Mengapa saya waktu login, PIN tidak diperlukan. Terjadian ini setelah verifikasi failed until menjadi Dana Premium.</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/02 - 10:49:0</td>
                        <td>Miki</td>
                        <td>mikisuharman20@hotmail.com</td>
                        <td>Sampai kapan ya promo cashback ramayana 50%? </td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/02 - 11:51:1</td>
                        <td>Hasanudin</td>
                        <td>dodengtempur17@gmail.com</td>
                        <td>Akun dana saya di bekukan tanpa ada konfirmasi .
Minta di aktifkan kembali untuk akun dana saya yang di bekukan?
Karna saya sulit untuk melakukan transaksi apapun.</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/02 - 11:52:2</td>
                        <td>Hasanudin</td>
                        <td>dodengtempur17@gmail.com</td>
                        <td>Akun dana saya di bekukan tanpa ada konfirmasi .
Minta di aktifkan kembali untuk akun dana saya yang di bekukan?
Karna saya sulit untuk melakukan transaksi apapun.</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/02 - 01:20:0</td>
                        <td>Ryan berliana</td>
                        <td>berlianajr@gmail.com</td>
                        <td>Saya sudah top up melalu bri briva rekening terdebet tetapi saldo dana tidak berubah ini gmna kelanjutannya min tolong respon secepat mungkin</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/02 - 01:39:2</td>
                        <td>Rahul</td>
                        <td>rahullusian09@gmail.com</td>
                        <td>Bagaimana cara mengatasi gagal verifikasi user Premium &amp;#34;Sistem sedang sibuk.Mohon coba kembali lain waktu&amp;#34; udah 3 hari</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/02 - 02:49:5</td>
                        <td>Renaldi </td>
                        <td>Renaldi_ulya@yahoo.com</td>
                        <td>saya memiliki akun DANA, dan nomor saya hilang, dan di akun DANA saya ada uang yang ingin saya cairkan ,apakah pihak DANA bisa membantu?</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/02 - 03:39:3</td>
                        <td>Fahruroje jamalullail</td>
                        <td>fahrul030294@gmail.com</td>
                        <td>Kenapa saya upgrade premium gagal mulu, mohon bantuan nya</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/02 - 03:39:5</td>
                        <td>wangga alfyan</td>
                        <td>wanggaalfyan7@gmail.com</td>
                        <td>saya ingin menghapus akun dana premium saya di nomor 085313476238 karena saya ingin mendaftar akun premium di nomor lain</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/02 - 04:42:4</td>
                        <td>Robby Tanuwijaya</td>
                        <td>b19rby@gmail.com</td>
                        <td>Halo, di toko saya pake mesin kasir MOKA, dan skr sudah 10 hari sudah aktif cashback 50%nya.
Saya mau konfirmasi aja, menurut MOKA toko hanya dikenakan 1&amp;#45;2% per transaksi cashback. Apa benar begitu atau ada tambahan2 biaya lainnya ? Terima kasih.
0817191176
Robby</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/02 - 05:02:4</td>
                        <td>Devita Dewi Kushartanti</td>
                        <td>devitadewik@gmail.com</td>
                        <td>kenapa setiap selesai transaksi saya tidak mendapatkan cashback dan tidak mendapatkan spin n win?</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/02 - 05:02:5</td>
                        <td>Christiawan Nugroho</td>
                        <td>christiawannugroho@outlook.com</td>
                        <td>Halo selamat sore. Saya pengguna aktif aplikasi DANA baik di platform iOS maupun Android so far membantu saya ketika saya nggak bawa uang cash karena saya bisa pakai DANA scan. Sebagai pengguna saya mempunyai beberapa problem ketika menggunakan DANA yaitu
1. Masih sedikitnya merchants yang menggunakan DANA.
2. Merchants toko yang sudah disebutkan DANA pun seringkali mesinnya rusak atau tidak kompatible sehingga ketika saya berharap bisa pakai aplikasi DANA sebagai pengganti cashs eringkali terkendala dan saya harus tetap mengeluarkan kartu debit saya. Hal itu cukup mengganggu sebab saya seringkali top up DANA saya dengan nominal cukup besar untuk transaksi payment maupun transaksi pembelian sehingga ketika hal tersebut terjadi saya merasa kecewa dan rugi waktu.
3. Indonesia telah mengimplementasikan GPN sebagai generasi milenial yang mendorong terciptanya ekosistem keuangan nasional saya menggunakan kartu debit logo GPN selain VISA/MasterCard . Pertanyaan saya kenapa kartu GPN belum bisa dimasukkan kedalam akun DANA? Mengapa baru Visa dan mastercard? GPN telah terimplementasi cukup lama di Indonesia, mengapa belum di suport oleh Dana? Kapan bisa masuk dan terhubung di dana? Dari beberapa akun yang saya miliki hanya 1 yang bsia terhubung ke Dana karena hal tersebut. Saya berharap banyak untuk support dana di aplikasi mobile dengan terhubung ke paltform GPN dan semakin besarnya serta mudahnya dana dapat diakses karena menurut saya hal tersebut memjadi hambatan dan kalau rival dana menjawab tantangan tersebut. Sebagai pengguna menurut saya wajar jika saya beralih ke aplikasi yang lebih fleksibel terima kasih.

Waiting for the feedback.

best,
Christiawan Nugroho (Mr.)</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/02 - 05:16:5</td>
                        <td>Baymax</td>
                        <td>baymax09a@gmail.com</td>
                        <td>Saya mendaftar di akun premium tapi kenapa tidak mendapat vocher pulsa 25000</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/02 - 05:18:5</td>
                        <td>Triyanto</td>
                        <td>triyantoteknisi03@gmail.com</td>
                        <td>Saya minta data transaksi 4 hari tetakir</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/02 - 06:26:3</td>
                        <td>Maria</td>
                        <td>mariawastujaya@gmail.com</td>
                        <td>Tolong dibantu. Hari ini tgl 02 juni 2019 saya membeli yoshinoya di mall kota kasablanka dengan cashback 50% dimana saya seharusnya mendapatkan cashback 15000,tetapi setelah saya cek di histori tidak ada cashback yang masuk.</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/02 - 06:27:0</td>
                        <td>Maria</td>
                        <td>mariawastujaya@gmail.com</td>
                        <td>Tolong dibantu. Hari ini tgl 02 juni 2019 saya membeli yoshinoya di mall kota kasablanka dengan cashback 50% dimana saya seharusnya mendapatkan cashback 15000,tetapi setelah saya cek di histori tidak ada cashback yang masuk.</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/02 - 06:32:3</td>
                        <td>F XAVERIUS</td>
                        <td>wetakara9@gmail.com</td>
                        <td>Tanggal 17 mei lalu pengiriman uang saya gagal, dana tersebut masih terpending sampai  sekarang. Berapa lama saya harus menunggu? Kenapa pelayanan dana saat ini menurun? Saya kirim email tanpa di balas? Apakah pelaporan saya itu palsu? Nomor hp saya 081334282878, tolong jangan kecewakan saya </td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/02 - 07:02:0</td>
                        <td>Dian Teruna</td>
                        <td>diantman@gmail.com</td>
                        <td>(938945) 

Salam

Saya memang sering install hp android saya, dan percayalah saya tidak pernah curang atau mengakali aplikasi dana. Kenapa saya tidak mendapatkan bonus pulsa lagi? Mohon solusinya biar saya mendapatkan bonus pulsa 50% lagi. Terimakasih.</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/02 - 10:08:4</td>
                        <td>Harty suryani</td>
                        <td>adillawijay@gmail.com</td>
                        <td>Akun dana saya Tidak mendapat cashback dan promo2 dr dana</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/02 - 10:35:1</td>
                        <td>Muhammad Khaidir zakaria</td>
                        <td>kunha0asakura@gmail.com</td>
                        <td>KTP saya rusak saya merasa ngirim uang tapi di suruh jadi premium tapi pas diminta photo KTP di suruh ulang melulu soalnya KTP saya info agak susah di dapatkan dari photo soalnya sdh kehujanan berulang kali di dompet saya apakah ada kemudahan saya mw kirim uang sesegera mungkin ?</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/02 - 10:38:1</td>
                        <td>Muhammad Khaidir zakaria</td>
                        <td>kunha0asakura@gmail.com</td>
                        <td>Saya mau ngirim uang dan sudah top up di sana tapi pas mau kirim di suruh upgrade ke premium dan diminta foto KTP tapi karena KTP saya agak rusak karena sering kehujanan jadi informasi yg diperoleh via foto tidak bisa di baca apakah ada bantuan untuk kasus saya ini ?????</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/02 - 10:40:3</td>
                        <td>Muhammad Khaidir zakaria</td>
                        <td>kunha0asakura@gmail.com</td>
                        <td>saya mau mengirim uang via dana dan sudah top up tapi di minta menjadi premium tapi pada saat menginput photo ktp tidak bisa di peroleh karena ktp saya agak burem karena sering kehujanan apakah yang harus saya lakukan agar dapat mengirim uang ??????????</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/02 - 10:42:1</td>
                        <td>Ryan prada</td>
                        <td>ryanprada205@gmail</td>
                        <td>Tidak bisa log in karena nomer hilang</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/03 - 01:15:4</td>
                        <td>nurmelati</td>
                        <td>melati205@yahoo.com</td>
                        <td>Knp casback saya gk pernah dapat ya pak setiap minggunya...padahal saya uda belanja diatas 100 ribu</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/03 - 01:16:1</td>
                        <td>nurmelati</td>
                        <td>melati205@yahoo.com</td>
                        <td>Knp casback saya gk pernah dapat ya pak setiap minggunya...padahal saya uda belanja diatas 100 ribu</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/03 - 01:48:0</td>
                        <td>wangga alfyan</td>
                        <td>wanggaalfyan7@gmail.com</td>
                        <td>Saya ingin melakukan pencabutan data akun premium di nomor 085313476238</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/03 - 01:54:5</td>
                        <td>Abigail Christin</td>
                        <td>abigailchrstn@yahoo.co.id</td>
                        <td>Halo,

Saya Abigail, saya hendak menghapus akun dana saya, dengan alasan saya sudah memiliki aplikasi serupa. Jadi mohon untuk ditanggapi permintaan saya. Terima kasih.</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/03 - 04:09:1</td>
                        <td>Septa Permana</td>
                        <td>septapermana88@gmail.com</td>
                        <td>Nomor saya tidak berlaku dan saya tidak bisa masuk ke aplikasi</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/03 - 04:40:5</td>
                        <td>wahyudin</td>
                        <td>wahyudinwahyudin@icloud.com</td>
                        <td>selamat pagi, bisakah membantu saya, saya memiliki voucher diskon 10000 untuk tagihan listrik dan lain, sebelumnya voucher otomatis terpakai ketika membayar tagihan, tapi kenapa sekarang tidak, padahal syarat dan ketentuan sudah terpenuhi, mohon penjelasannya. terima kasih.</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/03 - 04:56:5</td>
                        <td>Ari Supriatna </td>
                        <td>arisupriatna05@gmail.com</td>
                        <td>Saya tidak bisa verifikasi premium</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/03 - 06:26:2</td>
                        <td>Nina sartika ningrum</td>
                        <td>ttengks@gmail.com</td>
                        <td>Knp saya transfer proses terus..sedangkan saldo kepotong</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/03 - 06:39:3</td>
                        <td>tjang otib</td>
                        <td>babehnyegabus@gmail.com</td>
                        <td>Sudah beberapa hari ini saldo dana saya tidak bisa di gunakan transaksi.

Akun dan dana saya juga tidak bisa di up grade...memang jenis.hape saya tergolong jadul.
Adakah langkah atau cara laen untuk di up grade ke dana pr
emium.</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/03 - 08:13:4</td>
                        <td>Eva Setya</td>
                        <td>evasetyaw@gmail.com</td>
                        <td>Kak .. bank dana sya gk bisa digunkan lgi .. karna sim saya terblokir .. :( gk bsa ngrim kodenya .. gimana cara mengtasinya .. help pls </td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/03 - 08:29:3</td>
                        <td>Xii madil</td>
                        <td>ntsmadil9@gmail.com</td>
                        <td>NTS</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/03 - 09:39:5</td>
                        <td>RISKI SE</td>
                        <td>riski.diny@gmail.com</td>
                        <td>sampai berapa kali transfer ke bank gratis untuk pengguna dana perbulanya?</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/03 - 09:41:0</td>
                        <td>Samuel sitorus</td>
                        <td>lionelsammy@gmail.com</td>
                        <td>Apakah bisa melakukan top up dengan kartu kredit?</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/03 - 10:27:5</td>
                        <td>Faruq Mahdi</td>
                        <td>faruqmahdi1999@gmail.com</td>
                        <td>Selamat siang, admin. Saya ingin mengajukan penggantian nomor telpon akun saya karena nomor yang saya gunakan di akun saya saat ini sudah tidak aktif sejak lama dan sekarang saya tidak dapat masuk ke akun saya karena sistem mengirimkan kode OTP ke nomor sayang yang sudah tidak aktif tersebut. Ada kah solusi untuk saya? Mohon bantuannya, terima kasih</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/03 - 10:31:1</td>
                        <td>Akbar</td>
                        <td>akbarrijaldi5@gmail.com</td>
                        <td>Apa dana tdk pernah melakukan pengecekan min 1 bulan sekali pada partner yg terkait seperi KFC? Sudah 2 bulan ini saya coba pakai dana d KFC yg berbeda tp hasilnya ttp sama.

Saya tdk dapat melalukan transaksi dengan dana n pihak KFC pun tdk punya solusi untuk mengatasi masalah tersebut</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/03 - 10:31:4</td>
                        <td>Akbar</td>
                        <td>akbarrijaldi5@gmail.com</td>
                        <td>Apa dana tdk pernah melakukan pengecekan min 1 bulan sekali pada partner yg terkait seperi KFC? Sudah 2 bulan ini saya coba pakai dana d KFC yg berbeda tp hasilnya ttp sama.

Saya tdk dapat melalukan transaksi dengan dana n pihak KFC pun tdk punya solusi untuk mengatasi masalah tersebut</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/03 - 10:32:2</td>
                        <td>Agustina Anggreyani Br Sinaga </td>
                        <td>anggre967@gmail.com</td>
                        <td>Selamat pagi min,
Saya barusan download dana. Tapi katanya sudah terdaftar dan minta pin. Saya tidak tahu pin nya, saya coba2 dan sekarang sudah terblokir. Tolong bantuannya. Trims </td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/03 - 10:40:0</td>
                        <td>Jusnaini</td>
                        <td>bundaffh@gmail.com</td>
                        <td>Say tf hari ini 300 salad udah terpotong Dan uang belum sampai, tolong di bantu</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/03 - 11:15:0</td>
                        <td>JHON NEFI ADRIAN</td>
                        <td>jhonnefiadrian@yahoo.co.id</td>
                        <td>Mohon bantuannya, pengiriman uang ke nomor rekening BNI SYARIAH : 0513825486 dari nomor akun dana 082386927706 sampai saat ini tidak masuk. Jumlahnya Rp. 411.461

ID TRANSAKSI : 2019 0603    1012    1420    0101    0016    6271    8048    2107    9

</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/03 - 11:42:1</td>
                        <td>agus saputra</td>
                        <td>agus.saputra.m@gmail.com</td>
                        <td>Tolong dibantu untuk reset password akun saya
nomor telepon 08989333886
alamat email: agus.saputra.m@gmail.com

saya sudah mengikuti petunjuk reset password, tapi saya lupa pembelanjaan terakhir.
apakah bisa di bantu?

saya hubungi nomor 1500445, tidak bisa terhubung</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/03 - 12:22:3</td>
                        <td>Samsul bahri</td>
                        <td>apobahri33@gmail.com</td>
                        <td>Sy ingin mendaftarkan kuliner sy menggunakan aplikasi dana gmn cara y.tolong dbantu</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/03 - 01:10:5</td>
                        <td>Lilywati</td>
                        <td>li2.0ng7777@gmail.com</td>
                        <td>Saya mau mengganti nomer telp yang terdaftar, karena tidak bisa masuk karena hp saya rusak. Tidak bisa menerima otp untuk masuk ke aplikasi. Mohon diberitahu apa yang harus dilakukan</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/03 - 01:32:1</td>
                        <td>Lilywati</td>
                        <td>li2.0ng7777@gmail.com</td>
                        <td>Saya mau mengganti nomer telepon yang terdaftar, karena hp saya rusak. Tidak bisa mendapat kode otp</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/03 - 01:51:4</td>
                        <td>Taufik</td>
                        <td>taufikaisyah@yahoo.co.id</td>
                        <td>Met siang dana..
Saya mau menanyakan ttg cashback 50% buat makanan khususnya di KFC..apakah cashback nya 1x/minggu ditentukan harinya cm khusus pada hari senin?
Biasanya kapan boleh kapan saja dan direset ulang pada hari senin, apakah berubah ketentuannya? </td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/03 - 01:51:5</td>
                        <td>Taufik</td>
                        <td>taufikaisyah@yahoo.co.id</td>
                        <td>Met siang dana..
Saya mau menanyakan ttg cashback 50% buat makanan khususnya di KFC..apakah cashback nya 1x/minggu ditentukan harinya cm khusus pada hari senin?
Biasanya kapan boleh kapan saja dan direset ulang pada hari senin, apakah berubah ketentuannya? </td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/03 - 02:50:0</td>
                        <td>Andi Muhammad Mauraga Iskandar</td>
                        <td>ragaiskandar@gmail.com</td>
                        <td>Saya ingin menghapus akun DANA saya agar saya bisa memverifikasi ktp saya ke akun DANA saya yg lain

No telp akun yg ingin saya hapus: 08974646708</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/03 - 02:52:0</td>
                        <td>Hasina akriyanti</td>
                        <td>aniputra928@gmail.com</td>
                        <td>Kenapa nomer saya berubah begini dan nomor yang saya daftarin itu nomer telkomsel ini nmr saya yang saya daftarin 081213995212</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/03 - 03:08:1</td>
                        <td>Rangga Wiseno</td>
                        <td>rangga.wiseno@gmail.com</td>
                        <td>test 123</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/03 - 03:08:1</td>
                        <td>Tanya Promo</td>
                        <td>juanalmer@icloud.com</td>
                        <td>Tanya promo terbaru DANA dong apa aja</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/03 - 03:25:0</td>
                        <td>Juan almer</td>
                        <td>juanalmersiregar@yahoo.co.id</td>
                        <td>Tanya Promo dong, di DANA ada apa aja ya</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/03 - 03:35:2</td>
                        <td>AGUNG REZARTHA</td>
                        <td>agung.rezartha@dana.id</td>
                        <td>This is a test email from website.
Please kindly reply if you receive this message</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/03 - 03:50:3</td>
                        <td>Sahra Ummu Aziz Aziimah</td>
                        <td>zahra.aziimah@gmail.com</td>
                        <td>Saya ingin menghapus akun dana saya dengan nomor 081283936826 dan sudah membuat akun lagi dengan nomor 081282136936.
Akum baru tidak dpt diverifikasi karna data semua akun sudah dinomor sebelumya. 
Jadi tolong hapus akun saya yang lama. 
Terimakasih. </td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/03 - 04:03:5</td>
                        <td>Jovita</td>
                        <td>alicejovita@yahoo.com</td>
                        <td>Hallo, saya mau nanya kenapa promo cashback 50% jika bertransaksi di kfc , saya tidak mendapatkan cashback ? Padahal promo masih berlaku . Dituliskan bawa promo berlaku seminggu sekali , dan saya tidak menggunakannya selama 2 minggu . Tetap saya tidak mendapatkan cashback . Mohon di cek kembali . Terimakasih </td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/03 - 04:09:4</td>
                        <td>AGUNG SATRIAWAN</td>
                        <td>joeagsan@gmail.com</td>
                        <td>Saya ingin hapus akun DANA saya karena saya merasa tidak cocok dengan aplikasi ini yang lambat dan berat. </td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/03 - 05:04:3</td>
                        <td>H. PARULIAN S</td>
                        <td>putrasama19@gmail.com</td>
                        <td>Selamat sore
Maaf mengganggu waktunya sebentar..
Jadi gini, kenapa saya nggak mendapatkan voucher pulsa ya padahal saya sudah melakukan verifikasi akun
Mohon konfirmasi secepatnya
Terimakasih</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/03 - 06:19:5</td>
                        <td>Eveline</td>
                        <td>evelinebarent2906@gmail.com</td>
                        <td>Permisi mbak ,saya mau tanya pin dana saya saya ingat saya tidak pernah disuruh isi sebelumnya . Saya minta bantuan mbak bagaimana caranya saya bisa dapat mengetahui pin tsb terima kasih mohon bantuanya</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/03 - 06:33:4</td>
                        <td>carolina siahaan</td>
                        <td>ranto.klen@gmail.com</td>
                        <td>2019    0603    1112    1280    0100    1662    0430    7320    794
ID transaksi di yoshinoya
cash back belum masuk segera ditindak lanjuti scptnya..makasih</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/03 - 07:03:0</td>
                        <td>aditya ari novianto</td>
                        <td>kevins_drummer@yahoo.com</td>
                        <td>akun saya jadi nama nya afni hervianti?mohon di rubah dan tidak bisa konek bukalapak maupun blackberry massanger</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/03 - 07:10:3</td>
                        <td>Agis septiana</td>
                        <td>berassatuhome@gmail.com</td>
                        <td>Verifikasi KTP gagal .
Karna KTP sudah terdaftar sebelumnya .bagaimana cara verifikasi berjalan lancar
</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/03 - 07:20:5</td>
                        <td>Sona Pribady</td>
                        <td>sonapribady12@gmail.com</td>
                        <td>Hi Dana,

saya Sona Pribady, pemilik akun Dana dengan nomor HP 0856 1153164. Sekitar 30 menit, ada call dari nomor 0852 1123 95321 yang memberitahu bahwa saya menang undian untuk Samsung Galaxy S10.

Yang bersangkutan meminta untuk mengirim sejumlah data via email ke help@dana.id dan memberitahu beberapa persyaratan untuk pengambilan hadiah.

Saya memang pernah mendapat voucher &amp;#45;1 undian S10 via roulette bedug. Namun berhubung saya belum mendapat info tertulis sebelumnya (via email / pengumuman di stories instagram) untuk hasil undian, serta saat di telepon saya sedang di jalan, saya ingin mengonfirmasi terkait hal tersebut melalui media ini.

Minta bantu dari pihak Dana untuk mengonfirmasi dan memberi info via email ya.

Thank you untuk waktunya,
Sona</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/03 - 07:35:5</td>
                        <td>Dani</td>
                        <td>danielnostradam@gmail.com</td>
                        <td>cara ganti nomor pin akun dana..?</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/03 - 07:43:0</td>
                        <td>Kian lie</td>
                        <td>sayakianlie@gmail.com</td>
                        <td>Selamat malam, 

Saya nasabah dana dengan nomor telepon 0812&amp;#45;8541&amp;#45;4566 Hari ini pada pukul 18:59 WIB, sy dihubungi oleh nomor +62852112385321 yang mengatas namakan dana, dan menyatakan bahwa sy mendapat hadiah samsung galaxy tab. Yang menjadi pertanyaan syaa, apakah benar itu dari dana? Sekian dan mohon konfirmasi nya. Terima kasih.</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/03 - 08:11:0</td>
                        <td>Billy</td>
                        <td>billyagraha@gmail.com</td>
                        <td>akun BANK saya SUMSEL&amp;#45;BABEL bisa gak?  Tapi sya lihat di daftar BANK SUMSEL BABEL tidk ada</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/03 - 08:12:1</td>
                        <td>Billy</td>
                        <td>billyagraha@gmail.com</td>
                        <td>akun BANK saya SUMSEL&amp;#45;BABEL bisa gak?  Tapi sya lihat di daftar BANK SUMSEL BABEL tidk ada</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/03 - 08:37:4</td>
                        <td>Karim</td>
                        <td>karim.tabalar2@gmail.com</td>
                        <td>Apakah dana untuk kedepannya bisa mengadakan kerjasama dengan PayPal?
Saran. Kelemahan security Dana adalah ketika ada notifikasi dari Dana ketika diklik adalah langsung Log In ke aplikasi Dana tanpa memasukkan PIN, this is risky.</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/03 - 09:05:3</td>
                        <td>Ria</td>
                        <td>riasari0102@gmail.com</td>
                        <td>Selamat malam, saya, ria sari, salah satu pemilik akun dana. 
Saya mau melaporkan bahwa saya kehilangan no hp yang saya gunakan untuk pendaftaran akun dana. 
Apa tindakan yg bs saya lakukan? Bgmn saya bisa membuka kembali akun dana saya? Trmksh</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/03 - 09:46:5</td>
                        <td>RIZKY NAUPAL</td>
                        <td>rizky02dogle@gmail.com</td>
                        <td>mohon bantuan untuk memindahkan saldo saya dari 082111318168 ke 0895327332951. masalah yang saya temukan karena id yang 082111318168 tidak bisa memperifikasi upgrade ke premium dana, alasannya admin karena duplicate data. maka saya tidak bisa mengupgrade dengan data yang sama. namun ketika ingin upgrade premium menggunakan data istri saya pun. pengalihan prefikasi sedang sibuk, dan tidak bisa upgrade. jadi saya mohon bantuannya untuk mengakumulasikan saldo saya ke akun yang sudah premium, untuk bisa tarik saldo saya. karena saya butuh untuk modal. terimakasih

no.ktp 3201030206940011
sudah compline 2 hari lalu untuk upgrade premi.
dat sudah dikirim dengan jelas ke pihak CS dana melalui email.
tolong admin, saya perlu karena urgent

sekali lagi terimakasih dan semoga di proses dengan baik.
</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/03 - 09:51:2</td>
                        <td>Feri wibowo</td>
                        <td>85feriwibowo@gmail.com</td>
                        <td>Saya dpt 3 voucer biyasanya setiap transaksi ada penawaran voucer tapi tadi saya transaksi tidak ada knp ya..??
</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/03 - 09:57:3</td>
                        <td>teguh firmansah</td>
                        <td>teguhtrias142@gmail.com</td>
                        <td>Akun e&amp;#45;ktp saya sudah terverifikasi dan nomor yang terdaftar dulu sudah tidak aktif,bagaimana caranya mereset kembali ladi dri awal?</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/03 - 10:04:4</td>
                        <td>Fhiye</td>
                        <td>Fhiye1576@gmail.com</td>
                        <td>Apa syarat pengajuan pinjaman harus pakai deposit sebesar yg sudah ditentukan oleh pihak dana? </td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/03 - 10:08:2</td>
                        <td>Riki Mulyadi</td>
                        <td>rikimulyadi1111@gmail.com</td>
                        <td>Mohon untuk penghapusan akun dana saya ini dikarenakan saya sudah memiliki akun dana yang lain. </td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/03 - 10:23:3</td>
                        <td>MUHAMMAD ZAINI</td>
                        <td>zaini7446@gmail.com</td>
                        <td>Mohon bantuannya Status Tarik saldo / Transfer Pending</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/03 - 11:04:1</td>
                        <td>Alexander</td>
                        <td>alexander.murad@gmail.com</td>
                        <td>Kalo ga rela kasih promo mah mendingan tutup aja senin ini barusan pake dana di KFC ga bisa, mundur aja kalo ga ga rela kasih promo. Gila udah ngantri lama ternyata ga bisa pake promo nya. Bego banget system nya.</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/04 - 12:33:1</td>
                        <td>Johnc8</td>
                        <td>johnc2@gmail.com</td>
                        <td>It&amp;#039;s perfect time to make some plans for the long run and it is time to be happy. I have learn this publish and if I may I want to suggest you few attentiongrabbing things or advice. Maybe you could write next articles regarding this article. I desire to read even more things about it&amp;#33; fkgeebgfefee</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/04 - 12:33:2</td>
                        <td>Johng110</td>
                        <td>johng294@gmail.com</td>
                        <td>Fckin amazing things here. Im very glad to see your post. Thanks a lot and i&amp;#039;m looking forward to contact you. Will you kindly drop me a mail? bbgdedckdkba</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/04 - 01:02:4</td>
                        <td>Iwan ruswandi</td>
                        <td>ruswandifeni@gmail.com</td>
                        <td>Mohon di aktifkan kembali akun dana saya dgn no tlp 08561457983</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/04 - 02:44:3</td>
                        <td>Welly</td>
                        <td>welly_w2@yahoo.com</td>
                        <td>Saya ingin menanyakan jika saya belum menjadi premium dana, apakah uang saya di akun dana tidak bisa di ambil? Terimakasih....
</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/04 - 03:36:0</td>
                        <td>Zulpianda nublah</td>
                        <td>zulpiandan@gmail.com</td>
                        <td>Saya sudah 2x tidak.dpt cash back dari akun dana padahal saya merupakan akun premium di dana dan saya sudah melakukan transaksi sesuai s&amp;k yg ada.. tolong dong benahi akun dana saya.. saya juga mau dapt cash back seperti yg lain.. knpa belakangan ini sya ga dpt cash back lagi? Sementara yg lalu lalu saya dapat cash back..</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/04 - 03:39:0</td>
                        <td>Zulpianda nublah</td>
                        <td>zulpiandan@gmail.com</td>
                        <td>Saya sudah 2x tidak.dpt cash back dari akun dana padahal saya merupakan akun premium di dana dan saya sudah melakukan transaksi sesuai s&amp;k yg ada.. tolong dong benahi akun dana saya.. saya juga mau dapt cash back seperti yg lain.. knpa belakangan ini sya ga dpt cash back lagi? Sementara yg lalu lalu saya dapat cash back..</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/04 - 07:17:0</td>
                        <td>Mahdan091</td>
                        <td>Www.adanzenit@gmail.com</td>
                        <td>Bunos pukol beduk kok ga ada bos. Padahal 2 kali kita dapat 10000</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/04 - 07:19:0</td>
                        <td>Mahdan091</td>
                        <td>Www.adanzenit@gmail.com</td>
                        <td>Bunos pukol beduk kok ga ada bos. Padahal 2 kali kita dapat 10000</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/04 - 07:24:4</td>
                        <td>Mahdan091</td>
                        <td>www.adanzenit@yahoo.com</td>
                        <td>Bunoa pukul beduk ko ga ada bos...? Padahal aku dapat 10000 2 kli</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/04 - 08:19:4</td>
                        <td>Jose Grde</td>
                        <td>jose28garda@gmail.com</td>
                        <td>Min.. saya mau top up DANA lewat Alfamart, sudah 2 kali saya kesana, tp katanya sistem tidak merespon (offline). Kalau boleh tau, jam online nya agar bisa top up, kapan ya?? 
Terimakasih.</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/04 - 08:41:1</td>
                        <td>Hana</td>
                        <td>hana.alief812@gmail.com</td>
                        <td>Isi pulsa blum masuk2 nih gimana.tolong di poroses secepatnya</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/04 - 08:41:2</td>
                        <td>Hana</td>
                        <td>hana.alief812@gmail.com</td>
                        <td>Isi pulsa blum masuk2 nih gimana.tolong di poroses secepatnya</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/04 - 09:00:2</td>
                        <td>Juahta</td>
                        <td>juahta.tarigan07@gmail.com</td>
                        <td>Saya ada 2 masalah.
1.Saya isi pulsa handphone tapi belum saya terima. Keterangan masih proses. Proses kok lama banget.

2.Saya ikut tap and win dapat potongan 10 rbu untuk isi token, sudah saya isi dan berhasil tapi kok potongannya tidak ada?
Mohon solusinnya.</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/04 - 09:17:5</td>
                        <td>Irman Mustapa</td>
                        <td>mustafa.alifi10@gmail.com</td>
                        <td>Saya beli pulsa dgn no 081289870301 belum masuk pulsanya sebesar 20.000 sekitar jam ,09:10</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/04 - 09:18:4</td>
                        <td>Irman Mustapa</td>
                        <td>mustafa.alifi10@gmail.com</td>
                        <td>Saya beli pulsa dgn no 081289870301 belum masuk pulsanya sebesar 20.000 sekitar jam ,09:10</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/04 - 09:19:4</td>
                        <td>Irman Mustapa</td>
                        <td>mustafa.alifi10@gmail.com</td>
                        <td>Saya beli pulsa dgn no 081289870301 belum masuk pulsanya sebesar 20.000 sekitar jam ,09:10</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/04 - 09:23:2</td>
                        <td>anastasia</td>
                        <td>anastasiasjaaf@gmail.com</td>
                        <td>halo cs, pada pukul 8.23 pagi tgl 4 juni 2019 saya melakukan pembelian pulsa untuk no telkomsel saya sebesar 50ribu dan mendapat potongan harga sebesar 25rb dengan voucher 50 %, tqpi hingga sekarang pukul 9.25 pagi, 1 jam kemudian, pulsa saya masih nelum terisi juga. Saya tidak mau menghubungi contact center karena untuk telp ke 1500... biaya nya mahal dan pulsa saya memang belum terisi. Tolong bantuannya dalam hal ini secepatnya, terima kasih. </td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/04 - 09:34:2</td>
                        <td>Hendra kelana </td>
                        <td>hkelana921@gmail.com</td>
                        <td>Isi Pulsa sudah hampir 1 Jam belum sukses, proses terus bos ku, tolong diproses la bosku </td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/04 - 09:51:5</td>
                        <td>Wasta</td>
                        <td>chachacellnet@gmail.com</td>
                        <td>Pulsa ke nomor 081398755168 yg 50000 blum masuk juga dari tadi status nya masih diproses , kenapa bos, mohon segera di respons&amp;#33;&amp;#33;&amp;#33;</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/04 - 09:53:3</td>
                        <td>Wasta</td>
                        <td>chachacellnet@gmail.com</td>
                        <td>Pulsa ke nomor 081398755168 yg 50000 blum masuk juga dari tadi status nya masih diproses , kenapa bos, mohon segera di respons&amp;#33;&amp;#33;&amp;#33;</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/04 - 09:53:4</td>
                        <td>Wasta</td>
                        <td>chachacellnet@gmail.com</td>
                        <td>Pulsa ke nomor 081398755168 yg 50000 blum masuk juga dari tadi status nya masih diproses , kenapa bos, mohon segera di respons&amp;#33;&amp;#33;&amp;#33;</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/04 - 10:15:4</td>
                        <td>riki oktariadi</td>
                        <td>rikiokta669@gmail.com</td>
                        <td>Mbak saya minta bantuan untuk pembatalan pulsa telkomsel dikarnakan pulsa nya progress trus uang nya sudah dikembalikan saya minta bantuan nya untuk pembatalan pulsa tsb denom 100.000..</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/04 - 10:17:3</td>
                        <td>Krisna furnama fitri</td>
                        <td>krisnafurnamafitri@gmail.com</td>
                        <td>Mau tanya td saya beli plsa..  Kok sampe sekarang ga masuk cuma on proses.  Sedangkan pembeloan ke 2 nya itu uda sukses..  No pesananya 2019    0604    1112    1280    0100    1667    6560    7326    118.. Mohon bantuanya</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/04 - 10:21:4</td>
                        <td>Muh fadlian</td>
                        <td>fadliansyah202020@gmail.com</td>
                        <td>Tolong diperhatikan masalah trf saya ke rek 0710355446 an nur hapipah  sebesar 300 ribu yg sudah terpending hampir 3 hari padahal uang sangat diperlukan 
Pengirim an muh fadliansyah no telp 085219907724</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/04 - 10:24:0</td>
                        <td>Nur Iman Setiawan</td>
                        <td>nuriman.setiawan@gmail.com</td>
                        <td>Tolong bantuaanya min, tadi saya mau ganti pin dana saya,  pas proses ganti pin server busy trus selama 3x proses, jadi nya nomer dana saya ke lock selama 1 jam min, tolong di bantu unlock dan penggantian pin nya donk min.
No Dana : 085772033408</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/04 - 10:24:1</td>
                        <td>Rico</td>
                        <td>rico.akui@gmail.com</td>
                        <td>Dh, mohon bantuannya untuk transaksi pengisian pulsa sebesar dg id transaksi 
2019    0604    1112    1280    0100    1669    7160    7279    853

Kondisi order masih diprogress hanya untuk pengisian pulsa.


Mohon untuk segeea direspon terima kasih

Salam,
Rico</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/04 - 10:43:3</td>
                        <td>rudy utomo</td>
                        <td>utomo_rudy85@yahoo.com</td>
                        <td>kenapa akun saya selalu ditolak pada waktu transaksi? </td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/04 - 10:52:5</td>
                        <td>Rian</td>
                        <td>r1_4n_2@yahoo.com</td>
                        <td>No hp terdaftar :082226868744

 no transaksi : 20190604111212800100166578107275878

saya komplain tentang pulsa yg dari tadi tidak masuk constumer marah dan uang diambil eh saldo dana ga kembali </td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/04 - 11:01:1</td>
                        <td>Dwi Hendra Kusuma</td>
                        <td>hendrakusuma.info@gmail.com</td>
                        <td>Beli pulsa sudah bayar tapi belum masuk...
Transaksi ID  : 20190604111212800100166694607236314
mhn di tindak lanjuti</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/04 - 11:24:0</td>
                        <td>Ade Hidayat Arifin</td>
                        <td>lebetea@yahoo.com</td>
                        <td>Akun dana istrisaya nmr kartu as nya sudah expired..  Tidak bisa dikirim konfrms sms...  Bisa ga mau ganti nmr yang lain, tapi akun nya ttp yang itu. Mohon penjelassannya.  Trims</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/04 - 11:25:0</td>
                        <td>Anj</td>
                        <td>anjarpranoto7@gmail.com</td>
                        <td>Bagaimana caranya hapus history transaksi di Dana? </td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/04 - 11:27:3</td>
                        <td>Alpine</td>
                        <td>fajar.anwafa@gmail.com</td>
                        <td>Ada bbrp transaksi yg tertera ada cashback di prj, tp cashbackny blm masuk. Ada solusiny?</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/04 - 11:33:4</td>
                        <td>Ahmad ashuri slamet</td>
                        <td>autonet6@gmail.com</td>
                        <td>Mohon untuk bantuan reset pin. Ketika saya reset da notif hub cs DANA</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/04 - 11:57:5</td>
                        <td>Panji Krisdiantoro</td>
                        <td>panji.thok19@gmail.com</td>
                        <td>Saya lupa pin dana saya dan tidak bisa log in akun dana saya</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/04 - 12:20:4</td>
                        <td>Hasbie Farizi</td>
                        <td>Hasbie044@gmail.com</td>
                        <td>Mbak saya lupaa pin dana . Tolong fast respon yaak</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/04 - 12:24:0</td>
                        <td>DINDA SHALEHA</td>
                        <td>dindashahrp@gmail.com</td>
                        <td>selamat siang mbak . saya mau melakukan pembayaran tetapi saya lupa pin DANA . sudah saya cobal 3 kali dan akun dana saya di bekukan. tolong fast respon.</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/04 - 12:55:4</td>
                        <td>Renita</td>
                        <td>renny03s@gmail.com</td>
                        <td>Dana sy terlogout dan sy lupa PIN.
Semua pertanyaan sdh dijawab dgn benar tp ttp tdk bs.

Saya call cso tdk bs.
Tlg dibantu</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/04 - 01:10:3</td>
                        <td>Sindu masri p</td>
                        <td>sinzakki@gmail.com</td>
                        <td>
&amp;#45;&amp;#45;&amp;#45;&amp;#45;&amp;#45;&amp;#45;&amp;#45;&amp;#45;&amp;#45;&amp;#45; Forwarded message &amp;#45;&amp;#45;&amp;#45;&amp;#45;&amp;#45;&amp;#45;&amp;#45;&amp;#45;&amp;#45;
From: Sin Zakki &#60;sinzakki@gmail.com&#62;
Date: Tue, Jun 4, 2019, 13:01
Subject: tolong / saran
To: &#60;help@dana.id&#62;


Kak, jika akun dana ada di orang lain karna ketipu saat itu(bisa ketipu karna gak baca benar2 sms kode utpdan peringatannya). apakah akun itu bisa di log out di hp orang lain itu no dana: 081390579009 . No hp ini masih aktif. Jika bisa saya sangat berterimakasih, jika tidak, beri saran apa yg akan saya lakukan. Untuk saldo jika sudah berkurang gakpapa yg penting akun kembali(klo bisa dalam keadaan tidak tertaut ke manapun).</td>
                    </tr>

                                        <tr class="box_modal_full2" alt="member_detail.php">
                        <td>2019/06/04 - 02:06:1</td>
                        <td>Lewinsky Cai</td>
                        <td>lewinskycai@ymail.com</td>
                        <td>Please Help, sya mau reset PIN dana karena kelupaan. Thank you</td>
                    </tr>

                                    
                
                
            </tbody>
        </table>
    </div>
</div><div class="clearfix"></div>
<script type="text/javascript" src="https://dana.id/newcms/assets/js/plugin.js"></script>
<script type="text/javascript" src="https://dana.id/newcms/assets/js/alertify.js"></script>
<script type="text/javascript" src="https://dana.id/newcms/assets/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://dana.id/newcms/assets/js/fancybox.js"></script>
<script type="text/javascript" src="https://dana.id/newcms/assets/js/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="https://dana.id/newcms/assets/js/controller.js"></script>


</body>
</html>
